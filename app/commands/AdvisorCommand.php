<?php
namespace App\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Argun\Aria\Models\Customer,Argun\Aria\Models\Cron;

use Dater, DB;

class AdvisorCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'advisor';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'updates the db for customer stat.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$date = Dater::now();
		$month = $date->month;
		$year = $date->year;

		$from = $date->startOfMonth()->format(Dater::$SQLFormat);
		$to = $date->endOfMonth()->format(Dater::$SQLFormat);

		DB::connection()->getPdo()->beginTransaction();
		//check for cron related problem
		$this->info('done!');

		$cron = new Cron;
		$cron->name = 'advisor';
		if(!$cron->save()) $this->error($cron->getErrors()->first());

		DB::connection()->getPdo()->commit();
		return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}
}