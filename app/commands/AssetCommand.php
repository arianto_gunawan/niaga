<?php
namespace App\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Argun\Aria\Models\Item;
use Argun\Aria\Models\Transaction;
use Argun\Aria\Models\WarehouseItem;
use Argun\Aria\Models\Depreciation;
use Argun\Aria\Models\Customer,Argun\Aria\Models\Cron;
use Argun\Aria\Models\CustomerStat;
use Dater, DB;
class AssetCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'asseter';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'updates the db for warehouse asset.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$date = Dater::now()->format(Dater::$SQLFormat);
		$wi = WarehouseItem::table();
		$i = Item::table();
		$d = Depreciation::table();

		DB::connection()->getPdo()->beginTransaction();
		Customer::whereIn('type', array(Customer::TYPE_WAREHOUSE, Customer::TYPE_VWAREHOUSE))
		  ->chunk(200, function ($warehouses) use($date, $wi, $i, $d) {

		  	foreach($warehouses as $warehouse)
		  	{
				$query = DB::table($wi)->select(array(DB::raw('SUM( quantity * '.$i.'.price ) as total_asset, SUM( quantity ) as wq')))->where('warehouse_id','=',$warehouse->id)->where('quantity','>',0);
				$asset = $query->join($i,$wi.'.item_id','=', DB::raw("$i.id AND $i.type != ".Item::TYPE_ASSET_TETAP))->first();

				$depreciations = Depreciation::where('buy_date', '<=', $date)->where('expire_date', '>', $date)->join($wi, $wi.'.item_id', '=', DB::raw("$d.item_id AND $wi.warehouse_id = $warehouse->id"))->get();

				$asset_tetap = 0;
				foreach ($depreciations as $dep) {
					$asset_tetap += $dep->calcValue();
				}

				//save to db
				CustomerStat::where('customer_id', '=', $warehouse->id)->update(array('balance' => ($asset_tetap + $asset->total_asset)));
			}
		});

		$cron = new Cron;
		$cron->name = 'asseter';
		if(!$cron->save()) echo $cron->getErrors()->first();

		DB::connection()->getPdo()->commit();
		echo 'done';
		return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}
}