<?php
namespace App\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Argun\Aria\Models\Depreciation, Argun\Aria\Models\Cron;
use Argun\Aria\Models\Transaction;
use Argun\Aria\Libraries\CCManager;
use Argun\Aria\Models\WarehouseItem;
use Argun\Aria\Models\Customer;
use Dater, DB;

class BestSellerCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'bestseller';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'creates best seller items.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		//find the date - had to do it this way becos of php date bug
		$date = Dater::now(); //cron is run every 1st of the month
		$thisMonth = $date->month;
		$thisYear = $date->year;
		$date->subMonth();
		$lastMonth = $date->month;
		$lastYear = $date->year;
		if($lastMonth == $thisMonth) $lastMonth--;
		if($lastMonth < 1)
		{
			$lastMonth = 12;
			$lastYear--;
		}

		//recreate the date
		$date = Dater::createMY($lastMonth,$lastYear);
		$from = $date->startOfMonth()->format(Dater::$SQLFormat);
		$to = $date->endOfMonth()->format(Dater::$SQLFormat);

		DB::connection()->getPdo()->beginTransaction();

		$total = 0;
		$wi = WarehouseItem::table();
		$d = Depreciation::table();
		$c = Customer::table();
		$dep = Depreciation::with('item')->where(function($query) use($from,$to)
			{
				$query->where('buy_date','>=',$from)->where('buy_date','<=',$to)->where('expire_date','>=',$from)->where('expire_date','>=',$to);
			})->orWhere(function($query) use($from,$to)
			{
				$query->where('buy_date','<=',$from)->where('buy_date','<=',$to)->where('expire_date','>=',$from)->where('expire_date','>=',$to);
			})->orWhere(function($query) use($from,$to)
			{
				$query->where('buy_date','<=',$from)->where('buy_date','<=',$to)->where('expire_date','>=',$from)->where('expire_date','<=',$to);
			})->join($wi, "$wi.item_id", '=', DB::raw("$d.item_id AND $wi.quantity = 1"))
			->join($c,"$c.id",'=',DB::raw("$wi.warehouse_id AND $c.type = ".Customer::TYPE_WAREHOUSE))->get();

		//group by warehouses
		$warehouse = array();
		foreach($dep as $d)
		{
			$wid = $d->warehouse_id;
			if(!isset($warehouse[$wid])) $warehouse[$wid] = array('total' => 0, 'description' => '');
			$depValue = $d->depreciate($date->endOfMonth());
			$warehouse[$wid]['total'] += $depValue;
			$warehouse[$wid]['description'] .= $d->item->code.' is depreciating ('.number_format($depValue,2).')<br/>';
		}

		//create transaction for each warehouse
		$cc = new CCManager;
		$date = $date->endOfMonth()->format(Dater::$format); //cron is run every 1st of the month
		foreach ($warehouse as $warehouse_id => $data) {

			//empty depreciation, dont save
			if(empty($data['total']))
				continue;

			$transaction = new Transaction;
			$transaction->date = $date;
			$transaction->init(Transaction::TYPE_DEPRECIATION, 0);
			$transaction->sender_id = $warehouse_id;
			$transaction->total = $transaction->real_total = $data['total'];
			$transaction->description = $data['description'];

			try{
				$cc->update(array(
					'date' => $date,
					'type' => Transaction::TYPE_DEPRECIATION,
					'customer' => $transaction->sender,
					'total' => $transaction->total,
				));
			}
			catch(\Exception $e) {
				DB::connection()->getPdo()->rollback();
				echo $e->getMessage();
				return;
			}

			//update the balances
			$transaction->receiver_balance = 0;
			$transaction->sender_balance = 0;

			//gets the transaction id
			if(!$transaction->save())
			{
				DB::connection()->getPdo()->rollback();
				echo 'error saving transaction';
				return;
			}

			if(!$transaction->invoice)
			{
				$transaction->invoice = $transaction->id;
				$transaction->save();
			}

			echo 'saved depreciation for '.$warehouse_id;
		}

		DB::connection()->getPdo()->commit();

		$cron = new Cron;
		$cron->name = 'depreciation';
		if(!$cron->save()) $this->error($cron->getErrors()->first());

		echo 'done';
		return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('last', InputArgument::OPTIONAL, 'set stat for this month or last month',0),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('last', null, InputOption::VALUE_OPTIONAL, 'An example option.', 0),
		);
	}
}