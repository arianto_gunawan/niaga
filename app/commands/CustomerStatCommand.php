<?php
namespace App\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Argun\Aria\Libraries\CCManager;
use Argun\Aria\Models\Customer,Argun\Aria\Models\Cron;

use Dater, DB;
class CustomerStatCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'customerstat';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'updates the db for customer stat.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$date = Dater::now();
		$thisMonth = $date->month;
		$thisYear = $date->year;
		//get last month data
		$last = $date->copy()->subMonth();
		$lastMonth = $last->month;
		$lastYear = $last->year;
		if($lastMonth == $date->month) $lastMonth--;
		if($lastMonth < 1)
		{
			$lastMonth = 12;
			$lastYear--;
		}
		$last = Dater::createFromFormat('m/Y',date("$lastMonth/$lastYear"));

		$createLast = true;
		$validator = \Validator::make(array('date' => $last->format(Dater::$SQLFormat)), array('date' => 'cdate'));
		if($validator->fails())
			$createLast = false;

		$cc = new CCManager;

		DB::connection()->getPdo()->beginTransaction();
		Customer::whereIn('type', array(Customer::TYPE_CUSTOMER, Customer::TYPE_RESELLER, Customer::TYPE_ACCOUNT, Customer::TYPE_WAREHOUSE, Customer::TYPE_SUPPLIER, Customer::TYPE_BANK))
		  ->chunk(200, function ($customer) use($thisMonth, $thisYear, $lastMonth, $lastYear, $createLast, $cc) {
			foreach($customer as $c)
			{
				echo 'creating stat for customer: '.$c->name.', id = '.$c->id;
				if(!$cc->createStat($c,$thisMonth,$thisYear))
				{
					DB::connection()->getPdo()->rollback();
					echo $cc->getErrors()->first();
					return;
				}
				if($createLast)
				{
					if(!$cc->createStat($c,$lastMonth,$lastYear))
					{
						DB::connection()->getPdo()->rollback();
						echo $cc->getErrors()->first();
						return;
					}
				}
			}
			echo 'done!';
		});

		$cron = new Cron;
		$cron->name = 'customerstat';
		if(!$cron->save()) echo $cron->getErrors()->first();

		DB::connection()->getPdo()->commit();
		return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}
}