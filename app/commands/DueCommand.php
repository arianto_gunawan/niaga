<?php
namespace App\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Argun\Aria\Models\Notification, Argun\Aria\Models\Transaction;
use Argun\Aria\Models\Cron, Argun\Aria\Libraries\Apps;
use Argun\Aria\Models\BalanceTracker;
use Dater, DB, Cache;
class DueCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'due';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'checks the db for due transactions.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		DB::connection()->getPdo()->beginTransaction();

		$today = Dater::now()->format(Dater::$SQLFormat);
		$nextWeek = Dater::now()->addDays(7)->format(Dater::$SQLFormat);
		echo $nextWeek;
		//find one that's due in a week
		BalanceTracker::where('transaction_ids','!=', '')->chunk(200, function($data) use($today, $nextWeek) {
			foreach($data as $d) {
				$transactions = Transaction::whereIn('id', $d->transaction_ids)->where('due', '=', $nextWeek)->get();
				//add to notif
				foreach ($transactions as $t) {
					echo 'adding notif for: '.$t->id;
					$n = Notification::firstOrCreate(array('app_id' => Apps::TRANSACTIONS, 'entity_id' => $t->id));
					$n->user_id = 1; //admin
					$n->action = 'due';
					$n->date = $t->due;
					$n->start = $today;
					$n->comment = '('.$t->printType().') Invoice #.'.$t->getDetailLink($t->invoice).' is due on '.Dater::display($t->due);
					if(!$n->save())
						throw new \Exception('cannot save notifs.<br/>'.$n->getErrors()->first());
				}
			}
		});

		$cron = new Cron;
		$cron->name = 'due';
		if(!$cron->save()) echo $cron->getErrors()->first();

		echo 'done';
		DB::connection()->getPdo()->commit();
		return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}
}