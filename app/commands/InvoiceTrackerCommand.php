<?php
namespace App\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Argun\Aria\Models\Updater;
use Argun\Aria\Models\Cron;
use Argun\Aria\Models\Customer;
use Argun\Aria\Libraries\Apps, Argun\Aria\Libraries\Keys;
use Argun\Aria\Libraries\InvoiceTracker;
use Dater, DB, Cache;
class InvoiceTrackerCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'invoicetracker';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'updates the db for customer stat.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		DB::disableQueryLog();
		DB::connection()->getPdo()->beginTransaction();

		Updater::with('customer')->where('app_id','=', Apps::TRACK_INVOICE)->where('flag','=', Updater::NEED_UPDATE)->chunk(50, function($data) {
			foreach($data as $d) {
				if(!$d->customer) //if customer is deleted
					continue;
				//update the data first
				switch ($d->customer->type) {
					case Customer::TYPE_CUSTOMER:
					case Customer::TYPE_RESELLER:
						InvoiceTracker::updateCustomer($d->customer);
						break;
					case Customer::TYPE_SUPPLIER:
						InvoiceTracker::updateSupplier($d->customer);
					default: break;
				}
			}

			$d->flag = Updater::NO_UPDATE;

			if(!$d->save())
				echo ('cannot save invoice: '.$d->entity_id);
			Cache::forget(Keys::updater($d->entity_id,Apps::TRACK_INVOICE));
		});

		$cron = new Cron;
		$cron->name = 'invoicetracker';
		if(!$cron->save()) echo $cron->getErrors()->first();

		echo 'done';
		DB::connection()->getPdo()->commit();
		return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}
}