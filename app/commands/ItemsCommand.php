<?php
namespace App\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Argun\Aria\Models\ItemStat, Argun\Aria\Models\Cron;
use Argun\Aria\Models\Transaction, Argun\Aria\Models\TransactionDetail;

use Dater, DB;

class ItemsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'itemstat';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'updates the db for item stat.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		//find the date
		$date = Dater::now();
		$last = $this->option('last');
		//get last month data
		if($last == 1)
		{
			$last = $date->copy()->subMonth();
			$lastMonth = $last->month;
			$lastYear = $last->year;
			if($lastMonth == $date->month) $lastMonth--;
			if($lastMonth < 1)
			{
				$lastMonth = 12;
				$lastYear--;
			}
			$last = Dater::createFromFormat('m/Y',date("$lastMonth/$lastYear"));
			$validator = \Validator::make(array('date' => $last->format(Dater::$SQLFormat)), array('date' => 'cdate'));
			$from = $last->startOfMonth()->format(Dater::$SQLFormat);
			$to = $last->endOfMonth()->format(Dater::$SQLFormat);
			if($validator->fails())
			{
				$this->info('sdh tutup buku choi');
				return;
			}
		}
		else
		{
			$from = $date->startOfMonth()->format(Dater::$SQLFormat);
			$to = $date->endOfMonth()->format(Dater::$SQLFormat);
		}

		DB::connection()->getPdo()->beginTransaction();

		$t = Transaction::table();
		$td = TransactionDetail::table();
		$stat = DB::table($td)
			->select(array(DB::raw("SUM( $td.total ) as total_price"), DB::raw("AVG( $td.price ) as average_price"),"$td.item_id","$t.type", DB::raw("SUM( $td.quantity ) as items")))
			->where("$td.date",'>=',$from)->where("$td.date",'<=',$to)
			->join($t,"$td.transaction_id",'=',"$t.id")
			->groupBy("$td.item_id")->groupBy("$t.type")
			->get();

		foreach($stat as $s)
		{
			$save = true;
			if(!$is = ItemStat::where('item_id','=',$s->item_id)->where('date','=',$from)->first())
			{
				$is = new ItemStat;
				$is->item_id = $s->item_id;
				$is->date = $from;
			}
			$this->info("generating stats for $s->item_id");
			switch($s->type)
			{
				case Transaction::TYPE_SELL:
					$is->sell_items = $s->items;
					$is->sell_total = abs($s->total_price);
					$is->sell_average = $s->average_price;
					break;
				case Transaction::TYPE_BUY:
					$is->buy_items = $s->items;
					$is->buy_total = $s->total_price;
					$is->buy_average = $s->average_price;
					break;
				case Transaction::TYPE_RETURN:
					$is->return_items = $s->items;
					$is->return_total = $s->total_price;
					$is->return_average = $s->average_price;
					break;
				case Transaction::TYPE_RETURN_SUPPLIER:
					$is->supplier_items = $s->items;
					$is->supplier_total = abs($s->total_price);
					$is->supplier_average = $s->average_price;
					break;
				case Transaction::TYPE_USE:
					$is->use_items = $s->items;
					$is->use_total = abs($s->total_price);
					$is->use_average = $s->average_price;
					break;
				default:
					$save = false;
			}
			if(!$save) continue;
			if(!$is->save())
			{
				DB::connection()->getPdo()->rollback();
				$this->info($is->getErrors()->first());
				return;
			}
		}
		$this->info('done!');

		$cron = new Cron;
		$cron->name = 'itemstat';
		if(!$cron->save()) $this->error($cron->getErrors()->first());

		DB::connection()->getPdo()->commit();
		return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('last', InputArgument::OPTIONAL, 'set stat for this month or last month',0),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('last', null, InputOption::VALUE_OPTIONAL, 'An example option.', 0),
		);
	}
}