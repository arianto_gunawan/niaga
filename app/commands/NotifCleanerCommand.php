<?php
namespace App\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Argun\Aria\Models\Notification, Argun\Aria\Models\Transaction;
use Argun\Aria\Models\Cron;
use Argun\Aria\Models\BalanceTracker, Argun\Aria\Libraries\Keys;
use Dater, DB, Cache;
class NotifCleanerCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'notifclean';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'cleans notif db.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		DB::connection()->getPdo()->beginTransaction();

		//find old notifs that expires 10 days ago
		$old = Dater::now()->subDays(10)->format(Dater::$SQLFormat);

		//find one that's due in a week
		Notification::where('date', '<=', $old)->delete();

		$cron = new Cron;
		$cron->name = 'notifclean';
		if(!$cron->save()) echo $cron->getErrors()->first();

		echo 'done';
		DB::connection()->getPdo()->commit();
		return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}
}