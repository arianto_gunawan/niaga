<?php
namespace App\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Produksi,App\Models\AProduksi,App\Models\Cron;

class ProduksiCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'pruneproduksi';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'prunes the produksi db, move old data to archive.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		//get old produksi
		$from = Dater::toSQL(Dater::now()->subMonths(3)->endOfMonth()->format(Dater::$format));
		$produksi = Produksi::where('status','=',Produksi::STATUS_BOTH)->where('jahit_in','<=',$from)->get();

		DB::connection()->getPdo()->beginTransaction();
		foreach($produksi as $p)
		{
			$a = new AProduksi($p->getAttributes());
			$a->id = $p->id;
			if(!$a->save())
			{
				DB::connection()->getPdo()->rollback();
				pre($a->getErrors());exit;
			}
			$this->info('pruning '.$p->id.', to: '.$a->id);
			$p->delete();
		}

		$this->info('done!');

		$cron = new Cron;
		$cron->name = 'pruneproduksi';
		if(!$cron->save()) $this->error($cron->getErrors()->first());

		DB::connection()->getPdo()->commit();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}