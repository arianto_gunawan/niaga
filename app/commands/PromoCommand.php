<?php
namespace App\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Argun\Aria\Models\Promo,Argun\Aria\Models\Cron;

use Dater, DB;
class PromoCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'promoter';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'updates the db for customer stat.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$today = Dater::now()->format(Dater::$SQLFormat);
		$yesterday = Dater::now()->subDay()->format(Dater::$SQLFormat);

		//find promo that starts today or ends today/yesterday
		$promos = Promo::where('start','=',$today)->orWhere('stop','>=',$yesterday)->get();

		DB::connection()->getPdo()->beginTransaction();
		//check for cron related problem
		foreach ($promos as $key => $p)
		{
			$save = false;
			$this->info('start = '.$p->start.', stop = '.$p->stop);
			if($p->start == $today && $p->status == Promo::STATUS_OFF) //promo starts today
			{
				$this->info('turning on');
				$save = true;
				$p->status = Promo::STATUS_ON;
			}
			elseif($p->stop == $today && $p->status == Promo::STATUS_ON) //promo ends today
			{
				$this->info('turning off');
				$save = true;
				$p->status = Promo::STATUS_OFF;
			}
			elseif($p->stop == $yesterday && Promo::STATUS_ON ) //1 day promo
			{
				$this->info('turning off 2');
				$save = true;
				$p->status = Promo::STATUS_OFF;
			}

			if($save)
			{
				if(!$p->save())
				{
					DB::connection()->getPdo()->rollback();
					$this->info($cc->getErrors()->first());
					return;
				}
			}
		}
		$this->info('done!');

		$cron = new Cron;
		$cron->name = 'promoter';
		if(!$cron->save()) $this->error($cron->getErrors()->first());

		DB::connection()->getPdo()->commit();
		return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}
}