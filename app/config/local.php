<?
return array(
	'cache_key' => 'crystastic',
	'default_warehouse' => 1,
	'item_url' => '/aria/public/items/search',
	'ongkos_url' => '/aria/public/gaji/item',
	'account_url' => '/aria/public/accounts/search',
	'customer_url' => '/aria/public/customers/search',
	'user_url' => '/aria/public/users/search',
	'personnel_url' => '/aria/public/personnels/search',
	'city_url' => '/aria/public/geo/cities',
	'geoData_url' => '/aria/public/geo/data',
	'item_image_url' => '/aria/public/img/items/',
	'item_image_path' => '../../aria/public/img/items/',
	'template_error' => '/aria/public/templates/error.html',
	'tx_image_url' => '/aria/public/img/tx/',
	'tx_image_path' => '../../aria/public/img/tx/',
	'member_image_path' => '../../aria/public/img/m/',
	'member_image_url' => '/aria/public/img/m/',
	'member_font_path' => '../../aria/public/img/m/bebas.ttf',
	'member_basecc_path' => '../../aria/public/img/m/basecc.jpg',
	'member_basecr_path' => '../../aria/public/img/m/basecr.jpg',
);
?>