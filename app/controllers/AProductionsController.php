<?
use App\Models\Personnel, App\Models\Tag;
use App\Models\AProduksi;
class AProductionsController extends BaseController
{
	public function __construct()
	{
		parent::__construct();

		//load jahit
		$this->_data['jahit'] = Personnel::jahitProduksi()->lists('group','group');
		ksort($this->_data['jahit']);
		array_unshift($this->_data['jahit'],'---');

		$this->_data['potong'] = Personnel::potongProduksi()->lists('name','id');
		$this->_data['potong'][0] = 'Potong';
		ksort($this->_data['potong']);

		$this->_data['size'] = Tag::where('type','=',Tag::TYPE_SIZE)->lists('name','id');
		$this->_data['size'][0] = 'Size';
		ksort($this->_data['size']);
	}

	public function getIndex()
	{
		$this->addScript(array('dateinput'));

		if(!$sort = Input::get('sort',false)) $sort = 'id';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$from = Input::get('from',false);
		$to = Input::get('to',false);

		//init the query
		$query = AProduksi::with(array('item','potong','size'));
		//dates are set!
		if($from && $to)
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where(function($query) use($from,$to)
			{
				$query->where(function($query) use($from,$to)
				{
					$query->where('potong_in','>=',$from)->where('potong_in','<=',$to);
				});
				$query = $query->or_where(function($query) use($from,$to)
				{
					$query->where('jahit_in','>=',$from)->where('jahit_out','<=',$to);
				});
			});
		}

		//find potong
		if($potong = Input::get('potong',false))
			$query = $query->where('potong_id','=',$potong);
		if($jahit = Input::get('jahit',false))
			$query = $query->where('jahit','=',$jahit);
		if($customer = Input::get('customer',false))
			$query = $query->where('customer','=',$customer);
		if($warna = Input::get('warna',false))
			$query = $query->where('warna','=',$warna);
		if($kode = Input::get('kode',false))
			$query = $query->where('temp_name','LIKE',"%$kode%");
		if($id = Input::get('id',false))
			$query = $query->where('id','=',Produksi::fromSerial($id));
		if($size = Input::get('size',false))
			$query = $query->where('size_id','=',$size);

		$this->_data['produksi'] = $query->orderBy($sort,$dir)->paginate(30);
		$this->layout->action = 'Barang di Produksi';
		$this->layout->content = View::make('productions.index',$this->_data);
	}
}