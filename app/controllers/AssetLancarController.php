<?
use App\Models\Item;
class AssetLancarController extends AssetTetapController
{
	protected $type = Item::TYPE_ASSET_LANCAR;
	protected $action = 'AssetLancarController';
	protected $_title = 'Aset Lancar';
}