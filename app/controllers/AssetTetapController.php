<?
use App\Libraries\ItemsManager;
use App\Models\Item, App\Models\Depreciation;
class AssetTetapController extends ItemsController
{
	protected $type = Item::TYPE_ASSET_TETAP;
	protected $action = 'AssetTetapController';
	protected $_title = 'Aset Tetap';

	public function getIndex()
	{
		$this->loadIndex();
		$this->_data['action'] = $this->action;
		$this->layout->action = 'Item List';
	    $this->layout->content = View::make('items.index',$this->_data);

		if($this->type == Item::TYPE_ASSET_TETAP)
			$this->layout->content = View::make('items.asset_tetap',$this->_data);
	}

	public function postIndex()
	{
		$this->loadIndex();
		return Response::json($this->_data['items']);
	}

	public function loadIndex()
	{
		if(!$sort = Input::get('sort',false)) $sort = 'id';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$code = Input::get('code',false);
		$name = Input::get('name',false);

		//init the query
		if($this->type == Item::TYPE_ASSET_TETAP)
			$query = Item::with('depreciation');
		else
			$query = new Item;

		$query = $query->where('type','=',$this->type);

		//dates are set!
		if(!empty($code)) {
			if(is_numeric($code))
				$query = $query->where('id','=', $code);
			else
				$query = $query->where('code','LIKE',"%$code%");
		}
		if(!empty($name)) {
			$name = str_replace(' ', '%', $name);
			$query = $query->where('name','LIKE',"%$name%");
		}

  	$this->_data['items'] = $query->orderBy($sort,$dir)->paginate(15);
  	$this->_data['items'] = ItemsManager::toArray($this->_data['items']);
	}

	public function getCreate()
	{
		$this->_data['type'] = $this->type;
		$this->layout->action = $this->_title;
		$this->layout->content = View::make('items.asset_form',$this->_data)->with('item',new Item);
	}

	public function postCreate()
	{
		try {
		DB::connection()->getPdo()->beginTransaction();

		$input = json_decode(Input::get('item'),true);

		$item = new Item($input);
		$item->type = $this->type;
		$item->code = $item->pcode = strtoupper($item->code);
		$item->name = strtoupper($item->name);

		if(!$item->save())
			throw new ModelException($item->getErrors(), __LINE__);

		$file = Input::file('file');
		if(!empty($file))
		{
			$file = $file->move($item->getUploadPath(), $item->id.'.jpg');
			Image::make($file)->resize(300, null, function($constraint) {
				$constraint->aspectRatio();
			});
		}

		if($this->type == Item::TYPE_ASSET_TETAP)
		{
			$depreciation = Input::get('depreciation',0);
			$dep = new Depreciation;
			$dep->item_id = $item->id;
			$dep->value = $depreciation;
			if(!$dep->save())
				throw new ModelException($dep->getErrors(), __LINE__);
		}

		DB::connection()->getPdo()->commit();
		Session::flash('success', 'success', 'Asset created.');
		return Response::json(array(
			'url' => URL::action($this->action.'@getIndex'),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getEdit($id)
	{
		$this->loadItem($id);
		$this->layout->action = 'Edit '.$this->_data['item']->name;
		$this->layout->content = View::make('items.asset_form',$this->_data);
	}

	public function postEdit($id)
	{
		try
		{

		$this->loadItem($id);

		$input = json_decode(Input::get('item'), true);
		$this->_data['item']->fill($input);
		$this->_data['item']->pcode = $this->_data['item']->code;

		DB::connection()->getPdo()->beginTransaction();

		if(!$this->_data['item']->save())
			throw new ModelException($this->_data['item']->getErrors(), __LINE__);

		$file = Input::file('file');
		if(!empty($file))
		{
			$file = $file->move($this->_data['item']->getUploadPath(), $this->_data['item']->id.'.jpg');
			Image::make($file)->resize(300, null, function($constraint) {
				$constraint->aspectRatio();
			});
		}

/*		$dep = Depreciation::find($id);
		//for old items
		if(!$dep)
		{
			$dep = new Depreciation;
			$dep->item_id = $id;
		}
		if($this->type == Item::TYPE_ASSET_TETAP && $depreciation = Input::get('depreciation',false))
		{
			$dep->value = $depreciation;
			if(!$dep->save())
			{
				DB::connection()->getPdo()->rollback();
				return Redirect::back()->withInput()->withErrors($dep->getErrors());
			}
		}
*/
		DB::connection()->getPdo()->commit();
		Session::flash('success', 'success', 'Asset edited.');
		return Response::json(array(
			'url' => URL::action($this->action.'@getDetail', array($this->_data['item']->id)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}

	}
}