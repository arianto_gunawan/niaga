<?php
use App\Models\User;
class AuthController extends BaseController
{
	public function getLogin()
	{
		if(!Auth::guest())
			return Redirect::action('TransactionsController@getIndex');

		return View::make('login');
	}

	public function postLogin()
	{
		$credentials = array(
			'username'    => Input::get('username'),
			'password' => Input::get('password')
		);

		try
		{
			$user = Auth::attempt($credentials, false);

			if ($user)
			{
				$user = User::where('username','=',$credentials['username'])->first();
				if($user->active != 1)
				{
					Auth::logout();
					return Redirect::route('login')->withErrors(array('login' => 'no can do login'));
				}

				return Redirect::action('TransactionsController@getIndex');
			}
			else
			{
				Session::flash('error', 'User not found');
				return Redirect::back();
			}
		}
		catch(\Exception $e)
		{
			Session::flash('error', $e->getMessage());
			return Redirect::route('login');
		}
	}

	public function getLogout()
	{
		Auth::logout();
		return Redirect::route('login');
	}
}