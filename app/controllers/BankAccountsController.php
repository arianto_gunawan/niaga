<?
use App\Models\Customer;
class BankAccountsController extends CustomersController
{
	protected $type = Customer::TYPE_BANK;
	protected $action = 'BankAccountsController';
}