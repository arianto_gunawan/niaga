<?
use App\Models\Settings, App\Models\UserSetting;
use App\Libraries\InputForm, App\Models\Notification;
use App\Libraries\Keys;
use Illuminate\Support\MessageBag;
use Illuminate\Routing\Controller;
class BaseController extends Controller
{
	protected $_data;
	protected $_asset;
	protected $_access;
	protected $_lm;
	protected $_messages;
	protected $_appSettings;
	protected $_notifCount;
	protected $_bdays;
	protected $_libs;

	public $layout = 'layouts.default';

	public function __construct()
	{
		$this->beforeFilter('csrf', array('on' => array('post', 'put', 'patch', 'delete')));

		$this->_data = array();
		$this->_libs = array();
		$this->_asset['header_css'] = array();
		$this->_asset['header_js'] = array();
		$this->_asset['footer_js'] = array();

		// Get the user details from when they logged in / old sessions
		$this->_data['_user'] = Auth::user();

		if($this->_data['_user'])
		{
			$id = $this->_data['_user']->id;
			$this->_data['app'] = Cache::remember(Keys::us($id), Keys::CACHE_GENERIC_TIME, function () use($id) {
				$app = UserSetting::where('user_id','=',$id)->lists('value','name');
				return $app;
			});
		}

		$this->_appSettings = App::make('appsettings');

		//check for notifs
		$this->_notifCount = 0;
		$this->_data['notifications'] = Cache::remember(Keys::notifs(), Keys::CACHE_SHORT_TIME, function () {
			//check for certain activities
			$date = Dater::now()->format(Dater::$SQLFormat);
			$acts = Notification::where('start','<=',$date)->where('date','>=',$date)->orderBy('date','asc')->take(6)->get();
			return $acts;
		});
		if(!empty($this->_data['notifications']))
			$this->_notifCount = count($this->_data['notifications']);

		$this->_data['defaults'] = Config::get('local');
		$this->_data['year'] = Dater::now()->year;
		$this->_access = App::make('access');
		$this->_lm = App::make('lm');
		$this->_messages = new MessageBag;
		InputForm::load(Input::old());
	}

	protected function setupLayout()
	{
		if(!is_null($this->layout))
		{
			$this->layout = \View::make($this->layout);
		}
		$this->layout->local = Config::get('local');
		$this->layout->header_js = $this->_asset['header_js'];
		$this->layout->header_css = $this->_asset['header_css'];
		$this->layout->footer_js = $this->_asset['footer_js'];
		$this->layout->access = $this->_access;
		$this->layout->user = $this->_data['_user'];
		$this->layout->notifCount = $this->_notifCount;
		$this->layout->ld = Config::get('layout');
		$this->layout->action = '';
	}

	protected function addScript($files = array(), $construct = false)
	{
		foreach($files as $file)
		{
			switch($file)
			{
				case 'chart':
					$this->_asset['footer_js']['chart'] = 'js/chart.min.js';
					break;
				default: break;
			}
		}
		if($construct) return;

		$this->layout->header_js = $this->_asset['header_js'];
		$this->layout->header_css = $this->_asset['header_css'];
		$this->layout->footer_js = $this->_asset['footer_js'];
	}

	//assumes: an active transaction
	protected function rollback($msg = null)
	{
		DB::connection()->getPdo()->rollback();
		if($msg instanceof MessageBag)
			return Redirect::back()->withInput()->withErrors($msg);

		return Redirect::back()->withInput()->with('error', $msg);
	}

	protected function error($message = null, $name = null)
	{
		if(!$name) $name = 'error';
		if(!$message)
			$this->_messages->add($name,$this->_messages->first());
		elseif($message instanceof MessageBag)
			$this->_messages = $message;
		else
			$this->_messages->add($name,$message);

		return false;
	}
}