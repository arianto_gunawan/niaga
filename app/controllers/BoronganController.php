<?
use App\Models\Borongan, App\Models\BoronganDetail, App\Models\Worker;
use App\Models\Item, App\Models\Tag;
use App\Models\Produksi;
class BoronganController extends BaseController
{
	public function __construct()
	{
		parent::__construct();

		//load jahit
		$this->_data['jahit'] = Worker::jahit()->get();
	}

	public function getIndex()
	{
		$this->loadBoronganList();
		$this->_data['can_delete'] = $this->_access->can('delete', Apps::BORONGAN);
		$this->layout->action = 'Borongan';
		$this->layout->content = View::make('borongan.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadBoronganList();
		return Response::json($this->_data['borongan']);
	}

	protected function loadBoronganList()
	{
		$from = Input::get('from',false);
		$to = Input::get('to',false);

		$query = Borongan::with(array('jahit'));
		//dates are set!
		if($from && $to)
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where('from','>=',$from)->where('to','<=',$to);
		}
		if($jahit_id = Input::get('jahit_id',false))
			$query = $query->where('jahit_id','=',$jahit_id);

		$borongan = $query->paginate(30);
		$response = array();

		$response['currentPage'] = $borongan->getCurrentPage();
		$response['lastPage'] = $borongan->getLastPage();
		$response['data'] = array();
		$index = 0;
		foreach ($borongan as $t) {
			$response['data'][$index] = $t->toArray();
			$response['data'][$index]['date'] = Dater::display($t->date);
			$response['data'][$index]['link'] = $t->getDetailLink();
			$index++;
		}

		$this->_data['borongan'] = $response;
	}

	public function getDetail($id)
	{
		if(!$this->_data['borongan'] = Borongan::find($id))
			return Redirect::to(404);

		$this->_data['details'] = BoronganDetail::with(array('item'))->where('borongan_id','=',$id)->get();
		$this->layout->content = View::make('borongan.detail',$this->_data);
	}

	public function getStat()
	{
		//sort
		if(!$sort = Input::get('sort',false)) $sort = 'bulan';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$this->_data['sort'] = $sort;
		$this->_data['dir'] = $dir;

		$this->_data['stat'] = DB::table(Borongan::table())
			->select(array(DB::raw('YEAR( FROM_UNIXTIME( date ) ) AS tahun'), DB::raw('MONTH( FROM_UNIXTIME( date ) ) AS bulan'), DB::raw('SUM( total ) as price'), 'group', DB::raw('SUM( total_items ) as items')))
			->where(DB::raw('YEAR( FROM_UNIXTIME( date ) )'),'=',$this->_data['year'])
			->groupBy('bulan')->groupBy('group')
			->order_by($sort,$dir)->order_by('id',$dir)
			->get();
		$this->layout->content = View::make('borongan.stat',$this->_data);
	}

	public function getAdd()
	{
		$this->_data['action'] = 'New Borongan';
		$this->layout->content = View::make('borongan.add',$this->_data);
	}

	//saves
	public function postAdd()
	{
		try {
		DB::connection()->getPdo()->beginTransaction();
		$borongan = $this->findBorongan();
		//try saving
		$b = new Borongan;
		$b->date = Dater::today()->toDateTimeString();
		$b->user_id = $this->_data['_user']->id;
		$b->permak = Input::get('permak',0);
		$b->tres = Input::get('tres',0);
		$b->lain2 = Input::get('lain2',0);
		$b->jahit_id = Input::get('jahit_id',false);
		$b->total_items = 0;
		$b->total = bcadd($b->permak, bcadd($b->tres, $b->lain2));
		$from = Input::get('from',false);
		if(!$from)
			throw new Exception('From date salah', 1);
		$b->from = Dater::toSQL($from);
		$to = Input::get('to',false);
		if(!$to)
			throw new Exception('To date salah', 1);
		$b->to = Dater::toSQL($to);
		if(!$b->save())
			throw new ModelException($b->getErrors());

		foreach ($borongan as $value) {
			$detail = new BoronganDetail;
			$detail->borongan_id = $b->id;
			$detail->item_id = $value['item']['id'];
			$detail->ongkos = $value['ongkos'];
			$detail->quantity = $value['quantity'];
			$detail->produksi_id = $value['produksi_id'];
			$detail->total = $value['total'];
			if(!$detail->save())
				throw new ModelException($detail->getErrors());

			//update produksi status
			$produksi = Produksi::find($value['produksi_id']);
			$produksi->status = Produksi::STATUS_BOTH;
			if(!$produksi->save())
				throw new ModelException($produksi->getErrors());

			$b->total = bcadd($b->total, $value['total']);
			$b->total_items += $value['quantity'];
		}

		if(!$b->save())
			throw new ModelException($b->getErrors());

		DB::connection()->getPdo()->commit();
		Session::flash('success', 'Borongan created.');
		return Response::json(array(
			'url' => URL::action('BoronganController@getIndex'),
		));
		return Response::json($response);

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	//for the LOAD button, find borongan
	public function postLoad()
	{
		try {
			$borongan = $this->findBorongan();
			$response['data'] = $borongan;

			return Response::json($response);

		} catch(ModelException $e) {
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			return Response::json($e->getMessage(), 500);
		}
	}

	protected function findBorongan()
	{
		$from = Input::get('from',false);
		$to = Input::get('to',false);
		if(!$from || !$to)
			throw new Exception('Tanggal error', 1);

		if(!$jahit_id = Input::get('jahit_id',false))
			throw new Exception('Jahit tidak boleh kosong', 1);

		$from = Dater::toSQL($from);
		$to = Dater::toSQL($to);
		//find date
		$query = Produksi::with(array('item'))->where('gudang_date','>=',$from)->where('gudang_date','<=',$to)->where('status','=',Produksi::STATUS_GUDANG);
		//find jahit
		$query = $query->where('jahit_id','=',$jahit_id);
		//fail-safe: make sure item is set
		$data = $query->where('item_id','>',0)->get();
		$borongan = [];
		foreach ($data as $key => $val) {
			$borongan[$key] = $val->toArray();
			$borongan[$key]['serial'] = $val->serial();
			$borongan[$key]['edit_link'] = URL::action('SetoranController@getIndex',array($val->id));

			$borongan[$key]['ongkos'] = $this->ongkos($val->item);
			$borongan[$key]['produksi_id'] = $val->id;
			$borongan[$key]['total'] = bcmul($val->quantity,$borongan[$key]['ongkos']);
		}
		return $borongan;
	}

	//ajax
	public function getItem()
	{
		$code = Input::get('code');
		$item = Item::where('code','=',$code)->first();
		return Response::json($this->ongkos($item));
	}

	protected function ongkos($item)
	{
		if(!$item)
			return 0;

		//crystalsports_items
		$ongkos = $item->tags()->where(Tag::table().'.type','=',Tag::TYPE_JAHIT)->first();
		if(!$ongkos)
			return 0;

		return $ongkos->price;
	}


}