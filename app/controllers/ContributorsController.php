<?

use App\Models\Transaction, App\Models\Tag;
use App\Models\Customer, App\Models\Operation;
use App\Models\Settings, App\Models\Location;
use App\Models\WarehouseItem, App\Models\Item, App\Libraries\ItemsManager;
use App\Models\CustomerStat;
use App\Models\ItemTag, App\Models\TransactionDetail;

//UPDATE `transaction_details` inner join `transactions` on `transaction_details`.`transaction_id` = `transactions`.`id` set `transaction_details`.`transaction_disc` = `transactions`.`discount`

class ContributorsController extends BaseController
{
	//TODO: save old data?
	public function getIndex()
	{
		$this->loadIndex();
		$this->_data['brands'] = json_encode(Item::$brandsJSON);
		$this->_data['sizes'] = Tag::loadSizes() + array( 0 => 'Accessories');
		$this->_data['sizes'] = json_encode($this->_data['sizes']);
		$this->_data['genres'] = Tag::loadGenres() + array( 0 => 'Accessories');
		$this->_data['genres'] = json_encode($this->_data['genres']);
		$this->layout->content = View::make('contributors.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadIndex();
		return Response::json($this->_data['responseJSON']);
	}

	protected function loadIndex()
	{
		//table names
		$t = Transaction::table();
		$td = TransactionDetail::table();
		$sellType = Transaction::TYPE_SELL;
		$c = Customer::table();
		$i = Item::table();
		$it = ItemTag::table();

		//init
		$tags = array();
		$select = array(
			"$td.item_id", "$i.code", "$i.name", "$i.size", "$i.brand", "$i.genre", "$i.type",
			DB::raw("SUM( $td.quantity ) as total_quantity"),
			DB::raw("SUM( ($td.total * (100 - $td.transaction_disc)) / 100 ) as total_value"),
		);

		//check filter
		$filterBrand = Input::get('brand',false);

		//default period, 1 month
		$date = Dater::now();
		$from = Dater::toSQL(Input::get('from',$date->startOfMonth()->format(Dater::$format)));
		$to = Dater::toSQL(Input::get('to',$date->endOfMonth()->format(Dater::$format)));
		//display global contributors
		$warehouseId = $customerId = false;
		$cInput = Input::get('customer_id',false);
		if($cInput) {
			$customer = Customer::where('id','=',$cInput)->withTrashed()->first();
			if($customer->type == Customer::TYPE_CUSTOMER || $customer->type == Customer::TYPE_RESELLER) $customerId = $customer->id;
			if($customer->type == Customer::TYPE_WAREHOUSE) $warehouseId = $customer->id;
		}

		//top 50
		$data = DB::table($td)->select($select)
		->where($td.'.date','>=',$from)->where($td.'.date','<=',$to)
		->where("$td.transaction_type", '=', Transaction::TYPE_SELL)
		->join($i,"$i.id",'=',"$td.item_id");
		//filters
		if(!empty($filterBrand))
			$data = $data->where("$i.brand",'=',$filterBrand);
		if(!empty($warehouseId)) {
			$data = $data->join($c, "$c.id",'=', "$td.sender_id")->where("$td.sender_id",'=',$warehouseId);
		} elseif (!empty($customerId)) {
			$data = $data->join($c, "$c.id",'=', "$td.receiver_id")->where("$td.receiver_id",'=',$customerId);
		}
		$top50 = $data->groupBy("$td.item_id")->orderBy('total_value','desc')->take(50)->get();
		foreach ($top50 as $key => $value) {
			$value->link = Item::getDetailUrl($value->item_id, $value->type);
		}

		//best selling size
		$select = array(
			"$i.size",
			DB::raw("SUM( $td.quantity ) as total_quantity"),
			DB::raw("SUM( ($td.total * (100 - $td.transaction_disc)) / 100 ) as total_value"),
		);
		$data = DB::table($td)->select($select)
		->where($td.'.date','>=',$from)->where($td.'.date','<=',$to)
		->where("$td.transaction_type", '=', Transaction::TYPE_SELL)
		->join($i,"$i.id",'=',"$td.item_id");
		//filters
		if(!empty($filterBrand))
			$data = $data->where("$i.brand",'=',$filterBrand);
		if(!empty($warehouseId)) {
			$data = $data->join($c, "$c.id",'=', "$td.sender_id")->where("$td.sender_id",'=',$warehouseId);
		} elseif (!empty($customerId)) {
			$data = $data->join($c, "$c.id",'=', "$td.receiver_id")->where("$td.receiver_id",'=',$customerId);
		}
		$bySize = $data->groupBy("$i.size")->orderBy('total_value','desc')->get();

		//best brand
		$select = array(
			"$i.brand",
			DB::raw("SUM( $td.quantity ) as total_quantity"),
			DB::raw("SUM( ($td.total * (100 - $td.transaction_disc)) / 100 ) as total_value"),
		);
		$data = DB::table($td)->select($select)
		->where($td.'.date','>=',$from)->where($td.'.date','<=',$to)
		->where("$td.transaction_type", '=', Transaction::TYPE_SELL)
		->join($i,"$i.id",'=',"$td.item_id");
		if(!empty($warehouseId)) {
			$data = $data->join($c, "$c.id",'=', "$td.sender_id")->where("$td.sender_id",'=',$warehouseId);
		} elseif (!empty($customerId)) {
			$data = $data->join($c, "$c.id",'=', "$td.receiver_id")->where("$td.receiver_id",'=',$customerId);
		}
		$byBrand = $data->groupBy("$i.brand")->orderBy('total_value','desc')->get();

		//best genre
		$select = array(
			"$i.genre",
			DB::raw("SUM( $td.quantity ) as total_quantity"),
			DB::raw("SUM( ($td.total * (100 - $td.transaction_disc)) / 100 ) as total_value"),
		);
		$data = DB::table($td)->select($select)
		->where($td.'.date','>=',$from)->where($td.'.date','<=',$to)
		->where("$td.transaction_type", '=', Transaction::TYPE_SELL)
		->join($i,"$i.id",'=',"$td.item_id");
		//filters
		if(!empty($filterBrand))
			$data = $data->where("$i.brand",'=',$filterBrand);
		if(!empty($warehouseId)) {
			$data = $data->join($c, "$c.id",'=', "$td.sender_id")->where("$td.sender_id",'=',$warehouseId);
		} elseif (!empty($customerId)) {
			$data = $data->join($c, "$c.id",'=', "$td.receiver_id")->where("$td.receiver_id",'=',$customerId);
		}
		$byGenre = $data->groupBy("$i.genre")->orderBy('total_value','desc')->get();

		//by brand, genre, size
		$select = array(
			"$i.genre","$i.brand","$i.size",
			DB::raw("SUM( $td.quantity ) as total_quantity"),
			DB::raw("SUM( ($td.total * (100 - $td.transaction_disc)) / 100 ) as total_value"),
		);
		$data = DB::table($td)->select($select)
		->where($td.'.date','>=',$from)->where($td.'.date','<=',$to)
		->where("$td.transaction_type", '=', Transaction::TYPE_SELL)
		->join($i,"$i.id",'=',"$td.item_id");
		//filters
		if(!empty($filterBrand))
			$data = $data->where("$i.brand",'=',$filterBrand);
		if(!empty($warehouseId)) {
			$data = $data->join($c, "$c.id",'=', "$td.sender_id")->where("$td.sender_id",'=',$warehouseId);
		} elseif (!empty($customerId)) {
			$data = $data->join($c, "$c.id",'=', "$td.receiver_id")->where("$td.receiver_id",'=',$customerId);
		}
		$by3 = $data->groupBy("$i.brand")->groupBy("$i.genre")->groupBy("$i.size")->orderBy('total_value','desc')->get();

//		pre($byGenre);
//		pre(DB::getQueryLog());exit;
//		pre($data);exit;

		$this->_data['links']['from'] = Dater::display($from);
		$this->_data['links']['to'] = Dater::display($to);

		$this->_data['responseJSON'] = array(
			'top50' => $top50,
			'byBrand' => $byBrand,
			'byGenre' => $byGenre,
			'bySize' => $bySize,
			'by3' => $by3,
		);
		$this->_data['data'] = json_encode($this->_data['responseJSON']);
	}
}