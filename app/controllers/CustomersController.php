<?
use App\Models\Customer, App\Models\CustomerStat, App\Libraries\InvoiceTracker;
use App\Models\Transaction, App\Models\Location, App\Models\TransactionDetail;
use App\Models\CustomerClass;
use App\Models\Item, App\Libraries\ItemsManager;
use App\Models\WarehouseItem;
use App\Libraries\CCManager, App\Libraries\Keys;
use App\Models\HashTag, App\Models\HT;
use App\Libraries\TransactionsManager;
class CustomersController extends BaseController
{
	protected $type = Customer::TYPE_CUSTOMER;
	protected $app = Apps::CUSTOMERS;
	protected $action = 'CustomersController';
	protected $folder = 'customers';
	protected $form = 'form';

	public function __construct()
	{
		BaseController::__construct();
		$this->_data['view_items'] = true;
		$this->_data['view_sales'] = false;
		switch($this->type)
		{
			case Customer::TYPE_CUSTOMER:
			case Customer::TYPE_RESELLER:
			case Customer::TYPE_WAREHOUSE:
				$this->_data['view_sales'] = true;
				break;
			case Customer::TYPE_BANK:
			case Customer::TYPE_ACCOUNT:
			case Customer::TYPE_VACCOUNT:
				$this->_data['view_items'] = false;
				break;
			default:
			break;
		}
		$this->_data['type'] = $this->type;
		$this->_data['action'] = $this->action;

		$this->_data['delete'] = false;
		$this->_data['restore'] = false;
		$this->_data['delete'] = $this->_access->can('delete', $this->app);
		$this->_data['restore'] = $this->_access->can('restore', $this->app);
	}

	public function getIndex()
	{
		$this->loadIndex();
		$this->layout->action = Customer::$types[$this->type].' List';
   	$this->layout->content = View::make($this->folder.'.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadIndex();
		return Response::json($this->_data['responseJSON']);
	}

	protected function loadIndex()
	{
		$cs = CustomerStat::table();
		$c = Customer::table();
		if(!$sort = Input::get('sort',false)) $sort = $c.'.name';
		if(!$dir = Input::get('dir',false)) $dir = 'asc';

		switch($sort)
		{
			case 'balance': $sort = $cs.'.balance'; break;
			case 'class': $sort = $cs.'.rating'; break;
			case 'name': $sort = $c.'.name'; break;
			case 'category': $sort = $c.'.category'; break;
			default: break;
		}

		if(!$this->_data['_user']->location_id)
		{
			$location = Input::get('location',null);
			$this->_data['locations'] = Location::lists('name','id');
			$this->_data['locations'][0] = '---';
			ksort($this->_data['locations']);
		}
		else
		{
			$location = null;
			$this->_data['locations'] = false;
		}

		$query = Customer::filter($location)->where('type','=',$this->type);
		$query = $query->join('customerstat',$c.'.id','=',$cs.'.customer_id');

		if($name = Input::get('name',false)) {
			$name = str_replace(' ', '%', $name);
			$query = $query->where('name','LIKE',"%$name%");
		}
		if($id = Input::get('id',false)) {
			$query = $query->where('memberId','=', $id);
		}

		if($deleted = Input::get('deleted', false))
			$query = $query->onlyTrashed();

		$customers = $query->orderBy($sort,$dir)->paginate(50);

		//format data for json
		$response = array();
		$response['currentPage'] = $customers->getCurrentPage();
		$response['lastPage'] = $customers->getLastPage();
		$response['data'] = array();
		foreach ($customers as $index => $t) {
			$response['data'][$index]['id'] = $t->id;
			$response['data'][$index]['memberId'] = $t->memberId;
			$response['data'][$index]['name'] = $t->name;
			$response['data'][$index]['balance'] = $t->balance;
			$response['data'][$index]['detail_link'] = $t->getDetailLink();
			$response['data'][$index]['edit_link'] = $t->getEditLink();
			$response['data'][$index]['transaction_link'] = $t->getTransactionsLink();
			$response['data'][$index]['expiry_date'] = Dater::display($t->contract_ends);
			$response['data'][$index]['locations'] = array();
			foreach($t->locations as $locationIndex => $l) {
				$response['data'][$index]['locations'][$locationIndex]['link'] = \URL::action('LocationsController@getDetail',array($l->id));
				$response['data'][$index]['locations'][$locationIndex]['name'] = $l->name;
			}
		}

		$this->_data['responseJSON'] = $response;
		$this->_data['customers'] = Response::json($response)->getData();
	}

	public function getDetail($id)
	{
		$this->loadCustomer($id);
		$this->_data['can_edit'] = $this->_access->can('edit', $this->app);

		//check the app
		$this->_data['view_transactions'] = true;
		if(!$this->_access->can('transaction', $this->app))
		{
			$this->_data['view_transactions'] = false;
			$this->layout->content = View::make('customers.transaction',$this->_data);
			return;
		}

		$this->_data['portal'] = false;
		$this->_data['unportal'] = false;
		$this->_data['portal_active'] = 0;

		if($this->_data['customer']->type == Customer::TYPE_RESELLER || $this->_data['customer']->type == Customer::TYPE_CUSTOMER) {
			$this->_data['portal'] = $this->_access->can('portal', $this->app);
			$this->_data['unportal'] = $this->_access->can('unportal', $this->app);
			if($this->_data['customer']->portalId > 0)
				$this->_data['portal_active'] = $this->_data['customer']->portal->active;
			$user = \Portal\Models\User::where('customer_id','=',$this->_data['customer']->id)->first();
			$this->_data['user'] = $user;
			if(!$user)
					$warehouse = null;
			else
					$warehouse = Customer::find($user->warehouse_id);
			$this->_data['defaultWarehouse'] = json_encode($warehouse);
		}

		$this->layout->action = $this->_data['customer']->name;
		InvoiceTracker::track($this->_data['customer']);

		$this->layout->content = View::make('customers.detail',$this->_data);
	}

	public function postDetail($id)
	{
		$this->loadCustomer($id);
		return Response::json($this->_data['customer']);
	}

	public function getTransactions($id)
	{
		$this->loadTransactions($id);
		$this->_data['typesJSON'] = json_encode(Transaction::$typesJSON);
		$this->_data['hide'] = false;
		if($this->_access->hide('balance',Apps::HIDE))
			$this->_data['hide'] = true;

		$this->_data['balance_tracker'] = InvoiceTracker::getBalanceTracker($this->_data['customer']->id);
		if(!$this->_data['balance_tracker']) $this->_data['balance_tracker'] = new \App\Models\BalanceTracker;

		//init the types
		$this->_data['typesJSON'] = json_encode(Transaction::$typesJSON);

		$this->layout->action = $this->_data['customer']->name.' | Transaction List';
		$this->layout->content = View::make('customers.transaction',$this->_data);
	}

	public function postTransactions($id)
	{
		$this->loadTransactions($id);

		return Response::json($this->_data['transactions']);
	}

	protected function loadTransactions($id)
	{
		$this->loadCustomer($id);
		//init the query
		$query = new Transaction;
		$table = $query->getTable();

		//check for location
		$lids = false;
		if($this->_lm->bound())
		{
			$lids = $this->_lm->get_location();
/*			$query = $query->join($table.' as t1','t1.id','=',$table.'.id');
			$query = $query->where(function($query) use($lids)
			{
				$query->whereIn('t1.receiver_id',$lids)->orWhereIn('t1.sender_id',$lids);
			});*/
		}

		//check the dates
		$from = Input::get('from',false);
		$to = Input::get('to',false);
		if($from && $to) //dates are set!
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where($table.'.date','>=',$from)->where($table.'.date','<=',$to);
		}
		//find the types
		if($type = Input::get('type',false))
			$query = $query->where($table.'.type','=',$type['id']);

		$this->_data['transactions'] = $query->where(function($query) use($id, $table){
			$query->where($table.'.sender_id','=',$id);
			$query->orWhere($table.'.receiver_id','=',$id);
		})->orderBy($table.'.'.'date','desc')->orderBy($table.'.id','desc')->paginate(50);

		$this->_data['transactions'] = TransactionsManager::toArray($this->_data['transactions'], $lids, $id);
	}

	public function getSearchItem($id)
	{
		$this->loadCustomer($id);

		$this->layout->action = $this->_data['customer']->name.' | Search Items';
		$this->layout->content = View::make($this->folder.'.search',$this->_data);
	}

	public function postSearchItem($id)
	{
		$this->loadCustomer($id);

		$name = Input::get('name', null);
		if($name) $name = str_replace(' ', '%', $name);

		$items_table = Item::table();
		$td_table = TransactionDetail::table();

		$customerId = $this->_data['customer']->id;
		$query = TransactionDetail::with(array('transaction', 'item'))
		->join("$items_table as items_table", "$td_table.item_id", '=', 'items_table.id')
		->where('items_table.name','like',"%$name%")->where(function($query) use($customerId) {
			$query->where('sender_id', '=', $customerId)->orWhere('receiver_id', '=', $customerId);
		});

		$this->_data['items'] = $query->orderBy($td_table.'.'.'date','desc')->orderBy($td_table.'.transaction_id','desc')->paginate(50);

		//format data for json
		$response = array();
		$response['currentPage'] = $this->_data['items']->getCurrentPage();
		$response['lastPage'] = $this->_data['items']->getLastPage();
		$response['data'] = array();
		foreach ($this->_data['items'] as $index => $t) {
			$response['data'][$index]['t_link'] = $t->transaction->getDetailLink();
			$response['data'][$index]['item_name'] = $t->item->name;
			$response['data'][$index]['item_code'] = $t->item->code;
			$response['data'][$index]['item_link'] = $t->item->getDetailLink();
			$response['data'][$index]['invoice'] = $t->transaction->invoice;
			$response['data'][$index]['t_date'] = $t->transaction->date;
			$response['data'][$index]['t_type'] = $t->transaction->printType();
			$response['data'][$index]['quantity'] = $t->quantity;
		}

		return Response::json($response);
	}

	public function getCreate()
	{
		$this->_data['customer'] = new Customer;
		$this->_data['create'] = true;
		$this->layout->action = 'Create';
		$this->_data['can_switch'] = false;
		$this->layout->content = View::make($this->folder.'.'.$this->form,$this->_data);
	}

	public function postCreate()
	{
		try {
		$input = Input::get('customer');

		$initial = Input::get('initial',0);

		DB::connection()->getPdo()->beginTransaction();

		$customer = new Customer($input);
		$customer->name = ucfirst(trim($customer->name));
		$customer->type = $this->type;
		if($this->checkExist($customer))
			throw new \Exception('customer already exists');

		if(!$customer->save())
			throw new ModelException($customer->getErrors(), __LINE__);

		$memberId = strtoupper(substr($customer->name,-1).Dater::now()->format(Dater::$memberFormat).substr($customer->name, -2, 1));
		$customer->memberId = $customer->id.$memberId;
		if(!$customer->save())
			throw new ModelException($customer->getErrors(), __LINE__);

		//create stat
		$stat = new CustomerStat;
		$stat->customer_id = $customer->id;
		$stat->balance = $initial;
		if(!$stat->save())
			throw new ModelException($stat->getErrors(), __LINE__);

		//create class
		$date = Dater::now()->startOfMonth()->format(Dater::$SQLFormat);
		$cc = new CCManager;
		if(!$cc->emptyStat($customer,$date))
			throw new \Exception('cannot save customer class');

		//check location
		if(!empty($this->_data['_user']->location_id))
			$this->_lm->assign($this->_data['_user']->location_id,$customer->id);

		DB::connection()->getPdo()->commit();

		Session::flash('success', $customer->printType().' created.');
		return Response::json(array(
			'url' => URL::action($customer->getController().'@getDetail', array($customer->id)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getEdit($id = false)
	{
		$this->loadCustomer($id);
		$this->_data['create'] = false;
		$this->_data['can_switch'] = $this->_access->can('addr-book',Apps::SWITCHER);
		$this->layout->content = View::make($this->folder.'.'.$this->form,$this->_data);
	}

	public function postEdit($id = false)
	{
		try {

		$this->loadCustomer($id);

		$input = Input::get('customer');
		$this->_data['customer']->fill($input);

		if($this->_access->can('addr-book',Apps::SWITCHER))
		{
			if($type = $input['type'])
			{
				$this->_data['customer']->type = $type;
				$this->action = $this->_data['customer']->getController();
			}
		}

		if($this->checkExist($this->_data['customer']))
			throw new \Exception('customer already exists');

		if(!$this->_data['customer']->save())
			throw new ModelException($this->_data['customer']->getErrors(), __LINE__);

		Session::flash('success', $this->_data['customer']->name.' edited.');
		return Response::json(array(
			'url' => URL::action($this->action.'@getDetail', array($id)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	protected function loadCustomer($id)
	{
		if(!$this->_data['customer'] = Customer::with('stat')->where('id','=',$id)->where('type','=',$this->type)->withTrashed()->first())
			return App::abort(404);

		if(!in_array($this->type, array(Customer::TYPE_CUSTOMER)))
			if(!$this->_lm->can($id))
				return App::abort(404);

		return $this->_data['customer'];
	}

	//ajax
	public function getSearch()
	{
		$term = Input::get('q');
		$term = str_replace(' ', '%', $term);
		$query = Customer::with('stat')->filter();

		switch(Input::get('type'))
		{
			case 'c': $query = $query->whereIn('type',array(Customer::TYPE_CUSTOMER,Customer::TYPE_RESELLER)); break;
			case 's': $query = $query->where('type','=',Customer::TYPE_SUPPLIER); break;
			case 'a': $query = $query->where('type','=',Customer::TYPE_ACCOUNT); break;
			case 'w': $query = $query->where('type','=',Customer::TYPE_WAREHOUSE); break;
			case 'wv': $query = $query->whereIn('type',array(Customer::TYPE_VWAREHOUSE,Customer::TYPE_WAREHOUSE)); break;
			//only supplier and customer
			case 'cs': $query = $query->whereIn('type',array(Customer::TYPE_SUPPLIER,Customer::TYPE_RESELLER)); break;
			case 'loc': $query = $query->whereIn('type',array(Customer::TYPE_SUPPLIER,Customer::TYPE_RESELLER,Customer::TYPE_WAREHOUSE,Customer::TYPE_BANK)); break;
			case 'cash': $query = $query->whereNotIn('type',array(Customer::TYPE_BANK,Customer::TYPE_VWAREHOUSE,Customer::TYPE_WAREHOUSE,Customer::TYPE_VACCOUNT)); break;
			default: break;
		}
		$customers = $query->where('name','LIKE',"%$term%")->take(5)->get();

		$response = array();
		$i = 0;
		foreach($customers as $c)
		{
			$response[$i] = array();
			$response[$i]['name'] = $c->name;
			$response[$i]['id'] = $c->id;
			$response[$i]['balance'] = !empty($c->stat)?$c->stat->balance:0;
			$response[$i]['type'] = $c->type;
			$i++;
		}

		return Response::json($response);
	}

	public function getStat($id)
	{
		$this->loadStat($id);
		$this->layout->action = $this->_data['customer']->name.' | Stats';
		$this->layout->content = View::make('customers.stat',$this->_data);
	}

	public function postStat($id)
	{
		$this->loadStat($id);
		return Response::json(array('data' => $this->_data['stats']));
	}

	protected function loadStat($id)
	{
		$this->loadCustomer($id);

		$from = Input::get('from',null);
		$to = Input::get('to',null);
		if($from && $to)
		{
			$from = Dater::createFromFormat('d/m/Y',"1/$from");
			$to = Dater::createFromFormat('d/m/Y',"1/$to");
		}
		else
		{
			$from = Dater::now()->subMonths(2)->startOfMonth();
			$to = Dater::now()->endOfMonth();
		}

		//init dates for angular
		$this->_data['dates']['fromYear'] = $from->year;
		$this->_data['dates']['fromMonth'] = $from->month;
		$this->_data['dates']['toYear'] = $to->year;
		$this->_data['dates']['toMonth'] = $to->month;
		$from = Dater::toSQL($from->format(Dater::$format));
		$to = Dater::toSQL($to->format(Dater::$format));

		//init data
		$customer_id = $this->_data['customer']->id;
		$stat = CustomerClass::where('date','>=',$from)->where('date','<=',$to)->where('customer_id','=',$customer_id)->get();

		//4. save the data
		$this->_data['stats'] = array();
		foreach($stat as $data)
		{
			$date = Dater::fromSQL($data->date);
			$startMonth = $data->date;
			$endMonth = Dater::toSQL($date->endOfMonth());

			//get transfer out
			$sql = DB::table(Transaction::table())
			->select(array(DB::raw('SUM( total ) as transfer_out')))
			->where('date','>=',$startMonth)->where('date','<=',$endMonth)
			->where('sender_id','=',$customer_id)
			->where('type', '=', Transaction::TYPE_TRANSFER)
			->first();
			$transfer_out = $sql->transfer_out;

			$this->_data['stats'][] = array(
				'date' => "$date->month/$date->year",
				'cash_in' => $data->cash_in,
				'sell' => abs($data->sell),
				'cash_out' => abs($data->cash_out),
				'return' => abs($data->return),
				'return_supplier' => abs($data->return_supplier),
				'move' => $data->move,
				'use' => $data->use,
				'buy' => $data->buy,
				'transfer' => $data->transfer,
				'transfer_out' => $transfer_out
			);
		}
	}

	//ITEMS
	public function getItems($id)
	{
		$this->loadItems($id);
		$this->layout->action = $this->_data['customer']->name.' | Items';
    $this->layout->content = View::make('customers.items',$this->_data);
	}

	public function postItems($id)
	{
		$this->loadItems($id);
		return Response::json($this->_data['items']);
	}

	protected function loadItems($id)
	{
		$this->loadCustomer($id);

		$items_table = Item::table();
		$w_table = WarehouseItem::table();

		if(!$sort = Input::get('sort',false)) $sort = 'quantity';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$query = WarehouseItem::with(array('item', 'item.group'))->join($items_table,"$items_table.id",'=',"$w_table.item_id")->where('warehouse_id','=',$id);
		if($name = Input::get('name',false)) {
			$query = $query->whereHas('item', function($query) use($name) {
				$query->where('name','LIKE', "%$name%");
			});
		}

		if(!Input::get('show0'))
			$query = $query->where('quantity','>',0);

		$query = $query->orderBy($sort, $dir);

		$this->_data['items'] = $query->paginate(1000);

		$response = array();
		$response['currentPage'] = $this->_data['items']->getCurrentPage();
		$response['lastPage'] = $this->_data['items']->getLastPage();
		$response['data'] = array();
		foreach ($this->_data['items'] as $key => $detail) {
			$response['data'][$key]['image'] = Item::getImagePath($detail->item_id);
			$response['data'][$key]['link'] = Item::getDetailUrl($detail->item_id,$detail->item->type);
			$response['data'][$key]['name'] = $detail->item->name;
			$response['data'][$key]['id'] = $detail->item_id;
			$response['data'][$key]['code'] = $detail->item->code;
			$response['data'][$key]['price'] = $detail->item->price;
			$response['data'][$key]['description'] = $detail->item->group ? $detail->item->group->description : $detail->item->description;
			$response['data'][$key]['quantity'] = $detail->quantity;
			$response['data'][$key]['edit'] = Item::getEditButton($detail->item_id,$detail->type);
		}

		$this->_data['items'] = $response;
	}

	public function getAlokasi($id)
	{
		$this->loadCustomer($id);

		$items_table = Item::table();
		$w_table = WarehouseItem::table();

		//init
		//only size that matters S M L XL
		$sizes = array(
			6421 => 'S',
			6353 => 'M',
			6376 => 'L',
			6374 => 'XL',
		);

		//1. get 50 random items group

	}

	//gets any transaction involving items for a customer
	public function getSales($customerId)
	{
		$this->loadSales($customerId);
		$this->_data['typesJSON'] = json_encode(Transaction::$typesJSON);
		$this->layout->action = $this->_data['customer']->name.' | Sales';
        $this->layout->content = View::make('customers.sales',$this->_data);
	}

	public function postSales($customerId)
	{
		$this->loadSales($customerId);
		return Response::json($this->_data['items']);
	}

	public function loadSales($customerId)
	{
		$this->loadCustomer($customerId);

		$from = Input::get('from',null);
		$to = Input::get('to', null);
		$type = Input::get('type', null);
		$itemId = Input::get('item', null);

		$items_table = Item::table();
		$t_table = Transaction::table();
		$td_table = TransactionDetail::table();
		$customer_table = Customer::table();

		$query = DB::table("$td_table as td_table")->select(array(
			'items_table.id as item_id','items_table.code','items_table.type','items_table.name',
			'td_table.*','t_table.invoice as invoice',
			'sender.name as sender_name', 'sender.id as sender_id', 'sender.type as sender_type',
			'receiver.name as receiver_name', 'receiver.id as receiver_id', 'receiver.type as receiver_type',
		))
		->join("$items_table as items_table", 'td_table.item_id', '=', 'items_table.id')
		->join("$customer_table as sender", 'td_table.sender_id', '=', 'sender.id')
		->join("$customer_table as receiver", 'td_table.receiver_id', '=', 'receiver.id')
		->join("$t_table as t_table", 'td_table.transaction_id', '=', 't_table.id');
		if($from && $to)
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where('td_table.date','>=',$from)->where('td_table.date','<=', $to);
		}
		if($type && is_array($type) && $type['id'] > 0) {
			$query = $query->where('td_table.transaction_type', '=', $type['id']);
		}
		if($itemId && $itemId > 0)
			$query = $query->where('items_table.id', '=', $itemId);
		$query = $query->where(function($query) use($customerId) {
			$query->where('td_table.sender_id', '=', $customerId)->orWhere('td_table.receiver_id', '=', $customerId);
		});
		$this->_data['items'] = $query->orderBy('td_table.date','DESC')->paginate(200);

		$response = array();
		$response['currentPage'] = $this->_data['items']->getCurrentPage();
		$response['lastPage'] = $this->_data['items']->getLastPage();
		$response['data'] = array();
		foreach ($this->_data['items'] as $key => $detail) {
			$response['data'][$key]['image'] = Item::getImagePath($detail->item_id);
			$response['data'][$key]['link'] = Item::getDetailUrl($detail->item_id,$detail->type);
			$response['data'][$key]['date'] = Dater::display($detail->date);
			$response['data'][$key]['transaction_link'] = URL::action('TransactionsController@getDetail',array($detail->transaction_id,$detail->date));
			$response['data'][$key]['name'] = $detail->name;
			$response['data'][$key]['invoice'] = $detail->invoice;
			$response['data'][$key]['code'] = $detail->code;
			$response['data'][$key]['type'] = Transaction::$types[$detail->transaction_type];
			$response['data'][$key]['price'] = $detail->price;
			$response['data'][$key]['discount'] = $detail->discount;
			$response['data'][$key]['total'] = $detail->total;
			$response['data'][$key]['quantity'] = $detail->quantity;
			$response['data'][$key]['sender'] = array(
				'link' => Customer::detailLink($detail->sender_id, $detail->sender_type),
				'name' => $detail->sender_name
				);
			$response['data'][$key]['receiver'] = array(
				'link' => Customer::detailLink($detail->receiver_id, $detail->receiver_type),
				'name' => $detail->receiver_name
				);
		}
		$this->_data['items'] = $response;
	}

	//gets the total item sold in a month
/*	public function getSales($customerId)
	{
		$this->loadCustomer($customerId);

		$month = Input::get('month',null);
		$year = Input::get('year', null);
		if($month && $year)
		{
			$from = Dater::toSQL(Dater::createFromFormat('m/Y',"$month/$year")->startOfMonth()->format(Dater::$format));
			$to = Dater::toSQL(Dater::createFromFormat('m/Y',"$month/$year")->endOfMonth()->format(Dater::$format));
		}
		else
		{
			$now = Dater::now();
			$from = Dater::toSQL($now->startOfMonth()->format(Dater::$format));
			$to = Dater::toSQL($now->endOfMonth()->format(Dater::$format));
			$month = $now->month;
			$year = $now->year;
		}

		$items_table = Item::table();
		$t_table = Transaction::table();
		$td_table = TransactionDetail::table();

		if(!$sort = Input::get('sort',false)) $sort = 'code';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		switch($sort)
		{
			case 'name': $sort = 'items_table.name'; break;
			case 'code': $sort = 'items_table.code'; break;
			case 'total_sell': $sort = 'total_sell'; break;
			case 'total_return': $sort = 'total_return'; break;
			default: break;
		}

		$query = DB::table("$td_table as td_table")->select(array(
			DB::raw('items_table.id as item_id'),
			'items_table.code','items_table.type','items_table.name',
			DB::raw('SUM(CASE WHEN td_table.transaction_type = '.Transaction::TYPE_SELL.' THEN td_table.quantity ELSE 0 END) as total_sell'),
			DB::raw('SUM(CASE WHEN td_table.transaction_type = '.Transaction::TYPE_RETURN.' THEN td_table.quantity ELSE 0 END) as total_return'),
		))
		->join("$items_table as items_table", 'td_table.item_id', '=', 'items_table.id')
		->where('td_table.date','>=',$from)->where('td_table.date','<=', $to)->where(function($query) use($customerId) {
			$query->where('sender_id', '=', $customerId)->orWhere('receiver_id', '=', $customerId);
		})
		->where(function($query) {
			$query->where('td_table.transaction_type','=',Transaction::TYPE_SELL)->orWhere('td_table.transaction_type','=',Transaction::TYPE_RETURN);
		});

		$this->_data['items'] = $query->groupBy('items_table.id')->orderBy($sort,$dir)->paginate(200);

		$response = array();
		$response['currentPage'] = $this->_data['items']->getCurrentPage();
		$response['lastPage'] = $this->_data['items']->getLastPage();
		$response['data'] = array();
		foreach ($this->_data['items'] as $key => $detail) {
			$response['data'][$key]['image'] = Item::getImagePath($detail->item_id);
			$response['data'][$key]['link'] = Item::getDetailUrl($detail->item_id,$detail->type);
			$response['data'][$key]['name'] = $detail->name;
			$response['data'][$key]['code'] = $detail->code;
			$response['data'][$key]['total_sell'] = $detail->total_sell;
			$response['data'][$key]['total_return'] = $detail->total_return;
			$response['data'][$key]['edit'] = Item::getEditButton($detail->item_id,$detail->type);
		}

		$this->_data['month'] = $month;
		$this->_data['year'] = $year;

		$this->_data['items'] = $response;
		$this->layout->action = $this->_data['customer']->name.' | Sales';
        $this->layout->content = View::make('customers.sales',$this->_data);
	}*/

	public function getHash($id)
	{
		$this->loadCustomer($id);

		$this->_data['types'] = Transaction::$types;

		//start generating stat
		$date = Input::get('date',null);
		if($date)
		{
			$date = Dater::createFromFormat('m/Y',$date);
			$month = $date->month;
			$year = $date->year;
		}
		else
		{
			$date = Dater::now();
			$month = $date->month;
			$year = $date->year;
		}

		$date = Dater::now();
		$current_month = $date->month;
		$current_year = $date->year;

		$ht = HT::table();
		$h = HashTag::table();
		$query = DB::table($ht)->select(array(
			DB::raw('SUM(total) as sum_total'),
			'transaction_type','hash_id',
			"h.name as hash_name"
		))
		->where('month','=',$current_month)->where('year','=',$current_year);

		$query = $query->where(function($query) use($id)
		{
			$query->where('receiver_id','=',$id)->orWhere('sender_id','=',$id);
		});
		if($filters = \App::make('access')->filter(Apps::TRANSACTION_FILTERS))
		{
			$query = $query->whereIn('transaction_type',$filters);
		}

		//filter
		if($type = Input::get('type',false))
			$query = $query->where('transaction_type','=',$type);
		if($hashtag = strtolower(Input::get('hashtag',false)))
		{
			if($hashtag = HashTag::where('name','=',$hashtag)->first())
				$query = $query->where('hash_id','=',$hashtag->id);
		}

		$query = $query->join("$h as h","h.id",'=',"$ht.hash_id");
		$this->_data['transactions'] = $query->groupBy('transaction_type','hash_id')->get();

		$this->_data['hash_url'] = \App::make('access')->can('stat', Apps::HASHTAG) ? 'HashController@getStat' : 'HashController@getTransactions';

		$this->layout->action = $this->_data['customer']->name.' | Hashtags';
		$this->layout->content = View::make('customers.hash',$this->_data);
	}

	protected function checkExist($c)
	{
		if(empty($c->id))
			$cust = Customer::where('name','=',$c->name)->where('birthdate','=',$c->birthdate)->first();
		else
			$cust = Customer::where('name','=',$c->name)->where('birthdate','=',$c->birthdate)->where('id','!=',$c->id)->first();
		if(!$cust) return false;
		return true;
	}

	public function postDelete($id)
	{
		$customer = Customer::where('id', '=', $id)->where('type', '=', $this->type)->first();
		if(!$customer)
			return App::abort(404);

		if($customer->type != Customer::TYPE_VWAREHOUSE && $customer->type != Customer::TYPE_VACCOUNT) {
			if($customer->stat->balance != 0)
				return Response::json(array(
					'success' => false,
					'title' => 'Error',
					'msg' => array('Cannot delete customer, balance NEED to be 0'),
					'deleted_at' => $customer->deleted_at
				));
		}

		$customer->delete();

		return Response::json(array(
			'success' => true,
			'title' => 'Attention',
			'msg' => array($customer->name.' deleted'),
			'deleted_at' => $customer->deleted_at
		));
	}

	public function postRestore($id)
	{
		$customer = Customer::where('id', '=', $id)->where('type', '=', $this->type)->withTrashed()->first();
		if(!$customer)
			return App::abort(404);
		$customer->restore();

		return Response::json(array(
			'success' => true,
			'title' => 'Attention',
			'msg' => $customer->name.' restored',
			'deleted_at' => $customer->deleted_at
		));
	}
/*
	public function getMemberi($id)
	{
		$customer = $this->loadCustomer($id);
		$type = strpos($customer->memberId, 'M300CR');
		if($type === false) $type = 'local.member_basecc_path';
		else $type = 'local.member_basecr_path';
		$base = Image::make(Config::get($type));
		return $base->text($customer->name, 610, 400, function($font) {
				$font->file(Config::get('local.member_font_path'));
		    $font->size(58);
		    $font->color('#FDEB03');
		})->response();
		//save(Config::get('local.member_image_path').$customer->id.'.jpg');
exit;
	}
*/

	public function postPortal($id)
	{
		try{
		DB::connection()->getPdo()->beginTransaction();

		$customer = Customer::where('id', '=', $id)->where('type', '=', $this->type)->first();
		if(!$customer)
			return App::abort(404);

		//check if customer exist in portal
		$user = \Portal\Models\User::where('customer_id','=',$customer->id)->first();
		if($user && $user->active == 1)
			throw new \Exception('sudah ada portal');

		if(!$user) {
			$user = new \Portal\Models\User;
			$user->customer_id = $customer->id;
			//create name (first 3 letter of name + last 3 number of id)
			$user->username = strtolower(substr(str_replace(' ', '', $customer->name), 0, 3)).substr($customer->id, -3);
		}
		//new pass
		$password = strtolower(substr(str_replace(' ', '', $customer->name), 0, 3)).substr($customer->id, 0, 3).mt_rand(10,99);
		$user->password = \Hash::make($password);
		$user->active = 1;
		if(!$user->save()) {
			throw new ModelException($user->getErrors(), __LINE__);
		}

		//save to customer data
		$customer->portalId = $user->id;
		$customer->password = $password;
		if(!$customer->save()) {
			throw new ModelException($user->getErrors(), __LINE__);
		}
		DB::connection()->getPdo()->commit();

		return Response::json(array(
			'success' => true,
			'title' => 'Attention',
			'active' => 1,
			'msg' => array('Username: '.$user->username.'  --  Password:'.$password),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function postAddwarehouse($id)
	{
		try{
		$customer = Customer::where('id', '=', $id)->where('type', '=', $this->type)->first();
		if(!$customer)
			throw new Exception('Salah customer');

		//check if customer exist in portal
		$user = \Portal\Models\User::where('customer_id','=',$customer->id)->first();
		if(!$user)
			throw new \Exception('blm ada portal');

		$warehouse = Input::get('warehouse', array());
		if(!isset($warehouse['id']))
			throw new \Exception('Tidak ada warehouse');

		$user->warehouse_id = $warehouse['id'];
		if(!$user->save()) {
			throw new ModelException($user->getErrors(), __LINE__);
		}

		return Response::json(array(
			'success' => true,
			'title' => 'Attention',
			'active' => 1,
			'msg' => array('Warehouse Assigned'),
		));

		} catch(ModelException $e) {
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			return Response::json($e->getMessage(), 500);
		}
	}

	public function postUnportal($id)
	{
		try{
		DB::connection()->getPdo()->beginTransaction();

		$customer = Customer::where('id', '=', $id)->where('type', '=', $this->type)->first();
		if(!$customer)
			return App::abort(404);

		//check if customer exist in portal
		$user = \Portal\Models\User::where('customer_id','=',$customer->id)->first();
		if(!$user)
			throw new Exception('belum punya portal');
		$user->active = 0;
		$user->warehouse_id = 0;
		if(!$user->save()) {
			throw new ModelException($user->getErrors(), __LINE__);
		}

		return Response::json(array(
			'success' => true,
			'title' => 'Attention',
			'active' => 0,
			'msg' => array('Portal deleted'),
		));
		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}
}