<?
use App\Models\Personnel,App\Models\PC,App\Models\Cuti;
use  App\Libraries\GajiManager;
class CutiController extends BaseController
{
	public function __construct()
	{
		BaseController::__construct();
		$this->_gm = new GajiManager;
	}

	public function getIndex()
	{
		$this->_data['cuti'] = Cuti::with('personnel')->orderBy('id','desc')->paginate(30);

		$this->layout->action = 'Cuti List';
		$this->layout->content = View::make('cuti.index',$this->_data);
	}

	public function getCreate()
	{
		$this->_data['types'] = Cuti::$types;

		$this->layout->action = 'New Pelanggaran';
		$this->layout->content = View::make('cuti.form',$this->_data);
	}

	public function postCreate()
	{
		$input = Input::get('cuti');
		if(!isset($input['personnel_id']) || !$personnel = Personnel::find($input['personnel_id']))
			return Redirect::back()->withInput()->with('error','Personnel not found');

		DB::connection()->getPdo()->beginTransaction();

		$c = new Cuti($input);
		//get current date
		$from = Dater::createFromFormat(Dater::$format,$c->date_from);
		$to = Dater::createFromFormat(Dater::$format,$c->date_to);

		//if not cuti tahunan, split by month
		if(!in_array($c->type,array(Cuti::TAHUNAN,Cuti::SAKIT)))
		{
			//check the month
			if($from->month == $to->month)
			{
				if(!$this->createCuti($from,$to,$c->type,false))
				{
					DB::connection()->getPdo()->rollback();
					return Redirect::back()->withInput()->withErrors($this->_messages)->with('error',$this->_messages->first());
				}
			}
			else //different month, year doesnt matter
			{
				//find the diff from FROM to end of month
				$end_month = $from->endOfMonth();
				if(!$this->createCuti($from,$end_month,$c->type,false))
				{
					DB::connection()->getPdo()->rollback();
					return Redirect::back()->withInput()->withErrors($this->_messages)->with('error',$this->_messages->first());
				}

				//find the diff from beginning of the year to TO
				$begin_month = $to->startOfMonth();
				if(!$this->createCuti($begin_month,$to,$c->type,false))
				{
					DB::connection()->getPdo()->rollback();
					return Redirect::back()->withInput()->withErrors($this->_messages)->with('error',$this->_messages->first());
				}
			}

			DB::connection()->getPdo()->commit();
			return Redirect::action('CutiController@getIndex')->with('success', 'cuti added');
		}

		//within the same year
		if($from->year == $to->year)
		{
			if(!$this->createCuti($from,$to,$c->type))
			{
				DB::connection()->getPdo()->rollback();
				return Redirect::back()->withInput()->withErrors($this->_messages)->with('error',$this->_messages->first());
			}
		}
		else
		{
			//find the diff from FROM to end of the year
			$end_year = Dater::create($from->year, 12, 31, 23, 59, 59);
			if(!$this->createCuti($from,$end_year,$c->type))
			{
				DB::connection()->getPdo()->rollback();
				return Redirect::back()->withInput()->withErrors($this->_messages)->with('error',$this->_messages->first());
			}

			//find the diff from beginning of the year to TO
			$begin_year = Dater::create($to->year, 1, 1, 0, 0, 0);
			if(!$this->createCuti($begin_year,$to,$c->type))
			{
				DB::connection()->getPdo()->rollback();
				return Redirect::back()->withInput()->withErrors($this->_messages)->with('error',$this->_messages->first());
			}
		}

		DB::connection()->getPdo()->commit();
		return Redirect::action('CutiController@getIndex')->with('success', 'cuti added');
	}

	public function getEdit($id)
	{
		$c = Cuti::with('personnel')->findOrFail($id);

		$this->_data['types'] = Cuti::$types;

		$c->date_from = Dater::display($c->date_from);
		$c->date_to = Dater::display($c->date_to);

		InputForm::load($p,'cuti');
		InputForm::set_array(array('nama' => $c->personnel->name));

		$this->layout->action = 'Edit Cuti';
		$this->layout->content = View::make('cuti.form',$this->_data);
	}

	public function postEdit($id)
	{
		$c = Cuti::findOrFail($id);
		$c->fill(Input::get('cuti'));

		$c->date_from = Dater::toSQL($c->date_from);
		$c->date_to = Dater::toSQL($c->date_to);

		if(!$c->save())
			return Redirect::back()->withInput()->withErrors($personnel->errors);

		return Redirect::action('CutiController@getIndex')->with('success', 'cuti edited');
	}

	protected function createPC($personnel_id, $year = null)
	{
		$pc = new PC;
		$pc->personnel_id = $personnel_id;
		if(!$year) $pc->year = Dater::now()->year;
		else $pc->year = $year;
		$pc->sisa_tahunan = Cuti::MAX_CUTI;
		$pc->sisa_sakit = Cuti::MAX_CUTI;
		if(!$pc->save())
			return false;

		return $pc;
	}

	protected function createCuti($from, $to, $type, $savePC = true)
	{
		$c = new Cuti(Input::get('cuti'));

		$diff = $from->diffInDays($to) + 1;
		if(!$pc = PC::where('personnel_id','=',$c->personnel_id)->where('year','=',$from->year)->first())
			$pc = $this->createPC($c->personnel_id,$from->year);

		switch($type)
		{
			case Cuti::TAHUNAN:
				if($pc->sisa_tahunan - $diff < 0)
					return $this->error('cuti tahunan sisa: '.$pc->sisa_tahunan.', mau cuti: '.$diff);
				$pc->sisa_tahunan = $pc->sisa_tahunan - $diff;
				break;
			case Cuti::SAKIT:
				if($pc->sisa_sakit - $diff < 0)
					return $this->error('cuti tahunan sisa: '.$pc->sisa_sakit.', mau cuti: '.$diff);
				$pc->sisa_sakit = $pc->sisa_sakit - $diff;
				break;
		}

		$c->sisa_tahunan = $pc->sisa_tahunan;
		$c->sisa_sakit = $pc->sisa_sakit;

		if($savePC)
			if(!$pc->save())
				return $this->error('error saving cuti');

		$c->date_from = Dater::toSQL($from->format(Dater::$format));
		$c->date_to = Dater::toSQL($to->format(Dater::$format));
		if(!$c->save())
			return $this->error($c->getErrors());

		if(!$this->_gm->addCuti($c,$diff))
			return $this->error($this->_gm->getErrors());

		return true;
	}

	public function postDelete($id)
	{
		$c = Cuti::findOrFail($id);
		DB::connection()->getPdo()->beginTransaction();

		$total_cuti = Dater::fromSQL($c->date_from)->diffInDays(Dater::fromSQL($c->date_to)) + 1;
		if(in_array($c->type,array(Cuti::TAHUNAN,Cuti::SAKIT)))
		{
			//find the sisa
			$year = Dater::fromSQL($c->date_from)->year;
			$pc = PC::where('personnel_id','=',$c->personnel_id)->where('year','=',$year)->first();

			switch($c->type)
			{
				case Cuti::TAHUNAN:
					$pc->sisa_tahunan = $total_cuti + $pc->sisa_tahunan;
					if($pc->sisa_tahunan > Cuti::MAX_CUTI) //impossible, but oh well, just in case
						$pc->sisa_tahunan = Cuti::MAX_CUTI;
					break;
				case Cuti::SAKIT:
					$pc->sisa_sakit = $total_cuti + $pc->sisa_sakit;
					if($pc->sisa_sakit > Cuti::MAX_CUTI) //impossible, but oh well, just in case
						$pc->sisa_sakit = Cuti::MAX_CUTI;
					break;
			}

			if(!$pc->save())
			{
				DB::connection()->getPdo()->rollback();
				return Redirect::back()->withInput()->with('error','error saving sisa cuti');
			}
		}

		if(!$this->_gm->deductCuti($c,$total_cuti))
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->with('error',$this->_gm->getErrors()->first());
		}

		if(!$c->delete())
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->with('error','error deleting cuti');
		}

		DB::connection()->getPdo()->commit();
		return Redirect::action('CutiController@getIndex')->with('success', 'cuti deleted');
	}
}