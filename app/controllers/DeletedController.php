<?
use App\Libraries\StatManager;
use App\Models\Transaction, App\Models\Item, App\Models\Customer, App\Models\TransactionDetail;
use App\Models\Deleted,App\Models\DeletedDetail;
use App\Libraries\TransactionsManager, App\Libraries\HashManager;
class DeletedController extends BaseController
{
		public function getIndex()
	{
		$this->loadIndex();
		$this->_data['hide'] = false;
		if($this->_access->hide('balance',Apps::HIDE))
			$this->_data['hide'] = true;
		$this->layout->action = 'Deleted Transactions List';
		$this->layout->content = View::make('transactions.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadIndex();
		return Response::json($this->_data['responseJSON']);
	}

	protected function loadIndex()
	{
		if(!$sort = Input::get('sort',false)) $sort = 'date';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$from = Input::get('from',false);
		$to = Input::get('to',false);

		//init the query
		$query = Deleted::with(array('receiver','sender'));
		//dates are set!
		if($from && $to)
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where('date','>=',$from)->where('date','<=',$to);
		}

		//find the types
		if($type = Input::get('type',false))
			$query = $query->where('type','=',$type);
		if($invoice = Input::get('invoice',false))
			$query = $query->where('invoice','=',$invoice);
		if($total = Input::get('total',false))
			$query = $query->where('total','=',$total);

		$transactions = $query->filter()->orderBy($sort,$dir)->orderBy('id',$dir)->paginate(50);

		//location filter - may not be necessary
		$lids = false;
		if($this->_lm->bound())
			$lids = $this->_lm->get_location();

		$this->_data['responseJSON'] = TransactionsManager::toArray($transactions, $lids);
		$this->_data['transactions'] = Response::json($this->_data['responseJSON'])->getData();
		$this->_data['typesJSON'] = json_encode(Transaction::$typesJSON);
		$this->_data['hide_balance'] = $this->_access->hide('balance',Apps::HIDE);
	}

	public function getDetail($id)
	{
		$this->_data['transaction'] = Deleted::findOrFail($id);

		$this->_data['deleted'] = true;
		$this->_data['image'] = false;
		if(!empty($this->_data['transaction']->detail_ids))
		{
			$itemsTable = Item::table();
			$detailTable = DeletedDetail::table();
			$details = DeletedDetail::with('item')->whereIn("$detailTable.id",array_filter(explode(',',$this->_data['transaction']->detail_ids)))->get();
			$this->_data['details'] = array();
			$name = $code = array();
			foreach ($details as $key => $detail) {
				$this->_data['details'][$key]['image'] = $detail->item->getImageUrl();
				$this->_data['details'][$key]['link'] = $detail->item->getUrl();
				$name[$key] = $this->_data['details'][$key]['name'] = $detail->item->name;
				$code[$key] = $this->_data['details'][$key]['code'] = $detail->item->code;
				$this->_data['details'][$key]['quantity'] = $detail->quantity;
				$this->_data['details'][$key]['price'] = $detail->price;
				$this->_data['details'][$key]['discount'] = $detail->discount;
				$this->_data['details'][$key]['total'] = $detail->total;
			}
		}

		$this->layout->action = 'DELETED Transaction #'.$this->_data['transaction']->id;
		$this->layout->content = View::make('transactions.detail',$this->_data);
	}
}
?>