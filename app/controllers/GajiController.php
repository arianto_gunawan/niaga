<?
use App\Models\Personnel, App\Models\Gaji, App\Models\AppSetting, App\Models\Cuti;
use App\Libraries\GajiManager;
use App\Models\Account;
class GajiController extends BaseController
{
	public function __construct()
	{
		BaseController::__construct();
		$this->_gm = new GajiManager;
	}

	public function getIndex()
	{
		$this->_data['private'] = 0;
		$this->_data['formAction'] = 'getIndex';
		$this->displayList(0);
	}

	public function getEdit($id)
	{
		$this->_data['gaji'] = Gaji::findOrFail($id);
		InputForm::load($this->_data['gaji'],'gaji');

		$personnel = Personnel::findOrFail($this->_data['gaji']->personnel_id);

		//check for private
		if($personnel->private == 1)
			if(!$this->_access->can('private-gpu',Apps::PERSONNELS))
				App::abort(404);

		$this->layout->action = 'Editing '.$this->_data['gaji']->personnel->name.' | GPU: '.number_format($this->_data['gaji']->gpu);
        $this->layout->content = View::make('gaji.form',$this->_data);
	}

	public function postEdit($id)
	{
		$gaji = Gaji::findOrFail($id);
		$personnel = Personnel::findOrFail($gaji->personnel_id);

		//check for private
		if($personnel->private == 1)
			if(!$this->_access->can('private-gpu',Apps::PERSONNELS))
				App::abort(404);

		$input = Input::get('gaji');
		$gaji->fill($input);
		if(!$gaji->save())
			return Redirect::back()->withInput()->with('error','error editing gaji')->withErrors($gaji->getErrors());

		if($personnel->private == 1)
			return Redirect::action('GajiController@getPrivate')->with('success','gaji edited');

		return Redirect::action('GajiController@getIndex')->with('success','gaji edited');
	}

	public function getPrivate()
	{
		$this->_data['private'] = 1;
		$this->_data['formAction'] = 'getPrivate';
		$this->displayList(1);
	}

	protected function displayList($private = 0)
	{
		$date = Input::get('date',null);

		if($date)
		{
			$date = Dater::createFromFormat('j/m/Y',"1/".$date);
			$month = $date->month;
			$year = $date->year;
		}
		else
		{
			$date = Dater::now();
			$month = $date->month;
			$year = $date->year;
		}

		$date = Dater::now();
		$current_month = $date->month;
		$current_year = $date->year;

		//init
		$query = new Personnel;
		if($name = Input::get('name',false))
			$query = $query->where('name','LIKE',"%$name%");

		$this->_data['personnels'] = $query->where('private','=',$private)->paginate(50);

		//1. collect the personnel ids
		$personnelIds = array();
		foreach ($this->_data['personnels'] as $p) {
			$personnelIds[] = $p->id;
		}

		//1.5 if nothing to load
		if(empty($personnelIds))
		{
			$this->_data['personnels'] = null;
			$this->_data['gaji'] = null;
			$this->_data['accounts'] = array();
			return $this->layout->content = View::make('gaji.index',$this->_data);
		}

		//2. get the gaji list for those personnels
		$personnelGaji = Gaji::where('month','=',$month)->where('year','=',$year)->whereIn('personnel_id', $personnelIds)->lists('personnel_id');

		//3. find those not generated
		$diff = array_diff($personnelIds, $personnelGaji);

		//4. generate new gaji if not need to
		if(!empty($diff))
		{
			$new = Personnel::whereIn('id',$diff)->get();
			foreach($new as $p)
			{
				$gaji = $this->_gm->initGaji($p,$date);
				if(!$gaji->save())
					return Redirect::back()->withInput()->with('error','error generating gaji for '.$p->name);
			}
		}

		//5. reload the gaji
		$this->_data['gaji'] = Gaji::with(array('personnel'))->where('month','=',$month)->where('year','=',$year)->whereIn('personnel_id', $personnelIds)->get();
		$this->_data['accounts'] = array();
		foreach($this->_data['gaji'] as $g)
		{
			$acct = $g->personnel->account_id;
			if(!isset($this->_data['accounts'][$acct]))
				$this->_data['accounts'][$acct] = array('total' => 0, 'name' => $g->personnel->account_id?$g->personnel->account->name:'No Account');
			$this->_data['accounts'][$acct]['total'] = $this->_data['accounts'][$acct]['total'] + GajiManager::getTotal($g);
		}

		$this->layout->content = View::make('gaji.index',$this->_data);
	}

	public function postReload($id, $private)
	{
		$gaji = Gaji::findOrFail($id);
		$personnel = Personnel::findOrFail($gaji->personnel_id);

		//save manual input
		$oldTerlambat = $gaji->terlambat;
		$oldBonus = $gaji->bonus;

		$gaji->delete();

		switch ($private) {
			case 1:
				$action = 'getPrivate';
				break;

			default:
				$action = 'getIndex';
				break;
		}

		$date = Dater::now();

		$gaji = $this->_gm->initGaji($personnel, $date);
		$gaji = $this->_gm->recalculate($gaji, $date);

		//resave manual input
		$gaji->terlambat = $oldTerlambat;
		$gaji->bonus = $oldBonus;

		if(!$gaji->save())
			return Redirect::back()->withInput()->with('error','error saving gaji for '.$p->name);

		return Redirect::action('GajiController@'.$action)->with('success','gaji edited');
	}
}