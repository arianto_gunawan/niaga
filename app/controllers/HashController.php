<?
use App\Models\HashTag,App\Models\HT,App\Models\Customer,App\Models\Transaction;
use App\Libraries\HashManager, App\Libraries\TransactionsManager;
class HashController extends BaseController
{
	public function __construct()
	{
		BaseController::__construct();
		$this->_hm = new HashManager;
	}

	public function getIndex()
	{
		$this->loadHash();
		$this->layout->action = 'Hashtags List';
		$this->layout->content = View::make('hash.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadHash();
		return Response::json($this->_data['hash']);
	}

	protected function loadHash()
	{
		$query = new HashTag;

		if($name = Input::get('name',false))
			$query = $query->where('name','LIKE',"%$name%");

	  	$data = $query->paginate(200);
	  	$response = array();
		$response['currentPage'] = $data->getCurrentPage();
		$response['lastPage'] = $data->getLastPage();
		$response['data'] = array();
		foreach ($data as $key => $detail) {
			$response['data'][$key]['detail_link'] = URL::action('HashController@getTransactions',array($detail->id));
			$response['data'][$key]['name'] = $detail->name;
		}
		$this->_data['hash'] = $response;
	}

	public function getStat($id)
	{
		$this->_data['types'] = Transaction::$types;
		$this->_data['hash'] = HashTag::findOrFail($id);

		//start generating stat
		$date = Input::get('date',null);
		if($date)
		{
			$date = Dater::createFromFormat('m/Y',$date);
			$month = $date->month;
			$year = $date->year;
		}
		else
		{
			$date = Dater::now();
			$month = $date->month;
			$year = $date->year;
		}

		$date = Dater::now();
		$current_month = $date->month;
		$current_year = $date->year;

		$ht = HT::table();
		$c = Customer::table();
		$query = DB::table($ht)->select(array(
			DB::raw('SUM(total) as sum_total'),
			'transaction_type','sender_id','receiver_id', 'sender_type', 'receiver_type',
			"c1.name as sender_name", "c2.name as receiver_name",
		))
		->where('hash_id','=',$id)->where('month','=',$current_month)->where('year','=',$current_year);

		//check for location
		if($this->_lm->bound())
		{
			$lids = $this->_lm->get_location();
			$query = $query->where(function($query) use($lids)
			{
				$query->whereIn('receiver_id',$lids)->orWhereIn('sender_id',$lids);
			});
		}
		if($filters = \App::make('access')->filter(Apps::TRANSACTION_FILTERS))
		{
			$query = $query->whereIn('transaction_type',$filters);
		}

		//filter
		if($type = Input::get('type',false))
			$query = $query->where('transaction_type','=',$type);
		if($sender_id = Input::get('sender_id',false))
			$query = $query->where('sender_id','=',$sender_id);
		if($receiver_id = Input::get('receiver_id',false))
			$query = $query->where('receiver_id','=',$receiver_id);

		$query = $query->join("$c as c1","c1.id",'=',"$ht.sender_id");
		$query = $query->join("$c as c2","c2.id",'=',"$ht.receiver_id");
		$this->_data['transactions'] = $query->groupBy('transaction_type','sender_id','receiver_id')->get();

		$this->layout->action = '#'.$this->_data['hash']->name;
		if($this->_access->can('transactions',Apps::HASHTAG))
			$this->layout->action .= ' - <a href="'.URL::action('HashController@getTransactions',array($id)).'" class="btn btn-info btn-sm">Transactions</a>';

		$this->layout->content = View::make('hash.stat',$this->_data);
	}

	public function getTransactions($id)
	{
		$this->loadTransactions($id);
		$this->_data['typesJSON'] = json_encode(Transaction::$typesJSON);
		$this->layout->action = '#'.$this->_data['hash']->name;
		$this->layout->content = View::make('hash.transactions',$this->_data);
	}

	public function postTransactions($id)
	{
		$this->loadTransactions($id);
		return Response::json($this->_data['transactions']);
	}

	protected function loadTransactions($id)
	{
		$this->_data['hash'] = HashTag::findOrFail($id);

		//init the query
		$query = HT::with('transaction','sender','receiver')->where('hash_id','=',$this->_data['hash']->id);
		$table = HT::table();

		//check for location
		$lids = array();
		if($this->_lm->bound())
		{
			$lids = $this->_lm->get_location();
			$query = $query->where(function($query) use($lids)
			{
				$query->whereIn('receiver_id',$lids)->orWhereIn('sender_id',$lids);
			});
		}
		if($filters = \App::make('access')->filter(Apps::TRANSACTION_FILTERS))
		{
			$query = $query->whereIn('transaction_type',$filters);
		}

		//sort
		if(!$sort = Input::get('sort',false)) $sort = 'date';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		//check the dates
		$from = Input::get('from',false);
		$to = Input::get('to',false);
		if($from && $to) //dates are set!
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where($table.'.date','>=',$from)->where($table.'.date','<=',$to);
		}
		//find the types
		if($type = Input::get('type',false))
			$query = $query->where('transaction_type','=',$type);

		$transactions = $query->orderBy($table.'.'.$sort,$dir)->orderBy($table.'.id',$dir)->paginate(50);

		//basically a copy of transactionmanager::toarray, except it uses $t->transaction->
		$response = array();
		$response['currentPage'] = $transactions->getCurrentPage();
		$response['lastPage'] = $transactions->getLastPage();
		$response['data'] = array();
		foreach ($transactions as $index => $t) {
			$response['data'][$index]['id'] = $t->transaction->id;
			$response['data'][$index]['date'] = Dater::display($t->transaction->date);
			$response['data'][$index]['sender_balance'] = $t->transaction->sender_balance;
			$response['data'][$index]['receiver_balance'] = $t->transaction->receiver_balance;
			$response['data'][$index]['description'] = $t->transaction->description;
			$response['data'][$index]['invoice'] = $t->transaction->invoice;
			$response['data'][$index]['total'] = $t->transaction->total;
			$response['data'][$index]['total_items'] = $t->transaction->total_items;
			$response['data'][$index]['type'] = $t->transaction->printType();
			$response['data'][$index]['detail_link'] = $t->transaction->getDetailLink();
			$response['data'][$index]['sender_url'] = $response['data'][$index]['sender_name'] = $response['data'][$index]['receiver_url'] = $response['data'][$index]['receiver_name'] = '';
			if($t->transaction->sender) {
				$response['data'][$index]['sender_url'] = $t->transaction->sender->getDetailLink();
				$response['data'][$index]['sender_name'] = $t->transaction->sender->name;
			}
			if($t->transaction->receiver) {
				$response['data'][$index]['receiver_url'] = $t->transaction->receiver->getDetailLink();
				$response['data'][$index]['receiver_name'] = $t->transaction->receiver->name;
			}
			if(is_array($lids) && !in_array($t->transaction->sender_id, $lids))
				$response['data'][$index]['sender_balance'] = 0;
			if(is_array($lids) && !in_array($t->transaction->receiver_id, $lids))
				$response['data'][$index]['receiver_balance'] = 0;
		}
		$this->_data['transactions'] = $response;
	}
}