<?
use App\Libraries\ItemsManager;
use App\Models\Item, App\Models\ItemGroup;
use App\Models\Tag, App\Models\WarehouseItem, App\Models\Customer;
use App\Models\Transaction, App\Models\TransactionDetail;
class ItemsController extends BaseController
{
	protected $action = 'ItemsController';
	protected $type = Item::TYPE_ITEM;

	public function __construct()
	{
		BaseController::__construct();
		$this->_libs['ItemsManager'] = new ItemsManager;
		$this->_data['type'] = $this->type;
		$this->_data['tag_types'] = Tag::$types;
	}

	public function getIndex()
	{
		$this->loadIndex();
		$this->_data['action'] = $this->action;
		$this->layout->action = 'Item List';
	    $this->layout->content = View::make('items.index',$this->_data);

		if($this->type == Item::TYPE_ASSET_TETAP)
			$this->layout->content = View::make('items.asset_tetap',$this->_data);
	}

	public function postIndex()
	{
		$this->loadIndex();
		return Response::json($this->_data['items']);
	}

	public function loadIndex()
	{
		if(!$sort = Input::get('sort',false)) $sort = 'id';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$code = Input::get('code',false);
		$name = Input::get('name',false);
		$desc = Input::get('desc',false);
		$alias = Input::get('alias',false);

		//init the query
		if($this->type == Item::TYPE_ASSET_TETAP)
			$query = Item::with('depreciation');
		else
			$query = new Item;

		$query = $query->where('type','=',$this->type);

		//dates are set!
		if(!empty($code)) {
			if(is_numeric($code))
				$query = $query->where('id','=', $code);
			else
				$query = $query->where('code','LIKE',"%$code%");
		}
		if(!empty($name)) {
			$name = str_replace(' ', '%', $name);
			$query = $query->where('name','LIKE',"%$name%");
		}
		if(!empty($desc)) {
			$desc = str_replace(' ', '%', $desc);
			$query = $query->whereHas('group', function($q) use($desc) {
				$q->where('description','LIKE',"%$desc%");
			});
		}
		if(!empty($alias)) {
			$alias = str_replace(' ', '%', $alias);
			$query = $query->whereHas('group', function($q) use($alias) {
				$q->where('alias','LIKE',"%$alias%");
			});
		}

  	$this->_data['items'] = $query->orderBy($sort,$dir)->paginate(10);
  	$this->_data['items'] = ItemsManager::toArray($this->_data['items']);
	}

	public function getDetail($id)
	{
		$item = $this->loadItem($id);
		$this->loadDetail($id);
		$this->layout->action = $this->_data['item']->code;
		$this->layout->content = View::make('items.detail',$this->_data);
	}

	public function postDetail($id)
	{
		$item = $this->loadItem($id);
		$this->loadDetail($id);
		$quantity = array();
		foreach ($this->_data['wi'] as $key => $value) {
			$quantity[$key]['quantity'] = $value->quantity;
			$quantity[$key]['warehouse_name'] = $value->warehouse->name;
			$quantity[$key]['warehouse_link'] = $value->warehouse->getDetailLink();
		}
		return Response::json(array(
			'item' => $item,
			'quantity' => $quantity,
		));
	}

	protected function loadDetail($id)
	{
		$this->_data['tags'] = $this->_data['item']->tags()->get();
		$this->_data['wi'] = WarehouseItem::with('warehouse')->where('item_id','=',$id)->leftJoin('customers','customers.id','=','warehouse_item.warehouse_id')->where('customers.type','=',Customer::TYPE_WAREHOUSE)->where(function($query) {
			$query->where('customers.deleted_at','=',null)->orWhere('customers.deleted_at','=','0000-00-00 00:00:00');
		})->get();
	}

	//ajax, usually for code search
	public function postSearch()
	{
		return $this->search();
	}

	//ajax, usually for name search
	public function getSearch()
	{
		return $this->search();
	}

	protected function search()
	{
		$term = $like = trim(rawurldecode(Input::get('q')));
		if(!Input::get('jump'))
			$like = str_replace(' ', '%', $term);
		$response = array();
		switch(Input::get('type'))
		{
			case 'code':
				if(is_numeric($term))
					$item = Item::with('quantities')->where('id','=',$term)->first();
				else
					$item = Item::with('quantities')->where('code','=',$term)->first();
				if(!$item) return Response::json($response);
				$quantities = $item->quantities->lists('quantity', 'warehouse_id');

				return Response::json(array('result' => array('type' => $item->type, 'code'=>$item->code,'name'=>$item->name,'id'=>$item->id,'cost'=>$item->cost,'price'=>$item->price,'quantities' => $quantities)));
				break;
			case 'b':
				$items = Item::with('quantities')->whereIn('type',array(Item::TYPE_ITEM, Item::TYPE_ASSET_LANCAR))->where('name','LIKE',"%$like%")->orderBy('name','asc')->take(10)->get();
				break;
			default:
				$items = Item::with('quantities')->where('name','LIKE',"%$like%")->orderBy('name','asc')->take(10)->get();
				break;
		}
		$i = 0;
		foreach($items as $item)
		{
			$response[$i] = array();
			$response[$i]['code'] = $item->code;
			$response[$i]['name'] = $item->name;
			$response[$i]['id'] = $item->id;
			$response[$i]['price'] = $item->price;
			$response[$i]['type'] = $item->type;
			$response[$i]['cost'] = $item->cost;
			$response[$i]['quantities'] = $item->quantities->lists('quantity', 'warehouse_id');
			//for old bootstrap
			$response[$i]['value'] = $item->name;
			$response[$i]['tokens'] = explode(' ',$item->name);
			$i++;
		}
		return Response::json($response);
	}

	public function getTransactions($id)
	{
		$this->loadTransactions($id);
		$this->_data['typesJSON'] = json_encode(Transaction::$typesJSON);
		$this->layout->action = $this->_data['item']->name;
		$this->layout->content = View::make('items.transactions',$this->_data);
	}

	public function postTransactions($id)
	{
		$this->loadTransactions($id);
		return Response::json($this->_data['transactions']);
	}

	public function loadTransactions($id)
	{
		$this->loadItem($id);

		$t_table = Transaction::table();
		$td_table = TransactionDetail::table();

		//init sort
		if(!$dir = Input::get('dir',false)) $dir = 'desc';
		switch(Input::get('sort',false))
		{
			case 'id': $sort = $t_table.'.id'; break;
			default: $sort = $t_table.'.date'; break;
		}

		//init date
		$from = Input::get('from',false);
		$to = Input::get('to',false);

		//init the query
		$query = TransactionDetail::with(array('transaction','transaction.receiver','transaction.sender'));
		$query = $query->where('item_id','=',$this->_data['item']->id)->join($t_table,$t_table.'.id','=',$td_table.'.transaction_id');

		//check for location
		$lids = false;
		if($this->_lm->bound())
		{
			$lids = $this->_lm->get_location();
			$query = $query->where(function($query) use($lids,$t_table)
			{
				$query->whereIn($t_table.'.receiver_id',$lids)->orWhereIn($t_table.'.sender_id',$lids);
			});
		}

		if($customer_id = Input::get('customer_id',false))
			$query = $query->where(function($query) use($customer_id,$t_table)
			{
				$query->where($t_table.'.receiver_id','=',$customer_id)->orWhere($t_table.'.sender_id','=',$customer_id);
			});
		//find the types
		if($type = Input::get('type',false))
			$query = $query->where($t_table.'.type','=',$type);

		$transactions = $query->orderBy($sort,$dir)->paginate(20);

		$response = array();
		$response['currentPage'] = $transactions->getCurrentPage();
		$response['lastPage'] = $transactions->getLastPage();
		$response['data'] = array();
		foreach ($transactions as $index => $t) {
			$response['data'][$index]['id'] = $t->id;
			$response['data'][$index]['date'] = Dater::display($t->transaction->date);
			$response['data'][$index]['invoice'] = $t->transaction->invoice;
			$response['data'][$index]['type'] = $t->transaction->printType();
			$response['data'][$index]['detail_link'] = $t->transaction->getDetailLink();
			$response['data'][$index]['quantity'] = $t->quantity;
			$response['data'][$index]['description'] = $t->transaction->description;
			$response['data'][$index]['price'] = $t->price;
			$response['data'][$index]['sender_url'] = $response['data'][$index]['sender_name'] = $response['data'][$index]['receiver_url'] = $response['data'][$index]['receiver_name'] = '';
			if($t->transaction->sender) {
				$response['data'][$index]['sender_url'] = $t->transaction->sender->getDetailLink();
				$response['data'][$index]['sender_name'] = $t->transaction->sender->name;
			}
			if($t->transaction->receiver) {
				$response['data'][$index]['receiver_url'] = $t->transaction->receiver->getDetailLink();
				$response['data'][$index]['receiver_name'] = $t->transaction->receiver->name;
			}
			if(is_array($lids) && !in_array($t->sender_id, $lids))
				$response['data'][$index]['sender_balance'] = 0;
			if(is_array($lids) && !in_array($t->receiver_id, $lids))
				$response['data'][$index]['receiver_balance'] = 0;
		}
		$this->_data['transactions'] = $response;
	}

	protected function loadItem($id)
	{
		$this->_data['type'] = $this->type;
		$this->layout->action = $this->action;
		if(!$this->_data['item'] = Item::with('group')->where('id','=',$id)->where('type','=',$this->type)->first())
			App::abort(404, 'Page not found');

		//for forms
		if($this->_data['item']->type == Item::TYPE_ITEM) {
			$this->_data['item']->alias = $this->_data['item']->group->alias;
			$this->_data['item']->description = $this->_data['item']->group->description;
		}
		return $this->_data['item'];
	}

	public function getPrice()
	{
		$this->layout->action = 'Adjust Price';
		$this->_data['tags'] = ItemsManager::loadTags();
		$this->layout->content = View::make('items.price',$this->_data);
	}

	public function postPrice()
	{
		$price = Input::get('price',0);
		$tags = Input::get('tag',array());

		DB::connection()->getPdo()->beginTransaction();

		$total_items = $this->_libs['ItemsManager']->updatePrice($price,$tags);
		if($total_items <= 0)
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->with('error', 'ngga ada item buat di update')->withErrors($im->getErrors())->withInput();
		}

		DB::connection()->getPdo()->commit();

		return Redirect::action($this->action.'@getIndex' )->with( 'success', $total_items.' item(s) updated.');
	}

	public function getStat($id)
	{
		$this->loadItem($id);
		$td = TransactionDetail::table();

		//1 year ago
		$from = Dater::now()->subMonths(11)->startOfMonth();
		$to = Dater::now()->endOfMonth();

		$data = DB::table($td)->select(array(
			"$td.transaction_type",
			DB::raw('SUM(quantity) as total_quantity'),
			DB::raw("DATE_FORMAT($td.date,'%M %Y') AS showdate"),
		))->where('item_id','=',$id)->whereIn('transaction_type',array(Transaction::TYPE_SELL, Transaction::TYPE_MOVE, Transaction::TYPE_RETURN, Transaction::TYPE_PRODUCTION))->where('date','>=',$from)->where('date','<=',$to)->groupBy("$td.transaction_type")->groupBy('showdate')->orderBy('showdate', 'DESC')->get();
		//format data
		$result = array();
		foreach ($data as $key => $value) {
			$date = $value->showdate;
			$type = $value->transaction_type;
			if(!isset($result[$date])) {
				$result[$date] = array();
				$result[$date]['date'] = $date;
				$result[$date]['data'] = array();
			}

			$result[$date]['data'][$type] = array('type_id' => $type, 'type_name' => Transaction::$types[$type],'quantity' => $value->total_quantity);
		}
		$this->_data['items'] = $result;
		$this->layout->action = 'Stats';
		$this->layout->content = View::make('items.stat',$this->_data);
	}

	public function getGroupStat($id)
	{
		$this->loadGroupDetail($id);
		$td = TransactionDetail::table();
		$it = Item::table();

		//1 year ago
		$from = Dater::now()->subMonths(11)->startOfMonth();
		$to = Dater::now()->endOfMonth();

		$data = DB::table($td)->select(array(
			"$td.transaction_type",
			DB::raw('SUM(quantity) as total_quantity'),
			DB::raw("DATE_FORMAT($td.date,'%M %Y') AS showdate"),
		))->leftJoin($it,"$it.id",'=',"$td.item_id")->where("$it.group_id",'=',$id)->whereIn('transaction_type',array(Transaction::TYPE_SELL, Transaction::TYPE_MOVE, Transaction::TYPE_RETURN, Transaction::TYPE_PRODUCTION))->where('date','>=',$from)->where('date','<=',$to)->groupBy("$td.transaction_type")->groupBy('showdate')->orderBy('showdate', 'DESC')->get();
		//format data
		$result = array();
		foreach ($data as $key => $value) {
			$date = $value->showdate;
			$type = $value->transaction_type;
			if(!isset($result[$date])) {
				$result[$date] = array();
				$result[$date]['date'] = $date;
				$result[$date]['data'] = array();
			}

			$result[$date]['data'][$type] = array('type_id' => $type, 'type_name' => Transaction::$types[$type],'quantity' => $value->total_quantity);
		}
		$this->_data['items'] = $result;
		$this->layout->action = $this->_data['group']->name.' Stats';
		$this->layout->content = View::make('items.groupstat',$this->_data);
	}

	public function getGroup()
	{
		$this->loadGroup();
		$this->layout->action = 'Group List';
    $this->layout->content = View::make('items.group',$this->_data);
	}

	public function postGroup()
	{
		$this->loadGroup();
		return Response::json($this->_data['items']);
	}

	public function loadGroup()
	{
		if(!$sort = Input::get('sort',false)) $sort = 'id';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$name = Input::get('name',false);
		$desc = Input::get('desc',false);
		$alias = Input::get('alias',false);

		$query = ItemGroup::with('items');

		if(!empty($name)) {
			$name = str_replace(' ', '%', $name);
			$query = $query->where('name','LIKE',"%$name%");
		}
		if(!empty($desc)) {
			$desc = str_replace(' ', '%', $desc);
			$query = $query->where('description','LIKE',"%$desc%");
		}
		if(!empty($alias)) {
			$alias = str_replace(' ', '%', $alias);
			$query = $query->where('alias','LIKE',"%$alias%");
		}

	  	$group = $query->orderBy($sort,$dir)->paginate(10);
	  	$wiTable = WarehouseItem::table();
		$response = array();
		$response['currentPage'] = $group->getCurrentPage();
		$response['lastPage'] = $group->getLastPage();
		$response['data'] = array();
		foreach ($group as $key => $detail) {
			$response['data'][$key]['image'] = $detail->getImageUrl();
			$response['data'][$key]['link'] = $detail->getDetailLink();
			$response['data'][$key]['name'] = $detail->name;
			$response['data'][$key]['master'] = $detail->master;
			$response['data'][$key]['variant'] = $detail->variant;
			$response['data'][$key]['alias'] = $detail->alias;
			$response['data'][$key]['description'] = $detail->description;
			$response['data'][$key]['total_quantity'] = 0;
			$response['data'][$key]['total_quantity'] = ItemsManager::findGroupInWarehouse($detail->id);
		}
		$this->_data['items'] = $response;
	}

	public function getGroupDetail($id)
	{
		$this->loadGroupDetail($id);
		$this->layout->action = $this->_data['group']->name;
		$this->layout->content = View::make('items.groupdetail',$this->_data);
	}

	public function postGroupDetail($id)
	{
		$this->loadGroupDetail($id);
		//format warehouse
		$warehouse = array();
		foreach ($this->_data['warehouse'] as $key => $value) {
			$data = $value->all();
			foreach ($data as $index => $v) {
				$warehouse[$key]['warehouse_name'] = $v->warehouse->name;
				$warehouse[$key]['warehouse_link'] = $v->warehouse->getDetailLink();
				$warehouse[$key]['quantity'] = $v->quantity;
			}
		}
		//format item
		$items = array();
		foreach ($this->_data['items'] as $key => $value) {
			$items[$key] = $value->toArray();
			$items[$key] = $value->getDetailLink();
		}

		return Response::json(array(
			'group' => $this->_data['group'],
			'items' => $this->_data['items'],
			'warehouse' => $warehouse,
		));
	}

	protected function loadGroupDetail($id)
	{
		$group = ItemGroup::findOrFail($id);
		$items = Item::where('group_id','=',$group->id)->orderBy('variant','asc')->get();
		if(!$items) $items = array();
		$warehouse = array();
		foreach ($items as $i) {
			$warehouse[$i->id] = WarehouseItem::with('warehouse')->where('item_id','=',$i->id)->leftJoin('customers','customers.id','=','warehouse_item.warehouse_id')->where('customers.type','=',Customer::TYPE_WAREHOUSE)->where(function($query) {
				$query->where('customers.deleted_at','=',null)->orWhere('customers.deleted_at','=','0000-00-00 00:00:00');
			})->get();
			if(!$warehouse[$i->id]) $warehouse[$i->id] = array();
		}
		$this->_data['group'] = $group;
		$this->_data['items'] = $items;
		$this->_data['warehouse'] = $warehouse;
	}

/*
	//function to create groups
	public function getGrouper()
	{
		Item::where('type', '=', 1)->chunk(200, function($data) {
			foreach ($data as $d) {
				$group = ItemGroup::where('name', '=', $d->pcode)->first();
				if($group) continue;
				//create new group
				pre($d->pcode);
				$group = new ItemGroup;
				$group->name = $d->pcode;
				$code = explode('/', $d->pcode);
				if(isset($code[0]))
					$group->master = $code[0];
				if(isset($code[1]))
					$group->variant = $code[1];
				$group->save();
			}
		});
		exit;
	}

	public function getItemUpdater()
	{
		Item::where('type','=', 1)->orderBy('id','asc')->chunk(200, function($data) {
			foreach ($data as $d) {
				$group = ItemGroup::where('name', '=', $d->pcode)->first();
				//now update the item
				$d->group_id = $group->id;
				$d->variant = $group->variant;
				$d->save();
			}
		});
		exit;
	}
	public function getImageMover()
	{
		Item::where('type','=', 1)->where('group_id','>',0)->orderBy('id','asc')->chunk(200, function($data) {
			foreach ($data as $d) {
				$old =  '/home/crystalsports/www/aria'.Item::getImagePath($d->id);
				$new = '/home/crystalsports/www/aria'.ItemGroup::getImagePath($d->group_id);
				//no file to move
				if(!file_exists($old)) continue;
				//new file already exist
				if(file_exists($new)) continue;
				rename($old, $new);
			}
		});
		exit;
	}
*/

	public function getCreate()
	{
		$this->layout->action = 'Create Item';
		$this->_data['item'] = new Item;
		$this->_data['update'] = false;
		$this->_data['selected'] = array();
		$this->_data['tags'] = ItemsManager::loadTagsJSON(Tag::$types);
		$this->layout->content = \View::make('items.form',$this->_data);
	}

	public function postCreate()
	{
		try {
		$input = json_decode(Input::get('item'), true);
		$tags = json_decode(Input::get('selected',array()), true);

		DB::connection()->getPdo()->beginTransaction();

		if(!$this->_libs['ItemsManager']->createItems($input, $tags, Input::file('file')))
			throw new ModelException($this->_libs['ItemsManager']->getErrors(), __LINE__);

		DB::connection()->getPdo()->commit();

		Session::flash('success', 'Item(s) created.');
		return Response::json(array(
			'url' => URL::action($this->action.'@getIndex'),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getEdit($id)
	{
		$this->_data['tags'] = ItemsManager::loadTagsJSON(Tag::$types);
		foreach($this->_data['tags'] as $key => $value) {
			if($value['type_id'] == Tag::TYPE_SIZE || $value['type_id'] == Tag::TYPE_TYPE)
				$this->_data['tags'][$key] = array();
		}

		$item = $this->loadItem($id);

		//load the item tags
		$this->_data['selected'] = array();
		foreach($item->tags as $tag)
		{
			$this->_data['selected'][] = $tag->id;
		}

		$this->layout->action = 'Edit '.$item->name;
		$this->_data['update'] = true;

		$this->layout->content = \View::make('items.form',$this->_data);
	}

	public function postEdit($id)
	{
		try{

		$input = json_decode(Input::get('item'),true);
		$tags = json_decode(Input::get('selected',''), true);

		DB::connection()->getPdo()->beginTransaction();

		if(!$item = $this->_libs['ItemsManager']->updateItem($id, $input, $tags, Input::file('file')))
			throw new ModelException($this->_libs['ItemsManager']->getError(), __LINE__);

		DB::connection()->getPdo()->commit();

		Session::flash('success', 'success', 'Item edited.');
		return Response::json(array(
			'url' => URL::action($this->action.'@getDetail',array($item->id)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}
}