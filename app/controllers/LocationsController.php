<?
use App\Models\Customer, App\Models\Location, App\Models\LC, App\Models\LU, App\Models\Settings, App\Models\User;
use App\Libraries\LocationManager;
class LocationsController extends BaseController
{
	public function getIndex()
	{
		if(!$sort = Input::get('sort',false)) $sort = 'id';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$name = Input::get('name',false);

		//init the query
		$query = new Location;
		if(!empty($name))
			$query = $query->where('name','LIKE',"%$name%");

    	$this->_data['locations'] = $query->orderBy($sort,$dir)->paginate(20);

		$this->_data['edit'] = $this->_access->can('edit',Apps::LOCATIONS);
		$this->_data['settings'] = $this->_access->can('settings',Apps::LOCATIONS);
		$this->_data['reports'] = $this->_access->can('location',Apps::REPORTS);
		$this->_data['create'] = $this->_access->can('create',Apps::LOCATIONS);

        $this->layout->action = 'Location List';
		$this->layout->content = View::make('locations.index',$this->_data);
	}

	public function getCreate()
	{
		$this->layout->action = 'Create Location';
		$this->_data['children'] = Location::select('id','name')->get();
		$this->_data['location'] = new Location;
		$this->_data['selected'] = array();
		$this->layout->content = View::make('locations.form',$this->_data)->with('location', new Location);
	}

	public function postCreate()
	{
		try
		{
		//begin transaction
		DB::connection()->getPdo()->beginTransaction();

		//1. find selected child ids
		$input = Input::get('location');
		$child_ids = Input::get('selected');

		$l = new Location;
		$l->name = $input['name'];
		$l->child_ids = implode(',',$child_ids);
		if(!$l->save())
			throw new ModelException($l->getErrors(), 1);

		foreach(Settings::$defaults as $name => $value)
		{
			$s = new Settings;
			$s->name = $name;
			$s->value = $value;
			$s->location_id = $l->id;
			if(!$s->save())
				throw new ModelException($s->getErrors(), 1);
		}

		//no child
		if(empty($child_ids))
		{
			DB::connection()->getPdo()->commit();
			Session::flash('success', 'Location '.$l->name.' created');
			return Response::json(array(
				'url' => URL::action('LocationsController@getDetail', array($l->id)),
			));
		}

		//2. combine selected child ids with child's child ids
		$grandkids_ids = array();
		foreach($child_ids as $id)
		{
			$kid = Location::findOrFail($id);
			$parent_ids = explode(',',$kid->parent_ids);
			if(empty($parent_ids)) $kid->parent_ids = array($l->id);
			else $kid->parent_ids = implode(',',array_filter(array_merge($parent_ids,array($l->id))));
			if(!$kid->save())
				throw new Exception('Cannot save location children', 1);
			$c = explode(',',$kid->child_ids);
			if(!empty($c))
				$grandkids_ids = array_filter(array_merge($grandkids_ids,$c));

			$this->_lm->forget($id);
		}

		//3. find the grandchild ids, add current loc to parent ids, no need to get their kids ids
		foreach($grandkids_ids as $id)
		{
			$kid = Location::findOrFail($id);
			$parent_ids = explode(',',$kid->parent_ids);
			if(empty($parent_ids)) $kid->parent_ids = array($l->id); //impossible to be empty
			else $kid->parent_ids = implode(',',array_filter(array_merge($parent_ids,array($l->id))));
			if(!$kid->save())
				throw new Exception('Cannot save location children', 1);
			$this->_lm->forget($id);
		}

		//resave current loc id
		$l->child_ids = implode(',',array_filter(array_unique(array_merge($grandkids_ids,$child_ids))));
		if(!$l->save())
			throw new Exception('Cannot save location', 1);

		DB::connection()->getPdo()->commit();
		Session::flash('success', 'Location '.$l->name.' created');
		return Response::json(array(
			'url' => URL::action('LocationsController@getDetail', array($l->id)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}

	}

	public function getEdit($id)
	{
		$this->layout->action = 'Edit Location';
		$this->_data['location'] = Location::findOrFail($id);

		//filter parents + self
		$parents = array_filter(array_merge($this->_data['location']->parent_ids(),array($this->_data['location']->id)));

		$this->_data['selected'] = Location::whereNotIn('id',$parents)->lists('id');

		$this->_data['children'] = $this->_data['location']->children();
		if(empty($this->_data['children'])) $this->_data['children'] = array();

		$this->layout->content = View::make('locations.form',$this->_data);
	}

	public function postEdit($id)
	{
		try
		{
		DB::connection()->getPdo()->beginTransaction();

		$input = Input::get('location');

		$l = Location::findOrFail($id);
		$l->name = $input['name'];

		$new_children = Input::get('selected',array());

		//find old children
		$old_children = explode(',',$l->child_ids);

		//compare with new children
		$diff = array_diff($new_children,$old_children);
		if(empty($diff)) //maybe a removal?
			$diff = array_diff($old_children,$new_children);

		//same shit, save and return
		if(empty($diff))
		{
			if(!$l->save())
				throw new Exception($l->getErrors(), 1);

			DB::connection()->getPdo()->commit();
			Session::flash('success', 'Location '.$l->name.' edited');
			return Response::json(array(
				'url' => URL::action('LocationsController@getDetail', array($l->id)),
			));
		}

		//filter out the parents id - prevent hackers
		$parents = array_filter(array_merge($l->parent_ids(),array($l->id)));
		$new_children = array_filter(array_diff($new_children,$parents));

		//1. combine all child ids from new_children and set the parent id
		$combined_child_ids = array();
		foreach($new_children as $id)
		{
			$kid = Location::findOrFail($id);
			$parent_ids = explode(',',$kid->parent_ids);
			if(empty($parent_ids)) $kid->parent_ids = array($l->id); //impossible to be empty
			else $kid->parent_ids = implode(',',array_unique(array_filter(array_merge($parent_ids,array($l->id)))));
			if(!$kid->save())
			{
				DB::connection()->getPdo()->rollback();
				return Redirect::back()->with('error', 'sum ting wong man')->withInput();
			}
			$c = explode(',',$kid->child_ids);
			if(!empty($c))
				$combined_child_ids = array_filter(array_merge($combined_child_ids,$c));

			$this->_lm->forget($id);
		}

		//2. add parent id to combined child id
		foreach($combined_child_ids as $id)
		{
			$kid = Location::findOrFail($id);
			$parent_ids = explode(',',$kid->parent_ids);
			if(empty($parent_ids)) $kid->parent_ids = array($l->id); //impossible to be empty
			else $kid->parent_ids = implode(',',array_unique(array_filter(array_merge($parent_ids,array($l->id)))));
			if(!$kid->save())
				throw new Exception('Cannot save child locations', 1);

			$this->_lm->forget($id);
		}

		//3. remove parent id from old kids not present in new kids
		$remove_diff = array_filter(array_diff($old_children,$new_children));
		foreach($remove_diff as $id)
		{
			$kid = Location::findOrFail($id);
			$parent_ids = explode(',',$kid->parent_ids);
			if(empty($parent_ids)) $kid->parent_ids = ''; //impossible to be empty
			else $kid->parent_ids = implode(',',array_unique(array_filter(array_diff($parent_ids,array($l->id)))));
			if(!$kid->save())
				throw new Exception('Cannot save child locations', 1);

			$this->_lm->forget($id);
		}

		//assign the new combined child ids with checked child ids, done!!!
		$l->child_ids = implode(',',array_filter(array_unique(array_merge($combined_child_ids,$new_children))));
		if(!$l->save())
			throw new ModelException($l->getErrors(), 1);

		$this->_lm->forget($l->id);

		DB::connection()->getPdo()->commit();
		Session::flash('success', 'Location '.$l->name.' edited');
		return Response::json(array(
			'url' => URL::action('LocationsController@getDetail', array($l->id)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getDetail($id)
	{
		$this->loadDetail($id);

		$this->layout->action = $this->_data['location']->name.' Detail';

		$this->_data['edit'] = $this->_access->can('edit',Apps::LOCATIONS);
		$this->_data['assign'] = $this->_access->can('assign',Apps::LOCATIONS);
		$this->_data['dismiss'] = $this->_access->can('dismiss',Apps::LOCATIONS);

		//get parents
		$this->_data['parents'] = $this->_data['location']->parents();
		if(empty($this->_data['parents'])) $this->_data['parents'] = array();
		else $this->_data['parents'] = $this->_data['parents']->lists('name','id');

		//get children
		$this->_data['children'] = $this->_data['location']->children();
		if(empty($this->_data['children'])) $this->_data['children'] = array();
		else $this->_data['children'] = $this->_data['children']->lists('name','id');

		$this->layout->content = View::make('locations.detail',$this->_data);
	}

	public function postDetail($id)
	{
		$this->loadDetail($id);
		return Response::json($this->_data['details']);
	}

	protected function loadDetail($id)
	{
		$this->_data['location'] = Location::findOrFail($id);

		$query = Customer::with('locations')->whereIn('id',$this->_lm->get_location($id));
		if($name = Input::get('name',false))
			$query = $query->where('name','LIKE',"%$name%");
		$data = $query->paginate(50);

		$this->_data['details'] = array();
		$this->_data['details']['currentPage'] = $data->getCurrentPage();
		$this->_data['details']['lastPage'] = $data->getLastPage();
		$result = array();
		foreach($data as $index => $d)
		{
			$result[$index]['link'] = $d->getDetailLink();
			$result[$index]['type'] = $d->printType();
			$result[$index]['name'] = $d->name;
			$result[$index]['owners'] = array();
			if($d->locations)
			{
				foreach ($d->locations as $key => $value) {
					$data = array();
					$data['link'] = URL::action('LocationsController@getDetail',array($value->id));
					$data['name'] = $value->name;
					$result[$index]['owners'][$key] = $data;
				}
			}
		}
		$this->_data['details']['data'] = $result;
	}

	public function postAssign($id)
	{
		try
		{
		$location = Location::findOrFail($id);
		$input = Input::get('customer');
		if(!$input && !isset($input['id']))
			throw new Exception('invalid input', 1);

		$customer = Customer::findOrFail($input['id']);
		if(!$customer)
			throw new Exception('customer not found', 1);

		$lc = LC::where('location_id','=',$id)->where('customer_id','=',$customer->id)->first();
		if($lc)
			return Redirect::back()->with('error', 'cust already exist');

		$lc = new LC;
		$lc->location_id = $id;
		$lc->customer_id = $customer->id;
		if(!$lc->save())
			throw new Exception("Error Processing Request", 1);

		$this->_lm->forget($lc->location_id);
		LocationManager::forgetParents($location);

		return Response::json(array(
			'success' => $customer->name.' assigned',
		));

		} catch(ModelException $e) {
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			return Response::json($e->getMessage(), 500);
		}
	}

	public function postDismiss($id)
	{
		$loc = Location::findOrFail($id);
		$cust_id = Input::get('customer_id',false);
		$lc = LC::where('location_id','=',$id)->where('customer_id','=',$cust_id)->first();

		if(!$lc)
			return App::abort(404);

		if(!LC::where('location_id','=',$id)->where('customer_id','=',$cust_id)->delete())
			return Redirect::back()->with('error', $lc->getErrors()->first());

		$this->_lm->forget($lc->location_id);
		LocationManager::forgetParents($loc);

		return Redirect::action('LocationsController@getDetail',array($id))->with( 'success', 'cust dismissed.' );
	}

	public function getSettings($id = Settings::LOC_GLOBAL)
	{
		$this->can_edit($id);

		$this->_data['settings'] = Settings::location($this->loc_id)->lists('value','name');

		list($this->_data['start_hour'],$this->_data['start_minutes']) = explode(':',$this->_data['settings']['start_time']);
		list($this->_data['stop_hour'],$this->_data['stop_minutes']) = explode(':',$this->_data['settings']['stop_time']);

		$this->layout->action = 'Edit '.$this->loc_name.' Settings';
		$this->layout->content = View::make('locations.settings',$this->_data);
	}

	public function postSettings($id = Settings::LOC_GLOBAL)
	{
		$this->can_edit($id);

		$this->_data['settings'] = Settings::location($this->loc_id)->lists('value','name');

		$input = Input::get('settings');

		$input['start_time'] = Input::get('start_hour').':'.Input::get('start_minutes');
		$input['stop_time'] = Input::get('stop_hour').':'.Input::get('stop_minutes');

		foreach($this->_data['settings'] as $name => $value)
		{
			if(!isset($input[$name]))
				Settings::where('name','=',$name)->where('location_id','=',$this->loc_id)->update(array('value' => 0));
			else
			{
				if($input[$name] == $this->_data['settings'][$name]) continue;
				Settings::where('name','=',$name)->where('location_id','=',$this->loc_id)->update(array('value' => $input[$name]));
			}
		}

		//bust cache
		Cache::forget('settings.'.$this->loc_id);

		Session::flash('success', 'Settings updated.');
		return Redirect::action('LocationsController@getIndex');
	}

	protected function can_edit($id)
	{
		//global user or admin trying to edit or same location
		if($this->_data['_user']->role_id == Access::ADMIN || empty($this->_data['_user']->location_id) || $this->_data['_user']->location_id == $id)
			return $this->load_location($id);

		//id not provided? load current user's location
		$this->load_location($this->_data['_user']->location_id);
	}

	protected function load_location($id)
	{
		$this->loc_id = $id;
		if($id == Settings::LOC_GLOBAL)
		{
			$this->loc_name = 'global';
			return;
		}

		$loc = Location::findOrFail($id);
		$this->loc_name = $loc->name;
	}
}