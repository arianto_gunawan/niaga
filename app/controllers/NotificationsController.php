<?
use App\Models\Notification;
use App\Models\BalanceTracker, App\Models\Customer, App\Models\Transaction;
class NotificationsController extends BaseController
{
	public function getIndex()
	{
		$this->layout->action = 'New Notifs';

		$date = Dater::now()->format(Dater::$SQLFormat);
		$acts = Notification::where('start','<=',$date)->where('date','>=',$date)->orderBy('date','asc')->paginate(30);

		$notifs = array();
		foreach($acts as $a)
		{
			if(!isset($notifs[$a->app_id])) $notifs[$a->app_id] = array();
			$notifs[$a->app_id][] = $a;
		}

		$this->_data['notifications'] = $notifs;
		$this->layout->content = View::make('notifications.index',$this->_data);
	}

	public function getDueReseller()
	{
		$this->layout->action = 'Payments Due - Reseller';
		$this->getDues(Customer::TYPE_RESELLER);
	}

	public function getDueSupplier()
	{
		$this->layout->action = 'Payments Due - Supplier';
		$this->getDues(Customer::TYPE_SUPPLIER);
	}

	protected function getDues($customerType = Customer::TYPE_RESELLER)
	{
		$c = Customer::table();
		$bt = BalanceTracker::table();

		$query = BalanceTracker::with('partial','customer')->where('partial_id','>',0);
		//1. get resellers with non empty
		$query = $query->join($c,"$c.id",'=',"$bt.customer_id")->where("$c.type",'=', $customerType);
		$data = $query->paginate(20);

		$result = array();
		$transactionIds = array();

		$now = Dater::now();

		//2. get the trans ids
		foreach ($data as $d) {
			//if partial's due date is not today
			if(!$d->partial || Dater::fromSQL($d->partial->due)->gt($now))
				continue;

			$result[$d->customer_id]['customer'] = array('name' => $d->customer->name, 'url' => $d->customer->getDetailLink($d->customer->name));
			$result[$d->customer_id]['transactions'] = array();
			$result[$d->customer_id]['transactions'][$d->partial_id] = array('date' => $d->partial->date, 'invoice' => $d->partial->invoice, 'total' => abs($d->partial->total) - $d->partial_balance, 'url' => $d->partial->getDetailLink($d->partial->invoice));
			$transactionIds = array_merge($transactionIds, $d->transaction_ids);
		}

		//3. get the trans
		$transactions = Transaction::whereIn('id',$transactionIds)->where('due','<=',Dater::toSQL($now))->get();
		foreach ($transactions as $t) {
			if($customerType == Customer::TYPE_RESELLER)
				$customerId = $t->receiver_id;
			else
				$customerId = $t->sender_id;

			if(isset($result[$customerId]['transactions'][$t->id]))
				continue;

			$result[$customerId]['transactions'][$t->id] = array(
				'date' => $t->date,
				'invoice' => $t->invoice,
				'total' => abs($t->total),
				'url' => $t->getDetailLink($t->invoice)
			);
		}
		$this->_data['result'] = $result;
		$this->_data['data'] = $data;

		$this->layout->content = View::make('notifications.due', $this->_data);
	}
}