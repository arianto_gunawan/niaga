<?
use App\Models\Operation, App\Models\Customer, App\Models\Transaction, App\Models\CustomerStat;
use App\Models\HashTag,App\Models\HT;
class OperationsController extends BaseController
{
	public function getIndex()
	{
		$this->loadOperations();
		$this->layout->action = 'Operation List';
	    $this->layout->content = View::make('operations.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadOperations();
		return Response::json($this->_data['operations']);
	}

	protected function loadOperations()
	{
		$name = Input::get('name',false);

		//init the query
		$query = new Operation;
		//dates are set!
		if(!empty($name))
			$query = $query->where('name','LIKE',"%$name%");

    	$data = $query->paginate(50);
    	$response = array();
			$response['currentPage'] = $data->getCurrentPage();
			$response['lastPage'] = $data->getLastPage();
			$response['data'] = array();
			foreach ($data as $key => $detail) {
				$response['data'][$key]['detail_link'] = URL::action('OperationsController@getDetail',array($detail->id));
				$response['data'][$key]['edit_link'] = URL::action('OperationsController@getEdit',array($detail->id));
				$response['data'][$key]['name'] = $detail->name;
		}

		$this->_data['operations'] = $response;
	}

	//ajax
	public function getSearch()
	{
		$term = Input::get('q');
		$response = array();
		$types = Operation::where('name','LIKE',"%$term%")->take(10)->get();
		$i = 0;
		foreach($types as $t)
		{
			$response[$i] = array();

			$response[$i]['name'] = $t->name;
			$response[$i]['value'] = $t->name;
			$response[$i]['id'] = $t->id;
			$response[$i]['tokens'] = explode(' ',$t->name);
			$i++;
		}
		return Response::json($response);
	}

	public function getCreate()
	{
		$this->layout->action = 'New Operation';
		$this->layout->content = View::make('operations.form',$this->_data);
	}

	public function postCreate()
	{
		$input = Input::get('operation');
		$type = new Operation($input);
		if(!$type->save())
			return Redirect::back()->withInput()->withErrors($type->getErrors());

		return Redirect::action('OperationsController@getDetail',array($type->id))->with('success', 'Operation edited');
	}

	public function getEdit($id = false)
	{
		$this->_data['operation'] = Operation::findOrFail($id);
		$this->layout->action = 'Edit Operation';
		$this->layout->content = \View::make('operations.form',$this->_data);
	}

	public function postEdit($id = false)
	{
		$type = Operation::findOrFail($id);
		$type->fill(Input::get('operation'));
		if(!$type->save())
			return Redirect::back()->withInput()->withErrors($type->getErrors());

		return Redirect::action('OperationsController@getDetail',array($type->id))->with('success', 'Operation edited');
	}

	public function getDetail($id)
	{
		$this->_data['operation'] = Operation::findOrFail($id);

		//init the query
		$query = Customer::accounts()->where('parent_id','=',$id);
		$this->_data['details'] = $query->paginate(30);

		$this->layout->action = 'Operation: '.$this->_data['operation']->name;
		$this->layout->caption = $this->_data['operation']->description;
		$this->layout->editButton = URL::action('OperationsController@getEdit',array($id));
		$this->layout->content = View::make('operations.detail',$this->_data);
	}

	public function getAccounts()
	{
		$this->loadAccounts();
		$this->layout->action = 'Journal Account List';
		$this->layout->content = View::make('operations.accounts.index',$this->_data);
	}

	public function postAccounts()
	{
		$this->loadAccounts();
		return Response::json($this->_data['accounts']);
	}

	protected function loadAccounts()
	{
		$name = Input::get('name',false);

		//init the query
		$query = Customer::accounts()->with('operation');
		//dates are set!
		if(!empty($name))
			$query = $query->where('name','LIKE',"%$name%");

	  	$data = $query->paginate(100);
	  	$response = array();
		$response['currentPage'] = $data->getCurrentPage();
		$response['lastPage'] = $data->getLastPage();
		$response['data'] = array();
		foreach ($data as $key => $detail) {
			$response['data'][$key]['detail_link'] = URL::action('OperationsController@getAccountDetail',array($detail->id));
			$response['data'][$key]['edit_link'] = URL::action('OperationsController@getEditAccount',array($detail->id));
			$response['data'][$key]['name'] = $detail->name;
		}

		$this->_data['accounts'] = $response;
	}

	public function getCreateAccount()
	{
		$this->layout->action = 'New Account';
		$this->_data['operations'] = Operation::get()->lists('name','id');
		$this->layout->content = View::make('operations.accounts.form',$this->_data);
	}

	public function postCreateAccount()
	{
		DB::connection()->getPdo()->beginTransaction();

		$input = Input::get('account');
		$account = new Customer($input);
		$account->parent_id = $input['parent_id'];
		$account->type = Customer::TYPE_ACCOUNT;
		if(!$account->save())
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->withErrors($account->getErrors());
		}

		$stat = new CustomerStat;
		$stat->customer_id = $account->id;
		$stat->balance = 0;
		if(!$stat->save())
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->with('error', 'error dol')->withErrors($stat->getErrors())->withInput();
		}

		DB::connection()->getPdo()->commit();
		return Redirect::action('OperationsController@getAccountDetail',array($account->id))->with('success', 'Account created');
	}

	public function getEditAccount($id)
	{
		$this->_data['account'] = Customer::accounts()->findOrFail($id);
		$this->layout->action = 'Edit Account';
		InputForm::load($this->_data['account'],'account');
		$this->_data['operations'] = Operation::get()->lists('name','id');
		$this->layout->content = \View::make('operations.accounts.form',$this->_data);
	}

	public function postEditAccount($id)
	{
		$account = Customer::accounts()->findOrFail($id);
		$input = Input::get('account');
		$account->parent_id = $input['parent_id'];
		$account->name = $input['name'];
		$account->description = $input['description'];
		if(!$account->save())
			return Redirect::back()->withInput()->withErrors($account->getErrors());

		return Redirect::action('OperationsController@getAccountDetail',array($account->id))->with('success', 'Account edited');
	}

	public function getAccountDetail($id)
	{
		$this->_data['account'] = Customer::with('stat')->accounts()->findOrFail($id);
		//load transaction
		$table = Transaction::table();
		$query = Transaction::filter();

		//sorting
		if(!$sort = Input::get('sort',false)) $sort = $table.'.date';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$from = Input::get('from',false);
		$to = Input::get('to',false);
		if($from && $to) //dates are set!
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where($table.'.date','>=',$from)->where($table.'.date','<=',$to);
		}
		else
		{
			$from = Dater::now()->subYear(1)->format(Dater::$SQLFormat);
			$to = Dater::now()->format(Dater::$SQLFormat);
			$query = $query->where($table.'.date','>=',$from)->where($table.'.date','<=',$to);
		}

		$this->_data['transactions'] = $query->where(function($query) use($id) {
			$query->where('receiver_id','=',$id)->orWhere('sender_id','=',$id);
		})->orderBy($sort,$dir)->paginate(50);

		$this->layout->action = $this->_data['account']->name.'('.$this->_data['account']->operation->name.')';
		$this->layout->caption = $this->_data['account']->description;
		$this->layout->editButton = URL::action('OperationsController@getEditAccount',array($id));
		$this->layout->content = View::make('operations.accounts.detail', $this->_data);
	}

	public function getAccountHash($id)
	{
		$this->_data['account'] = Customer::with('stat')->accounts()->findOrFail($id);
		$this->_data['types'] = Transaction::$types;

		//start generating stat
		$date = Input::get('date',null);
		if($date)
		{
			$date = Dater::createFromFormat('m/Y',$date);
			$month = $date->month;
			$year = $date->year;
		}
		else
		{
			$date = Dater::now();
			$month = $date->month;
			$year = $date->year;
		}

		$date = Dater::now();
		$current_month = $date->month;
		$current_year = $date->year;

		$ht = HT::table();
		$h = HashTag::table();
		$query = DB::table($ht)->select(array(
			DB::raw('SUM(total) as sum_total'),
			'transaction_type','hash_id',
			"h.name as hash_name"
		))
		->where('month','=',$current_month)->where('year','=',$current_year);

		$query = $query->where(function($query) use($id)
		{
			$query->where('receiver_id','=',$id)->orWhere('sender_id','=',$id);
		});
		if($filters = \App::make('access')->filter(Apps::TRANSACTION_FILTERS))
		{
			$query = $query->whereIn('transaction_type',$filters);
		}

		//filter
		if($type = Input::get('type',false))
			$query = $query->where('transaction_type','=',$type);
		if($hashtag = strtolower(Input::get('hashtag',false)))
		{
			if($hashtag = HashTag::where('name','=',$hashtag)->first())
				$query = $query->where('hash_id','=',$hashtag->id);
		}

		$query = $query->join("$h as h","h.id",'=',"$ht.hash_id");
		$this->_data['transactions'] = $query->groupBy('transaction_type','hash_id')->get();

		$this->_data['hash_url'] = \App::make('access')->can('stat', Apps::HASHTAG) ? 'HashController@getStat' : 'HashController@getTransactions';

		$this->layout->action = $this->_data['account']->name.' & Hashtags';
		$this->layout->content = View::make('operations.accounts.hash',$this->_data);
	}

	public function getAccountStat($id)
	{
	}
}