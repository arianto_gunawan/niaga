<?
use App\Models\Personnel,App\Models\Pelanggaran;
use App\Libraries\GajiManager;
class PelanggaranController extends BaseController
{
	public function __construct()
	{
		BaseController::__construct();
		$this->_gm = new GajiManager;
	}

	public function getIndex()
	{
		$this->_data['pelanggaran'] = Pelanggaran::with('personnel')->orderBy('id','desc')->paginate(30);

		$this->layout->action = 'Pelanggaran List';
		$this->layout->content = View::make('pelanggaran.index',$this->_data);
	}

	public function getCreate()
	{
		$this->_data['disipliner'] = Pelanggaran::$disipliner;

		$this->layout->action = 'New Pelanggaran';
		$this->layout->content = View::make('pelanggaran.form',$this->_data);
	}

	public function postCreate()
	{
		$p = new Pelanggaran(Input::get('pelanggaran'));
		//check for personnel
		$personnel = Personnel::find($p->personnel_id);
		if(!$personnel)
			return Redirect::back()->withInput()->with('error','Personnel does not exist');

		$p->date = Dater::toSQL($p->date);
		if(!empty($p->disipliner_date)) $p->disipliner_date = Dater::toSQL($p->disipliner_date);

		DB::connection()->getPdo()->beginTransaction();

		if(!$p->save())
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->withErrors($p->getErrors());
		}

		if(!$this->_gm->addPelanggaran($p))
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->with('error',$this->_gm->getErrors()->first());
		}

		DB::connection()->getPdo()->commit();

		return Redirect::action('PelanggaranController@getIndex')->with('success', 'pelanggaran added');
	}

	public function getEdit($id)
	{
		$p = Pelanggaran::with('personnel')->findOrFail($id);

		$this->_data['disipliner'] = Pelanggaran::$disipliner;

		$p->date = Dater::display($p->date);
		if(!empty($p->disipliner_date)) $p->disipliner_date = Dater::display($p->disipliner_date);
		InputForm::load($p,'pelanggaran');
		InputForm::set_array(array('nama' => $p->personnel->name));

		$this->layout->action = 'Edit: Pelanggaran';
		$this->layout->content = View::make('pelanggaran.form',$this->_data);
	}

	public function postDelete($id)
	{
		$p = Pelanggaran::findOrFail($id);
		DB::connection()->getPdo()->beginTransaction();

		if(!$p->delete())
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->with('error','error deleting pelanggaran');
		}

		if(!$this->_gm->deductPelanggaran($p))
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->with('error',$this->_gm->getErrors()->first());
		}

		DB::connection()->getPdo()->commit();
		return Redirect::action('PelanggaranController@getIndex')->with('success', 'pelanggaran deleted');
	}
}