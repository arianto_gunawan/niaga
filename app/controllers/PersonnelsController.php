<?
use App\Models\Customer;
use App\Models\Personnel,App\Models\Pelanggaran,App\Models\PC,App\Models\Cuti,App\Models\GPU;
class PersonnelsController extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->_data['departments'] = Personnel::$DEPTS;
	}

	public function getIndex()
	{
		$this->layout->action = 'Personnels List';
		$this->_data['deptsJSON'] = json_encode(Personnel::$deptsJSON);
		$this->_data['statusJSON'] = json_encode(Personnel::$statusJSON);

		$this->loadIndex();
   	$this->layout->content = View::make('personnels.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadIndex();
		return Response::json($this->_data['responseJSON']);
	}

	protected function loadIndex()
	{
		if(!$sort = Input::get('sort',false)) $sort = 'department';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$query = new Personnel;
		if($status = Input::get('status',false))
			$query = $query->where('status','=',$status);
		if($dept = Input::get('department',false))
			$query = $query->where('department','=',$dept);
		if($jab = Input::get('position',false))
			$query = $query->where('position','LIKE',"%$jab%");
		if($name = Input::get('name',false))
			$query = $query->where('name','LIKE',"%$name%");

		if($deleted = Input::get('deleted', false))
			$query = $query->onlyTrashed();

		$personnels = $query->orderBy($sort,$dir)->paginate(50);

		//format data for json
		$response = array();
		$response['currentPage'] = $personnels->getCurrentPage();
		$response['lastPage'] = $personnels->getLastPage();
		$response['data'] = array();
		foreach ($personnels as $index => $t) {
			$response['data'][$index]['id'] = $t->id;
			$response['data'][$index]['name'] = $t->name;
			$response['data'][$index]['department'] = $t->getDepartment();
			$response['data'][$index]['position'] = $t->position;
			$response['data'][$index]['birthdate'] = $t->birthdate;
			$response['data'][$index]['detail_link'] = $t->getDetailLink();
			$response['data'][$index]['edit_link'] = $t->getEditLink();
			$response['data'][$index]['date_in'] = $t->date_in;
			$response['data'][$index]['status'] = $t->getStatus();
		}

		$this->_data['responseJSON'] = $response;
		$this->_data['personnels'] = Response::json($response)->getData();
	}

	public function getCreate()
	{
		$this->layout->action = 'New Personnel';
		$this->_data['status'] = Personnel::$STATUS;
		$this->_data['edit_cuti'] = false;

		$this->layout->content = View::make('personnels.form',$this->_data);
	}

	public function postCreate()
	{
		DB::connection()->getPdo()->beginTransaction();

		$personnel = new Personnel(Input::get('personnel'));
		$personnel->name = ucfirst($personnel->name);
		$personnel->date_in = Dater::toSQL($personnel->date_in);
		$personnel->date_out = Dater::toSQL($personnel->date_out);
		$personnel->birthdate = Dater::toSQL($personnel->birthdate);

		if(!$personnel->save())
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->withErrors($personnel->getErrors());
		}

		$pc = new PC;
		$pc->personnel_id = $personnel->id;
		$pc->year = Dater::now()->year;
		$pc->sisa_tahunan = Cuti::MAX_CUTI;
		$pc->sisa_sakit = Cuti::MAX_CUTI;
		if(!$pc->save())
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->with('error','error creating cuti');
		}

		DB::connection()->getPdo()->commit();
		return Redirect::action('PersonnelsController@getIndex')->with('success', $personnel->name.' created');
	}

	public function getEdit($id)
	{
		$this->_data['personnel'] = Personnel::findOrFail($id);

		$this->_data['status'] = Personnel::$statusJSON;
		$this->layout->action = 'Edit '.$personnel->name;

		//adds the cuti
		$this->_data['edit_cuti'] = $this->_access->can('edit-cuti',Apps::PERSONNELS);
		if($this->_data['edit_cuti'])
			$this->_data['pc'] = PC::where('personnel_id','=',$personnel->id)->where('year','=',Dater::now()->year)->first();

    $this->layout->content = View::make('personnels.form',$this->_data);
	}

	public function postEdit($id)
	{
		$personnel = Personnel::findOrFail($id);

		DB::connection()->getPdo()->beginTransaction();

		$personnel->fill(Input::get('personnel'));
		$personnel->date_in = Dater::toSQL($personnel->date_in);
		$personnel->date_out = Dater::toSQL($personnel->date_out);
		$personnel->birthdate = Dater::toSQL($personnel->birthdate);

		if(!$personnel->save())
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->withErrors($personnel->getErrors());
		}

		if($this->_access->can('edit-cuti',Apps::PERSONNELS))
		{
			$sisa_tahunan = Input::get('sisa_tahunan',false);
			$sisa_sakit = Input::get('sisa_sakit',false);
			if($sisa_tahunan && $sisa_sakit)
			{
				$year = Dater::now()->year;
				$pc = PC::where('personnel_id','=',$personnel->id)->where('year','=',$year)->first();
				if(!$pc)
				{
					$pc = new PC;
					$pc->personnel_id = $personnel->id;
					$pc->year = $year;
					$pc->sisa_sakit = $sisa_sakit;
					$pc->sisa_tahunan = $sisa_tahunan;
					if(!$pc->save())
					{
						DB::connection()->getPdo()->rollback();
						return Redirect::back()->withInput()->with('error','error saving cuti');
					}
				}
				else
				{
					$pc->where('personnel_id','=',$pc->personnel_id)->where('year','=',$pc->year)->update(array('sisa_tahunan' => $sisa_tahunan,'sisa_sakit' => $sisa_sakit));
				}
			}
		}

		DB::connection()->getPdo()->commit();
		return Redirect::action('PersonnelsController@getDetail',array($id))->with('success', $personnel->name.' edited');
	}

	public function getDetail($id)
	{
		$this->loadPersonnel($id);

		$this->_data['delete'] = false;
		$this->_data['restore'] = false;
		$this->_data['delete'] = $this->_access->can('delete', Apps::PERSONNELS);
		$this->_data['restore'] = $this->_access->can('restore', Apps::PERSONNELS);

		$this->layout->action = $this->_data['personnel']->name;
		$this->layout->content = View::make('personnels.detail',$this->_data);
	}

	public function getCuti($id)
	{
		$this->loadPersonnel($id);
		$this->_data['cuti'] = Cuti::where('personnel_id','=',$this->_data['personnel']->id)->orderBy('id','desc')->paginate(30);
		$this->_data['nav'] = 'cuti';

		$this->layout->action = $this->_data['personnel']->name."'s Cuti List";
		$this->layout->content = View::make('personnels.cuti',$this->_data);
	}

	public function getPelanggaran($id)
	{
		$this->loadPersonnel($id);
		$this->_data['pelanggaran'] = Pelanggaran::where('personnel_id','=',$this->_data['personnel']->id)->orderBy('id','desc')->paginate(30);
		$this->_data['nav'] = 'pelanggaran';

		$this->layout->action = $this->_data['personnel']->name."'s Pelanggaran List";
		$this->layout->content = View::make('personnels.pelanggaran',$this->_data);
	}

	protected function loadPersonnel($id)
	{
		$this->_data['personnel'] = Personnel::withTrashed()->findOrFail($id);
		$this->_data['edit_gpu'] = $this->_access->can('edit-gpu',Apps::PERSONNELS);
		$this->_data['edit_cuti'] = $this->_access->can('edit-cuti',Apps::PERSONNELS);

		if($this->_data['edit_cuti'])
		{
			$this->_data['sisa_cuti'] = PC::where('personnel_id','=',$this->_data['personnel']->id)->where('year','=',Dater::now()->year)->first();
			if(!$this->_data['sisa_cuti'])
			{
				$this->_data['sisa_cuti'] = new PC;
				$this->_data['sisa_cuti']->sisa = 'not set';
			}
		}

		$this->_data['delete_personnel'] = $this->_access->can('delete',Apps::PERSONNELS);
		$this->_data['restore_personnel'] = $this->_access->can('restore',Apps::PERSONNELS);

		$this->_data['view_cuti'] = $this->_access->can('index',Apps::CUTI);
		$this->_data['view_pelanggaran'] = $this->_access->can('index',Apps::PELANGGARAN);


		$this->_data['view_gpu'] = $this->_access->can('gpu', Apps::PERSONNELS);
		if($this->_data['personnel']->private == 1)
			$this->_data['view_gpu'] = $this->_access->can('private-gpu', Apps::PERSONNELS);
	}

	//ajax
	public function getSearch()
	{
		$term = Input::get('q');
		$personnel = Personnel::where('name','LIKE',"%$term%")->take(5)->get();

		$response = array();
		$i = 0;
		foreach($personnel as $c)
		{
			$response[$i] = array();
			$response[$i]['name'] = $c->name;
			$response[$i]['id'] = $c->id;
			$response[$i]['value'] = $c->name;
			$response[$i]['tokens'] = explode(' ',$c->name);
			$i++;
		}

		return Response::json($response);
	}

	public function postDelete($id)
	{
		$personnel = Personnel::findOrFail($id);
		$personnel->delete();

		return Response::json(array(
			'success' => false,
			'title' => 'Error',
			'msg' => array('Cannot delete personnel, balance NEED to be 0'),
			'deleted_at' => $personnel->deleted_at
		));
	}

	public function postRestore($id)
	{
		$personnel = Personnel::onlyTrashed()->findOrFail($id);
		$personnel->restore();

		return Redirect::action('PersonnelsController@getIndex')->with('success', $personnel->name.' restored');
	}

	public function getGpu()
	{
		$this->layout->action = 'GPU List';

		if(!$sort = Input::get('sort',false)) $sort = 'name';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$query = new Personnel;
		if($dept = Input::get('department',false))
			$query = $query->where('department','=',$dept);
		if($name = Input::get('name',false))
			$query = $query->where('name','LIKE',"%$name%");

		ksort($this->_data['departments']);

		$this->_data['personnels'] = $query->where('private','=',0)->orderBy($sort,$dir)->paginate(50);
		$this->_data['private'] = $this->_access->can('private-gpu',Apps::PERSONNELS);

		$this->layout->content = View::make('personnels.gpu',$this->_data);
	}

	public function getPrivateGpu()
	{
		$this->layout->action = 'Private GPU List';

		if(!$sort = Input::get('sort',false)) $sort = 'name';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$query = new Personnel;
		if($dept = Input::get('department',false))
			$query = $query->where('department','=',$dept);
		if($name = Input::get('name',false))
			$query = $query->where('name','LIKE',"%$name%");

		ksort($this->_data['departments']);

		$this->_data['personnels'] = $query->where('private','=',1)->orderBy($sort,$dir)->paginate(50);

		$this->layout->content = View::make('personnels.gpu',$this->_data);
	}

	public function getGpuHistory($id)
	{
		$personnel = Personnel::findOrFail($id);
		$gpu = GPU::findOrFail($id);

		$this->layout->action = 'Gaji for '.$personnel->name;
		$this->layout->content = View::make('personnels.gpu_detail',$this->_data);
	}

	public function getEditGpu($id)
	{
		$personnel = Personnel::findOrFail($id);
		InputForm::load($personnel,'personnel');
		$this->_data['private'] = $personnel->private;

		//check for private
		if($personnel->private == 1)
			if(!$this->_access->can('private-gpu',Apps::PERSONNELS))
				App::abort(404);

		$this->_data['accounts'] = Customer::filter()->where('type','=',Customer::TYPE_BANK)->lists('name','id');

		$this->layout->action = 'GPU for '.$personnel->name;
		$this->layout->content = View::make('personnels.gpu_form',$this->_data);
	}

	public function postEditGpu($id)
	{
		$personnel = Personnel::findOrFail($id);
		$gpu = Input::get('gpu');

		$personnel->bulanan = $gpu['bulanan'];
		$personnel->harian = $gpu['harian'];
		$personnel->premi = $gpu['premi'];
		$personnel->account_id = Input::get('account_id');
		$personnel->private = isset($gpu['private']) ? 1 : 0;

		if(!$personnel->save())
			return Redirect::back()->withInput()->with('error','error saving gpu');

		if($personnel->private == 1 && $this->_access->can('private-gpu',Apps::PERSONNELS))
			return Redirect::action('PersonnelsController@getPrivateGpu')->with('success', $personnel->name.' edited');

		return Redirect::action('PersonnelsController@getGpu')->with('success', $personnel->name.' edited');
	}
}