<?
use App\Models\Borongan,App\Models\BoronganDetail;
use App\Models\Produksi, App\Models\AProduksi;

use App\Models\Transaction, App\Models\Customer, App\Models\TransactionDetail;
use App\Models\Borongan,App\Models\BoronganDetail;
use App\Models\Produksi, App\Models\AProduksi;
use App\Models\Item, App\Models\WarehouseItem;
class PrintController extends \Argun\Aria\PrintController
{
	public $layout = 'layouts.print';

	public function getTransaction($id,$date)
	{
		if(!$this->_data['transaction'] = Transaction::where('date','=',$date)->where('id','=',$id)->first())
			return App::abort(404);

		if(!empty($this->_data['transaction']->detail_ids))
		{
			$this->_data['details'] = TransactionDetail::with('item')->whereIn('id',array_filter(explode(',',$this->_data['transaction']->detail_ids)))->get();
		}
		$this->layout->content = View::make('print.transaction',$this->_data);
	}

	public function getBorongan($id)
	{
		if(!$this->_data['borongan'] = Borongan::find($id))
			return Redirect::to(404);

		$this->_data['details'] = BoronganDetail::with('item')->whereIn('id',array_filter(explode(',',$this->_data['borongan']->detail_ids)))->get();
		$this->layout->content = View::make('print.borongan',$this->_data);
	}

	public function getCItems($id)
	{
		if($this->_lm->bound())
		{
			if(!in_array($id,$this->_lm->get_location()))
				return App::abort(404);
		}

		if(!$this->_data['customer'] = Customer::with('stat')->where('id','=',$id)->first())
			return App::abort(404);

		$items_table = Item::table();
		$w_table = WarehouseItem::table();

		if(!$sort = Input::get('sort',false)) $sort = 'quantity';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		switch($sort)
		{
			case 'name': $sort = $items_table.'.name'; break;
			case 'code': $sort = $items_table.'.code'; break;
			default: break;
		}

		$query = DB::table($w_table)->select(array('quantity','item_id',$items_table.'.type',$items_table.'.code',$items_table.'.price',$items_table.'.name'))->where('warehouse_id','=',$id)->where('quantity','>',0);
		$query = $query->join($items_table,$w_table.'.item_id','=',$items_table.'.id');
		if($name = Input::get('name',false))
			$query = $query->where($items_table.'.name','LIKE',"%$name%");
		$this->_data['items'] = $query->orderBy($sort,$dir)->paginate(500);

        $this->layout->content = View::make('print.items',$this->_data);
	}

	public function getCSales($customerId, $month = null)
	{
		if($this->_lm->bound())
		{
			if(!in_array($id,$this->_lm->get_location()))
				return App::abort(404);
		}

		if(!$this->_data['customer'] = Customer::with('stat')->where('id','=',$customerId)->first())
			return App::abort(404);

		if($month)
		{
			$from = Dater::toSQL(Dater::createFromFormat('m/Y',$month)->startOfMonth()->format(Dater::$format));
			$to = Dater::toSQL(Dater::createFromFormat('m/Y',$month)->endOfMonth()->format(Dater::$format));
		}
		else
		{
			$from = Dater::toSQL(Dater::now()->startOfMonth()->format(Dater::$format));
			$to = Dater::toSQL(Dater::now()->endOfMonth()->format(Dater::$format));
		}

		$items_table = Item::table();
		$t_table = Transaction::table();
		$td_table = TransactionDetail::table();

		if(!$sort = Input::get('sort',false)) $sort = 'code';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		switch($sort)
		{
			case 'name': $sort = 'items_table.name'; break;
			case 'code': $sort = 'items_table.code'; break;
			case 'total_sell': $sort = 'total_sell'; break;
			case 'total_return': $sort = 'total_return'; break;
			default: break;
		}

		$query = DB::table("$td_table as td_table")->select(array(
			DB::raw('items_table.id as item_id'),
			'items_table.code','items_table.type','items_table.name',
			DB::raw('SUM(CASE WHEN td_table.transaction_type = '.Transaction::TYPE_SELL.' THEN td_table.quantity ELSE 0 END) as total_sell'),
			DB::raw('SUM(CASE WHEN td_table.transaction_type = '.Transaction::TYPE_RETURN.' THEN td_table.quantity ELSE 0 END) as total_return'),
		))
		->join("$items_table as items_table", 'td_table.item_id', '=', 'items_table.id')
		->where('td_table.date','>=',$from)->where('td_table.date','<=', $to)->where(function($query) use($customerId) {
			$query->where('sender_id', '=', $customerId)->orWhere('receiver_id', '=', $customerId);
		})
		->where(function($query) {
			$query->where('td_table.transaction_type','=',Transaction::TYPE_SELL)->orWhere('td_table.transaction_type','=',Transaction::TYPE_RETURN);
		});

		$this->_data['items'] = $query->groupBy('items_table.id')->orderBy($sort,$dir)->paginate(200);


        $this->layout->content = View::make('print.csales',$this->_data);
    }

	public function getProduksi($type = 'produksi')
	{
		$from = Input::get('from',false);
		$to = Input::get('to',false);

		//init the query
		if($type == 'produksi')
			$query = Produksi::with(array('item','potong','size'));
		elseif($type == 'arsip')
			$query = Produksi::with(array('item','potong','size'))->where('status','!=',Produksi::STATUS_PRODUKSI);
		else
			$query = AProduksi::with(array('item','potong','size'));
		//dates are set!
		if($from && $to)
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where(function($query) use($from,$to)
			{
				$query->where('jahit_in','>=',$from)->where('jahit_in','<=',$to);
			});
		}
		//find potong
		if($potong = Input::get('potong',false))
			$query = $query->where('potong_id','=',$potong);
		if($jahit = Input::get('jahit',false))
			$query = $query->where('jahit','=',$jahit);
		if($customer = Input::get('customer',false))
			$query = $query->where('customer','=',$customer);
		if($warna = Input::get('warna',false))
			$query = $query->where('warna','=',$warna);

		$this->_data['produksi'] = $query->orderBy('id','desc')->paginate(100);
		$this->layout->content = View::make('print.produksi',$this->_data);
	}

	public function getBorongan($id)
	{
		if(!$this->_data['borongan'] = Borongan::find($id))
			return Redirect::to(404);

		$this->_data['details'] = BoronganDetail::with('item')->whereIn('id',array_filter(explode(',',$this->_data['borongan']->detail_ids)))->get();
		$this->layout->content = View::make('print.borongan',$this->_data);
	}

	public function getProduksi($type = 'produksi')
	{
		$from = Input::get('from',false);
		$to = Input::get('to',false);

		//init the query
		if($type == 'produksi')
			$query = Produksi::with(array('item','potong','size'));
		elseif($type == 'arsip')
			$query = Produksi::with(array('item','potong','size'))->where('status','!=',Produksi::STATUS_PRODUKSI);
		else
			$query = AProduksi::with(array('item','potong','size'));
		//dates are set!
		if($from && $to)
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where(function($query) use($from,$to)
			{
				$query->where('jahit_in','>=',$from)->where('jahit_in','<=',$to);
			});
		}
		//find potong
		if($potong = Input::get('potong',false))
			$query = $query->where('potong_id','=',$potong);
		if($jahit = Input::get('jahit',false))
			$query = $query->where('jahit','=',$jahit);
		if($customer = Input::get('customer',false))
			$query = $query->where('customer','=',$customer);
		if($warna = Input::get('warna',false))
			$query = $query->where('warna','=',$warna);

		$this->_data['produksi'] = $query->orderBy('id','desc')->paginate(100);
		$this->layout->content = View::make('print.produksi',$this->_data);
	}
}
