<?
//NOTE OLD
use App\Models\Personnel, App\Models\Tag;
use App\Models\Produksi, App\Models\Item;
class ProductionsController extends BaseController
{
	public function __construct()
	{
		parent::__construct();

		//load jahit
		$this->_data['jahit'] = Personnel::jahitProduksi()->lists('group','group');
		ksort($this->_data['jahit']);
		array_unshift($this->_data['jahit'],'---');

		$this->_data['potong'] = Personnel::potongProduksi()->lists('name','id');
		$this->_data['potong'][0] = 'Potong';
		ksort($this->_data['potong']);

		$this->_data['size'] = Tag::where('type','=',Tag::TYPE_SIZE)->lists('name','id');
		$this->_data['size'][0] = 'Size';
		ksort($this->_data['size']);
	}

	public function getIndex()
	{
		$this->addScript(array('dateinput'));

		if(!$sort = Input::get('sort',false)) $sort = 'id';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$from = Input::get('from',false);
		$to = Input::get('to',false);

		//init the query
		$query = Produksi::with(array('item','potong','size'));
		//dates are set!
		if($from && $to)
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where(function($query) use($from,$to)
			{
				$query->where(function($query) use($from,$to)
				{
					$query->where('potong_in','>=',$from)->where('potong_in','<=',$to);
				});
				$query = $query->orWhere(function($query) use($from,$to)
				{
					$query->where('jahit_in','>=',$from)->where('jahit_out','<=',$to);
				});
			});
		}

		//find potong
		if($potong = Input::get('potong',false))
			$query = $query->where('potong_id','=',$potong);
		if($jahit = Input::get('jahit',false))
			$query = $query->where('jahit','=',$jahit);
		if($customer = Input::get('customer',false))
			$query = $query->where('customer','=',$customer);
		if($warna = Input::get('warna',false))
			$query = $query->where('warna','=',$warna);
		if($kode = Input::get('kode',false))
			$query = $query->where('temp_name','LIKE',"%$kode%");
		if($id = Input::get('id',false))
			$query = $query->where('id','=',Produksi::fromSerial($id));
		if($size = Input::get('size',false))
			$query = $query->where('size_id','=',$size);

		$this->_data['produksi'] = $query->where('status','=',Produksi::STATUS_PRODUKSI)->orderBy($sort,$dir)->paginate(30);
		$this->layout->action = 'Barang di Produksi';
		$this->layout->content = View::make('productions.index',$this->_data);
	}

	public function postUpdate()
	{
		$input = Input::get('produksi');
		DB::connection()->getPdo()->beginTransaction();
		$msg = false;
		foreach($input as $pid => $data)
		{
			$save = true;
			$produksi = Produksi::find($pid);
			if(isset($data['jahit_in']) && !empty($data['jahit_in'])) $produksi->jahit_in = Dater::toSQL($data['jahit_in']);
			else $data['jahit_in'] = null;
			if(isset($data['jahit_out']) && !empty($data['jahit_out'])) $produksi->jahit_out = Dater::toSQL($data['jahit_out']);
			else $data['jahit_out'] = null;
			if(isset($data['jahit']) && !empty($data['jahit'])) $produksi->jahit = trim($data['jahit']);
			if(isset($data['urgent']) && !empty($data['urgent'])) $produksi->urgent = $data['urgent'];
			if(isset($data['description']) && !empty($data['description'])) $produksi->description = $data['description'];

			//special case
			if(isset($data['q_out']) && !empty($data['q_out']))
			{
				if($data['q_out'] == $produksi->quantity)
				{
					$produksi->q_out = $produksi->quantity;
				}
				elseif($produksi->q_out < $produksi->quantity) //barang keluar != potong, split produksi
				{
					$split = new Produksi;
					$split->quantity = $produksi->quantity - $data['q_out'];
					$split->jahit_in = null;
					$split->jahit_out = null;
					$split->permak_in = null;
					$split->permak_out = null;
					$split->q_in = 0;
					$split->q_out = 0;
					$split->potong_in = $produksi->potong_in;
					$split->user_id = $produksi->user_id;
					$split->description = $produksi->description;
					$split->warna = $produksi->warna;
					$split->customer = $produksi->customer;
					$split->temp_name = $produksi->temp_name;
					$split->item_id = $produksi->item_id;
					$split->potong_id = $produksi->potong_id;
					$split->size_id = $produksi->size_id;
					$split->urgent = $produksi->urgent;

					if(!$split->save())
					{
						DB::connection()->getPdo()->rollback();
						return Redirect::back()->withInput()->withErrors($split->getErrors());
					}

					//deduct quantity from current produksi
					$produksi->quantity = $data['q_out'];
					$produksi->q_out = $data['q_out'];
				}
				else
				{
					$msg .= $produksi->id.' salah<br/>';
				}
			}
			//another special case, switch status to archive
			if(isset($data['q_in']) && !empty($data['q_in']))
			{
				if($data['q_in'] == $produksi->quantity)
				{
					$produksi->status = Produksi::STATUS_ARSIP;
					$produksi->q_in = $data['q_in'];
				}
				else
				{
					$msg .= $produksi->id.' = jumlah masuk tdk sama<br/>';
				}
			}
			if($save)
			{
				if(!$produksi->save())
				{
					DB::connection()->getPdo()->rollback();
					return Redirect::back()->withInput()->withErrors($produksi->getErrors());
				}
			}
		}

		//commit db transaction
		DB::connection()->getPdo()->commit();
		if($msg)
			return Redirect::action('ProductionsController@getIndex')->with( 'error', $msg );
		return Redirect::action('ProductionsController@getIndex')->with( 'success', 'Produksi updated.' );
	}

	public function getCreate()
	{
		$this->addScript(array('dateinput','pform'));
		$this->_data['rows'] = Session::get('rows',1);
		$this->_data['action'] = 'New Produksi';

		$this->_data['potong'] = Personnel::potongProduksi()->lists('name','id');
		ksort($this->_data['potong']);

		$this->_data['size'] = Tag::where('type','=',Tag::TYPE_SIZE)->lists('name','id');
		asort($this->_data['size']);

		$this->_data['default_size'] = 0;
		foreach($this->_data['size'] as $id => $val)
		{
			if($val == 'M') $this->_data['default_size'] = $id;
		}
		$this->layout->action = 'Create Produksi';
		$this->layout->content = View::make('productions.form',$this->_data);
	}

	public function postCreate()
	{
		Session::forget('rows');
		$rows = count(Input::get('details'));

		$date = Input::get('date');
		if(empty($date) || $date == '00/00/0000')
			return Redirect::back()->withInput()->with('error','tanggal salah');

		$potong = Input::get('potong_id');

		//start transaction
		DB::connection()->getPdo()->beginTransaction();

		$data = Input::get('produksi');
		$date = Dater::toSQL($date);
		foreach($data as $i => $d)
		{
			//skip empty code, quantity < 1
			if(empty($d['item_id']) || $d['quantity'] < 1)
				if(empty($d['temp_name']))
					continue;

			$produksi = new Produksi($d);
			//set default values
			$produksi->potong_id = $potong;
			$produksi->potong_in = $date;
			$produksi->warna = strtoupper($produksi->warna);
			$produksi->customer = strtoupper($produksi->customer);

			if(!$produksi->save())
			{
				DB::connection()->getPdo()->rollback();
				return Redirect::back()->withInput()->withErrors($produksi->getErrors());
			}
		}

		//commit db transaction
		DB::connection()->getPdo()->commit();

		return Redirect::action('ProductionsController@getIndex')->with( 'success', 'Produksi created.' );
	}

	public function getEdit($id)
	{
		$p = Produksi::with('item')->findOrFail($id);

		$this->_data['default_size'] = 0;

		$this->addScript(array('dateinput','typeahead'));

		//better typeability, load to form
		$this->_data['produksi'] = $p;
		$this->_data['action'] = 'Edit produksi #'.$p->id;

		$this->layout->action = 'Edit: '.$p->serial();
		$this->layout->content = View::make('productions.edit',$this->_data);
	}

	public function postEdit($id)
	{
		$p = Produksi::findOrFail($id);

		return $this->validate_edit($p);
	}

	protected function validate_edit($p)
	{
		DB::connection()->getPdo()->beginTransaction();

		$old_quantity = $p->quantity;
		$old_item_id = $p->item_id;

		$input = Input::get('produksi');
		$p->fill($input);
		$p->potong_in = Dater::toSQL($input['potong_in']);
		$p->jahit_in = Dater::toSQL($input['jahit_in']);
		$p->jahit_out = Dater::toSQL($input['jahit_out']);
		$p->warna = strtoupper($p->warna);
		$p->customer = strtoupper($p->customer);
		$p->jahit = $input['jahit'];

		if($p->item_id)
		{
			$item = Item::find($p->item_id);
			if($item)
				$p->temp_name = $item->code;
			else
				$p->item_id = 0;
		}

		if(!$p->save())
		{
			DB::connection()->getPdo()->rollback();
			return Redirect::back()->withInput()->withErrors($p->getErrors());
		}

		DB::connection()->getPdo()->commit();
		return Redirect::action('ProductionsController@getEdit',array($p->id))->with( 'success', 'produksi edited.' );
	}

	public function getStats()
	{
		$this->addScript(array('dateinput'));
		$month = Input::get('month',null);
		if($month)
		{
			$date = Dater::createFromFormat('m/Y',$month);
			$this->_data['links']['from'] = $date->startOfMonth()->format(Dater::$format);
			$this->_data['links']['to'] = $date->endOfMonth()->format(Dater::$format);
		}
		else
		{
			$date = Dater::now();
			$this->_data['links']['from'] = $date->startOfMonth()->format(Dater::$format);
			$this->_data['links']['to'] = $date->endOfMonth()->format(Dater::$format);
		}

		$from = Dater::toSQL($this->_data['links']['from']);
		$to = Dater::toSQL($this->_data['links']['to']);

		//stats for potong
		$this->_data['potong'] = Produksi::with(array('potong'))
			->select(array(DB::raw('SUM( quantity ) as total_potong'), 'potong_id'))
			->where('status','=',Produksi::STATUS_PRODUKSI)
			->where(function($query) use($from,$to)
			{
				$query->where('potong_in','>=',$from)->where('potong_in','<=',$to);
			})
			->groupBy('potong_id')->get();
		$this->_data['apotong'] = Produksi::with(array('potong'))
			->select(array(DB::raw('SUM( quantity ) as total_potong'), 'potong_id'))
			->where('status','!=',Produksi::STATUS_PRODUKSI)
			->where(function($query) use($from,$to)
			{
				$query->where('potong_in','>=',$from)->where('potong_in','<=',$to);
			})
			->groupBy('potong_id')->get();

		//stats for ke jahit
		$this->_data['jahit_out'] = DB::table(Produksi::table())
			->select(array(DB::raw('SUM( q_out ) as total_jahit'), 'jahit'))
			->where('status','=',Produksi::STATUS_PRODUKSI)
			->where('q_out','>','0')
			->where(function($query) use($from,$to)
			{
				$query->where('jahit_out','>=',$from)->where('jahit_out','<=',$to);
			})
			->groupBy('jahit')->get();
		$this->_data['ajahit_out'] = DB::table(Produksi::table())
			->select(array(DB::raw('SUM( q_in ) as total_jahit'), 'jahit'))
			->where('status','!=',Produksi::STATUS_PRODUKSI)
			->where(function($query) use($from,$to)
			{
				$query->where('jahit_in','>=',$from)->where('jahit_in','<=',$to);
			})
			->groupBy('jahit')->get();

		$this->layout->action = 'Report From: '.Dater::display($from).' to '.Dater::display($to);
		$this->layout->content = View::make('productions.stat',$this->_data);
	}

	public function getChart()
	{
		$this->addScript(array('chart'));
		//get the stock
		$this->_data['stok'] = DB::table(Produksi::table())
			->select(array(DB::raw('SUM( q_out ) as total_jahit'), 'jahit'))
			->where('q_out','>','0')
			->where('status','=',Produksi::STATUS_PRODUKSI)
			->where('customer','LIKE','%stok%')
			->groupBy('jahit')->lists('total_jahit','jahit');
		//get the total, pesenan = total - stok
		$this->_data['total'] = DB::table(Produksi::table())
			->select(array(DB::raw('SUM( q_out ) as total_jahit'), 'jahit'))
			->where('q_out','>','0')
			->where('status','=',Produksi::STATUS_PRODUKSI)
			->groupBy('jahit')->lists('total_jahit','jahit');

		$this->_data['pesenan'] = '[';
		$this->_data['labels'] = '[';
		$this->_data['data_stok'] = '[';
		foreach($this->_data['total'] as $jahit => $total_jahit)
		{
			if(!isset($this->_data['stok'][$jahit])) $this->_data['stok'][$jahit] = 0;
			$this->_data['pesenan'] .= ($this->_data['total'][$jahit] - $this->_data['stok'][$jahit]).',';
			$this->_data['data_stok'] .= $this->_data['stok'][$jahit].',';
			$this->_data['labels'] .= '"'.$jahit.'",';
		}
		$this->_data['labels'] = rtrim($this->_data['labels'],',').']';
		$this->_data['data_stok'] = rtrim($this->_data['data_stok'],',').']';
		$this->_data['pesenan'] = rtrim($this->_data['pesenan'],',').']';
		$this->layout->content = View::make('productions.chart',$this->_data);
	}
}