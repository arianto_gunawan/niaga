<?
use App\Models\Worker, App\Models\Tag;
use App\Models\Produksi, App\Models\Item;
class ProduksiController extends BaseController
{
	protected $controller = 'ProduksiController';
	public function __construct()
	{
		parent::__construct();

		//load jahit
		$this->_data['jahit'] = Worker::jahit()->get();
		$this->_data['potong'] = Worker::potong()->get();
		$this->_data['size'] = Tag::where('type','=',Tag::TYPE_SIZE)->get();
	}

	public function getIndex($id = false)
	{
		$this->loadProduksiList(Produksi::STATUS_PRODUKSI, $id);
		$this->_data['can_delete'] = $this->_access->can('delete', Apps::PRODUKSI);
		$this->layout->action = 'Barang di Produksi';
		$this->layout->content = View::make('produksi.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadProduksiList();
		return Response::json($this->_data['produksi']);
	}

	//setoran masuk ke load setoran list
	protected function loadProduksiList($defaultStatus = Produksi::STATUS_PRODUKSI, $id = false)
	{
		if(!$sort = Input::get('sort',false)) $sort = 'id';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$from = Input::get('from',false);
		$to = Input::get('to',false);

		//init the query
		$query = Produksi::with(array('item','potong','size','jahit'));
		//dates are set!
		if($from && $to)
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where(function($query) use($from,$to)
			{
				$query->where(function($query) use($from,$to)
				{
					$query->where('potong_date','>=',$from)->where('potong_date','<=',$to);
				});
				$query = $query->orWhere(function($query) use($from,$to)
				{
					$query->where('jahit_date','>=',$from)->where('jahit_date','<=',$to);
				});
			});
		}

		//if pid is set
		if($id && $id > 0)
			$query = $query->where('id','=',$id);

		//SEARCH
		if($potong_id = Input::get('potong_id',false))
			$query = $query->where('potong_id','=',$potong_id);
		if($jahit_id = Input::get('jahit_id',false))
			$query = $query->where('jahit_id','=',$jahit_id);
		if($customer = Input::get('customer',false))
			$query = $query->where('customer','LIKE',"%$customer%");
		if($warna = Input::get('warna',false))
			$query = $query->where('warna','LIKE',"%$warna%");
		if($kode = Input::get('kode',false))
			$query = $query->where('temp_name','LIKE',"%$kode%");
		if($serial = Input::get('serial',false)) {
			$query = $query->where(function($query) use($serial) {
				$query->where('id','=',Produksi::fromSerial($serial))->orWhere('original_id','=',Produksi::fromSerial($serial));
			});
		}

		if($defaultStatus == Produksi::STATUS_PRODUKSI)
			$query = $query->where('status', '=', $defaultStatus);
		else {
			if($status = Input::get('status',false)) {
				if($status > 0)
					$query = $query->where('status','=',$status);
				else
					$query = $query->where('status', '!=', Produksi::STATUS_PRODUKSI);
			}
			else
				$query = $query->where('status', '!=', Produksi::STATUS_PRODUKSI);
		}

		if($invoice = Input::get('invoice',false))
			$query = $query->where('invoice','=', $invoice);

		$produksi = $query->orderBy($sort,$dir)->paginate(30);
		$response = array();

		$response['currentPage'] = $produksi->getCurrentPage();
		$response['lastPage'] = $produksi->getLastPage();
		$response['data'] = array();
		$index = 0;
		foreach ($produksi as $t) {
			$response['data'][$index] = $t->toArray();
			$response['data'][$index]['date'] = Dater::display($t->date);

			$response['data'][$index]['edit_link'] = $t->getEditLink();
			$response['data'][$index]['serial'] = $t->serial();
			$response['data'][$index]['original'] = $t->originalSerial();

			if(!empty($t->transaction_id)) {
				$response['data'][$index]['transaction_link'] = $t->transaction->getDetailLink();
			}
			$index++;
		}

		$this->_data['produksi'] = $response;
	}

	public function getCreateProduksi()
	{
		$this->layout->action = 'Create Produksi';
		$this->layout->content = View::make('produksi.form',$this->_data);
	}

	public function postCreateProduksi()
	{
		$date = Dater::toSQL(Input::get('date'));
		$potong = Input::get('potong');
		$potong = $potong['id'];

		//start transaction
		DB::connection()->getPdo()->beginTransaction();

		$data = Input::get('details');

		foreach($data as $d)
		{
			//skip empty code, quantity < 1
			if($d['quantity'] < 1 || empty($d['temp_name']))
					continue;

			$produksi = new Produksi($d);
			//set default values
			$produksi->potong_id = $potong;
			$produksi->potong_date = $date;
			$produksi->warna = strtoupper($produksi->warna);
			$produksi->customer = strtoupper($produksi->customer);
			$produksi->size_id = $d['size']['id'];

			if(!$produksi->save())
				throw new ModelException($produksi->getErrors(), __LINE__);
		}

		//commit db transaction
		DB::connection()->getPdo()->commit();

		Session::flash('success', 'Produksi created.');
		return Response::json(array(
			'url' => URL::action('ProduksiController@getIndex'),
		));
	}

	//assign jahit to row
	public function postSaveRow()
	{
		try {
		DB::connection()->getPdo()->beginTransaction();
		$data = Input::get('data');
		$split = false;

		//get the data from db
		$produksi = Produksi::with(array('item','potong','size','jahit'))->find($data['id']);

		if(!$produksi->jahit_date) //prevent hack edit
			$produksi->jahit_date = Dater::now()->toSQLFormat();
		if(!$produksi->jahit_id)
			$produksi->jahit_id = $data['jahit_form']['id'];

		if(!$produksi->save())
			throw new ModelException($produksi->getErrors());

		//response
		$response = array();
		$response['data'] = $produksi->toArray();
		$response['data']['serial'] = $produksi->serial();
		$response['data']['original'] = $produksi->originalSerial();
		$response['data']['jahit'] = Worker::find($produksi->jahit_id);
		$response['data']['size'] = $produksi->size;
		$response['success'] = true;
		$response['msg'] = 'serial: '.$produksi->serial().' updated';
		$response['split'] = $split;

		DB::connection()->getPdo()->commit();

		return Response::json($response);

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getEdit($id)
	{
		$p = Produksi::with(array('potong','size','jahit'))->findOrFail($id);

		$this->_data['produksi'] = $p->toArray();
		$this->_data['produksi']['serial'] = Produksi::toSerial($p->id);

		if(!empty($this->_data['produksi']['original_id']))
			$this->_data['produksi']['original_serial'] = Produksi::toSerial($p->original_id);

		//permission
		$this->_data['pisah_jahit'] = $this->_access->can('pisah-jahit',Apps::PRODUKSI);
		$this->_data['ganti_jahit'] = $this->_access->can('ganti-jahit',Apps::PRODUKSI);

		//condition
		$this->_data['can_pisah_jahit'] = false;
		if(empty($this->_data['produksi']['jahit']) || !isset($this->_data['produksi']['jahit']['name']))
			$this->_data['can_pisah_jahit'] = true;

		$this->_data['can_ganti_jahit'] = false;
		if(!empty($this->_data['produksi']['jahit']) || isset($this->_data['produksi']['jahit']['name']))
			$this->_data['can_ganti_jahit'] = true;

		$this->layout->action = 'Edit: '.$p->serial();
		$this->layout->content = View::make('produksi.edit', $this->_data);
	}

	//flag setor
	public function postSetor()
	{
		try {
		DB::connection()->getPdo()->beginTransaction();
		$data = Input::get('data');
		$produksi = Produksi::find($data['id']);
		if(!$produksi->setor_date)
		{
			$produksi->setor_date = Dater::now()->toSQLFormat();
			$produksi->status = Produksi::STATUS_SETOR;
		}
		if(!$produksi->save())
			throw new ModelException($produksi->getErrors());

		$response = array();
		$response['success'] = true;
		$response['msg'] = 'serial: '.$produksi->serial().' masuk setoran';

		DB::connection()->getPdo()->commit();
		return Response::json($response);
		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function postEdit($id)
	{
		DB::connection()->getPdo()->beginTransaction();

		$p = Produksi::findOrFail($id);
		$warna = Input::get('warna', false);
		$customer = Input::get('customer',false);
		if(empty($warna))
			$warna = $p->warna;
		if(empty($customer))
			$customer = $p->customer;

		//if more than 1 original
		if($p->original_id > 0) {
			$setorans = Produksi::where('original_id','=',$p->original_id)->get();
			foreach ($setorans as $s) {
				$s = Produksi::findOrFail($s->id);
				$s->warna = $warna;
				$s->customer = $customer;
				if(!$s->save()) {
					DB::connection()->getPdo()->rollback();
					return Redirect::back()->withInput()->withErrors($s->getErrors());
				}
			}
		}
		else {
			$p->warna = $warna;
			$p->customer = $customer;
			if(!$p->save()) {
				DB::connection()->getPdo()->rollback();
				return Redirect::back()->withInput()->withErrors($p->getErrors());
			}
		}
		DB::connection()->getPdo()->commit();
		return Redirect::action('ProduksiController@getIndex')->with('success', $p->serial().' edited');
	}

	public function postPisahJahit($id)
	{
		$produksi = Produksi::findOrFail($id);

		$split_q = Input::get('split_q');
		if(!$split_q || empty($split_q)) {
			Session::flash('success', 'Tidak ada yang dipisah.');
			return Redirect::action('ProduksiController@getEdit', array('id' => $id));
		}

		if($split_q >= $produksi->quantity) {
			Session::flash('error', 'Jumlah produksi salah.');
			return Redirect::action('ProduksiController@getEdit', array('id' => $id));
		}

		DB::connection()->getPdo()->beginTransaction();

		//create new instance
		$new = $produksi->replicate();
		$new->quantity = $split_q;

		//save original id
		if($produksi->original_id)
			$new->original_id = $produksi->original_id;
		else {
			$new->original_id = $produksi->id;
			$produksi->original_id = $produksi->id;
		}

		$produksi->quantity = $produksi->quantity - $new->quantity;

		if(!$new->save()) {
			DB::connection()->getPdo()->rollback();
			Session::flash('error', 'error saving new produksi');
			return Redirect::action('ProduksiController@getEdit', array('id' => $id));
		}

		if(!$produksi->save()) {
			DB::connection()->getPdo()->rollback();
			Session::flash('error', 'error saving old produksi');
			return Redirect::action('ProduksiController@getEdit', array('id' => $id));
		}

		Session::flash('success', $new->serial().' created.');

		DB::connection()->getPdo()->commit();
		return Redirect::action('ProduksiController@getIndex');		
	}

	public function postGantiJahit($id)
	{
		$produksi = Produksi::findOrFail($id);

		$jahit_id = Input::get('jahit_id');
		if(!$jahit_id || empty($jahit_id)) {
			Session::flash('error', 'bukan penjahit 1.');
			return Redirect::action('ProduksiController@getEdit', array('id' => $id));
		}

		//check if valid penjahit
		$valid = Worker::where('type', '=', Worker::TYPE_JAHIT)->where('id', '=', $jahit_id)->first();
		if(!$valid) {
			Session::flash('error', 'bukan penjahit 1.');
			return Redirect::action('ProduksiController@getEdit', array('id' => $id));
		}

		DB::connection()->getPdo()->beginTransaction();

		$produksi->jahit_id = $jahit_id;
		if(!$produksi->save()) {
			DB::connection()->getPdo()->rollback();
			Session::flash('error', 'error saving old produksi');
			return Redirect::action('ProduksiController@getEdit', array('id' => $id));
		}

		Session::flash('success', 'produksi edited.');

		DB::connection()->getPdo()->commit();
		return Redirect::action('ProduksiController@getIndex');		
	}

	public function getPotongList()
	{
		$this->layout->action = 'Potong List';
		$this->_data['workers'] = Worker::potong()->withTrashed()->get();
		$this->_data['edit_action'] = 'getEditPotong';
		$this->_data['delete_action'] = 'postDeletePotong';
		$this->_data['restore_action'] = 'postRestorePotong';
		$this->layout->content = View::make('produksi.workerlist',$this->_data);
	}

	public function getJahitList()
	{
		$this->layout->action = 'Jahit List';
		$this->_data['workers'] = Worker::jahit()->withTrashed()->orderBy('deleted_at','ASC')->get();
		$this->_data['edit_action'] = 'getEditJahit';
		$this->_data['delete_action'] = 'postDeleteJahit';
		$this->_data['restore_action'] = 'postRestoreJahit';
		$this->layout->content = View::make('produksi.workerlist',$this->_data);
	}

	public function getCreatePotong()
	{
		$this->layout->action = 'New Potong';
		$this->layout->content = View::make('produksi.workerform',$this->_data);
	}

	public function getCreateJahit()
	{
		$this->layout->action = 'New Jahit';
		$this->layout->content = View::make('produksi.workerform',$this->_data);
	}

	public function postCreatePotong()
	{
		return $this->createWorker(Worker::TYPE_POTONG, 'getPotongList');
	}

	public function postCreateJahit()
	{
		return $this->createWorker(Worker::TYPE_JAHIT, 'getJahitList');
	}

	protected function createWorker($type, $action)
	{
		$input = Input::get('name');

		$w = new Worker;
		$w->name = trim($input);
		$w->type = $type;
		if(!$w->save())
		{
			pre($w->getErrors());exit;
			return Redirect::back()->withInput()->withErrors($w->getErrors());
		}

		return Redirect::action('ProduksiController@'.$action)->with('success', $w->name.' created');
	}

	public function getEditPotong($id)
	{
		$w = Worker::findOrFail($id);
		InputForm::load($w->name,'name');
		$this->layout->action = 'Edit Potong';
		$this->layout->content = View::make('produksi.workerform',$this->_data);
	}

	public function getEditJahit($id)
	{
		$w = Worker::findOrFail($id);
		InputForm::load($w->name,'name');
		$this->layout->action = 'Edit Jahit';
		$this->layout->content = View::make('produksi.workerform',$this->_data);
	}

	public function postEditPotong($id)
	{
		return $this->editWorker($id, Worker::TYPE_JAHIT, 'getPotongList');
	}

	public function postEditJahit($id)
	{
		return $this->editWorker($id, Worker::TYPE_JAHIT, 'getJahitList');
	}

	protected function editWorker($id, $type, $action)
	{
		$input = Input::get('name');

		$w = Worker::findOrFail($id);
		$w->name = trim($input);
		if(!$w->save())
		{
			pre($w->getErrors());exit;
			return Redirect::back()->withInput()->withErrors($w->getErrors());
		}

		return Redirect::action('ProduksiController@'.$action)->with('success', $w->name.' edited');
	}

	public function postDeleteJahit($id)
	{
		$w = Worker::where('id', '=', $id)->where('type', '=', Worker::TYPE_JAHIT)->first();
		if(!$w)
			return App::abort(404);
		$w->delete();

		return Redirect::action('ProduksiController@getJahitList')->with('success', $w->name.' deleted');
	}

	public function postDeletePotong($id)
	{
		$w = Worker::where('id', '=', $id)->where('type', '=', Worker::TYPE_POTONG)->first();
		if(!$w)
			return App::abort(404);
		$w->delete();

		return Redirect::action('ProduksiController@getPotongList')->with('success', $w->name.' deleted');
	}

	public function postRestoreJahit($id)
	{
		$w = Worker::where('id', '=', $id)->where('type', '=', Worker::TYPE_JAHIT)->withTrashed()->first();
		if(!$w)
			return App::abort(404);
		$w->restore();

		return Redirect::action('ProduksiController@getJahitList')->with('success', $w->name.' restored');
	}

	public function postRestorePotong($id)
	{
		$w = Worker::where('id', '=', $id)->where('type', '=', Worker::TYPE_POTONG)->withTrashed()->first();
		if(!$w)
			return App::abort(404);
		$w->restore();

		return Redirect::action('ProduksiController@getPotongList')->with('success', $w->name.' restored');
	}

	public function postDelete()
	{
		$id = Input::get('id');

		//only for admin
		$p = Produksi::where('id','=',$id)->first();
		if($p->status >= Produksi::STATUS_GUDANG)
			return Response::json(array('error' => true,'msg' => 'sudah turun tidak bisa dihapus'));

		$p->delete();

		return Response::json(array('success' => true,'msg' => 'Kitir '.$p->serial().' deleted'));
	}

	public function postRestore()
	{
		$id = Input::get('id');
		//only for admin
		$p = Produksi::where('id','=',$id)->withTrashed()->first();
		$p->restore();

		return Response::json(array('msg' => 'Kitir '.$p->serial().' restored'));
	}
}