<?
use App\Models\UserSetting;
use App\Models\Customer, App\Models\User;
class ProfileController extends BaseController
{
	public function getIndex()
	{
		$this->layout->action = 'Set Defaults';
		$result = UserSetting::where('user_id','=',$this->_data['_user']->id)->get();
		$this->_data['settings'] = array();
		foreach ($result as $key => $value) {
			$row = array();
			$row['name'] = $value->name;
			$row['value'] = $value->value;
			$this->_data['settings'][$value->name] = $row;
		}

  	$this->_data['account'] = Customer::filter()->where('type','=',Customer::TYPE_BANK)->orderBy('name','asc')->get();
  	$this->_data['warehouse'] = Customer::filter()->where('type','=',Customer::TYPE_WAREHOUSE)->get();

		$this->layout->content = View::make('profile.index',$this->_data);
	}

	public function postIndex()
	{
		try {
		$this->_data['settings'] = UserSetting::lists('value','name');

		DB::connection()->getPdo()->beginTransaction();
		$input = Input::get('settings');

		foreach($this->_data['settings'] as $name => $value)
		{
			if(!isset($input[$name]))
				UserSetting::where('name','=',$name)->where('user_id','=',$this->_data['_user']->id)->update(array('value' => 0));
			else
			{
				if($input[$name]['value']['id'] == $this->_data['settings'][$name]) continue;
				UserSetting::where('name','=',$name)->where('user_id','=',$this->_data['_user']->id)->update(array('value' => $input[$name]['value']['id']));
			}
		}
		DB::connection()->getPdo()->commit();

/*		$password = '';
		if(Input::get('update_password',false))
		{
			$user = User::find($this->_data['_user']->id);
			$password = User::generatePassword();
			$user->password = Hash::make($password);
			if(!$user->save())
				return Redirect::back()->with('error', 'sum ting wong, cannot update password')->withInput();
			$password = '<br/>Password: '.$password;
		}*/

		//bust cache
		Cache::forget('user_settings.'.$this->_data['_user']->id);
		return Response::json(array(
			'success' => true,
			'title' => 'Attention',
			'msg' => array('Default Banks and Warehouses Updated'),
		));
		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function postPassword()
	{
	}
}