<?
use App\Models\Transaction;
use App\Models\Customer, App\Models\Operation;
use App\Models\Settings,App\Models\Location,App\Models\CustomerClass;
use App\Libraries\RecordManager, App\Models\WarehouseItem, App\Models\Item, App\Libraries\ItemsManager;
use App\Models\CustomerStat;
use App\Models\Depreciation, App\Models\ItemTag, App\Models\TransactionDetail;
use App\Libraries\StatManager, App\Libraries\CCManager;
use App\Libraries\GeoManager, App\Models\Province, App\Models\City;
class ReportsController extends BaseController
{
	public function getProfitLoss()
	{
		$dates = $this->getMonthYear(Dater::now());

		$rm = new RecordManager;
		$this->_data['pl'] = $rm->getProfitLoss($dates['thisMonth'], $dates['thisYear']);
		$this->_data['compare'] = $rm->getProfitLoss($dates['lastMonth'], $dates['lastYear']);

		$ops = Operation::lists('name','id');
		$this->_data['operations'] = array();
		foreach($ops as $id => $name) {
			$this->_data['operations'][] = array('id' => $id, 'name' => $name);
		}

		$this->_data['compareDate'] = array('month' => $dates['lastMonth'], 'year' => $dates['lastYear']);

		$this->layout->action = 'Profit Loss';
    $this->layout->content = View::make('reports.profitloss',$this->_data);
	}

	public function postProfitLoss()
	{
		$input = Input::get('dates', null);
		if(!$input) return Response::json(array());
		$rm = new RecordManager;
		$pl = $rm->getProfitLoss($input['month'], $input['year']);
		return Response::json($pl);
	}

	public function getRevenue()
	{
		$month = Input::get('month',null);
		if($month)
		{
			$date = Dater::inputMY($month);
			$this->_data['links']['from'] = $date->startOfMonth()->format(Dater::$format);
			$this->_data['links']['to'] = $date->endOfMonth()->format(Dater::$format);
		}
		else
		{
			$date = Dater::now();
			$this->_data['links']['from'] = $date->startOfMonth()->format(Dater::$format);
			$this->_data['links']['to'] = $date->endOfMonth()->format(Dater::$format);
		}

		$from = Dater::toSQL($this->_data['links']['from']);
		$to = Dater::toSQL($this->_data['links']['to']);

		$cc = CustomerClass::table();
		$c = Customer::table();
		$data = DB::table($cc)->select(array(
			'date', "$c.type", "$c.category",
			DB::raw('SUM(cash_in) as total_income'),
			DB::raw('SUM(sell) as total_sell'),
			DB::raw('SUM(cash_out) as total_expense'),
			DB::raw('SUM(`return`) as total_return'),
		))->where('date','=',$from)->join($c,"$c.id",'=',"$cc.customer_id")->groupBy('type')->groupBy('category')->get();
		$this->_data['this_month'] = array();
		foreach(Customer::$types as $key => $val)
			$this->_data['this_month'][$key] = array();
		foreach($data as $d)
		{
			if($d->type == Customer::TYPE_CUSTOMER || $d->type == Customer::TYPE_RESELLER)
				$this->_data['this_month'][$d->type][$d->category] = $d;
			else
				$this->_data['this_month'][$d->type] = $d;
		}

/*
		$lastMonth = DB::table($cc)->select(array(
			'month', 'year', "$c.type", "$c.category",
			DB::raw('SUM(income) as total_income'),
			DB::raw('SUM(sell) as total_sell'),
			DB::raw('SUM(expense) as total_expense'),
			DB::raw('SUM(`return`) as total_return'),
		))->where('month','=',$dates['lastMonth'])->where('year','=',$dates['lastYear'])->join($c,"$c.id",'=',"$cc.customer_id")->groupBy('type')->groupBy('category')->get();
		$this->_data['last_month'] = array();
		foreach($lastMonth as $d)
		{
			if(!isset($this->_data['last_month'][$d->type])) $this->_data['last_month'][$d->type] = array();
			if($d->type == Customer::TYPE_CUSTOMER || $d->type == Customer::TYPE_RESELLER)
				$this->_data['last_month'][$d->type][$d->category] = $d;
			else
				$this->_data['last_month'][$d->type] = $d;
		}
*/
		$this->layout->action = 'Revenue From: '.$this->_data['links']['from'].' to '.$this->_data['links']['to'];
        $this->layout->content = View::make('reports.customers',$this->_data);
	}

	//run this
	//UPDATE `customer_class` join `customers` on `customers`.`id` = `customer_class`.`customer_id` SET `customer_type`=`customers`.`type`
	public function getCashFlow($loc_id = Settings::LOC_GLOBAL)
	{
		$month = Input::get('month',null);
		if($month)
		{
			$date = Dater::inputMY($month);
			$this->_data['links']['from'] = $date->startOfMonth()->format(Dater::$format);
			$this->_data['links']['to'] = $date->endOfMonth()->format(Dater::$format);
		}
		else
		{
			$date = Dater::now();
			$this->_data['links']['from'] = $date->startOfMonth()->format(Dater::$format);
			$this->_data['links']['to'] = $date->endOfMonth()->format(Dater::$format);
		}

		$from = Dater::toSQL($this->_data['links']['from']);
		$to = Dater::toSQL($this->_data['links']['to']);

		$cc = CustomerClass::table();
		$data = DB::table($cc)->select(array(
			'date', "customer_type",
			DB::raw('SUM(cash_in) as total_income'),
			DB::raw('SUM(sell) as total_sell'),
			DB::raw('SUM(cash_out) as total_expense'),
			DB::raw('SUM(buy) as total_buy'),
		))->where('date','=',$from)->groupBy('customer_type')->remember(5)->get();
		$this->_data['cf'] = array();
		foreach(Customer::$types as $key => $val)
			$this->_data['cf'][$key] = array();
		foreach($data as $d)
		{
			$this->_data['cf'][$d->customer_type] = $d;
		}

		$dates = $this->getMonthYear($date);
		$rm = new RecordManager;
		$this->_data['pl'] = $rm->getProfitLoss($dates['thisMonth'], $dates['thisYear'],$loc_id);

		$this->_data['operations'] = Operation::lists('name','id');
		$this->layout->action = 'Cash Flow From: '.$this->_data['links']['from'].' to '.$this->_data['links']['to'];
    $this->layout->content = View::make('reports.cashflow',$this->_data);
	}

	protected function getMonthYear($date)
	{
		$thisMonth = $date->month;
		$thisYear = $date->year;
		$date->subMonth();
		$lastMonth = $date->month;
		$lastYear = $date->year;
		if($lastMonth == $thisMonth) $lastMonth--;
		if($lastMonth < 1)
		{
			$lastMonth = 12;
			$lastYear--;
		}
		return array('thisMonth' => $thisMonth, 'thisYear' => $thisYear, 'lastMonth' => $lastMonth, 'lastYear' => $lastYear);
	}

	public function getAspc()
	{
		$month = Input::get('month',null);
		if($month)
			$date = Dater::inputMY($month);
		else
			$date = Dater::now();

		$dates = $this->getMonthYear($date);

		$rm = new RecordManager;
		$this->_data['this_month'] = $rm->getProfitLoss($dates['thisMonth'], $dates['thisYear'],Settings::LOC_GLOBAL);

		$wi = WarehouseItem::table();
		$i = Item::table();
		$c = Customer::table();
		$this->_data['asset'] = DB::table($wi)->select(array(
			DB::raw("SUM($wi.quantity * $i.price) as total_asset"),
			"$c.name",
			"$wi.warehouse_id"
		))->join($c,"$c.id",'=',"$wi.warehouse_id")->where("$c.type",'=',Customer::TYPE_WAREHOUSE)->join($i,"$i.id",'=',"$wi.item_id")->groupBy("$wi.warehouse_id")->get();

		$cs = CustomerStat::table();
		$cash = DB::table($cs)->select(array(
			DB::raw("SUM($cs.balance) as total_cash"),
		))->join($c,"$c.id",'=',"$cs.customer_id")->where("$c.type",'=',Customer::TYPE_ACCOUNT)->first();

		$this->_data['cash'] = $cash->total_cash;
		$this->layout->content = View::make('reports.aspc',$this->_data);
	}


	public function getGeo()
	{
		$this->_data['provinceData'] = GeoManager::getData(Customer::TYPE_RESELLER);

		$this->_data['provinces'] = GeoManager::getProvincesArray();
		$this->_data['customerTypes'] = array(
			array('id' => Customer::TYPE_RESELLER, 'name' => 'Reseller'),
			array('id' => Customer::TYPE_CUSTOMER, 'name' => 'Customer'),
			array('id' => Customer::TYPE_WAREHOUSE, 'name' => 'Warehouse'),
			array('id' => Customer::TYPE_BANK, 'name' => 'Banks'),
			array('id' => Customer::TYPE_SUPPLIER, 'name' => 'Supplier'),
		);

		$this->_data['defaultData'] = array(
			'customerType' => Customer::TYPE_RESELLER,
		);

		$this->layout->content = View::make('reports.geo',$this->_data);
	}


	public function getTag()
	{
		$tagId = 6444;
		$detailTable = TransactionDetail::table();
		$tagTable = ItemTag::table();
		$itemTable = Item::table();
		$transactionTable = Transaction::table();

		//get the current month
		$start = Dater::createMY(3,2015)->startOfMonth()->format(Dater::$SQLFormat);
		$stop = Dater::createMY(3,2015)->endOfMonth()->format(Dater::$SQLFormat);

		//load
		$data = DB::table($tagTable)->select(array(
			DB::raw("SUM($detailTable.total) as total_amount"),
			DB::raw("SUM($detailTable.quantity) as total_items"),
			"$itemTable.name as itemName",
			"$detailTable.price as detailPrice", //todo: make average!
//			"$transactionTable.type as transactionType",
		))->where('tag_id', '=', $tagId);
		$data = $data->join($itemTable,"$itemTable.id",'=',"$tagTable.item_id");
		$data = $data->join($detailTable, "$detailTable.item_id", '=', "$itemTable.id");
		$data = $data->join($transactionTable,"$transactionTable.id", '=', "$detailTable.transaction_id")->where("$transactionTable.date", '>=', $start)->where("$transactionTable.date", '<=', $stop)->where("$transactionTable.type",'=',2)->groupBy("itemName")->get();

		$this->_data['data'] = $data;
		$this->layout->content = View::make('reports.tags', $this->_data);
	}

	public function getBalance()
	{
		$data = array();

		//init
		$cs_table = CustomerStat::table();
		$c_table = Customer::table();

		//1. query for bank balance
		$data['kas'] = Customer::with('stat')->where('type', '=', Customer::TYPE_BANK)->get();
		//2. piutang
		//2.1 piutang reseller
		$data['piutang_reseller'] = Customer::with('stat')->where('type', '=', Customer::TYPE_RESELLER)->get();
		//2.2 piutang customer
		$data['piutang_customer'] = Customer::with('stat')->where('type', '=', Customer::TYPE_CUSTOMER)->get();

		//asset tetap
		$query = Item::with('depreciation')->where('type','=',Item::TYPE_ASSET_TETAP)->get();
		$data['asset_tetap'] = ItemsManager::toArray($query);
		$data['asset_tetap'] = $data['asset_tetap']['data'];

		//supplier
		$data['hutang_usaha'] = Customer::with('stat')->where('type', '=', Customer::TYPE_SUPPLIER)->get();

		//display
		$this->_data['data'] = $data;
		$this->layout->content = View::make('reports.balance', $this->_data);
	}
}