<?
use App\Models\Customer,App\Models\CustomerStat,App\Models\Location,App\Models\CustomerClass;
use App\Libraries\GeoManager;
class ResellersController extends CustomersController
{
	protected $type = Customer::TYPE_RESELLER;
	protected $action = 'ResellersController';
	protected $folder = 'resellers';

	protected function loadIndex()
	{
		$cs = CustomerStat::table();
		$cc = CustomerClass::table();
		$c = Customer::table();
		if(!$sort = Input::get('sort',false)) $sort = $c.'.name';
		if(!$dir = Input::get('dir',false)) $dir = 'asc';

		switch($sort)
		{
			case 'balance': $sort = $cs.'.balance'; break;
			case 'name': $sort = $c.'.name'; break;
			case 'sell': $sort = $cc.'.sell'; break;
			case 'return': $sort = $cc.'.return'; break;
			default: break;
		}

		if(!$this->_data['_user']->location_id)
		{
			$location = Input::get('location',null);
			$this->_data['locations'] = Location::lists('name','id');
			$this->_data['locations'][0] = '---';
			ksort($this->_data['locations']);
		}
		else
		{
			$location = null;
			$this->_data['locations'] = false;
		}

		$query = Customer::select("$c.*","$cs.balance","$cc.sell","$cc.return")->with('locations')->filter($location)->where('type','=',$this->type);
		$query = $query->join($cs,$c.'.id','=',$cs.'.customer_id');
		$date = Dater::now()->startOfMonth()->format(Dater::$SQLFormat);
		$query = $query->join($cc,"$c.id",'=',"$cc.customer_id")->where("$cc.date",'=',$date);

		if($name = Input::get('name',false)) {
			$name = str_replace(' ', '%', $name);
			$query = $query->where('name','LIKE',"%$name%");
		}

		$this->_data['customers'] = $query->orderBy($sort,$dir)->paginate(30);
		$this->_data['action'] = $this->action;

		if($deleted = Input::get('deleted', false))
			$query = $query->onlyTrashed();

		$customers = $query->orderBy($sort,$dir)->paginate(50);

		//format data for json
		$response = array();
		$response['currentPage'] = $customers->getCurrentPage();
		$response['lastPage'] = $customers->getLastPage();
		$response['data'] = array();
		foreach ($customers as $index => $t) {
			$response['data'][$index]['id'] = $t->id;
			$response['data'][$index]['name'] = $t->name;
			$response['data'][$index]['discount'] = $t->discount;
			$response['data'][$index]['balance'] = $t->balance;
			$response['data'][$index]['sell'] = $t->sell;
			$response['data'][$index]['return'] = $t->return;
			$response['data'][$index]['detail_link'] = $t->getDetailLink();
			$response['data'][$index]['edit_link'] = $t->getEditLink();
			$response['data'][$index]['locations'] = array();
			foreach($t->locations as $locationIndex => $l) {
				$response['data'][$index]['locations'][$locationIndex]['link'] = \URL::action('LocationsController@getDetail',array($l->id));
				$response['data'][$index]['locations'][$locationIndex]['name'] = $l->name;
			}
		}

		$this->_data['responseJSON'] = $response;
		$this->_data['customers'] = Response::json($response)->getData();
	}

	public function getIndex()
	{
		$this->loadIndex();
		$this->layout->action = 'List';
		$this->layout->content = View::make($this->folder.'.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadIndex();
		return Response::json($this->_data['responseJSON']);
	}
}