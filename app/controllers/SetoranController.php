<?
use App\Models\ProduksiTransaction, App\Models\TransactionDetail;
use App\Models\Produksi;
use App\Libraries\ItemsManager;
class SetoranController extends ProduksiController
{
	const WAREHOUSE = 2343;
	public function getIndex($id = false)
	{
		$this->loadProduksiList(Produksi::STATUS_SETOR, $id);
		$this->layout->action = 'Barang sudah setor';
		$this->_data['statusJSON'] = json_encode(Produksi::$statusJSON);
		$this->_data['can_edit_item'] = $this->_access->can('edit_item',Apps::SETORAN);
		$this->_data['can_gudang'] = $this->_access->can('gudang',Apps::SETORAN);
		$this->_data['can_delete'] = $this->_access->can('delete', Apps::SETORAN);
		$this->layout->content = View::make('setoran.index', $this->_data);
	}

	public function postIndex()
	{
		$this->loadProduksiList(Produksi::STATUS_SETOR);
		return Response::json($this->_data['produksi']);
	}

	//function to edit item name
	public function postEditItem()
	{
		try {
		DB::connection()->getPdo()->beginTransaction();
		$data = Input::get('data');

		//get the data from db
		$produksi = Produksi::with(array('item','potong','size','jahit'))->find($data['id']);

		//save only the edited parts
		if(!$produksi)
			throw new ModelException('Setoran tidak ada');

		//also update siblings
		$siblingsIds = array();
		if(!empty($data['item']['id'])) {
			$produksi->item_id = $data['item']['id'];
			//check for original
			if($produksi->original_id > 0) {
				//find siblings
				$siblings = Produksi::where('original_id','=',$produksi->original_id)->where('id', '!=', $produksi->id)->get();
				if(!empty($siblings)) {
					foreach ($siblings as $s) {
						//check, sudah turun, skip
						if(!empty($s->invoice)) continue;
						$s->item_id = $produksi->item_id;
						if(!$s->save())
							throw new ModelException($s->getErrors());
						$siblingsIds[] = Produksi::toSerial($s->id);
					}
				}
			}
		}

		if(!$produksi->save())
			throw new ModelException($produksi->getErrors());

		//response
		$response = array();
		$response['msg'] = 'serial: '.$produksi->serial().' updated';
		$response['siblings'] = $siblingsIds;
		if(count($siblingsIds) > 0)
			$response['msg'] .= ', '.implode(', ', $siblingsIds).' juga di update';

		DB::connection()->getPdo()->commit();

		return Response::json($response);

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	//function to store item to gudang
	public function postGudang()
	{
		try {
		DB::connection()->getPdo()->beginTransaction();
		$produksiId = Input::get('id');
		$invoice = Input::get('invoice');

		if(!$invoice)
			throw new ModelException('Invoice tidak boleh kosong');

		//get the data from db
		$produksi = Produksi::with(array('item','potong','size','jahit'))->find($produksiId);

		//save only the edited parts
		if(!$produksi)
			throw new ModelException('Kitir tidak ada');

		if(empty($produksi->item_id))
			throw new ModelException('Belum ada item');

		if($produksi->transaction_id > 0)
			throw new ModelException('Sudah ada di gudang');

		//save to transaction
		if(!$transaction = ProduksiTransaction::where('invoice','=',$invoice)->where('type','=',ProduksiTransaction::TYPE_PRODUCTION)->first())
		{
			$transaction = new ProduksiTransaction;
			$transaction->date = Dater::now()->toSQLFormat();
			$transaction->init(ProduksiTransaction::TYPE_PRODUCTION);
			$transaction->receiver_id = self::WAREHOUSE; //gudang new
			$transaction->invoice = $invoice;
			$transaction->total_items = 0;
			$transaction->detail_ids = '';

			//gets the transaction id
			if(!$transaction->save())
				throw new ModelException($transaction->getErrors());
		}

		//save to transaction detail
		$detail = new TransactionDetail;
		$detail->transaction_id = $transaction->id;
		$detail->item_id = $produksi->item->id;
		$detail->quantity = $produksi->quantity;
		$detail->date = $transaction->date;
		$detail->transaction_type = ProduksiTransaction::TYPE_PRODUCTION;
		$detail->sender_id = 0;
		$detail->receiver_id = self::WAREHOUSE;
		if(!$detail->save())
			throw new ModelException('Error saving transaction detail');

		//add to warehouse item
		ItemsManager::add($detail->receiver_id, $produksi->item, $produksi->quantity);

		//store transaction id
		$produksi->status = Produksi::STATUS_GUDANG;
		$produksi->transaction_id = $transaction->id;
		$produksi->detail_id = $detail->id;
		$produksi->invoice = $invoice;
		$produksi->gudang_date = Dater::now()->toSQLFormat();
		if(!$produksi->save())
			throw new ModelException($produksi->getErrors());

		//update transaction quantity
		$transaction->total_items += $produksi->quantity;
		$detail_ids = array_filter(explode(',', $transaction->detail_ids));
		$detail_ids[] = $detail->id;
		$transaction->detail_ids = trim(implode(',',$detail_ids));
		if(!$transaction->save())
			throw new ModelException($transaction->getErrors());

		//response
		$response = array();
		$response['msg'] = 'serial: '.$produksi->serial().' sudah masuk transaksi';
		$response['data']['transaction_link'] = $transaction->getDetailLink();
		$response['data']['invoice'] = $invoice;
		$response['data']['status'] = $produksi->status;

		DB::connection()->getPdo()->commit();

		return Response::json($response);
		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getEdit($id)
	{
		$p = Produksi::with(array('potong','size','jahit'))->findOrFail($id);

		$this->_data['produksi'] = $p->toArray();
		$this->_data['produksi']['serial'] = Produksi::toSerial($p->id);

		if(!empty($this->_data['produksi']['original_id']))
			$this->_data['produksi']['original_serial'] = Produksi::toSerial($p->original_id);

		$this->layout->action = 'Edit: '.$p->serial();
		$this->_data['edit_jahit'] = $this->_access->can('edit-jahit',Apps::SETORAN);
		$this->layout->content = View::make('setoran.edit', $this->_data);
	}

	public function postEdit($id)
	{
		DB::connection()->getPdo()->beginTransaction();

		$p = Produksi::findOrFail($id);
		$warna = Input::get('warna', false);
		$customer = Input::get('customer',false);
		if(empty($warna))
			$warna = $p->warna;
		if(empty($customer))
			$customer = $p->customer;

		//if more than 1 original
		if($p->original_id > 0) {
			$setorans = Produksi::where('original_id','=',$p->original_id)->get();
			foreach ($setorans as $s) {
				$s = Produksi::findOrFail($s->id);
				$s->warna = $warna;
				$s->customer = $customer;
				if(!$s->save()) {
					DB::connection()->getPdo()->rollback();
					return Redirect::back()->withInput()->withErrors($s->getErrors());
				}
			}
		}
		else {
			$p->warna = $warna;
			$p->customer = $customer;
			if(!$p->save()) {
				DB::connection()->getPdo()->rollback();
				return Redirect::back()->withInput()->withErrors($p->getErrors());
			}
		}
		DB::connection()->getPdo()->commit();
		return Redirect::action('SetoranController@getIndex')->with('success', $p->serial().' edited');
	}

	public function postEditJahit($id)
	{
		$p = Produksi::findOrFail($id);
		$jahit_id = Input::get('jahit_id', false);

		$p->jahit_id = $jahit_id;
		if(!$p->save())
			return Redirect::back()->withInput()->withErrors($p->getErrors());

		return Redirect::action('SetoranController@getIndex')->with('success', $p->serial().' edited');
	}

}