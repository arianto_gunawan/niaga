<?
use App\Models\AppSetting, App\Models\Customer;
class SettingsController extends BaseController
{
	public function getIndex()
	{
		$this->_appSettings->flush();
		$this->_data['settings'] = $this->_appSettings->getAll();

		$ids = array();
		foreach ($this->_data['settings'] as $key => $value) {
			if(is_numeric($value))
				$ids[] = $value;
		}

		$this->_data['sell'] = Customer::find($this->_data['settings']['sell_100']);
		$this->_data['ongkir'] = Customer::find($this->_data['settings']['ongkir']);

		$this->layout->action = 'Edit App Settings';
		$this->layout->content = View::make('settings.index',$this->_data);
	}

	public function postIndex()
	{
		try {
		$this->_data['settings'] = $this->_appSettings->getAll();

		$input = Input::get('settings');
		if(!$this->_appSettings->edit($input))
			throw new \Exception('Unable to save settings');

		return Response::json(array(
			'title' => 'success',
			'msg' => array('Settings Updated'),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}

	}
}