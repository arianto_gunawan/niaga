<?
use App\Models\Spg\User as SPG, App\Models\Customer;
use App\Models\User;
class SpgController extends BaseController
{
	public function __construct()
	{
		parent::__construct();

		//load jahit
		$this->_data['stores'] = Customer::where('type','=',Customer::TYPE_WAREHOUSE)->get();
	}

	public function getIndex()
	{
		//init the query
		$spg = SPG::with(array('warehouse'))->paginate(30);
		$response = array();

		$response['currentPage'] = $spg->getCurrentPage();
		$response['lastPage'] = $spg->getLastPage();
		$response['data'] = array();
		$index = 0;
		foreach ($spg as $t) {
			$response['data'][$index]['name'] = $t->name;
			$response['data'][$index]['username'] = $t->username;
			$response['data'][$index]['warehouse_name'] = $t->warehouse->name;
			$response['data'][$index]['warehouse_link'] = $t->warehouse->getTransactionsLink();
			$response['data'][$index]['edit_link'] = $t->getEditLink();
			$index++;
		}

		$this->_data['can_delete'] = $this->_access->can('delete', Apps::SPG);
		$this->_data['can_assign'] = $this->_access->can('delete', Apps::SPG);
		$this->_data['spg'] = $response;
		$this->layout->action = 'Daftar SPG';
		$this->layout->content = View::make('spg.index',$this->_data);
	}

	public function getCreate()
	{
		$this->layout->action = 'Create SPG';
		$this->_data['spg'] = array();
		$this->_data['update'] = false;
		$this->layout->content = View::make('spg.form',$this->_data);
	}

	public function postCreate()
	{
		try{
		DB::connection()->getPdo()->beginTransaction();

		$id = Input::get('warehouse_id',false);
		$warehouse = Customer::where('id', '=', $id)->where('type', '=', Customer::TYPE_WAREHOUSE)->first();
		if(!$warehouse)
			return App::abort(404);

		if(!$username = Input::get('username',false))
			throw new Exception('Username is required', 1);

		if(!$name = Input::get('name',false))
			throw new Exception('Name is required', 1);

		$user = new SPG;
		$user->warehouse_id = $warehouse->id;
		$user->username = $username;
		$user->name = ucfirst($name);

		//new pass
		$password = User::generatePassword();
		$user->password = \Hash::make($password);
		$user->active = 1;
		if(!$user->save()) {
			throw new ModelException($user->getErrors(), __LINE__);
		}

		DB::connection()->getPdo()->commit();

		Session::flash('success', 'Username: '.$user->username.'  --  Password:'.$password);
		return Response::json(array(
			'url' => URL::action('SpgController@getIndex'),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getEdit($id)
	{
		$this->layout->action = 'Edit SPG';
		$spg = SPG::findOrFail($id);
		$this->_data['spg'] = $spg->toArray();
		$this->_data['spg']['password'] = null;
		$this->_data['update'] = false;
		$this->layout->content = View::make('spg.form',$this->_data);
	}

	public function postEdit($id)
	{

	}

	//assign jahit to row
	public function postAssign()
	{
		try {
		DB::connection()->getPdo()->beginTransaction();
		$data = Input::get('data');
		$split = false;

		//get the data from db
		$produksi = Produksi::with(array('item','potong','size','jahit'))->find($data['id']);

		if(!$produksi->jahit_date) //prevent hack edit
			$produksi->jahit_date = Dater::now()->toSQLFormat();
		if(!$produksi->jahit_id)
			$produksi->jahit_id = $data['jahit_form']['id'];

		if(!$produksi->save())
			throw new ModelException($produksi->getErrors());

		//response
		$response = array();
		$response['data'] = $produksi->toArray();
		$response['data']['serial'] = $produksi->serial();
		$response['data']['original'] = $produksi->originalSerial();
		$response['data']['jahit'] = Worker::find($produksi->jahit_id);
		$response['data']['size'] = $produksi->size;
		$response['success'] = true;
		$response['msg'] = 'serial: '.$produksi->serial().' updated';
		$response['split'] = $split;

		DB::connection()->getPdo()->commit();

		return Response::json($response);

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function postDelete()
	{
		$id = Input::get('id');

		//only for admin
		$p = SPG::where('id','=',$id)->first();
		$p->active = SPG::STATUS_OFF;
		if(!$p->save())
			return Response::json(array('error' => true,'msg' => 'spg sudah dihapus'));

		$p->delete();

		return Response::json(array('success' => true,'msg' => 'SPG '.$p->name.' deleted'));
	}

	public function postRestore()
	{
		$id = Input::get('id');
		//only for admin
		$p = SPG::where('id','=',$id)->withTrashed()->first();
		$p->restore();

		$p->active = SPG::STATUS_ACTIVE;
		if(!$p->save())
			return Response::json(array('error' => true,'msg' => 'spg sudah active'));

		return Response::json(array('msg' => 'SPG '.$p->name.' restored'));
	}
}