<?
use App\Models\Customer;
class SuppliersController extends CustomersController
{
	protected $type = Customer::TYPE_SUPPLIER;
	protected $action = 'SuppliersController';
}