<?
use App\Models\Tag,App\Models\ItemTag;
use App\Models\Transaction,App\Models\TransactionDetail,App\Models\Item;
use App\Libraries\ItemsManager;
class TagsController extends BaseController
{
	public function __construct()
	{
		BaseController::__construct();
		$this->_data['typesJSON'] = json_encode(Tag::$typesJSON);
	}

	public function getIndex()
	{
		$this->loadIndex();

		$this->layout->action = 'Tags List';
		$this->layout->content = View::make('tags.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadIndex();
		return Response::json($this->_data['tags']);
	}

	protected function loadIndex()
	{
		$query = new Tag;
		if($name = Input::get('name',false))
			$query = $query->where('name','LIKE',"%$name%");

		$data = $query->orderBy('id','desc')->paginate(100);

		$response = array();
		$response['currentPage'] = $data->getCurrentPage();
		$response['lastPage'] = $data->getLastPage();
		$response['data'] = array();
		foreach ($data as $index => $t) {
			$response['data'][$index]['id'] = $t->id;
			$response['data'][$index]['name'] = $t->name;
			$response['data'][$index]['detail_link'] = URL::action('TagsController@getDetail', array($t->id));
			$response['data'][$index]['edit_link'] = URL::action('TagsController@getEdit', array($t->id));
		}
		$this->_data['tags'] = $response;
	}

	public function getDetail($id = 0)
	{
		$this->_data['tag'] = Tag::findOrFail($id);
		$this->layout->action = $this->_data['tag']->name;
		$this->_data['items'] = ItemsManager::toArray($this->_data['tag']->items()->paginate(30));
		$this->_data['edit'] = $this->_access->can('edit',Apps::TAGS);

		$this->layout->action = 'Tag: '.$this->_data['tag']->name;
		$this->layout->editButton = URL::action('TagsController@getEdit',array($id));
		$this->layout->content = View::make('tags.detail',$this->_data);
	}

	public function postDetail($id)
	{
		$tag = Tag::findOrFail($id);
		$items = ItemsManager::toArray($tag->items()->paginate(30));
		return Response::json($items);
	}

	public function getCreate()
	{
		$this->layout->action = 'Create Tag';
		$this->_data['tag'] = new Tag;
		$this->layout->content = View::make('tags.form',$this->_data);
	}

	public function postCreate()
	{
		try
		{
		$tag = new Tag(Input::get('tag'));
		if(!$tag->save())
			throw new ModelException($tag->getErrors(), 1);

		Cache::forget('item_tags');
		Session::flash('success', 'Tag '.$tag->name.' created.');
		return Response::json(array(
			'url' => URL::action('TagsController@getDetail', array($tag->id)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getEdit($id)
	{
		$this->_data['tag'] = Tag::findOrFail($id);
		$this->layout->action = 'Edit Tag';
		$this->layout->content = View::make('tags.form',$this->_data);
	}

	public function postEdit($id)
	{
		try
		{
		$tag = Tag::findOrFail($id);

		$tag->fill(Input::get('tag'));
		if(!$tag->save())
			throw new ModelException($tag->getErrors(), 1);

		Cache::forget('item_tags');

		Session::flash('success', 'Tag '.$tag->name.' edited.');
		return Response::json(array(
			'url' => URL::action('TagsController@getDetail', array($tag->id)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getStat()
	{
		$from = Input::get('from',null);
		$to = Input::get('to',null);
		if($from && $to)
		{
			$from = Dater::toSQL(Dater::createFromFormat('m/Y',$from)->startOfMonth()->format(Dater::$format));
			$to = Dater::toSQL(Dater::createFromFormat('m/Y',$to)->startOfMonth()->format(Dater::$format));
		}
		else
		{
			$from = Dater::toSQL(Dater::now()->subMonths(2)->startOfMonth()->format(Dater::$format));
			$to = Dater::toSQL(Dater::now()->endOfMonth()->format(Dater::$format));
		}

		$t = Transaction::table();
		$td = TransactionDetail::table();
		$i = Item::table();
		$tag = Tag::table();
		$it = ItemTag::table();
		$stat = DB::table($tag)->select(array(
			"$tag.id","$tag.name",
			DB::raw("SUM($td.quantity) as items_sold"),
		))
		->join($it,"$it.tag_id",'=',"$tag.id")->join($i,"$i.id",'=',"$it.item_id")->join($td,"$td.item_id",'=',"$i.id")->where("$td.date",'>=',$from)->where("$td.date",'<',$to)
		->join($t,"$t.id",'=',"$td.transaction_id")->where("$t.type",'=',Transaction::TYPE_SELL)->groupBy("$tag.id")->orderBy('items_sold','desc')->take(10)->get();
		pre(DB::getQueryLog(),$stat);
		exit;
	}
}