<?
use App\Models\Customer, App\Models\CustomerStat, App\Libraries\CCManager;
use App\Models\CustomerClass;
class ToolsController extends BaseController
{
	//fix old warehouse stats
	public function getFw()
	{
		DB::connection()->getPdo()->beginTransaction();

		//get the ids in customer table NOT IN customerstat
		$c = Customer::table();
		$cs = CustomerStat::table();
		$ids = DB::table($c)->select($c.'.id')->whereRaw($c.'.id NOT IN (select '.$cs.'.customer_id from '.$cs.')')->lists('id');

		//create the stat
		foreach($ids as $id)
		{
			$stat = new CustomerStat;
			$stat->customer_id = $id;
			$stat->balance = 0;
			if(!$stat->save())
			{
				DB::connection()->getPdo()->rollback();
				pre($stat->getErrors());
				exit;
			}
		}

		$ccm = new CCManager;
		$cc = CustomerClass::table();
		$ids = DB::table($c)->select($c.'.id')->whereRaw($c.'.id NOT IN (select '.$cc.'.customer_id from '.$cc.')')->lists('id');

		$date = Dater::now()->startOfMonth()->format(Dater::$SQLFormat);
		foreach ($ids as $key => $id) {
			if(!$ccm->emptyStat($id, $date))
			{
				DB::connection()->getPdo()->rollback();
				pre($ccm->getErrors());
				exit;
			}
		}

		DB::connection()->getPdo()->commit();
		pre('done');
		exit;
	}
}