<?
use App\Libraries\StatManager;
use App\Libraries\InvoiceTracker;
use App\Models\Transaction, App\Models\Customer, App\Models\TransactionDetail;
use App\Libraries\CCManager, App\Models\Item;
use App\Libraries\TransactionsManager, App\Libraries\HashManager;
use App\Models\Location;

class TransactionsController extends BaseController
{
	public function __construct()
	{
		BaseController::__construct();
		$loc = Customer::filter()->select('name','id')->where('type','=',Customer::TYPE_WAREHOUSE)->orderBy('name','asc');
		$this->_data['location'] = json_encode($loc->get()->toArray());
		$this->_data['default_warehouse'] = Config::get('local.default_warehouse');
	}

	public function getIndex()
	{
		$this->loadIndex();
		$this->_data['hide'] = false;
		if($this->_access->hide('balance',Apps::HIDE))
			$this->_data['hide'] = true;
		$this->layout->action = 'Transaction List';
		$this->layout->content = View::make('transactions.index',$this->_data);
	}

	public function postIndex()
	{
		$this->loadIndex();
		return Response::json($this->_data['responseJSON']);
	}

	protected function loadIndex()
	{
		if(!$sort = Input::get('sort',false)) $sort = 'date';
		if(!$dir = Input::get('dir',false)) $dir = 'desc';

		$from = Input::get('from',false);
		$to = Input::get('to',false);

		//init the query
		$query = Transaction::with(array('receiver','sender'));
		//dates are set!
		if($from && $to)
		{
			$from = Dater::toSQL($from);
			$to = Dater::toSQL($to);
			$query = $query->where('date','>=',$from)->where('date','<=',$to);
		}

/*		//search by location id, called from TODO: find the fucking page!
		if(($loc_id = Input::get('loc_id',false)) > 0)
		{
			$locationModel = Location::where('id', '=', $loc_id)->first();
			$query = $query->whereIn('location_id', $locationModel->viewables());
		}

		//called from report operations
		if($operation = Input::get('operation', false))
		{
			$op_ids = Customer::accounts()->where('parent_id','=',$operation)->lists('id');
			$query = $query->where(function($query) use($op_ids)
			{
				$query->whereIn('sender_id',$op_ids)->orWhereIn('receiver_id',$op_ids);
			});
		}*/

		//find the types
		if($type = Input::get('type',false)) {
			if($type['id'] > 0)
				$query = $query->where('type','=',$type['id']);
		}
		if($invoice = Input::get('invoice',false))
			$query = $query->where('invoice','=',$invoice);
		if($total = Input::get('total',false))
			$query = $query->where('total','=',$total);

		$transactions = $query->filter()->orderBy($sort,$dir)->orderBy('id',$dir)->paginate(50);

		//location filter - may not be necessary
		$lids = false;
		if($this->_lm->bound())
			$lids = $this->_lm->get_location();

		$this->_data['responseJSON'] = TransactionsManager::toArray($transactions, $lids);
		$this->_data['transactions'] = Response::json($this->_data['responseJSON'])->getData();
		$this->_data['typesJSON'] = json_encode(Transaction::$typesJSON);
		$this->_data['hide_balance'] = $this->_access->hide('balance',Apps::HIDE);
	}

	public function getBuy()
	{
		$this->layout->action = 'New Buy';
		$this->_data['due'] = true;
		$this->_data['customerType'] = 's';
		$this->_data['txt']['sender'] = 'Supplier';
		$this->_data['txt']['receiver'] = 'Warehouse';
		$this->_data['type'] = Transaction::TYPE_BUY;
		$this->_data['defaultWarehouse'] = json_encode(Customer::find($this->_data['app']['default_buy_warehouse']));
		$this->layout->content = View::make('transactions.buy',$this->_data);
	}

	public function postBuy()
	{
		return $this->createTransaction(Transaction::TYPE_BUY);
	}

	public function getSell()
	{
		$this->_data['accounts'] = json_encode(Customer::filter()->select('name','id')->where('type','=',Customer::TYPE_BANK)->orderBy('name','ASC')->get()->toArray());

		$this->layout->action = 'New Sell';
		$this->_data['due'] = true;
		$this->_data['customerType'] = 'c';
		$this->_data['txt']['sender'] = 'Warehouse';
		$this->_data['txt']['receiver'] = 'Customer';
		$this->_data['type'] = Transaction::TYPE_SELL;
		$this->_data['defaultWarehouse'] = json_encode(Customer::find($this->_data['app']['default_sell_warehouse']));
		$this->layout->content = View::make('transactions.sell',$this->_data);
	}

	public function getSellBatch()
	{
		$this->layout->action = 'New Sell';
		$this->_data['due'] = true;
		$this->_data['customerType'] = 'c';
		$this->_data['txt']['sender'] = 'Warehouse';
		$this->_data['txt']['receiver'] = 'Customer';
		$this->_data['type'] = Transaction::TYPE_SELL;
		$this->_data['defaultWarehouse'] = json_encode(Customer::find($this->_data['app']['default_sell_warehouse']));
		$this->layout->content = View::make('transactions.sellbatch',$this->_data);
	}

	public function postSell()
	{
		return $this->createTransaction(Transaction::TYPE_SELL);
	}

	public function getReturn()
	{
		$this->layout->action = 'New Return';
		$this->_data['due'] = false;
		$this->_data['customerType'] = 'c';
		$this->_data['txt']['sender'] = 'Customer';
		$this->_data['txt']['receiver'] = 'Warehouse';
		$this->_data['defaultWarehouse'] = json_encode(Customer::find($this->_data['app']['default_sell_warehouse']));
		$this->_data['type'] = Transaction::TYPE_RETURN;
		$this->layout->content = View::make('transactions.buy',$this->_data);
	}

	public function postReturn()
	{
		return $this->createTransaction(Transaction::TYPE_RETURN);
	}

	public function getReturnSupplier()
	{
		$this->layout->action = 'New Return Supplier';
		$this->_data['customerType'] = 's';
		$this->_data['txt']['sender'] = 'Warehouse';
		$this->_data['txt']['receiver'] = 'Supplier';
		$this->_data['defaultWarehouse'] = json_encode(Customer::find($this->_data['app']['default_buy_warehouse']));
		$this->_data['type'] = Transaction::TYPE_RETURN_SUPPLIER;
		$this->layout->content = View::make('transactions.sell',$this->_data);
	}

	public function postReturnSupplier()
	{
		return $this->createTransaction(Transaction::TYPE_RETURN_SUPPLIER);
	}

	protected function createTransaction($type = null)
 	{
		try {

		$class = array();

		//start transaction
		DB::connection()->getPdo()->beginTransaction();

		$customer = Input::get('customer',array());
		$warehouse = Input::get('warehouse', array());

		$input = Input::get('transaction');
		$transaction = new Transaction($input);
		switch($type)
		{
			case Transaction::TYPE_BUY:
			case Transaction::TYPE_RETURN:
				$transaction->sender_id = $customer['id'];
				$transaction->receiver_id = $warehouse['id'];
				break;
			case Transaction::TYPE_SELL:
			case Transaction::TYPE_RETURN_SUPPLIER:
				$transaction->sender_id = $warehouse['id'];
				$transaction->receiver_id = $customer['id'];
				break;
			default: //don't update stats for move, production
				break;
		}
		$transaction->init($type);

		//gets the transaction id
		if(!$transaction->save())
			throw new ModelException($transaction->getErrors(), __LINE__);

		if(!$details = $transaction->createDetails(Input::get('details')))
			throw new ModelException($transaction->getErrors(), __LINE__);

		//add to customer stat
		$sm = new StatManager;
		switch($type)
		{
			case Transaction::TYPE_BUY:
			case Transaction::TYPE_RETURN:
				//add balance to sender(supplier)
				$sender_balance = $sm->add($transaction->sender_id,$transaction,true); //skip 1 because the transaction is already created?
				if($sender_balance === false)
					throw new ModelException($sm->getErrors());

				$transaction->sender_balance = $sender_balance;
				break;
			case Transaction::TYPE_SELL:
			case Transaction::TYPE_RETURN_SUPPLIER:
				$transaction->setAttribute('total',0 - $transaction->total); //make negative

				//deduct balance from receiver(customer)
				$receiver_balance = $sm->deduct($transaction->receiver_id,$transaction,true);
				if($receiver_balance === false)
					throw new ModelException($sm->getErrors());

				$transaction->receiver_balance = $receiver_balance;
				break;
			default: //don't update stats for move, production
				break;
		}

		if(!$transaction->save())
			throw new ModelException($transaction->getErrors());

		$paid = Input::get('paid');
		//special case: paid is checked
		if($type == Transaction::TYPE_SELL && isset($paid['checked']) && $paid['checked'])
		{
			//calculate total
			$amount = isset($paid['amount']) ? $paid['amount'] : 0;
			if($amount <= 0) $amount = abs($transaction->total);

			$payment = $transaction->attachIncome($transaction->date, $transaction->receiver_id, $paid['account']['id'],$amount);
			$class['income'] = $payment->total;

			//another special case, ongkir is filled, create journal
			$ongkir = isset($input['ongkir']) ? $input['ongkir'] : null;
			if(!empty($ongkir))
				$transaction->attachOngkir($transaction->date, $payment->receiver_id, abs($ongkir), \App::make('appsettings')->get('ongkir'));
		}

		InvoiceTracker::flag($transaction);
		TransactionsManager::checkSell($transaction, $details);
		HashManager::save($transaction);
		$cc = new CCManager;
		$class['date'] = Dater::fromSQL($transaction->date)->startOfMonth()->format(Dater::$SQLFormat);
		//update customer class
		switch ($transaction->type) {
			case Transaction::TYPE_SELL:
				$class['type'] = Transaction::TYPE_SELL;
				$class['total'] = $transaction->total;
				$class['customer'] = $transaction->receiver;
				$cc->update($class);
				break;
			case Transaction::TYPE_RETURN:
				$class['type'] = Transaction::TYPE_RETURN;
				$class['total'] = $transaction->total;
				$class['customer'] = $transaction->sender;
				$cc->update($class);
				break;
			default:
				break;
		}

		//commit db transaction
		DB::connection()->getPdo()->commit();

		Session::flash('success', 'Transaction # ' . $transaction->id. ' created.');
		return Response::json(array(
			'url' => URL::action('TransactionsController@getDetail',array($transaction->id,$transaction->date)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getTransfer()
	{
		$this->layout->action = 'New Transfer';
		$this->_data['accounts'] = json_encode(Customer::filter()->select('name','id')->whereIn('type',array(Customer::TYPE_BANK,Customer::TYPE_VACCOUNT))->orderBy('name', 'ASC')->get()->toArray());
		$this->layout->content = View::make('transactions.transfer',$this->_data);
	}

	public function postTransfer()
	{
		try
		{

		$sender = Input::get('sender');
		$receiver = Input::get('receiver');

		if(empty($sender))
			throw new Exception('sender (-) cannot be empty');

		if(empty($receiver))
			throw new Exception('receiver (+) cannot be empty');

		DB::connection()->getPdo()->beginTransaction();
		$input = Input::get('transaction');
		$transaction = new Transaction($input);
		$transaction->init(Transaction::TYPE_TRANSFER);
		$transaction->sender_id = $sender['id'];
		$transaction->receiver_id = $receiver['id'];
		$transaction->total = abs($transaction->total);

		if(!$sender = Customer::find($sender['id']))
			throw new Exception('Sender does not exist');

		if(!$receiver = Customer::find($receiver['id']))
			throw new Exception('Receiver does not exist');

		$transaction->sender_type = $sender->type;
		$transaction->receiver_type = $receiver->type;

		if(!$transaction->validate())
			throw new ModelException($transaction->getErrors());

		//add to receiver
		$sm = new StatManager;
		$receiver_balance = $sm->add($transaction->receiver_id,$transaction);
		if($receiver_balance === false)
			throw new ModelException($sm->getErrors()->first());

		//deduct from sender
		$sender_balance = $sm->deduct($transaction->sender_id,$transaction);
		if($sender_balance === false)
			throw new ModelException($sm->getErrors()->first());

		$transaction->sender_balance = $sender_balance;
		$transaction->receiver_balance = $receiver_balance;

		if(!$transaction->save())
			throw new ModelException($transaction->getErrors());

		if(!$transaction->invoice)
		{
			$transaction->invoice = $transaction->id;
			$transaction->save();
		}

		HashManager::save($transaction);

		$date = Dater::fromSQL($transaction->date)->startOfMonth()->format(Dater::$SQLFormat);
		$cc = new CCManager;
		$cc->update(array(
			'date' => $date,
			'type' => Transaction::TYPE_TRANSFER,
			'customer' => $transaction->sender,
			'total' => 0 - $transaction->total,
		));
		$cc->update(array(
			'date' => $date,
			'type' => Transaction::TYPE_TRANSFER,
			'customer' => $transaction->receiver,
			'total' => $transaction->total,
		));

		//commit db transaction
		DB::connection()->getPdo()->commit();
		Session::flash('success', 'Adjust # ' . $transaction->id. ' created.');

		return Response::json(array(
			'url' => URL::action('TransactionsController@getDetail',array($transaction->id,$transaction->date)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	//DATE NO LONGER NECESSARY
	public function getDetail($id)
	{
		$this->loadDetail($id);
		$this->layout->action = 'Invoice: '.$this->_data['transaction']->invoice;

		//check for print mode
		if(Input::get('receipt', false))
			$this->layout = View::make('layouts.receipt', $this->_data);
		else
			$this->layout->content = View::make('transactions.detail',$this->_data);
	}

	//JSON response
	public function postDetail($id)
	{
		$this->loadDetail($id);
		$transaction = $this->_data['transaction']->toArray();
		$transaction['sender_url'] = $transaction['sender_name'] = $transaction['receiver_url'] = $transaction['receiver_name'] = '';
		if($this->_data['transaction']->sender) {
			$transaction['sender_url'] = $this->_data['transaction']->sender->getDetailLink();
			$transaction['sender_name'] = $this->_data['transaction']->sender->name;
		}
		if($this->_data['transaction']->receiver) {
			$transaction['receiver_url'] = $this->_data['transaction']->receiver->getDetailLink();
			$transaction['receiver_name'] = $this->_data['transaction']->receiver->name;
		}

		return Response::json(array(
			'details' => $this->_data['details'],
			'transaction' => $transaction
		));
	}

	protected function loadDetail($id)
	{
		if(!$this->_data['transaction'] = Transaction::where('id','=',$id)->filter(true)->first())
			return App::abort(404);

		$this->_data['edit'] = $this->_access->can('edit',Apps::TRANSACTIONS);
		$this->_data['image'] = $this->_access->can('image',Apps::TRANSACTIONS);
		$this->_data['delete'] = $this->_access->can('delete',Apps::TRANSACTIONS);

		if(!empty($this->_data['transaction']->detail_ids))
		{
			$itemsTable = Item::table();
			$detailTable = TransactionDetail::table();
			$details = TransactionDetail::with(array('item','item.group'))->whereIn("$detailTable.id",array_filter(explode(',',$this->_data['transaction']->detail_ids)))->get();
			$this->_data['details'] = array();
			foreach ($details as $key => $detail) {
				$this->_data['details'][$key]['image'] = $detail->item->getImageUrl();
				$this->_data['details'][$key]['link'] = $detail->item->getUrl();
				$this->_data['details'][$key]['name'] = $detail->item->name;
				$this->_data['details'][$key]['code'] = $detail->item->code;
				$this->_data['details'][$key]['quantity'] = $detail->quantity;
				$this->_data['details'][$key]['pcode'] = $detail->item->pcode;
				$this->_data['details'][$key]['alias'] = '';
				if($detail->item->group)
					$this->_data['details'][$key]['alias'] = $detail->item->group->alias;
				$this->_data['details'][$key]['description'] = $detail->item->description;
				$this->_data['details'][$key]['price'] = $detail->price;
				$this->_data['details'][$key]['discount'] = $detail->discount;
				$this->_data['details'][$key]['total'] = $detail->total;
				$this->_data['details'][$key]['id'] = $detail->item->id;
			}
		}
	}

	public function getCashIn()
	{
		$this->layout->action = 'New Cash In';
		$this->_data['defaultAccount'] = $this->_data['app']['default_income_account'];
		$this->_data['accounts'] = json_encode(Customer::filter()->select('name','id')->where('type','=',Customer::TYPE_BANK)->orderBy('name', 'ASC')->get()->toArray());
		$this->layout->content = View::make('transactions.cash_in',$this->_data);
	}

	public function postCashIn()
	{
		return $this->create_payment(Transaction::TYPE_CASH_IN);
	}

	public function getCashOut()
	{
		$this->layout->action = 'New Cash Out';
		$this->_data['defaultAccount'] = $this->_data['app']['default_expense_account'];
		$this->_data['accounts'] = json_encode(Customer::filter()->select('name','id')->where('type','=',Customer::TYPE_BANK)->orderBy('name', 'ASC')->get()->toArray());
		$this->layout->content = View::make('transactions.cash_in',$this->_data);
	}

	public function postCashOut()
	{
		return $this->create_payment(Transaction::TYPE_CASH_OUT);
	}

	protected function create_payment($type = null)
	{
		try {

		//start transaction
		DB::connection()->getPdo()->beginTransaction();
		$details = array_filter(Input::get('details'));
		if(empty($details))
			throw new \Exception('cash in/out cannot be empty');

		$account = Input::get('account');
		if(empty($details))
			throw new \Exception('account cannot be empty');

		//init
		$inputDate = Input::get('date');
		$sm = new StatManager;
		$cc = new CCManager;
		$date = Dater::createFromFormat(Dater::$format,$inputDate)->startOfMonth()->toSQLFormat();

		//get the customers
		$ids = array();
		foreach ($details as $value) {
			if(isset($value['customer']['id']))
				$ids[] = $value['customer']['id'];
		}
		$ids = array_filter($ids);
		$db = Customer::whereIn('id', $ids)->lists('type', 'id');

		$urls = array();
		foreach($details as $c)
		{
			$c['customerId'] = $c['customer']['id'];
			//skip invalid customers
			if(!isset($db[$c['customerId']])) //impossible, but who knows
				continue;
			elseif($db[$c['customerId']] == Customer::TYPE_WAREHOUSE || $db[$c['customerId']] == Customer::TYPE_VWAREHOUSE)
				continue;

			$transaction = new Transaction();
			$transaction->date = $inputDate;
			$transaction->invoice = isset($c['invoice']) ? $c['invoice'] : null;
			$transaction->description = isset($c['description']) ? $c['description'] : '';
			$transaction->total = $c['total'];

			switch($type)
			{
				case Transaction::TYPE_CASH_OUT:
					$transaction->sender_id = $account['id'];
					$transaction->init($type, $c['customerId']);

					//make negative
					if($transaction->total > 0)
						$transaction->total = 0 - $transaction->total;
					$sender_balance = $sm->deduct($transaction->sender_id,$transaction);
					if($sender_balance === false)
						throw new \Exception($sm->getErrors()->first());
					$receiver_balance = $sm->deduct($transaction->receiver_id,$transaction);
					if($receiver_balance === false)
						throw new \Exception($sm->getErrors()->first());

					//update monthly stats
					$cc->update(array(
						'date' => $date,
						'type' => Transaction::TYPE_CASH_OUT,
						'customer' => $transaction->receiver,
						'total' => $transaction->total,
					));
					break;
				case Transaction::TYPE_CASH_IN:
					$transaction->receiver_id = $account['id'];
					$transaction->init($type, $c['customerId']);

					//make positive
					if($transaction->total < 0)
						$transaction->total = 0 - $transaction->total;
					$sender_balance = $sm->add($transaction->sender_id,$transaction);
					if($sender_balance === false)
						throw new \Exception($sm->getErrors()->first());
					$receiver_balance = $sm->add($transaction->receiver_id,$transaction);
					if($receiver_balance === false)
						throw new \Exception($sm->getErrors()->first());

					//update monthly stats
					$cc->update(array(
						'date' => $date,
						'type' => Transaction::TYPE_CASH_IN,
						'customer' => $transaction->sender,
						'total' => $transaction->total,
					));
					break;
			}

			//update the balances
			$transaction->receiver_balance = $receiver_balance;
			$transaction->sender_balance = $sender_balance;

			//gets the transaction id
			if(!$transaction->save())
				throw new ModelException($transaction->getErrors());

			if(!$transaction->invoice)
			{
				$transaction->invoice = $transaction->id;
				$transaction->save();
			}

			//track for customers
			InvoiceTracker::flag($transaction);
			HashManager::save($transaction);

			//save urls for links
			$urls[] = '#'.$transaction->id;
		}

		DB::connection()->getPdo()->commit();

		Session::flash('success', 'Transactions ' . implode(', ', $urls) . ' created.');
		return Response::json(array(
			'url' => URL::action('TransactionsController@getIndex'),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getMove()
	{
		$this->layout->action = 'New Move';
		$this->_data['defaultWarehouse'] = json_encode(Customer::find($this->_data['app']['default_move_warehouse']));
		$this->layout->content = View::make('transactions.move',$this->_data);
	}

	public function postMove()
	{
		try {

		$input = Input::get('transaction');
		$sender = Input::get('sender');
		$receiver = Input::get('receiver');
		$transaction = new Transaction($input);
		$transaction->init(Transaction::TYPE_MOVE);
		$transaction->sender_id = $sender['id'];
		$transaction->receiver_id = $receiver['id'];

		//start transaction
		DB::connection()->getPdo()->beginTransaction();

		//gets the transaction id
		if(!$transaction->save())
			throw new ModelException($transaction->getErrors(), __LINE__);

		if(!$transaction->createDetails(Input::get('details')))
			throw new ModelException($transaction->getErrors(), __LINE__);

		//update the transaction
		if(!$transaction->save())
			throw new ModelException($transaction->getErrors(), __LINE__);

		//special case: journal is filled
		$account = Input::get('journal');
		//special case: paid is checked
		if(isset($account['checked']) && $account['checked'] && isset($account['account']) && isset($account['account']['id']))
		{
			$sm = new StatManager;
			$journal = new Transaction;
			$journal->date = $transaction->date;
			$journal->init(Transaction::TYPE_ADJUST);
			$journal->sender_id = $account['account']['id'];
			$journal->total = 0 - abs($transaction->total);
			$journal->invoice = $transaction->invoice;
			$journal->receiver_id = $transaction->sender_id;

			$sender_balance = $sm->deduct($journal->sender_id,$journal);
			if($sender_balance === false)
				throw new ModelException($sm->getErrors());

			$journal->sender_balance = $sender_balance;
			if(!$journal->save())
				throw new ModelException($journal->getErrors());
		}

		HashManager::save($transaction);
		InvoiceTracker::flag($transaction);

		//commit db transaction
		DB::connection()->getPdo()->commit();

		Session::flash('success', 'Transaction # ' . $transaction->id. ' created.');
		return Response::json(array(
			'url' => URL::action('TransactionsController@getDetail',array($transaction->id,$transaction->date)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getUse()
	{
		$this->layout->action = 'New Use';
		$this->_data['defaultWarehouse'] = json_encode(Customer::find($this->_data['app']['default_use_warehouse']));
		$this->layout->content = View::make('transactions.use',$this->_data);
	}

	public function postUse()
	{
		try {

		$warehouse = Input::get('warehouse');
		$transaction = new Transaction(Input::get('transaction'));
		$transaction->init(Transaction::TYPE_USE);
		$transaction->sender_id = $warehouse['id'];

		//start transaction
		DB::connection()->getPdo()->beginTransaction();

		//gets the transaction id
		if(!$transaction->save())
			throw new ModelException($transaction->getErrors(), __LINE__);

		if(!$transaction->createDetails(Input::get('details')))
			throw new ModelException($transaction->getErrors(), __LINE__);

		//update the transaction
		if(!$transaction->save())
			throw new ModelException($transaction->getErrors(), __LINE__);

		HashManager::save($transaction);

		//commit db transaction
		DB::connection()->getPdo()->commit();

		Session::flash('success', 'Transaction # ' . $transaction->id. ' created.');
		return Response::json(array(
			'url' => URL::action('TransactionsController@getDetail',array($transaction->id,$transaction->date)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getProduction()
	{
		$this->_data['action'] = 'New Production';
		$this->_data['rows'] = Session::get('rows',1);
		$this->layout->content = View::make('transactions.production',$this->_data);
	}

	public function postProduction()
	{
		Session::forget('rows');
		$rows = count(Input::get('details'));

		$transaction = new Transaction(Input::get('transaction'));
		$transaction->init(Transaction::TYPE_PRODUCTION);

		//start transaction
		DB::connection()->getPdo()->beginTransaction();

		//gets the transaction id
		if(!$transaction->save())
			return $this->rollback($transaction->getErrors())->with('rows',$rows);

		if(!$transaction->validate_details(Input::get('details')))
			return $this->rollback($transaction->getErrors())->with('rows',$rows);

		//update the detail ids
		if(!$transaction->save())
			return $this->rollback($transaction->getErrors())->with('rows',$rows);

		//commit db transaction
		DB::connection()->getPdo()->commit();

		return Redirect::action('TransactionsController@getDetail',array($transaction->id,$transaction->date) )->with( 'success', 'Transaction # ' . $transaction->id. ' created.' );
	}

	public function postDelete($id,$date)
	{
		try{

		if(!$t = Transaction::where('date','=',$date)->where('id','=',$id)->first())
			return App::abort(404);

		DB::connection()->getPdo()->beginTransaction();

		$deleter = new \App\Libraries\Deleter;
		$deleted = $deleter->delete($t);
		InvoiceTracker::flag($t);
		HashManager::delete($t);

		DB::connection()->getPdo()->commit();

		Session::flash('success', 'Transaction # ' . $deleted->id. ' deleted.');
		return Response::json(array(
			'url' => URL::action('DeletedController@getDetail',array($deleted->id)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function postImage($id,$date)
	{
		try{

		if(!$t = Transaction::where('date','=',$date)->where('id','=',$id)->first())
			return App::abort(404);

		$file = Input::file('file');
		if(!$file)
			throw new Exception('Image required', 1);

		$path = $t->getUploadPath();
		//check for file exist
		if(file_exists($path.'/'.$t->id.'.jpg')) {
			chmod($path.'/'.$t->id.'.jpg',0755);
			unlink($path.'/'.$t->id.'.jpg');
		}
		$file = $file->move($path, $t->id.'.jpg');

		//not efficient for multiple upload but oh well
		Image::make($file->getPathName())->resize(360, null, function($constraint) {
			$constraint->aspectRatio();
		})->save();


		Session::flash('success', 'Image #' . $t->id. ' uploaded.');
		return Response::json(array(
			'url' => URL::action('TransactionsController@getDetail',array($t->id,$t->date)),
		));

		} catch(ModelException $e) {
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getEdit($id,$date)
	{
		if(!$t = Transaction::where('date','=',$date)->where('id','=',$id)->first())
			return App::abort(404);

		//better typeability, load to form
		$this->_data['transaction'] = $t;
		$this->layout->action = 'Edit transaction #'.$t->id;

		$this->layout->content = View::make('transactions.edit',$this->_data);
	}

	public function postEdit($id,$date)
	{
		if(!$t = Transaction::where('date','=',$date)->where('id','=',$id)->first())
			return App::abort(404);

		$t->invoice = Input::get('invoice');
		$t->description = Input::get('description');
		$t->save();

		HashManager::edit($t);

		return Redirect::action('TransactionsController@getDetail',array($t->id,$t->date) )->with( 'success', 'Transaction # ' . $t->id. ' edited.' );
	}

	public function getAdjust()
	{
		$this->layout->action = 'New Adjust';
		$this->layout->content = View::make('transactions.adjust',$this->_data);
	}

	public function postAdjust()
	{
		try {

		//start transaction
		DB::connection()->getPdo()->beginTransaction();

		$sender = Input::get('sender');
		$receiver = Input::get('receiver');

		if(empty($sender))
			throw new Exception('sender (-) cannot be empty');

		if(empty($receiver))
			throw new Exception('receiver (+) cannot be empty');

		//init
		$input = Input::get('transaction');
		$sm = new StatManager;
		$cc = new CCManager;

		$date = Dater::createFromFormat(Dater::$format,$input['date'])->startOfMonth()->format(Dater::$SQLFormat);

		//get the customers
		$customerTypes = Customer::whereIn('id', array($sender['id'], $receiver['id']))->lists('type', 'id');

		//adjust only allows account to customer/reseller/bank or vice versa
		if(in_array($customerTypes[$receiver['id']], Customer::$notAdjustable) || in_array($customerTypes[$sender['id']], Customer::$notAdjustable))
			throw new Exception('cannot adjust warehouse or virtual accounts');
		//if none of them are account
		if($customerTypes[$receiver['id']] != Customer::TYPE_ACCOUNT && $customerTypes[$sender['id']] != Customer::TYPE_ACCOUNT)
			throw new Exception('adjust requires at least 1 journal account');
		//if both are account
		if($customerTypes[$receiver['id']] == Customer::TYPE_ACCOUNT && $customerTypes[$sender['id']] == Customer::TYPE_ACCOUNT)
			throw new Exception('cannot adjust 2 journal accounts');

		$transaction = new Transaction($input);
		$transaction->sender_id = $sender['id'];
		$transaction->receiver_id = $receiver['id'];
		$transaction->init(Transaction::TYPE_ADJUST);
		$transaction->total = abs($transaction->total);

		//make negative - for stats
		if($customerTypes[$sender['id']] == Customer::TYPE_ACCOUNT)
			$transaction->total = 0 - $transaction->total;

		$sender_balance = $sm->deduct($transaction->sender_id,$transaction);
		$receiver_balance = $sm->add($transaction->receiver_id,$transaction);
		if($receiver_balance === false || $sender_balance === false)
			throw new Exception('error saving statistics');

		//update the balances
		$transaction->receiver_balance = $receiver_balance;
		$transaction->sender_balance = $sender_balance;

		//gets the transaction id
		if(!$transaction->save())
			throw new ModelException($transaction->getErrors());

		if(!$transaction->invoice)
		{
			$transaction->invoice = $transaction->id;
			$transaction->save();
		}

		//update monthly stats
		$cc->update(array(
			'date' => $date,
			'type' => Transaction::TYPE_ADJUST,
			'customer' => $transaction->receiver,
			'total' => $transaction->total,
		));

		$cc->update(array(
			'date' => $date,
			'type' => Transaction::TYPE_ADJUST,
			'customer' => $transaction->sender,
			'total' => 0 - $transaction->total,
		));

		//track for customers
		InvoiceTracker::flag($transaction);
		HashManager::save($transaction);

		DB::connection()->getPdo()->commit();

		Session::flash('success', 'Adjust # ' . $transaction->id. ' created.');
		return Response::json(array(
			'url' => URL::action('TransactionsController@getDetail',array($transaction->id,$transaction->date)),
		));

		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}

	}

	public function getSell2()
	{
		$this->getSell();
		$this->layout->content = View::make('transactions.sell2',$this->_data);
	}
}