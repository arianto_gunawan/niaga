<?
use App\Models\User, App\Models\Role, App\Models\Acl, App\Models\Location, App\Models\UserSetting;
class UsersController extends BaseController
{
	public function getIndex()
	{
		$this->layout->action = 'User List';
		$this->layout->content = View::make('users.index')->with('users', User::with(array('role','location'))->where('role_id','!=',Access::ADMIN)->get());
	}

	public function getDetail($id = 0)
	{
		$this->layout->content = View::make('users.detail')->with('user', User::find($id));
	}

	public function getCreate()
	{
		$this->_data['roles'] = Role::select('id','name')->get();
		$this->_data['locations'] = Location::select('id','name')->get();
		$this->_data['user'] = new User;
		$this->_data['update'] = false;
		$this->layout->content = View::make('users.form',$this->_data);
	}

	public function postCreate()
	{
		try
		{
		DB::connection()->getPdo()->beginTransaction();

		$input = Input::get('user');

		$user = new User;
		$user->username = $input['username'];
		$user->location_id = $input['location_id'];
		$user->role_id = $input['role_id'];
		if($user->role_id == Access::ADMIN)
			throw new Exception('Invalid Role', 1);

		$password = User::generatePassword();
		$user->password = Hash::make($password);
		$user->active = 1;
		if(!$user->save())
			throw new Exception('cannot save user', 1);

		//create app settings
		foreach(UserSetting::$list as $name => $val)
		{
			$a = new UserSetting;
			$a->user_id = $user->id;
			$a->name = $name;
			$a->value = 0;
			if(!$a->save())
				throw new Exception('cannot save user settings', 1);
		}

		DB::connection()->getPdo()->commit();

		Session::flash('success', 'User '.$user->username.' created, Password: '.$password);
		return Response::json(array(
			'url' => URL::action('UsersController@getIndex'),
		));
		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getEdit($id)
	{
		$this->_data['user'] = User::findOrFail($id);

		$this->_data['roles'] = Role::select('id','name')->get();
		$this->_data['locations'] = Location::select('id','name')->get();
		$this->_data['update'] = true;

		$this->layout->content = View::make('users.form',$this->_data);
	}

	public function postEdit($id)
	{
		try
		{

		$input = Input::get('user');
		$user = User::findOrFail($id);
		$user->username = $input['username'];
		$user->location_id = $input['location_id'];
		$user->role_id = $input['role_id'];
		if($user->role_id == Access::ADMIN)
			throw new Exception('Invalid Role', 1);

		$password = '';
		if(Input::get('update_password',false))
		{
			$password = User::generatePassword();
			$user->password = Hash::make($password);
			$password = '<br/>Password: '.$password;
		}

		if(!$user->save())
			throw new Exception('Cannot save user', 1);

		Session::flash('success', 'User '.$user->username.' edited.'.$password);
		return Response::json(array(
			'url' => URL::action('UsersController@getIndex'),
		));
		} catch(ModelException $e) {
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getRoles()
	{
		$this->layout->action = 'Role List';
		$this->layout->content = View::make('users.roles',$this->_data)->with('roles', Role::all());
	}

	public function getCreateRole()
	{
		$this->_data['role'] = new Role;
		$this->layout->action = 'Create Role';
		$this->layout->content = View::make('users.roleform',$this->_data)->with('role', new Role);
	}

	public function postCreateRole()
	{
		try{
		$input = Input::get('role');
		$role = new Role;
		$role->name = $input['name'];
		if(!$role->save())
			throw new ModelException($role->getErrors(), 1);

		Session::flash('success', 'Role '.$role->name.' created');
		return Response::json(array(
			'url' => URL::action('UsersController@getRoles'),
		));
		} catch(ModelException $e) {
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getEditRole($id)
	{
		$this->_data['role'] = Role::findOrFail($id);
		$this->layout->action = 'Edit Role';
		$this->layout->content = View::make('users.roleform',$this->_data);
	}

	public function postEditRole($id)
	{
		try
		{
		$input = Input::get('role');
		$role = Role::findOrFail($id);
		$role->name = $input['name'];
		if(!$role->save())
			throw new Exception('cannot save role', 1);

		Session::flash('success', 'Role '.$role->name.' edited');
		return Response::json(array(
			'url' => URL::action('UsersController@getRoles'),
		));
		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getAcl($id)
	{
		$role = Role::findOrFail($id);

		$this->_data['acl'] = array();
		$this->_data['actions'] = Apps::$actions;

		//construct the list
		$this->_data['list'] = Apps::$actions;
		$this->_data['apps'] = Apps::$list;

		//construct the acl
		$acls = Acl::where('role_id','=',$id)->get();
		foreach($acls as $acl)
		{
			if(!isset($this->_data['acl'][$acl->app_id])) $this->_data['acl'][$acl->app_id] = array();
			$this->_data['acl'][$acl->app_id][] = $acl->action;
		}

		$this->layout->action = 'ACL for '.$role->name;
		$this->layout->content = View::make('users.acl',$this->_data);
	}

	public function postAcl($id)
	{
		try
		{
		$role = Role::findOrFail($id);

		//construct the list
		$list = Apps::$actions;

		//start db transaction
		DB::connection()->getPdo()->beginTransaction();

		//bust the cache
		Cache::forget('access.'.$role->id);

		//remove old roles
		Acl::where('role_id','=',$id)->delete();
		$input = Input::get('acl');
		foreach($input as $app_id => $app)
		{
			if(!is_array($app)) continue;
			foreach($app as $action)
			{
				$new_acl = new Acl;
				$new_acl->role_id = $id;
				$new_acl->app_id = $app_id;
				$new_acl->action = $action;
				if(!$new_acl->save())
					throw new ModelException($new_acl->getErrors(), 1);
			}
		}

		$role->sidenav = $this->_access->generateSidebar($role->id);
		if(!$role->save())
			throw new ModelException($role->getErrors(), 1);

		DB::connection()->getPdo()->commit();

		Session::flash('success', $role->name.' updated');
		return Response::json(array(
			'url' => URL::action('UsersController@getRoles'),
		));
		} catch(ModelException $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getErrors(), 500);
		} catch(Exception $e) {
			DB::connection()->getPdo()->rollback();
			return Response::json($e->getMessage(), 500);
		}
	}

	public function getBan($id)
	{
		return $this->toggleBan($id);
	}

	public function getUnban($id)
	{
		return $this->toggleBan($id);
	}

	protected function toggleBan($id)
	{
		$user = User::findOrFail($id);
		if($user->role_id == Access::ADMIN) return App::abort('404');
		if($user->active == 1)
		{
			$user->active = 0;
			$status = 'banned';
		}
		else
		{
			$user->active = 1;
			$status = 'activated';
		}

		$user->save();
		return Redirect::action('UsersController@getIndex')->with('success',$user->usename.' '.$status);
	}

	//ajax
	public function getSearch()
	{
		$term = Input::get('q');
		$users = User::where('role_id','!=',Access::ADMIN)->where('username','LIKE',"%$term%")->take(5)->get();

		$response = array();
		$i = 0;
		foreach($users as $c)
		{
			$response[$i] = array();
			$response[$i]['name'] = $c->username;
			$response[$i]['id'] = $c->id;
			$response[$i]['value'] = $c->username;
			$response[$i]['tokens'] = explode(' ',$c->name);
			$i++;
		}

		return Response::json($response);
	}
}