<?
use App\Models\Item;
class VItemsController extends AssetTetapController
{
	protected $type = Item::TYPE_SERVICE;
	protected $action = 'VItemsController';
	protected $_title = 'Virtual Item';
}