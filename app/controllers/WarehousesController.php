<?
use App\Models\Customer;
use App\Models\CustomerStat;
use App\Models\Item;
use App\Models\Transaction;
use App\Models\WarehouseItem;
use App\Models\Depreciation;
use App\Libraries\Keys;
class WarehousesController extends CustomersController
{
	protected $type = Customer::TYPE_WAREHOUSE;
	protected $action = 'WarehousesController';

	protected function loadCustomer($id)
	{
		parent::loadCustomer($id);

		$this->_data['asset'] = Cache::remember(Keys::asset($id), 2, function () use($id) {
			$wi = WarehouseItem::table();
			$i = Item::table();

			$this->_data['asset_tetap'] = 0;

			$date = Dater::now()->format(Dater::$SQLFormat);
			$d = Depreciation::table();
			//find depreciating, non expired assets
			$depreciations = Depreciation::where('buy_date', '<=', $date)->where('expire_date', '>', $date)->join($wi, $wi.'.item_id', '=', DB::raw("$d.item_id AND $wi.warehouse_id = $id AND $wi.quantity > 0"))->get();

			foreach ($depreciations as $dep) {
				$this->_data['asset_tetap'] += $dep->calcValue();
			}
		});
	}
}