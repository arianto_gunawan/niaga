<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserSeeder');
		$this->command->info('User tables seeded!');

//		$this->call('ItemSeeder');
//		$this->command->info('item tag tables seeded!');

//		$this->call('SettingsSeeder');
//		$this->command->info('settings tables seeded!');

//		$this->call('LocationSeeder');
//		$this->command->info('Location tables seeded!');

//		$this->call('SentrySeeder');
//		$this->command->info('Sentry tables seeded!');

//		$this->call('ContentSeeder');
//		$this->command->info('Content tables seeded!');
	}

}