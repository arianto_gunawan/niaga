<?
namespace App\Libraries;
use App\Models\ItemGroup;
use App\Models\Item, App\Models\ItemTag, App\Models\WarehouseItem;
use Illuminate\Support\MessageBag, App\Models\Tag, App\Models\Customer;
use Apps, App, Cache, Config, Dater, DB, Event, Input, InputForm, Redirect, Response, Session, URL, View, ModelException, Exception, Auth;
class ItemsManager
{
	public static $_tags;
	public static $_json;

	public function createItems($input, $inputTags, $file)
	{
		if(!isset($input['pcode']) || empty($input['pcode']))
			return $this->error('pcode is required','code');
		if(!preg_match("/([a-zA-Z]{2})([0-9]{5})\/([0-9]{2})/",$input['pcode']))
			return $this->error('pcode salah format','code');

		static::loadTags();

		//find the tags
		$inputTags = array_flip($inputTags);
		$inputTags = static::sortTags($inputTags);

		//then create the items
		$total = 0;
		foreach($inputTags['types'] as $key => $type_id)
		{
			foreach($inputTags['sizes'] as $key => $size_id)
			{
				if(!$item = $this->createCrystalItem($input, $inputTags, $type_id, $size_id, $file))
					return $this->error('error creating item');

				$total++;
			}
		}

		if($total < 1)
			return $this->error('TYPE + SIZE harus ada');

		return true;
	}

	//to avoid accidental creation
	public function createItem($input, $tags, $file = null, $item = false)
	{
		throw new \Exception('function disabled');
	}

	protected function sortTags($tags)
	{
		if(!is_array($tags))
			return array('types' => array(),'sizes' => array(),'jahit' => array());

		//get the item types
		$types = array_filter(array_keys(array_intersect_key($tags,static::$_tags[Tag::TYPE_TYPE])));
		//get the item sizes
		$sizes = array_filter(array_keys(array_intersect_key($tags,static::$_tags[Tag::TYPE_SIZE])));
		if(empty($sizes))
			$sizes = array( 0 => 0 ); //to keep the loop going in create

		$jahit = array_filter(array_keys(array_intersect_key($tags,static::$_tags[Tag::TYPE_JAHIT])));
		if(is_array($jahit) && count($jahit) > 0)
			$jahit = $jahit[0];

		return array(
			'types' => $types,
			'sizes' => $sizes,
			'jahit' => $jahit
		);
	}

	protected function createCrystalItem($input, $tags, $type_id, $size_id, $file, $item = false)
	{
		if(!$item)
			$item = new Item($input);

		$item->pcode = strtoupper(trim($item->pcode));
		$item->type = Item::TYPE_ITEM;

		//save group
		$group = ItemGroup::where('name', '=', $item->pcode)->first();
		if(!$group)
		{
			$group = new ItemGroup;
			$group->name = $item->pcode;
			$code = explode('/', $item->pcode);
			if(isset($code[0]))
				$group->master = $code[0];
			if(isset($code[1]))
				$group->variant = $code[1];
		}
		$group->description = strtoupper($input['description']);
		if(!isset($input['alias'])) $input['alias'] = '';
		$group->alias = strtoupper($input['alias']);
			if(!$group->save())
				return $this->error($group->getErrors());
		$item->group_id = $group->id;
		$item->variant = $group->variant;

		//combine the tags
		$tag_ids = array_filter(array($tags['jahit'],$type_id,$size_id));
		asort($tag_ids);
		$item->tag_ids = implode(',',$tag_ids);

		//1. generate the item code
		$item->code = static::$_tags[Tag::TYPE_TYPE][$type_id]->code.str_replace('/','', $item->pcode); //add type
		$item->code = $item->code.static::$_tags[Tag::TYPE_SIZE][$size_id]->code;

		$item->name = static::$_tags[Tag::TYPE_TYPE][$type_id]->code.' '.$item->pcode.' '.static::$_tags[Tag::TYPE_SIZE][$size_id]->name;

		//NEW: catch for contributor data
		$item->genre = $type_id;
		$item->size = $size_id;
		$brand = strtoupper(substr($item->pcode,0,2));
		$brand = array_search($brand, Item::$brands);
		if($brand)
			$item->brand = $brand;
		else
			$item->brand = 0;

		if(!$item->save())
			return $this->error($item->getErrors());

		//sync
		$item->tags()->sync($tag_ids);
		if(empty($file))
			return $item;

		//save image using group id
		$path = $group->getUploadPath();
		//check for file exist
		if(file_exists($path.'/'.$group->id.'.jpg')) {
			chmod($path.'/'.$group->id.'.jpg',0755);
			unlink($path.'/'.$group->id.'.jpg');
		}
		$file = $file->move($path, $group->id.'.jpg');

		//not efficient for multiple upload but oh well
		Image::make($file->getPathName())->resize(588, null, function($constraint) {
			$constraint->aspectRatio();
		})->save();

		return $item;
	}

	public function updateItem($id, $input, $inputTags, $file = null)
	{
		if(!isset($input['pcode']) || empty($input['pcode']))
			return $this->error('pcode is required');
		if(!preg_match("/([a-zA-Z]{2})([0-9]{5})\/([0-9]{2})/",$input['pcode']))
			return $this->error('pcode salah format','code');

		static::loadTags();
		$inputTags = array_flip($inputTags);
		$inputTags = static::sortTags($inputTags);

		$type_id = $inputTags['types'][0];
		$size_id = $inputTags['sizes'][0];
		$jahit_id = $inputTags['jahit'];

		$item = Item::findOrFail($id);
		$item->pcode = $input['pcode'];
		$item->price = $input['price'];
		$old_tags = explode(',', $item->tag_ids);
		$group_id = $item->group_id;

		// if(!empty($file)) $file = Image::make($file)->resize(300, null, function($constraint) {
		// 	$constraint->aspectRatio();
		// });

		//then create the items
		if(!$item = $this->createCrystalItem($input, $inputTags, $type_id, $size_id, $file, $item))
			return $this->error('error creating item');

		//NEXT, update jahit for other items
		//1. find if jahit has been modified
		if(!in_array($jahit_id, $old_tags)) { //not in array, modified
			//2. find other items in the same group, with the same type, but different size
			$itemTable = Item::table();
			$updates = Item::with('tags')->where('group_id','=',$group_id)->where('genre', '=', $type_id)->get();

			foreach($updates as $update) {
				//skip same id
				if($update->id == $item->id) continue;

				$new_tags = array_filter(array($inputTags['jahit'],$update->genre,$update->size));
				if(stripos($item->pcode, 'cc') !== false)
					$new_tags[] = ItemsManager::CORE_TAG;
				$update->tag_ids = implode(',',$new_tags);
				$update->save();
				$update->tags()->sync($new_tags);
			}
		}

		return $item;
	}

	//does not support multiple type + size
	public function updatePrice($price, $inputTags)
	{
		if(empty($price))
			return $this->error('price is required','code');

		static::loadTags();

		//find the tags
		$total = 0;
		$inputTags = static::sortTags($inputTags);

		$it = Item::table();
		$itt = ItemTag::table();
		foreach($inputTags['types'] as $key => $type_id)
		{
			foreach($inputTags['sizes'] as $key => $size_id)
			{
				$tags = $inputTags;
				$tag_ids = array_filter(array($tags['jahit'],$type_id,$size_id)); //check
				asort($tag_ids);

				$ids = DB::table($it)
					->select(array($it.'.id',DB::raw("COUNT(DISTINCT $itt.tag_id) as counter")))->join($itt,$itt.'.item_id','=',$it.'.id')
					->whereIn($itt.'.tag_id',$tag_ids)
					->groupBy($it.'.id')
					->having('counter','=',count($tag_ids))->lists($it.'.id');
				$total_ids = count($ids);
				$total += $total_ids;
				if($total_ids > 0)
				{
					$affected = DB::table($it)->whereIn('id',$ids)->update(array('price' => DB::raw('price + '.$price)));
				}
			}
		}

		return $total;
	}

	public static function loadTags()
	{
		Cache::forget('item_tags');
		static::$_tags = Cache::remember('item_tags', 300, function () {
			$tags = array();

			//initialize the tag types
			foreach(Tag::$types as $type => $val)
			{
				$tags[$type] = array();
			}

			$tags_db = Tag::orderBy('type')->orderBy('code','asc')->get();
			foreach($tags_db as $t)
			{
				$tags[$t->type][$t->id] = $t;
			}

			return $tags;
		});
		return static::$_tags;
	}

	//TODO: clean this up, make a universal function
	public static function loadTagsJSON($types)
	{
		if(empty(static::$_tags)) static::loadTags();
		Cache::forget('item_tags_json');
		static::$_json = Cache::remember('item_tags_json', 300, function () use($types) {
			$data = array();

			$count = 0;
			foreach($types as $type => $val)
			{
				$data[$count] = array();
				$data[$count]['name'] = $val;
				$data[$count]['type_id'] = $type;
				$data[$count]['data'] = array();
				foreach (static::$_tags[$type] as $value) {
					$data[$count]['data'][] = $value->toArray();
				}

				$count++;
			}

			return $data;
		});

		return static::$_json;
	}

	//item can be eloquent or std object instance
	public static function add($warehouse_id, $item, $quantity)
	{
		if($item->type == Item::TYPE_SERVICE) return true;

		if(!$wi = WarehouseItem::where('warehouse_id','=',$warehouse_id)->where('item_id','=',$item->id)->lockForUpdate()->first())
			$wi = WarehouseItem::create(array('warehouse_id' => $warehouse_id, 'item_id' => $item->id, 'quantity' => 0));

		$wi->quantity += $quantity;

		if(!$wi->save())
			throw new \Exception($wi->errors->first());
		return $wi;
	}

	public static function deduct($warehouse_id, $item, $quantity, $can_minus = false)
	{
		if($item->type == Item::TYPE_SERVICE) return true;

		if(!$wi = WarehouseItem::where('warehouse_id','=',$warehouse_id)->where('item_id','=',$item->id)->lockForUpdate()->first())
			$wi = WarehouseItem::create(array('warehouse_id' => $warehouse_id, 'item_id' => $item->id, 'quantity' => 0));

		if(!$can_minus && ($wi->quantity - $quantity) < 0) //check if minus is allowed
			throw new \Exception("{$item->name} cuma ada {$wi->quantity}, mau diambil {$quantity}");

		$wi->quantity -= $quantity;
		if(!$wi->save())
			throw new Exception($wi->errors->first());
		return $wi;
	}

	public static function toArray($items)
	{
		$response = array();
		if($items instanceof \Illuminate\Pagination\Paginator)
		{
			$response['currentPage'] = $items->getCurrentPage();
			$response['lastPage'] = $items->getLastPage();
		}
		$response['data'] = array();
		foreach ($items as $key => $detail) {
			$response['data'][$key]['image'] = $detail->getImageUrl();
			$response['data'][$key]['link'] = $detail->getDetailLink();
			$response['data'][$key]['edit_link'] = $detail->getEditLink();
			$response['data'][$key]['name'] = $detail->name;
			$response['data'][$key]['id'] = $detail->id;
			$response['data'][$key]['code'] = $detail->code;
			$response['data'][$key]['pcode'] = $detail->pcode;
			if($detail->type == Item::TYPE_ITEM) {
				if(!$detail->group) {
					pre($detail->group);exit;
				}
				$response['data'][$key]['description'] = $detail->group->description;
				$response['data'][$key]['alias'] = $detail->group->alias;
			}
			else {
				$response['data'][$key]['description'] = $detail->description;
				$response['data'][$key]['alias'] = $detail->alias;
			}

			$response['data'][$key]['price'] = $detail->price;
			$response['data'][$key]['edit'] = $detail->getEditLink();
			$response['data'][$key]['total_quantity'] = 0;
			$response['data'][$key]['total_quantity'] = self::findItemInWarehouse($detail->id);

			if($detail->depreciation)
			{
				$response['data'][$key]['d_date'] = $detail->depreciation->buy_date;
				$response['data'][$key]['d_monthly'] = $detail->depreciation->monthlyDepreciation();
				$response['data'][$key]['d_value'] = $detail->depreciation->calcValue();
			}
		}
		return $response;
	}

	public static function findItemInWarehouse($itemId)
	{
		$total = DB::table('items')->select(array(
			'items.id',
			DB::raw('SUM(warehouse_item.quantity) as total_quantity'),
		))->where('items.id','=', $itemId)->join('warehouse_item','items.id','=','warehouse_item.item_id')->leftJoin('customers','customers.id','=','warehouse_item.warehouse_id')->where('customers.type','=',Customer::TYPE_WAREHOUSE)->where(function($query) use($itemId) {
			$query->where('customers.deleted_at','=',null)->orWhere('customers.deleted_at','=','0000-00-00 00:00:00');
		})->first();

		if(!$total) return 0;
		return $total->total_quantity;
	}

	public static function findGroupInWarehouse($groupId)
	{
		$total = DB::table('items')->select(array(
			'items.group_id',
			DB::raw('SUM(warehouse_item.quantity) as total_quantity'),
		))->where('items.group_id','=', $groupId)->join('warehouse_item','items.id','=','warehouse_item.item_id')->leftJoin('customers','customers.id','=','warehouse_item.warehouse_id')->where('customers.type','=',Customer::TYPE_WAREHOUSE)->where(function($query) use($groupId) {
			$query->where('customers.deleted_at','=',null)->orWhere('customers.deleted_at','=','0000-00-00 00:00:00');
		})->first();

		if(!$total) return 0;
		return $total->total_quantity;
	}
}