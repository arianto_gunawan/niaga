<?
namespace App\Models;
class AProduksi extends Produksi
{
	protected $table = 'aproduksi';

	protected $fillable = array('potong_in', 'jahit_in', 'jahit_out', 'permak_in', 'permak_out', 'item_id', 'jahit','customer', 'user_id', 'potong_id', 'quantity', 'warna', 'temp_name', 'description', 'q_in', 'q_out', 'size_id', 'urgent', 'status', 'invoice', 'detail_id');

	public static function table()
	{
		return 'aproduksi';
	}
}