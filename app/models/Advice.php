<?
namespace App\Models;
class Advice extends BaseModel {
	protected $table = 'advices';
	protected $primaryKey = 'problem_id';

	const STATUS_ON = 1;
	const STATUS_OFF = 0;

	public static function table()
	{
		return 'advices';
	}
}