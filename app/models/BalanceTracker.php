<?
namespace App\Models;
class BalanceTracker extends BaseModel {
	protected $table = 'balance_trackers';
	protected $primaryKey = 'customer_id';

	public static function table()
	{
		return 'balance_trackers';
	}

	public function getTransactionIdsAttribute($value)
	{
		return explode(',',$value);
	}

	public function setTransactionIdsAttribute($value)
	{
		$this->attributes['transaction_ids'] = implode(',',array_filter($value));
	}

	public function getDates()
	{
		return array('partial_due');
	}

	public function printClass($id)
	{
		if(in_array($id,$this->transaction_ids)) return ' class="danger"';
		return '';
	}

	public function customer()
	{
		return $this->belongsTo('App\Models\Customer','customer_id');
	}

	public function partial()
	{
		return $this->belongsTo('App\Models\Transaction', 'partial_id');
	}
}