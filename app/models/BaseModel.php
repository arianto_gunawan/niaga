<?
namespace App\Models;
use Eloquent;
use Illuminate\Support\MessageBag;
use Dater;
class BaseModel extends Eloquent
{
	protected $errors;
	public $timestamps = false;
	public static $types = array();

	public function validate($data = null)
	{
		if(!$data) $data = $this->attributes;

		if(!isset(static::$rules) || empty(static::$rules)) return true;

		$old_rules = static::$rules;
		//check for update
		if($this->exists)
		{
			$rules = static::$rules;
			foreach($rules as $key => $val)
			{
				if(stripos($val, 'unique:') !== false)
					static::$rules[$key] = static::$rules[$key].','.$this->id;
			}
		}

		$validator = \Validator::make($data, static::$rules);

		if($validator->fails())
		{
			static::$rules = $old_rules;
			$this->errors = $validator->errors();
			return false;
		}
		static::$rules = $old_rules;
		return true;
	}

	public function validateDate($attr, $value)
	{
		$validator = \Validator::make(array($attr => $value), array($attr => 'date_format:d/m/Y'));

		if($validator->fails())
		{
			$this->errors = $validator->errors();
			return false;
		}

		return true;
	}

	public function getErrors()
	{
		if(!$this->errors instanceof MessageBag)
			$this->errors = new MessageBag;
		return $this->errors->toArray();
	}

	public function printType($key = false)
	{
		if(!$key) $key = $this->type;
		if(!isset(static::$types[$key])) return '';
		return static::$types[$key];
	}

	public function addError($key, $msg)
	{
		if(!$this->errors instanceof MessageBag)
			$this->errors = new MessageBag;
		$this->errors->add($key, $msg);
		return false;
	}

	function __clone()
	{
		$this->id = null;
		$this->exists = false;
		$this->original = array();
	}

	public function save(array $options = array())
	{
		if(!$this->validate())
			return false;

		//from laravel
		$query = $this->newQueryWithoutScopes();
		if ($this->fireModelEvent('saving') === false)
		{
			return false;
		}
		if ($this->exists)
		{
			$saved = $this->performUpdate($query);
		}
		else
		{
			$saved = $this->performInsert($query);
		}
		if ($saved) $this->finishSave($options);
		return $saved;
	}

	protected function dateToSQL($attr, $value)
	{
		//if correct sql format
		if(preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/",$value))
			return $this->attributes[$attr] = $value;

		if(!$this->validateDate($attr, $value))
			$this->attributes[$attr] = '0000-00-00';

		if($value instanceof Dater || $value instanceof \Carbon\Carbon)
			return $this->attributes[$attr] = $value->format(Dater::$SQLFormat);

		$this->attributes[$attr] = Dater::toSQL($value);
	}
}