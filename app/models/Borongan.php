<?
namespace App\Models;
class Borongan extends \App\Models\BaseModel
{
	protected $table = 'prod_borongan';
	protected $fillable = array('date', 'jahit_id', 'tres', 'permak' ,'lain2');

	public static function table()
	{
		return 'borongan';
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User','user_id');
	}

	public function jahit()
	{
		return $this->belongsTo('App\Models\Worker','jahit_id');
	}

	public function getDetailLink()
	{
		return \URL::action('BoronganController@getDetail',array($this->id));
	}
}
?>