<?
namespace App\Models;
class City extends BaseModel {
	protected $table = 'geo_city';

	public static function table()
	{
		return 'geo_city';
	}

	public function province()
	{
		return $this->belongsTo('App\Models\Province', 'province_id');
	}
}