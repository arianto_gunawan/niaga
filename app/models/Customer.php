<?
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Config;
class Customer extends BaseModel {
	use SoftDeletingTrait;

	protected $dates = ['deleted_at'];
	protected $table = 'customers';

	protected $fillable = array('name', 'address', 'birthdate', 'category', 'return_p', 'discount', 'description', 'phone', 'email', 'fax', 'contract_ends','phone2', 'province_id', 'city_id');
	public static $rules = array(
		'name' => 'required',
	);

	const TYPE_CUSTOMER = 1;
	const TYPE_WAREHOUSE = 2;
	const TYPE_BANK = 3;
	const TYPE_SUPPLIER = 4;
	const TYPE_VWAREHOUSE = 5;
	const TYPE_VACCOUNT = 6;
	const TYPE_RESELLER = 7;
	const TYPE_ACCOUNT = 8;

	public static $types = array(
		self::TYPE_CUSTOMER => 'Customer',
		self::TYPE_WAREHOUSE => 'Warehouse',
		self::TYPE_BANK => 'Banks',
		self::TYPE_SUPPLIER => 'Supplier',
		self::TYPE_VWAREHOUSE => 'V. Warehouse',
		self::TYPE_VACCOUNT => 'V. Account',
		self::TYPE_RESELLER => 'Reseller',
	);

	public static $typesJSON = array(
		array('id' => self::TYPE_CUSTOMER, 'name' => 'Customer'),
		array('id' => self::TYPE_WAREHOUSE, 'name' => 'Warehouse'),
		array('id' => self::TYPE_BANK, 'name' => 'Banks'),
		array('id' => self::TYPE_SUPPLIER, 'name' => 'Supplier'),
		array('id' => self::TYPE_VWAREHOUSE, 'name' => 'V. Warehouse'),
		array('id' => self::TYPE_VACCOUNT, 'name' => 'V. Account'),
		array('id' => self::TYPE_RESELLER, 'name' => 'Reseller'),
	);

	public static $notAdjustable = array(Customer::TYPE_VACCOUNT, Customer::TYPE_VWAREHOUSE, Customer::TYPE_WAREHOUSE);

	public static function table()
	{
		return 'customers';
	}

	public function stat()
	{
		return $this->hasOne('App\Models\CustomerStat');
	}

	public function portal()
	{
		return $this->hasOne('Portal\Models\User', 'customer_id');
	}

	public function getDetailLink()
	{
		return $this->generateLink('getDetail');
	}

	public function getTransactionsLink()
	{
		return $this->generateLink('getTransactions');
	}

	public function getStatLink()
	{
		return $this->generateLink('getStat');
	}

	public function getItemsLink()
	{
		return $this->generateLink('getItems');
	}

	public function getHashLink()
	{
		return $this->generateLink('getHash');
	}

	public function getSalesLink()
	{
		return $this->generateLink('getSales');
	}

	public function getEditLink()
	{
		return $this->generateLink('getEdit');
	}

	public function getController()
	{
		switch($this->type)
		{
			case self::TYPE_WAREHOUSE: return 'WarehousesController'; break;
			case self::TYPE_BANK: return 'BankAccountsController'; break;
			case self::TYPE_SUPPLIER: return 'SuppliersController'; break;
			case self::TYPE_VWAREHOUSE: return 'VWarehousesController'; break;
			case self::TYPE_VACCOUNT: return 'VAccountsController'; break;
			case self::TYPE_RESELLER: return 'ResellersController'; break;
			case self::TYPE_ACCOUNT: return 'OperationsController'; break;
			default: return 'CustomersController'; break;
		}
	}

	protected function generateLink($action)
	{
		if(!$this->id) return '';
		switch($this->type)
		{
			case self::TYPE_WAREHOUSE: $action = 'WarehousesController@'.$action; break;
			case self::TYPE_BANK: $action = 'BankAccountsController@'.$action; break;
			case self::TYPE_SUPPLIER: $action = 'SuppliersController@'.$action; break;
			case self::TYPE_VWAREHOUSE: $action = 'VWarehousesController@'.$action; break;
			case self::TYPE_VACCOUNT: $action = 'VAccountsController@'.$action; break;
			case self::TYPE_RESELLER: $action = 'ResellersController@'.$action; break;
			case self::TYPE_ACCOUNT:
				if($action == 'getDetail') $action = 'getAccountDetail';
				$action = 'OperationsController@'.$action;
				break;
			default: $action = 'CustomersController@'.$action; break;
		}
		return \URL::action($action,array($this->id));
	}

	//for reports
	public static function detailLink($id,$type)
	{
		if(!$id) return '';
		switch($type)
		{
			case self::TYPE_WAREHOUSE: $action = 'WarehousesController@getDetail'; break;
			case self::TYPE_BANK: $action = 'BankAccountsController@getDetail'; break;
			case self::TYPE_SUPPLIER: $action = 'SuppliersController@getDetail'; break;
			case self::TYPE_VWAREHOUSE: $action = 'VWarehousesController@getDetail'; break;
			case self::TYPE_VACCOUNT: $action = 'VAccountsController@getDetail'; break;
			case self::TYPE_RESELLER: $action = 'ResellersController@getDetail'; break;
			case self::TYPE_ACCOUNT: $action = 'OperationsController@getAccountDetail'; break;
			default: $action = 'CustomersController@getDetail'; break;
		}
		return \URL::action($action,array($id));
	}

	public function locations()
	{
		return $this->belongsToMany('App\Models\Location', 'location_customer');
	}

	public function scopeFilter($query,$location = null)
	{
		$lm = \App::make('lm');

		//check location
		if($location || $lm->bound())
		{
			$lids = $lm->get_location($location);
			$query = $query->where(function($query) use($lids)
			{
				$query->whereIn(Customer::table().'.id',$lids)->orWhereIn(Customer::table().'.type',array(Customer::TYPE_CUSTOMER, Customer::TYPE_ACCOUNT));
			})->whereNull('deleted_at');
		}

		return $query;
	}

	public function scopeAccounts($query)
	{
		return $query->where('type','=',Customer::TYPE_ACCOUNT);
	}

	public function operation()
	{
		return $this->belongsTo('App\Models\Operation','parent_id');
	}

	public function setBirthdateAttribute($value)
	{
		$this->dateToSQL('birthdate',$value);
	}

	public function setContractEndsAttribute($value)
	{
		$this->dateToSQL('contract_ends',$value);
	}
}