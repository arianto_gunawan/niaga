<?
namespace App\Models;
class CustomerClass extends BaseModel {
	protected $table = 'customer_class';

	public static function table()
	{
		return 'customer_class';
	}

	public function customer()
	{
		return $this->belongsTo('App\Models\Customer','customer_id');
	}
}