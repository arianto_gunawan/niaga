<?
namespace App\Models;
class CustomerStat extends BaseModel {
	protected $table = 'customerstat';
	protected $primaryKey = 'customer_id';
	public $timestamps = true;

	public static function table()
	{
		return 'customerstat';
	}

	public function customer()
	{
		return $this->belongsTo('App\Models\Customer','customer_id');
	}
}