<?
namespace App\Models;
class Cuti extends BaseModel {
	protected $table = 'p_cuti';
	protected $fillable = array('personnel_id', 'date_from', 'date_to', 'type' ,'sisa_cuti', 'description');

	public static $rules = array(
		'date_from' => 'required',
		'date_to' => 'required',
	);

	const TAHUNAN = 1;
	const SAKIT = 2;
	const MELAHIRKAN = 3;
	const MENDADAK = 4;

	const MAX_CUTI = 8;

	public static $types = array(
		self::TAHUNAN => 'Tahunan',
		self::SAKIT => 'Sakit',
		self::MELAHIRKAN => 'Melahirkan',
		self::MENDADAK => 'Mendadak',
	);

	public static function table()
	{
		return 'p_cuti';
	}

	public function personnel()
	{
		return $this->belongsTo('App\Models\Personnel','personnel_id')->withTrashed();
	}
}