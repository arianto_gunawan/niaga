<?
namespace App\Models;

class Deleted extends Transaction {
	protected $table = 'deleted';

	public static $rules = array(
		'date' => 'required|cdate',
	);

	public function getDetailLink($name = null)
	{
		if(!$name) $name = $this->printDate();
		return \URL::action('DeletedController@getDetail',array($this->id,$this->date));
	}
}