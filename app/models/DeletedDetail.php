<?
namespace App\Models;
class DeletedDetail extends TransactionDetail {
	protected $table = 'deleted_details';
	protected $fillable = array('transaction_id', 'item_id', 'quantity', 'price', 'discount', 'total', 'date');

	public static function table()
	{
		return 'deleted_details';
	}
}