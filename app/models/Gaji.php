<?
namespace App\Models;
class Gaji extends BaseModel {
	protected $table = 'gaji';

	protected $fillable = array('bonus','gaji_bulanan','gaji_harian','premi','sanksi','cuti_mendadak','terlambat','premi_hangus','cuti_mendadak');

	public static function table()
	{
		return 'gaji';
	}

	public function personnel()
	{
		return $this->belongsTo('App\Models\Personnel','personnel_id');
	}
}