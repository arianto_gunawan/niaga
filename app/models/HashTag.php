<?
namespace App\Models;
class HashTag extends BaseModel {
	protected $table = 'hashtags';
	protected $fillable = array('name');

	public static function table()
	{
		return 'hashtags';
	}
}