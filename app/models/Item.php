<?
namespace App\Models;
use Apps, App, Cache, Config, Dater, DB, Event, Input, InputForm, Redirect, Response, Session, URL, View, ModelException, Exception, Auth;
class Item extends BaseModel {
	protected $table = 'items';

	protected $fillable = array('name', 'code', 'pcode', 'price' , 'cost', 'description', 'url');

	const BRAND_00 = 0;
	const BRAND_CA = 1;
	const BRAND_CC = 2;
	const BRAND_CB = 3;
	const BRAND_CD = 4;
	const BRAND_CR = 5;
	const BRAND_CN = 6;
	const BRAND_CM = 7;
	const BRAND_CX = 8;
	const BRAND_CS = 9;
	const BRAND_HJ = 10;
	const BRAND_CP = 11;
	const BRAND_CJ = 12;
	const BRAND_PL = 13;
	const BRAND_DC = 14;
	const BRAND_CE = 15;

	public static $brands = array(
		self::BRAND_00 => 'No Brand',
		self::BRAND_CA => 'CA',
		self::BRAND_CC => 'CC',
		self::BRAND_CB => 'CB',
		self::BRAND_CD => 'CD',
		self::BRAND_CR => 'CR',
		self::BRAND_CN => 'CN',
		self::BRAND_CM => 'CM',
		self::BRAND_CX => 'CX',
		self::BRAND_CS => 'CS',
		self::BRAND_HJ => 'HJ',
		self::BRAND_CP => 'CP',
		self::BRAND_CJ => 'CJ',
		self::BRAND_PL => 'PL',
		self::BRAND_DC => 'DC',
		self::BRAND_CE => 'CE',
	);

	public static $brandsJSON = array(
		array('id' => self::BRAND_00, 'name' => 'No Brand'),
		array('id' => self::BRAND_CA, 'name' => 'CA'),
		array('id' => self::BRAND_CC, 'name' => 'CC'),
		array('id' => self::BRAND_CB, 'name' => 'CB'),
		array('id' => self::BRAND_CD, 'name' => 'CD'),
		array('id' => self::BRAND_CR, 'name' => 'CR'),
		array('id' => self::BRAND_CN, 'name' => 'CN'),
		array('id' => self::BRAND_CM, 'name' => 'CM'),
		array('id' => self::BRAND_CX, 'name' => 'CX'),
		array('id' => self::BRAND_CS, 'name' => 'CS'),
		array('id' => self::BRAND_HJ, 'name' => 'HJ'),
		array('id' => self::BRAND_CP, 'name' => 'CP'),
		array('id' => self::BRAND_CJ, 'name' => 'CJ'),
		array('id' => self::BRAND_PL, 'name' => 'PL'),
		array('id' => self::BRAND_DC, 'name' => 'DC'),
		array('id' => self::BRAND_CE, 'name' => 'CE'),
	);

	const TYPE_ITEM = 1;
	const TYPE_ASSET_LANCAR = 2;
	const TYPE_ASSET_TETAP = 3;
	const TYPE_SERVICE = 5;

	public static function table()
	{
		return 'items';
	}

	public static $types = array(
		self::TYPE_ITEM => 'Item',
		self::TYPE_ASSET_LANCAR => 'Asset Lancar',
		self::TYPE_ASSET_TETAP => 'Asset Tetap',
	);

	public static $rules = array(
		'code' => 'required|unique:items,code',
	);

	public function quantities()
	{
		return $this->hasMany('App\Models\WarehouseItem','item_id');
	}

	public function tags()
	{
		return $this->belongsToMany('App\Models\Tag','item_tag','item_id');
	}

	public function group()
	{
		return $this->belongsTo('App\Models\ItemGroup', 'group_id');
	}

	public function depreciation()
	{
		return $this->hasOne('App\Models\Depreciation','item_id');
	}

	public function getDetailLink()
	{
		return $this->generateLink('getDetail');
	}

	public function getTransactionsLink()
	{
		return $this->generateLink('getTransactions');
	}

	public function getCustomersLink()
	{
		return $this->generateLink('getCustomers');
	}

	public function getEditLink()
	{
		return $this->generateLink('getEdit');
	}

	public function getStatsLink()
	{
		return $this->generateLink('getStat');
	}

	protected function generateLink($action)
	{
		if(!$this->id) return '';
		switch($this->type)
		{
			case self::TYPE_ASSET_LANCAR: $action = 'AssetLancarController@'.$action; break;
			case self::TYPE_ASSET_TETAP: $action = 'AssetTetapController@'.$action; break;
			default: $action = 'ItemsController@'.$action; break;
		}
		return URL::action($action,array($this->id));
	}

	public function getUrl()
	{
		if(!$this->id) return '';
		switch($this->type)
		{
			case self::TYPE_ASSET_LANCAR: $action = 'AssetLancarController@getDetail'; break;
			case self::TYPE_ASSET_TETAP: $action = 'AssetTetapController@getDetail'; break;
			default: $action = 'ItemsController@getDetail'; break;
		}
		return URL::action($action,array($this->id));
	}

	public static function getDetailUrl($id,$type)
	{
		switch($type)
		{
			case self::TYPE_ASSET_LANCAR: $action = 'AssetLancarController@getDetail'; break;
			case self::TYPE_ASSET_TETAP: $action = 'AssetTetapController@getDetail'; break;
			default: $action = 'ItemsController@getDetail'; break;
		}
		return URL::action($action,array($id));
	}

	public static function getEditButton($id, $type)
	{
		switch($type)
		{
			case self::TYPE_ASSET_LANCAR: $action = 'AssetLancarController@getEdit'; break;
			case self::TYPE_ASSET_TETAP: $action = 'AssetTetapController@getEdit'; break;
			default: $action = 'ItemsController@getEdit'; break;
		}
		return URL::action($action,array($id));
	}

	public function getUploadPath()
	{
		$folder = str_pad(substr($this->id, -2), 2, '0', STR_PAD_LEFT);

		return Config::get('local.item_image_path').$folder;
	}

	public function getImageUrl()
	{
		if($this->type == Item::TYPE_ITEM && $this->group_id > 0)
			return self::getImagePath($this->group_id);
		return self::getImagePath($this->id);
	}

	public static function getImagePath($id)
	{
		$folder = str_pad(substr($id, -2), 2, '0', STR_PAD_LEFT);

		return 'https://cdn.corenationactive.com'.Config::get('local.item_image_url').$folder.'/'.$id.'.jpg';
	}
}
?>