<?
namespace App\Models;
class ItemAlert extends BaseModel {
	protected $table = 'itemalert';

	public static $rules = array(
		'item_id' => 'required',
		'type' => 'required',
		'quantity' => 'required',
	);

	const LE = 1;
	const GE = 2;

	public static $types = array(
		self::LE => '<=',
		self::GE => '>=',
	);

	public function item()
	{
		return $this->belongsTo('App\Models\Item','item_id');
	}
}
?>