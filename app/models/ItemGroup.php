<?
namespace App\Models;
use App\Models\BaseModel;
use Config, URL;
class ItemGroup extends BaseModel {
	protected $table = 'item_group';

	protected $fillable = array('name');

	public static function table()
	{
		return 'item_group';
	}

	public static $rules = array(
		'name' => 'required|unique:item_group,name',
	);

	public function items()
	{
		return $this->hasMany('App\Models\Item','group_id');
	}

	public function getDetailLink()
	{
		return URL::action('ItemsController@getGroupDetail', $this->id);
	}

	public function getStatLink()
	{
		return URL::action('ItemsController@getGroupStat', $this->id);
	}

	public function getUploadPath()
	{
		$folder = str_pad(substr($this->id, -2), 2, '0', STR_PAD_LEFT);

		return Config::get('local.item_image_path').$folder;
	}

	public function getImageUrl()
	{
		return self::getImagePath($this->id);
	}

	public static function getImagePath($id)
	{
		$folder = str_pad(substr($id, -2), 2, '0', STR_PAD_LEFT);

		return 'https://cdn.corenationactive.com'.Config::get('local.item_image_url').$folder.'/'.$id.'.jpg';
	}
}