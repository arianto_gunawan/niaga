<?
namespace App\Models;
class ItemStat extends BaseModel {
	protected $table = 'item_stat';

	public static function table()
	{
		return 'item_stat';
	}

	public function item()
	{
		return $this->belongsTo('App\Models\Item','item_id');
	}

	public function getDates()
	{
		return array('date');
	}
}
?>