<?
namespace App\Models;

class LC extends BaseModel {
	protected $table = 'location_customer';
	public $timestamps = false;

	public function customer()
	{
		return $this->belongsTo('App\Models\Customer','customer_id');
	}

	public function location()
	{
		return $this->belongsTo('App\Models\Location','location_id');
	}
}