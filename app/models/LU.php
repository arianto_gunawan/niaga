<?
namespace App\Models;

class LU extends BaseModel {
	protected $table = 'location_user';
	public $timestamps = false;

	public function user()
	{
		return $this->belongsTo('App\Models\User','user_id');
	}

	public function location()
	{
		return $this->belongsTo('App\Models\Location','location_id');
	}
}