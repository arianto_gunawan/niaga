<?
namespace App\Models;
class Notification extends BaseModel {
	protected $table = 'notifications';
	public $timestamps = true;
	protected $fillable = array('entity_id', 'app_id');

	public static function table()
	{
		return 'notifications';
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User','user_id');
	}
}