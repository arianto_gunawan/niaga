<?
namespace App\Models;
class Operation extends BaseModel {
	protected $table = 'operations';
	protected $fillable = array('name','description');
	public static $rules = array(
		'name' => 'required|unique:operations,name',
	);

	const TYPE_PRODUCTION = 1;
	const TYPE_OPERATION = 2;
	const TYPE_NONOP = 3;

	const TAXABLE = 1;
	const NON_TAXABLE = 0;

	public static function table()
	{
		return 'operations';
	}
}