<?
namespace App\Models;
class PC extends BaseModel {
	protected $table = 'personnel_cuti';
	protected $fillable = array('personnel_id', 'sisa', 'tahun');

	public static function table()
	{
		return 'personnel_cuti';
	}

	public function personnel()
	{
		return $this->belongsTo('App\Models\Personnel','personnel_id');
	}
}