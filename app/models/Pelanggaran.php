<?
namespace App\Models;
class Pelanggaran extends BaseModel {
	protected $table = 'p_pelanggaran';

	protected $fillable = array('personnel_id', 'description', 'date', 'jenis' ,'disipliner', 'administrative', 'disipliner_date');

	public static $rules = array(
		'date' => 'required',
		'disipliner_date' => 'required',
	);

	const TEGURAN = 1;
	const SP1 = 2;
	const SP2 = 3;

	public static $disipliner = array(
		self::TEGURAN => 'Teguran',
		self::SP1 => 'SP1',
		self::SP2 => 'SP2',
	);

	public static function table()
	{
		return 'p_pelanggaran';
	}

	public function personnel()
	{
		return $this->belongsTo('App\Models\Personnel','personnel_id')->withTrashed();
	}
}