<?
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class Personnel extends \App\Models\Personnel {

	const POTONG = 5;
	const JAHIT = 3;

	protected $dates = ['deleted_at'];
	protected $table = 'personnels';
	protected $fillable = array('name', 'address', 'group', 'phone', 'department', 'jabatan' ,'status', 'date_in','date_out','birthdate','birthplace');

	const STATUS_ACTIVE = 1;

	public static $DEPTS = array(
		1 => 'Finance',
		2 => 'Marketing',
		self::JAHIT => 'Jahit',
		4 => 'Sales',
		self::POTONG => 'Potong',
		6 => 'IT',
		7 => 'Human Resource',
		8 => 'Warehouse',
		9 => 'Production',
		0 => '',
		10 => 'Admin',
	);

	public static $deptsJSON = array(
		array('id' => 1, 'name' => 'Finance'),
		array('id' => 2, 'name' => 'Marketing'),
		array('id' => self::JAHIT, 'name' => 'Jahit'),
		array('id' => 4, 'name' => 'Sales'),
		array('id' => self::POTONG, 'name' => 'Potong'),
		array('id' => 6, 'name' => 'IT'),
		array('id' => 7, 'name' => 'Human Resource'),
		array('id' => 8, 'name' => 'Warehouse'),
		array('id' => 9, 'name' => 'Production'),
		array('id' => 10, 'name' => 'Admin'),
	);

	public function scopeJahitProduksi($query)
	{
		$query = $query->where('department','=',Personnel::JAHIT)->where('status','=',self::STATUS_ACTIVE)->where('group','!=','')->groupBy('group');
		return $query;
	}

	public function scopePotongProduksi($query)
	{
		$query = $query->where('department','=',Personnel::POTONG)->where('status','=',self::STATUS_ACTIVE);
		return $query;
	}

	public static $STATUS = array(
		self::STATUS_ACTIVE => 'Active',
		2 => 'Inactive',
		3 => 'Terminated',
	);

	public static $statusJSON = array(
		array('id' => 0, 'name' => 'All'),
		array('id' => self::STATUS_ACTIVE, 'name' => 'Active'),
		array('id' => 2, 'name' => 'Inactive'),
		array('id' => 3, 'name' => 'Terminated'),
	);

	public static $rules = array(
		'name' => 'required',
		'address' => 'required',
		'phone' => 'required',
	);

	public static function table()
	{
		return 'personnels';
	}

	public function printDepartment()
	{
		return static::$DEPTS[$this->department];
	}

	public function printStatus()
	{
		return static::$STATUS[$this->status];
	}

	public function account()
	{
		return $this->belongsTo('App\Models\Customer','account_id');
	}

	public function getMonthly()
	{
		return $this->bulanan + $this->premi + bcmul($this->harian, 26);
	}

	public function getDetailLink()
	{
		return URL::action('PersonnelsController@getDetail',array($this->id));
	}

	public function getEditLink()
	{
		return URL::action('PersonnelsController@getEdit',array($this->id));
	}

	public function getDepartment()
	{
		if(isset(self::$DEPTS[$this->department])) return self::$DEPTS[$this->department];
		return '';
	}

	public function getStatus()
	{
		if(isset(self::$STATUS[$this->status])) return self::$STATUS[$this->status];
		return '';
	}

	public function getCutiLink()
	{
		return URL::action('PersonnelsController@getCuti',array($this->id));
	}

	public function getPelanggaranLink()
	{
		return URL::action('PersonnelsController@getPelanggaran',array($this->id));
	}

	public function getGpuLink()
	{
		return URL::action('PersonnelsController@getGpu',array($this->id));
	}
}