<?
namespace App\Models;
class Problem extends BaseModel {
	protected $table = 'problems';

	const NETT10 = 1;

	public static function table()
	{
		return 'problems';
	}

	public function solutions()
	{
		return $this->belongsToMany('App\Models\Solution');
	}
}