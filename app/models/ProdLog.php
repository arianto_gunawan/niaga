<?
namespace App\Models;
class ProdLog extends \App\Models\BaseModel
{
	protected $table = 'prod_log';

	const SPLIT = 'split';
	const EDIT = 'edit';
	const DELETE = 'delete';
	const ARSIP = 'arsip';

	public static function table()
	{
		return 'prod_log';
	}
}