<?
namespace App\Models;

class ProduksiTransaction extends Transaction {
	public static $rules = array(
		'date' => 'required',
		'sender_id' => 'required',
		'receiver_id' => 'required|different:sender_id',
	);
}