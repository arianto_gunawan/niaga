<?
namespace App\Models;
class Province extends BaseModel {
	protected $table = 'geo_province';

	public static function table()
	{
		return 'geo_province';
	}

	public function cities()
	{
		return $this->hasMany('App\Models\City', 'province_id');
	}
}