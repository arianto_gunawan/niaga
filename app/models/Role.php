<?
namespace App\Models;
class Role extends BaseModel {
	protected $table = 'roles';

	public static $rules = array(
		'name' => 'required|unique:roles,name',
	);
}