<?
namespace App\Models;
class Settings extends BaseModel {
	const TIME = 3600;
	const LOC_GLOBAL = 0;
	protected $table = 'settings';

	//defaults
	public static $defaults = array(
		'start_time' => '00:00',
		'stop_time' => '00:00',
		'sunday_off' => 0,
		'turn_off' => 0,
	);

	public static $desc = array(
		'start_time' => 'app start',
		'stop_time' => 'app stop',
		'sunday_off' => 'wtf',
		'turn_off' => 'duh?',
	);

	public function scopeLocation($query, $loc = false)
	{
		if(!$loc) $loc = \Auth::user()->location_id;
		return $query->where('location_id','=',$loc);
	}
}