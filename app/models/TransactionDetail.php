<?
namespace App\Models;
class TransactionDetail extends BaseModel {
	protected $table = 'transaction_details';
	public $timestamps = false;

	protected $rules = array(
		'transaction_id' => 'required',
		'item_id' => 'required',
		'quantity' => 'required',
	);

	protected $fillable = array('item_id', 'quantity', 'price', 'discount');

	public static function table()
	{
		return 'transaction_details';
	}

	public function transaction()
	{
		return $this->belongsTo('App\Models\Transaction','transaction_id');
	}

	public function item()
	{
		return $this->belongsTo('App\Models\Item','item_id');
	}
}
?>