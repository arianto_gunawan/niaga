<?
namespace App\Models;
class Updater extends BaseModel {
	const NEED_UPDATE = 0;
	const NO_UPDATE = 1;
	protected $table = 'updater';

	public function customer()
	{
		return $this->belongsTo('App\Models\Customer','entity_id');
	}
}