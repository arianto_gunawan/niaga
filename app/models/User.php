<?
namespace App\Models;

use Illuminate\Auth\UserInterface;
use App\Models\BaseModel;
class User extends BaseModel implements UserInterface
{
	const ROLE_ADMIN = 0;

	protected $table = 'users';

	protected $hidden = array('password');
	protected $fillable = array('username','role_id','location_id');

	public static $rules = array(
		'username' => 'required|unique:users,username',
	);

	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	public function getAuthPassword()
	{
		return $this->password;
	}

	public static function generatePassword()
	{
		$string = 'abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ12356789_!@#$%^&*';
		$pass = '';
		$length = 9;
		for($i = 0; $i < 9; $i++)
		{
			$bit = mt_rand(0,strlen($string)-1);
			$pass .= substr($string,$bit,1);
		}

		return $pass;
	}

	public function location()
	{
		return $this->belongsTo('App\Models\Location','location_id');
	}

	public function role()
	{
		return $this->belongsTo('App\Models\Role','role_id');
	}

	public static function updateThrottle($data)
	{
//		.$this->app['request']->server('REMOTE_ADDR').$this->app['request']->server('HTTP_X_FORWARDED_FOR');
	}

	public function getRememberToken()
	{
		return $this->remember_token;
	}

	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
		return 'remember_token';
	}
}