<?
namespace App\Models;
class WebProduct extends \App\Models\BaseModel {
	protected $table = 'products';
	protected $connection = 'web';
	public $timestamps = false;

	public static function table()
	{
		return 'products';
	}
}
?>