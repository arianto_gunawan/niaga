<?
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class Worker extends \App\Models\BaseModel {
	use SoftDeletingTrait;

	protected $dates = ['deleted_at'];

	const TYPE_POTONG = 1;
	const TYPE_JAHIT = 2;

	protected $table = 'prod_worker';
	protected $fillable = array('name');

	public function scopeJahit($query)
	{
		$query = $query->where('type','=',Worker::TYPE_JAHIT);
		return $query;
	}

	public function scopePotong($query)
	{
		$query = $query->where('type','=',Worker::TYPE_POTONG);
		return $query;
	}
}