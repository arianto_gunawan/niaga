<?
namespace Portal\Models;
class User extends \App\Models\BaseModel
{
	protected $connection = 'portal';
	protected $table = 'users';

	public static function table()
	{
		return 'users';
	}
}