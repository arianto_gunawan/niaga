<?
namespace App\Models\Spg;
class User extends \App\Models\BaseModel
{
	protected $connection = 'spg';
	protected $table = 'users';

	const STATUS_ACTIVE = 1;
	const STATUS_OFF = 0;

	public static function table()
	{
		return 'users';
	}

	public function warehouse()
	{
		return $this->belongsTo('App\Models\Customer','warehouse_id')->withTrashed();
	}

	public function getEditLink()
	{
		return \URL::action('SpgController@getEdit',array($this->id)); 
	}
}