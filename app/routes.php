<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
date_default_timezone_set('Asia/Jakarta');

use App\Models\Settings, App\Models\LoginLog;

Apps::init();

App::singleton('access', function() {
	return new App\Libraries\Access;
});

App::singleton('appsettings', function() {
	return new App\Libraries\AppSettings;
});

App::singleton('lm', function()
{
    return new App\Libraries\LocationManager;
});

Route::get('logout',  array('as' => 'logout', 'uses' => 'AuthController@getLogout'));
Route::get('login',   array('as' => 'login', 'uses' => 'AuthController@getLogin'));
Route::post('login',  array('as' => 'login.post',  'uses' => 'AuthController@postLogin'));

Route::group(array('before' => 'auth.admin|settings'), function()
{
	Route::any('/', 'HomeController@getIndex');

	Route::controller('asset-lancar', 'AssetLancarController');
	Route::controller('asset-tetap', 'AssetTetapController');
	Route::controller('bank-accounts', 'BankAccountsController');
	Route::controller('customers', 'CustomersController');
	Route::controller('cuti', 'CutiController');
	Route::controller('deleted', 'DeletedController');
	Route::controller('gaji', 'GajiController');
	Route::controller('hash', 'HashController');
	Route::controller('home', 'HomeController');
	Route::controller('items', 'ItemsController');
	Route::controller('vitems', 'VItemsController');
	Route::controller('locations', 'LocationsController');
	Route::controller('notifications', 'NotificationsController');
	Route::controller('operations', 'OperationsController');
	Route::controller('pelanggaran', 'PelanggaranController');
	Route::controller('personnels', 'PersonnelsController');
	Route::controller('profile', 'ProfileController');
	Route::controller('reports', 'ReportsController');
	Route::controller('resellers', 'ResellersController');
	Route::controller('settings', 'SettingsController');
	Route::controller('suppliers', 'SuppliersController');
	Route::controller('tags', 'TagsController');
	Route::controller('transactions', 'TransactionsController');
	Route::controller('users', 'UsersController');
	Route::controller('vaccounts', 'VAccountsController');
	Route::controller('vwarehouses', 'VWarehousesController');
	Route::controller('warehouses', 'WarehousesController');
	Route::controller('produksi', 'ProduksiController');

	//custom
	Route::controller('aproduksi', 'AProductionsController');
	Route::controller('borongan', 'BoronganController');
	Route::controller('contributors', 'ContributorsController');
	Route::controller('setoran', 'SetoranController');
	Route::controller('tools', 'ToolsController');
	Route::controller('spg', 'SpgController');
});

Route::filter('auth.admin', function()
{
	if (Auth::guest())
		return Redirect::route('login');

	//check permission for anything other than named routes
	$apps = Apps::$list;

	//get controller
	$route = explode('/',Request::path());
	$controller = (isset($route[0]) && !empty($route[0])) ? $route[0] : 0;
	$action = (isset($route[1]) && !empty($route[1])) ? $route[1] : 'index';

	$app_id = array_search($controller,$apps);
	//no app id
	if(empty($app_id)) return; //not in acl
	//no action
	if(!isset(Apps::$actions[$app_id]) || !isset(Apps::$actions[$app_id][$action])) return; //not in acl
	if(!App::make('access')->can($action,$app_id))
		return App::abort(404);

	Session::put('current-page', $controller.'-'.$action);
});

Route::filter('settings', function()
{
	$user = Auth::user();
	if(!$user)
		return App::abort('404');

	//load local
	$location = $user->location_id;
	$settings = Cache::remember('settings.'.$location, Settings::TIME, function () use ($location) {
		return Settings::location($location)->lists('value','name');
	});

	if($settings['turn_off'] == 1 && $user->role_id != Access::ADMIN)
		return App::abort('404');

	if($settings['sunday_off'] == 1 && $user->role_id != Access::ADMIN)
	{
		if(date('w') == 0)
			return App::abort('404');
	}

	//check for start/stop time
	if($user->role_id != Access::ADMIN)
	{
		$time = date('Gi');
		$start = str_replace(':','',$settings['start_time']);
		$stop = str_replace(':','',$settings['stop_time']);

		//special case
		if($stop < $start)
		{
			if($time > $stop && $time < $start)
			{
				$log = new Loginlog;
				$log->date = \Carbon\Carbon::now()->toDateTimeString();
				$data = array(
					'username' => $user->username,
					'ip' => Request::getClientIp(),
					'uri' => Request::path(),
				);
				$log->meta = serialize($data);
				$log->save();
				return App::abort('404');
			}

		}
		elseif($time < $start || $time > $stop)
		{
			//add to log
			$log = new Loginlog;
			$log->date = \Carbon\Carbon::now()->toDateTimeString();
			$data = array(
				'username' => $user->username,
				'ip' => Request::getClientIp(),
				'uri' => Request::path(),
			);
			$log->meta = serialize($data);
			$log->save();
			return App::abort('404');
		}
	}
});