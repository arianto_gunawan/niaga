<?php
//runs daily batch 1, in order
Artisan::add(new \App\Commands\CustomerStatCommand);
Artisan::add(new \App\Commands\AssetCommand);

//runs daily batch 2
Artisan::add(new \App\Commands\InvoiceTrackerCommand);
Artisan::add(new \App\Commands\DueCommand);
Artisan::add(new \App\Commands\NotifCleanerCommand);
Artisan::add(new \App\Commands\ProfitCommand);

//not run, yet
//Artisan::add(new ProduksiCommand);
//Artisan::add(new \Argun\Aria\ItemsCommand);