<?
class ModelException extends Exception {
	private $_errors;

	public function __construct($errors, $code = 0, Exception $previous = null)
	{
		$this->_errors = $errors;
		if(!is_array($errors))
			$this->_errors = array($errors);

		parent::__construct(head(array_flatten($this->_errors)), $code, $previous);
	}

	public function getErrors() { return $this->_errors; }
}