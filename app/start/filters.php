<?
Route::filter('csrf', function()
{
	if(Request::ajax()) return;
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});