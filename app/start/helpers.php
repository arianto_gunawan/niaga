<?
function pre()
{
	$args = func_get_args();
	echo '<div><pre>';
	$i=0;
	foreach($args as $argument)
	{
		if($i>0)
			echo "\n-----------------------------------------\n\n";
		if(is_array($argument) || is_object($argument))
			print_r($argument);
		else
			var_dump($argument);
		$i++;
	}
	echo '</pre></div>';
}

function ajax()
{
	$req = getallheaders();
	return strpos($req['Accept'], 'json') !== false;
}

function print_quantity($num)
{
	return str_replace('.00','',$num);
}

function nf($num)
{
	if((int) $num == $num)
		return number_format($num);
	return number_format($num,2);
}

function get_weekdays($m, $y, $saturday = 1) {
	$lastday = date("t",mktime(0,0,0,$m,1,$y));
	$weekdays=0;
	for($d=29;$d<=$lastday;$d++) {
		$wd = date("w",mktime(0,0,0,$m,$d,$y));
		if($wd > 0 && $wd < 6) $weekdays++;
    }
    if($saturday)
	    return $weekdays + 24;
	return $weekdays + 20;
}