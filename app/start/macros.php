<?
use App\Libraries\Dater;

Validator::extend('cdate', function($attribute, $value, $parameters) {
	$settings = App::make('appsettings');
	$dateLimit = intval($settings->get('tutup_buku'));

	$input = Dater::fromSQL($value);
	if($input->isToday()) return true;

	$now = Dater::now();

	$limit = Dater::create($now->year, $now->month, $dateLimit, 0);

	//next year?
	if($input->year > $limit->year)
		return true;

	//same month & same year
	if($input->month == $limit->month && $limit->year == $input->year)
		return true;

	//last month, belom tutup buku
	if( ($limit->month - $input->month) == 1 )
		if($now->day <= $limit->day)
			return true;

	//last year
	if($input->month == 12 && $limit->month == 1 && ($limit->year - $input->year) == 1)
		if($now->day <= $limit->day)
			return true;

	return false;
});