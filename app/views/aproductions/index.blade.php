<div class="row">
{{ Form::open(array('url' => URL::current(), 'method' => 'GET')) }}
	<div class="col-xs-6 col-md-5">{{ Form::text('from',Input::get('from',null),array('class' => 'datepick form-control', 'placeholder' => 'From')) }}</div>
	<div class="col-xs-6 col-md-5">{{ Form::text('to',Input::get('to',null),array('class' => 'datepick form-control', 'placeholder' => 'To')) }}</div>
	<div class="col-xs-6 col-md-2">{{ Form::submit('Seach',array('class' => 'btn btn-primary')) }}</div>
</div>
<hr/>
<table class="table table-striped table-hover table-bordered">
	<thead><tr>
		<th>Kitir</th>
		<th>{{ HTML::sort('potong_in','Tgl Potong') }}</th>
		<th>Potong</th>
		<th>Q</th>
		<th>Barang</th>
		<th>Size</th>
		<th>Warna</th>
		<th>Cust</th>
		<th>Group</th>
		<th>{{ HTML::sort('jahit_out','Ke Jahit') }}</th>
		<th>Q</th>
		<th>{{ HTML::sort('jahit_in','Dari Jahit') }}</th>
		<th>Q</th>
		<th>Keterangan</th>
		<th>Urgent</th>
	</tr></thead>
	<tbody>
	<tr>
		<td>{{ Form::text('id',Input::get('id')) }}</td>
		<td>&nbsp;</td>
		<td>{{ Form::select('potong',$potong,Input::get('potong')) }}</td>
		<td>&nbsp;</td>
		<td>{{ Form::text('kode',Input::get('kode')) }}</td>
		<td>{{ Form::select('size',$size,Input::get('size')) }}</td>
		<td>{{ Form::text('warna',Input::get('warna')) }}</td>
		<td>{{ Form::text('customer',Input::get('customer')) }}</td>
		<td>{{ Form::select('jahit',$jahit,Input::get('jahit')) }}</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
{{ Form::close() }}

@foreach($produksi as $t)
	<tr class="{{ $t->printClass() }}">
		<td>{{ $t->serial() }} / {{ $t->id }}</td>
		<td>{{ HTML::link(URL::action('ProductionsController@getEdit',array($t->id)),Dater::display($t->potong_in)) }}</td>
		<td>{{ $t->potong->name }}</td>
		<td>{{ $t->quantity }}</td>
		<td>{{ $t->item->code}}</td>
		<td>{{ $t->size?$t->size->code:'' }}</td>
		<td>{{ $t->warna }}</td>
		<td>{{ $t->customer }}</td>
		<td>{{ $t->jahit }}</td>
		<td>{{ Dater::display($t->jahit_out) }}</td>
		<td>{{ number_format($t->q_out) }}</td>
		<td>{{ Dater::display($t->jahit_in) }}</td>
		<td>{{ number_format($t->q_in) }}</td>
		<td>{{ $t->description }}</td>
@if(!$t->urgent)
		<td>Tdk Urgent</td>
@else
		<td>Urgent</td>
@endif
	</tr>
@endforeach
	</tbody>
</table>
{{ $produksi->appends(array('kode' => Input::get('kode'), 'warna' => Input::get('warna'), 'customer' => Input::get('customer'), 'jahit' => Input::get('jahit'), 'potong' => Input::get('potong'), 'sort' => Input::get('sort'), 'dir' => Input::get('dir'), 'from' => Input::get('from'), 'to' => Input::get('to')))->links() }}

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$(".datepick").datepicker();
});
/*]]>*/ 
</script>
@stop