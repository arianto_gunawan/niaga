<form autocomplete="off" ng-controller="boronganFormController as cc" name="tForm" ng-submit="submit(tForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="row">
<div class="col s6">
<div class="input-field">
<label>From</label>
<input type="text" ng-model="cc.from" class="dateform" />
</div>
</div>

<div class="col s6">
<div class="input-field">
<label>To</label>
<input type="text" ng-model="cc.to" class="dateform" />
</div>
</div>
</div>

<div class="row">
<div class="col s12">
	<label>Jahit</label>
	<select class="browser-default" ng-model="cc.jahit" ng-options="j.name for j in cc.jahits" required></select>
</div>
</div>

<div class="row">
	<div ng-show="cc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
	<a ng-click="cc.loadBorongan()" class="waves-effect waves-light btn">Load</a>
</div>

<div class="input-field">
<select required class="browser-default" ng-model="cc.potong" ng-options="a.name for a in cc.potongs"></select>
</div>

<h5>Items</h5>
<table class="table bordered striped highlight centered">
	<thead><tr><th>Serial</th><th>Code</th><th>Quantity</th><th>Customer</th><th>Warna</th><th>Jahit</th><th>Total</th></tr></thead>
	<tbody>
	<tr ng-repeat="detail in cc.borongan track by $index">
		<td>
			<a href="@{{ detail.edit_link }}" target="_blank">@{{ detail.serial }}</a>
		</td>
		<td>
			@{{ cc.displayItemName($index) }}
		</td>
		<td>
			@{{ detail.quantity }}
		</td>
		<td>
			@{{ detail.customer }}
		</td>
		<td>
			@{{ detail.warna }}
		</td>
		<td>
			@{{ detail.ongkos | number }}
		</td>
		<td>@{{ cc.calculateTotal($index) | number }}</td>
	</tr>
	</tbody>
</table>

<div class="row">
<div class="col s6">
<div class="input-field">
	<label>Permak</label>
	<input type="text" ng-model="cc.permak" />
</div>
</div>
</div>

<div class="row">
<div class="col s6">
<div class="input-field">
	<label>Tres</label>
	<input type="text" ng-model="cc.tres" />
</div>
</div>
</div>

<div class="row">
<div class="col s6">
<div class="input-field">
	<label>Lain2</label>
	<input type="text" ng-model="cc.lain2" />
</div>
</div>
</div>

<div class="row">
<div class="col s6">
	<label>Total</label>
	<input disabled="disabled" value="@{{ cc.getTotal() | number: 2 }}">
</div>
</div>

<div ng-show="cc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="cc.submit(tForm.$valid)" ng-disabled="cc.invalidBorongan()" class="waves-effect waves-light btn">Submit</a>
</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	jahits: {{ json_encode($jahit) }},
	loadURL: "{{ URL::action('BoronganController@postLoad') }}",
	submitURL: "{{ URL::action('BoronganController@postAdd') }}"
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/transaction.js') }}"></script>
<script src="{{ asset('js/aria/controllers/produksi/boronganFormController.js') }}"></script>
@stop