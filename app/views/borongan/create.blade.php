<form autocomplete="off" ng-controller="produksiFormController as cc" name="tForm" ng-submit="submit(tForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group row">
	<div class="col-lg-6">
		<label>Dari:</label>
		{{ Form::text('from',InputForm::old('from'),array('class'=>'datepick form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		<label>Sampai:</label>
		{{ Form::text('to',InputForm::old('to'),array('class'=>'datepick form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		<label>Group:</label>
		{{ Form::select("borongan[group]",$jahit,InputForm::old('borongan[group]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		<label>Tres:</label>
		{{ Form::text('borongan[tres]',InputForm::old('borongan.tres'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		<label>Permak:</label>
		{{ Form::text('borongan[permak]',InputForm::old('borongan.permak'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		<label>Lain2:</label>
		{{ Form::text('borongan[lain2]',InputForm::old('borongan.lain2'),array('class' => 'form-control')) }}
	</div>
</div>

<div ng-show="cc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="cc.submit(tForm.$valid)" ng-disabled="cc.disableSubmit || tForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	jahits: {{ json_encode($jahit) }},
	submitURL: "{{ URL::current() }}"
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/transaction.js') }}"></script>
<script src="{{ asset('js/aria/controllers/produksi/produksiFormController.js') }}"></script>
@stop