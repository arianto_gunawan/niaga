<div class="row">
<div class="col s6">
<table class="table table-condensed">
	<tbody>
		<tr><td>Date</td><td>{{ Dater::display($borongan->date) }}</td></tr>
		<tr><td>Period</td><td>{{ Dater::display($borongan->from) }} -  {{ Dater::display($borongan->to) }}</td></tr>
		<tr><td>Group</td><td>{{ $borongan->jahit->name }}</td></tr>
		<tr><td>Items</td><td>{{ number_format($borongan->total_items) }}</td></tr>
		<tr class="ph"><td>User</td><td>{{ $borongan->user->username }}</td></tr>
	</tbody>
</table>
</div>

<div class="col s6">
<table class="table table-condensed">
	<tbody>
		<tr><td>Tres</td><td>{{ number_format($borongan->tres) }}</td></tr>
		<tr><td>Permak</td><td>{{ number_format($borongan->permak) }}</td></tr>
		<tr><td>Lain2</td><td>{{ number_format($borongan->lain2) }}</td></tr>
		<tr><td>Total</td><td>{{ number_format($borongan->total) }}</td></tr>
	</tbody>
</table>
</div>
</div>

<table class="table table-striped table-hover table-condensed">
	<thead><tr><th>Kitir</th><th>Item</th><th>Price</th><th>Quantity</th><th>Total</th></tr></thead>
	<tbody>
@foreach($details as $detail)
	<tr>
		<td>{{ $detail->serial() }}</td>
		<td>{{ HTML::link(URL::action('ItemsController@getDetail',array($detail->item_id)),$detail->item->code) }}</td>
		<td>{{ number_format($detail->ongkos) }}</td>
		<td>{{ number_format($detail->quantity) }}</td>
		<td>{{ number_format($detail->total) }}</td>
	</tr>
@endforeach
	</tbody>
</table>