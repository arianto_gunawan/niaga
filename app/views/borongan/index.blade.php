<div ng-controller="boronganListController as tlc">
<form id="produksiListForm" name="produksiListForm" autocomplete="off" ng-submit="tlc.submit()">
<div class="row">
<div class="col s4 m2">
  <label>From</label>
  <input type="text" ng-model="tlc.from" class="dateform" ng-change="tlc.resetPage()"/>
</div>
<div class="col s4 m2">
  <label>To</label>
  <input type="text" ng-model="tlc.to" class="dateform" ng-change="tlc.resetPage()"/>
</div>
<div class="col s4 m2 input-field">
	<input ng-click="tlc.submit()" type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>
</form>

<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div id="scrollTo">
<table id="setoran" class="bordered striped highlight small-table centered">
	<thead><tr>
		<th>Date</th><th>Jahit</th><th>Items</th><th>Tres</th><th>Permak</th><th>Lain2</th><th>Total</th>
	</tr></thead>
	<tbody>
	<tr>
		<td>&nbsp;</td>
		<td><select class="browser-default" ng-model="tlc.jahit" ng-options="j.name for j in tlc.jahits" required></select></td>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		<td><a ng-click="tlc.submit()" class="btn-floating btn-large waves-effect waves-light blue"><i class="material-icons">search</i></a></td>
	</tr>
	<tr ng-repeat="p in tlc.borongan track by p.id">
		<td><a href="@{{ p.link }}">@{{ p.date }}</a></td>
		<td>@{{ p.jahit.name }}</td>
		<td>@{{ p.total_items }}</td>
		<td>@{{ p.tres }}</td>
		<td>@{{ p.permak }}</td>
		<td>@{{ p.lain2 }}</td>
		<td>@{{ p.total | number }}</td>
<!-- @if($can_delete)
			<a type="submit" value="Delete" class="waves-effect waves-light red btn" ng-click="tlc.openDeleteModal($index)" ng-disabled="tlc.showProgress">Delete</a>
@endif -->
	</tr>
	</tbody>
</table>
</div>
<br/>
<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="center-align">
<a ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>

</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	jahits: {{ json_encode($jahit) }},
	paginator: {{ json_encode($borongan) }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/produksi/boronganListController.js') }}"></script>
@stop