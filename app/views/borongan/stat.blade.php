<?
$ongkos = 0;
$items = 0;
?>

<ul class="nav nav-pills printhide">
	<li>{{ HTML::link(URL::to_action('gaji@index'),'List') }}</li>
	<li class="active"><a href="#">Stats</a></li>
</ul>

<table class="table table-striped table-bordered">
<thead><tr><th>{{ HTML::sort('bulan') }}</th><th>{{ HTML::sort('group') }}</th><th>Total</th><th>{{ HTML::sort('items') }}</th></tr></thead>
@foreach($stat as $p)
<tr>
	<td>{{$p->bulan}}</td>
	<td>{{$p->group }}</td>
	<td>{{number_format($p->price)}}</td>
	<td>{{$p->items}}</td>
</tr>
<?
$ongkos += $p->price;
$items += $p->items;
?>
@endforeach
</table>
<p><strong>Total Ongkos:</strong> {{ number_format($ongkos) }}</p>
<p><strong>Total Items:</strong> {{ number_format($items) }}</p>
<p><strong>Ongkos/Items:</strong> {{ number_format(intval($ongkos/$items)) }}</p>