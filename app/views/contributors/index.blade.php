<div ng-controller="contributorController as it">

<form class="ph" id="transactionListForm" autocomplete="off" ng-submit="it.submit()">
<div class="row">
<div class="col s4 m2">
  <label>From</label>
  <input type="text" ng-model="it.from" class="dateform"/>
</div>
<div class="col s4 m2">
  <label>To</label>
  <input type="text" ng-model="it.to" class="dateform"/>
</div>
<div class="col s4 m2">
	<label>Addr Book</label>
  <input type="text" typeahead-min-length="3" ng-model="it.customer" uib-typeahead="item as item.name for item in it.getCustomer($viewValue)" >
</div>
<div class="col s4 m2">
	<label>Type</label>
  <select class="browser-default" ng-model="it.brand" ng-options="brand.name for brand in it.brands"></select>
</div>
<div class="col s4 m2">
	<input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>
</form>
<h5>Contributors from @{{ it.from }} - @{{ it.to }}</h5>
<div ng-show="it.showProgress" class="progress"><div class="indeterminate"></div></div>
<ul class="collapsible" data-collapsible="accordion">
<li>
<div class="collapsible-header active"><i class="material-icons">filter_drama</i>Top 50 Items</div>
<div class="collapsible-body">
<!-- TOP 50 START -->
<div class="table-responsive-vertical shadow-z-1">
<table class="table table-hover table-striped">
	<thead><tr><th>Name</th><th>Brand</th><th>Genre</th><th>Size</th><th>Quantity</th><th>Value</th></tr></thead>
	<tbody>
	<tr ng-repeat="result in it.data.top50">
		<td data-title="Name"><a href="@{{ result.link }}">@{{ result.name }}</a></td>
		<td data-title="Brand">@{{ it.displayBrand(result.brand) }}</td>
		<td data-title="Genre">@{{ it.displayGenre(result.genre) }}</td>
		<td data-title="Size">@{{ it.displaySize(result.size) }}</td>
		<td data-title="Quantity">@{{ result.total_quantity }}</td>
		<td data-title="Value">@{{ result.total_value | number: 2 }}</td>
	</tr>
	</tbody>
</table>
</div>
<!-- TOP 50 STOP -->
</div>
</li>
<li>
<div class="collapsible-header"><i class="material-icons">place</i>Group by Brand</div>
<div class="collapsible-body">
<!-- BY BRAND START -->
<div class="table-responsive-vertical shadow-z-1">
<table class="table table-hover table-striped">
	<thead><tr><th>Brand</th><th>Quantity</th><th>Value</th></tr></thead>
	<tbody>
	<tr ng-repeat="result in it.data.byBrand">
		<td data-title="Brand">@{{ it.displayBrand(result.brand) }}</td>
		<td data-title="Quantity">@{{ result.total_quantity }}</td>
		<td data-title="Value">@{{ result.total_value | number: 2 }}</td>
	</tr>
	</tbody>
</table>
</div>
<!-- BY BRAND STOP -->
</div>
</li>
<li>
<div class="collapsible-header"><i class="material-icons">whatshot</i>Group by Type</div>
<div class="collapsible-body">
<!-- BY GENRE START -->
<div class="table-responsive-vertical shadow-z-1">
<table class="table table-hover table-striped">
	<thead><tr><th>Type</th><th>Quantity</th><th>Value</th></tr></thead>
	<tbody>
	<tr ng-repeat="result in it.data.byGenre">
		<td data-title="Type">@{{ it.displayGenre(result.genre) }}</td>
		<td data-title="Quantity">@{{ result.total_quantity }}</td>
		<td data-title="Value">@{{ result.total_value | number: 2 }}</td>
	</tr>
	</tbody>
</table>
</div>
<!-- BY GENRE STOP -->
</div>
</li>
<li>
<div class="collapsible-header"><i class="material-icons">whatshot</i>Group by Size</div>
<div class="collapsible-body">
<!-- BY Size START -->
<div class="table-responsive-vertical shadow-z-1">
<table class="table table-hover table-striped">
	<thead><tr><th>Size</th><th>Quantity</th><th>Value</th></tr></thead>
	<tbody>
	<tr ng-repeat="result in it.data.bySize">
		<td data-title="Size">@{{ it.displaySize(result.size) }}</td>
		<td data-title="Quantity">@{{ result.total_quantity }}</td>
		<td data-title="Value">@{{ result.total_value | number: 2 }}</td>
	</tr>
	</tbody>
</table>
</div>
<!-- BY Size STOP -->
</div>
</li>
<li>
<div class="collapsible-header"><i class="material-icons">whatshot</i>Summary</div>
<div class="collapsible-body">
<!-- BY 3 START -->
<div class="table-responsive-vertical shadow-z-1">
<table class="table table-hover table-striped">
	<thead><tr><th>Brand</th><th>Genre</th><th>Size</th><th>Quantity</th><th>Value</th></tr></thead>
	<tbody>
	<tr ng-repeat="result in it.data.top50">
		<td data-title="Brand">@{{ it.displayBrand(result.brand) }}</td>
		<td data-title="Genre">@{{ it.displayGenre(result.genre) }}</td>
		<td data-title="Size">@{{ it.displaySize(result.size) }}</td>
		<td data-title="Quantity">@{{ result.total_quantity }}</td>
		<td data-title="Value">@{{ result.total_value | number: 2 }}</td>
	</tr>
	</tbody>
</table>
</div>
<!-- BY 3 STOP -->
</div>
</li>
</ul>

<div ng-show="it.showProgress" class="progress"><div class="indeterminate"></div></div>
</div>

@section('script')
<script type="text/javascript">
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	customersUrl: "{{ $defaults['customer_url'] }}",
	links: {{ json_encode($links) }},
	brands: {{ $brands }},
	sizes: {{ $sizes }},
	genres: {{ $genres }},
	data: {{ $data }}
}))
</script>
<script src="{{ asset('js/aria/services/customerService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/item/contributorController.js') }}"></script>
@stop