<? use App\Models\Customer; ?>
<ul class="tabs ph">
<li class="tab"><a target="_self" class="active">Detail</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getTransactionsLink() }}">Transactions</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getItemsLink() }}">Items</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getSalesLink() }}">Sales</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getStatLink() }}">Stats</a></li>
</ul>

<div ng-controller="customerDetailController as cd">
@if(Customer::TYPE_CUSTOMER)
<p>
	<i class="material-icons">person</i>
	@if($customer->memberId)
		{{ $customer->memberId }}</a>
	@else
		Not a Member
	@endif
</p>
@endif
<p>
	<i class="material-icons">room</i>
	@if($customer->address)
		<a href="https://maps.google.com/maps?q={{ Str::slug($customer->address) }}" target="_blank">{{ nl2br($customer->address) }}</a>
	@else
		No Address
	@endif
</p>
<p>
	<i class="material-icons">call</i>
	@if($customer->phone)
		<a href="tel:{{ $customer->phone }}">{{ $customer->phone }}</a>
	@else
		No Phone
	@endif
</p>
<p>
	<i class="material-icons">call</i>
	@if($customer->phone2)
		<a href="tel:{{ $customer->phone2 }}">{{ $customer->phone2 }}</a>
	@else
		No 2nd Phone
	@endif
</p>
<p>
	<i class="material-icons">print</i>
	@if($customer->fax)
		{{ $customer->fax }}
	@else
		No Phone
	@endif
</p>
<p>
	<i class="material-icons">email</i>
		@if($customer->email)
			<a href="mailto:{{ $customer->email }}">{{ $customer->email }}</a>
		@else
			No Email
		@endif
</p>
@if($customer->stat)
<p>
	<i class="material-icons">attach_money</i>
	{{ number_format($customer->stat->balance) }}
</p>
@endif
@if($customer->description)
<p>
	<i class="material-icons">info</i>
	{{ nl2br($customer->description) }}
</p>
@endif
@if($customer->portal)
<p>
	<i class="material-icons">info</i>
	{{ $customer->portal->username }} -- {{ $customer->password }}
</p>
@endif
@if($delete || $restore)
<p>
	<a href="#" class="waves-effect red btn" ng-click="cd.showModal()">@{{ cd.action.button }}</a>
</p>
@endif
@if($can_edit)
<p>
	<a href="{{ $customer->getEditLink() }}" class="btn">Edit</a>
</p>
@endif
@if($portal || $unportal)
<p>
	<a href="#" class="waves-effect blue btn" ng-click="cd.portalModal()">@{{ cd.portal.button }}</a>
</p>
@if($user && $user->active == 1)
<p>
	<div class="input-field col s6">
	<label>Add Warehouse</label>
	<input focus required type="text" typeahead-min-length="3" ng-model="cd.warehouse" uib-typeahead="customer as customer.name for customer in cd.getWarehouse($viewValue)">
</div>
</p>
<p>
	<a href="#" class="waves-effect blue btn" ng-click="cd.addWarehouse()">Add Warehouse</a>
</p>
@endif
@endif
<div id="customerModal" class="modal">
  <div class="modal-content">
    <h4>Are you sure??</h4>
    <p>@{{ cd.msg }}</p>
  </div>
  <div class="modal-footer">
    <a href="#!" ng-click="cd.closeModal()" class="waves-effect red btn">Cancel!</a>
    <a href="#!" ng-click="cd.submit()" class="waves-effect green btn">Sure</a>
  </div>
</div>
<div id="portalModal" class="modal">
  <div class="modal-content">
    <h4>Are you sure??</h4>
    <p>@{{ cd.msg }}</p>
  </div>
  <div class="modal-footer">
    <a href="#!" ng-click="cd.closeModal()" class="waves-effect red btn">Cancel!</a>
    <a href="#!" ng-click="cd.submitPortal()" class="waves-effect green btn">Sure</a>
  </div>
</div>

</div>

@section('script')
<script type="text/javascript">
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	action: {{ json_encode(array('deleted_at' => $customer->deleted_at, 'delete' => $delete, 'restore' => $restore)) }},
	portal: {{ json_encode(array('active' => $portal_active, 'delete' => $delete, 'restore' => $restore)) }},
@if($delete || $restore)
	deleteUrl: '{{ URL::action($action.'@postDelete',array($customer->id)) }}',
	restoreUrl: '{{ URL::action($action.'@postRestore',array($customer->id)) }}',
@endif
@if($portal || $unportal)
	portalUrl: '{{ URL::action($action.'@postPortal',array($customer->id)) }}',
	unportalUrl: '{{ URL::action($action.'@postUnportal',array($customer->id)) }}',
	warehouseUrl: '{{ URL::action($action.'@postAddwarehouse',array($customer->id)) }}',
	customersUrl: "{{ $defaults['customer_url'] }}",
	warehouseType: "w",
	defaultWarehouse: {{ $defaultWarehouse }}
@endif
}))
</script>
<script src="{{ asset('js/aria/services/customerService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/customer/customerDetailController.js') }}"></script>
@stop