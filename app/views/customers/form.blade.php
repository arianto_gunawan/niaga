<?
use App\Models\Customer;
$isVirtual = ($type == Customer::TYPE_VWAREHOUSE || $type == Customer::TYPE_VACCOUNT)
?>
<form autocomplete="off" ng-controller="customerFormController as cc" name="aForm" ng-submit="cc.submit(aForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<label>Name</label>
<input class="required" ng-model="cc.customer.name" required />
<p>@{{ cc.customer }}</p>
@if(!$isVirtual)
<div class="input-field">
<label>Address</label>
<textarea ng-model="cc.customer.address" class="materialize-textarea"></textarea>
</div>

<label>Email</label>
<input type="email" ng-model="cc.customer.email" />

<label>Phone</label>
<input ng-model="cc.customer.phone" />

<label>Phone 2</label>
<input ng-model="cc.customer.phone2" />

<label>Fax</label>
<input ng-model="cc.customer.fax" />
@endif

<div class="input-field">
<label>Description</label>
<textarea ng-model="cc.customer.description" class="materialize-textarea"></textarea>
</div>

@if(!$isVirtual)
@yield('type')

@if($can_switch)
<label>Type</label>
<select class="browser-default" ng-model="cc.type" ng-options="b.name for b in cc.types"></select>
@endif

@if($create)
@if($type != Customer::TYPE_WAREHOUSE && $type != Customer::TYPE_VWAREHOUSE)
<label>Initial Balance</label>
<input ng-model="cc.initial" />
@endif
@endif
@endif

<div class="input-field">
<a ng-click="cc.submit(aForm.$valid)" ng-disabled="cc.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</div>
<div ng-show="cc.disableSubmit" class="progress"><div class="indeterminate"></div></div>

</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	customer: {{ json_encode($customer) }},
@if($can_switch)
	types: {{ json_encode(Customer::$typesJSON) }},
@endif
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/customer/customerFormController.js') }}"></script>
@stop