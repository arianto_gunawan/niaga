<?
use App\Models\Customer;
?>
{{ Form::open(array('url' => URL::current(), 'method' => 'POST')) }}
<div class="form-group row">
	<div class="col-lg-6">
		{{ InputForm::text('customer[name]',$errors->first('name'),'name') }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('description') }}
		{{ Form::textarea('customer[description]',InputForm::old('customer[description]'),array('class' => 'form-control')) }}
	</div>
</div>

@if($can_switch)
<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('type') }}
		{{ Form::select('customer[type]',Customer::$types,InputForm::old('customer[type]'),array('class' => 'form-control')) }}
	</div>
</div>
@endif

{{ Form::submit('Save', array('class' => 'btn')) }}
{{ Form::close() }}