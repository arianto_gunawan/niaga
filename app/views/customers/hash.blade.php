@extends('customers.detail')
<?
use App\Models\Customer;
?>
@section('customer')
<div class="row ph">
{{ Form::open(array('url' => URL::current(), 'method' => 'GET')) }}
	<div class="col-xs-2">
		<input type="text" name="month" value="{{ Input::get('month',null) }}" placeholder="Month" class="datepick form-control" data-date-format="mm/yyyy" data-date-viewmode="months" data-date-minviewmode="months"/>
	</div>
	<div class="col-xs-4">
		<input type="text" name="hashtag" value="{{ Input::get('hashtag',null) }}" placeholder="Hashtag" class="form-control"/>
	</div>
	<div class="col-xs-4">
		{{ Form::select('type',$types,Input::get('type'),array('class' => 'form-control')) }}
	</div>
	<div class="col-xs-2">{{ Form::submit('Search',array('class' => 'btn')) }}</div>
{{ Form::close() }}
</div>

@if(isset($transactions))
<table class="table table-hover table-striped">
	<thead><tr><th>Type</th><th>Hash</th><th>Total</th></tr></thead>
	<tbody>
	@foreach($transactions as $t)
	<tr>
		<td>{{ $types[$t->transaction_type] }}</td>
		<td>
			<a href="{{ URL::action($hash_url,array($t->hash_id)) }}">{{ $t->hash_name }}</a>
		</td>
		<td>{{ number_format($t->sum_total) }}</td>
	</tr>
	@endforeach
	</tbody>
</table>
@endif
@stop

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('.datepick').datepicker();
});
/*]]>*/
</script>
@stop