<?
use App\Models\Customer;
?>
<div ng-controller="customerListController as tlc">

<form id="customerListForm" name="customerListForm" autocomplete="off" ng-submit="tlc.submit()">
<div class="row">
@if($type == Customer::TYPE_CUSTOMER)
<div class="col s3">
	<label>ID</label>
	<input type="text" ng-model="tlc.id" ng-change="tlc.resetPage()"/>
</div>
@endif
<div class="col s3">
	<label>Name</label>
	<input type="text" ng-model="tlc.name" ng-change="tlc.resetPage()"/>
</div>
<div class="col s3">
<div class="switch">
  <label>
    <input type="checkbox" ng-model="tlc.deleted">
    <span class="lever"></span>
    Show Deleted
  </label>
</div>
</div>
<div class="col s3">
	<input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>
<div class="switch ph hide-on-med-and-up">
<label>
  <input type="checkbox" ng-model="tlc.horizontal">
  horizontal
  <span class="lever"></span>
  vertical
</label>
</div>
</form>

<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div ng-class="tlc.horizontal ? '' : 'table-responsive-vertical'">
<table class="table table-striped table-bordered table-hover">
	<thead><tr>
@if($type == Customer::TYPE_CUSTOMER)
		<th>ID</th>
@endif
		<th>Name</th>
@if($type == Customer::TYPE_RESELLER)
	<th>Discount</th>
@endif
@if($type != Customer::TYPE_WAREHOUSE)
	<th>Balance</th>
@else
	<th>Estimated Assets</th>
@endif
@if($type == Customer::TYPE_CUSTOMER)
	<th>Expiry Date</th>
@endif
	<th>Actions</th>
@if($type != Customer::TYPE_CUSTOMER)
	<th>Location</th>
@endif
	</tr>
	</thead>
	<tbody>
	<tr ng-repeat="customer in tlc.customers">
@if($type == Customer::TYPE_CUSTOMER)
		<td data-title="ID">@{{ customer.memberId }}</td>
@endif
		<td data-title="Name"><a href="@{{ customer.transaction_link }} ">@{{ customer.name }}</a></td>
@if($type == Customer::TYPE_RESELLER)
		<td data-title="Discount">@{{ customer.discount }}&#37;</td>
@endif
@if($type != Customer::TYPE_WAREHOUSE)
		<td data-title="Balance">@{{ customer.balance | number: 2 }}</td>
@else
		<td data-title="Estimated Assets">@{{ customer.balance | number: 2 }}</td>
@endif
@if($type == Customer::TYPE_CUSTOMER)
		<td data-title="Expiry Date">@{{ customer.expiry_date }}</td>
@endif
		<td data-title="Actions">
			<a class="btn" href="@{{ customer.detail.link }}">Detail</a>
			<a class="btn" href="@{{ customer.edit_link }}">Edit</a>
		</td>
@if($type != Customer::TYPE_CUSTOMER)
		<td>
		<span ng-repeat="location in customer.locations">
			<a href="@{{ location.link }}">@{{ location.name }}</a>&nbsp;
		</span>
		</td>
@endif
	</tr>
	</tbody>
</table>
</div>

<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="center-align">
<a ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>

</div>


@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	paginator: {{ json_encode($customers) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/customer/customerListController.js') }}"></script>
@stop