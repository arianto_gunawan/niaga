<ul class="tabs ph">
<li class="tab"><a target="_self" href="{{ $customer->getDetailLink() }}">Detail</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getTransactionsLink() }}">Transactions</a></li>
<li class="tab"><a target="_self" class="active">Items</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getSalesLink() }}">Sales</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getStatLink() }}">Stats</a></li>
</ul>

<div ng-controller="customerItemController as ci">

<div ng-show="ci.showProgress" class="progress"><div class="indeterminate"></div></div>

<form class="ph" id="customerListForm" name="customerListForm" autocomplete="off" method="GET" action="{{ URL::current() }}">
<div class="row">
<div class="col s4 m2">
  <label>Name</label>
  <input ng-model="ci.name" name="name" ng-change="ci.resetPage()">
</div>
<div class="input-field col s4 m2">
  <a class="waves-effect waves-light btn" ng-click="ci.submit()">Search</a>
</div>
<div class="col s4 m2">
<div class="switch">
  <label>
    <input type="checkbox" ng-model="ci.show0" ng-change="ci.resetPage()">
    hide 0
    <span class="lever"></span>
    show 0
  </label>
</div>
</div>

<div class="col s4 m2">
<div class="switch">
  <label>
    <input type="checkbox" ng-model="ci.showImages" ng-change="ci.toggleImages()">
    no image
    <span class="lever"></span>
    <i class="material-icons">image</i>
  </label>
</div>
</div>

<div class="col s4 m2">
<a class="waves-effect waves-light btn" ng-json-export-excel data="ci.items" report-fields="{id: 'ID', code: 'Code', name: 'Name', description: 'Description', price: 'Price', quantity: 'Quantity'}" >CSV</a>
</div>
</div>
</form>

<div class="table-responsive-vertical">
<table class="table table-striped table-hover table-condensed">
	<thead><tr>
		<th ng-show="ci.showImages">Image</th>
		<th>ID</th>
		<th><a ng-click="ci.toggleSort('items.code')" href="#">Code <i ng-show="ci.sort == 'code'" class="material-icons">@{{ ci.dir == 'asc' ? 'expand_less' : 'expand_more' }}</i></a></th>
		<th><a ng-click="ci.toggleSort('items.name')" href="#">Name <i ng-show="ci.sort == 'name'" class="material-icons">@{{ ci.dir == 'asc' ? 'expand_less' : 'expand_more' }}</i></a></th>
		<th>Description</th>
		<th>Price</th>
		<th><a ng-click="ci.toggleSort('quantity')" href="#">Quantity <i ng-show="ci.sort == 'quantity'" class="material-icons">@{{ ci.dir == 'asc' ? 'expand_less' : 'expand_more' }}</i></a></th>
		<th class="ph">Actions</th></tr>
	</thead>
	<tbody>
	<tr ng-repeat="item in ci.items">
		<td data-title="Image" ng-show="ci.showImages"><img src="{{ asset('img/noimg.jpg') }}" class="b-lazy" data-blazy="@{{ item.image }}" width="200"/></td>
		<td data-title="ID"><a href="@{{ item.link }}">@{{ item.id }}</a></td>
		<td data-title="Code"><a href="@{{ item.link }}">@{{ item.code }}</a></td>
		<td data-title="Name">@{{ item.name }}</td>
		<td data-title="Description">&nbsp;@{{ item.description }}</td>
		<td data-title="Price">@{{ item.price | number: 2 }}</td>
		<td data-title="Quantity">@{{ item.quantity | number: 2 }}</td>
		<td data-title="Action" class="ph"><a href="@{{ item.edit }}">Edit</a></td>
	</tr>
	</tbody>
</table>
</div>

<div ng-show="ci.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row ph">
<div class="center-align">
<a ng-disabled="!ci.showPrevious" ng-click="ci.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!ci.showNext" ng-click="ci.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>

</div>

@section('script')
<script type="text/javascript">
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	paginator: {{ json_encode($items) }},
  submitURL: "{{ URL::current() }}"
}))
</script>
<script src="{{ asset('js/aria/controllers/customer/customerItemController.js') }}"></script>
@stop