<ul class="tabs ph">
<li class="tab"><a target="_self" href="{{ $customer->getDetailLink() }}">Detail</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getTransactionsLink() }}">Transactions</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getItemsLink() }}">Items</a></li>
<li class="tab"><a target="_self" class="active">Sales</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getStatLink() }}">Stats</a></li>
</ul>

<div ng-controller="customerSalesController as cs">
<form class="ph" id="customerListForm" name="customerListForm" autocomplete="off">
<div class="row">
<div class="col s4 m2">
  <label>From</label>
  <input type="text" ng-change="cs.resetPage()" ng-model="cs.from" class="pickdate" />
</div>
<div class="col s4 m2">
  <label>To</label>
  <input type="text" ng-change="cs.resetPage()" ng-model="cs.to" class="dateform" />
</div>
<div class="col s4 m2">
  <label>Type</label>
  <select class="browser-default" ng-model="cs.type" ng-change="cs.resetPage()" ng-options="type.name for type in cs.types"></select>
</div>
<div class="col s4 m2">
  <label>Item</label>
  <input type="text" typeahead-min-length="3" ng-model="cs.item" uib-typeahead="item as item.name for item in cs.getItem($viewValue)" >
</div>
<div class="col s4 m2">
  <a ng-click="cs.submit()" class="waves-effect waves-light btn">Search</a>
</div>
</div>

</form>
<div ng-show="cs.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="table-responsive-vertical shadow-z-1">
<table class="table table-striped table-hover">
	<thead><tr><th>Date</th><th>Invoice</th><th>Code</th><th>Name</th><th>Type</th><th>Sender</th><th>Receiver</th><th>Price</th><th>Quantity</th><th>Discount</th><th>Total</th></tr></thead>
	<tbody>
	<tr ng-repeat="item in cs.data">
    <td data-title="Date"><a href="@{{ item.transaction_link }}">@{{ item.date }}</a></td>
    <td data-title="Invoice"><a href="@{{ item.link }}">@{{ item.invoice }}</a></td>
		<td data-title="Code"><a href="@{{ item.link }}">@{{ item.code }}</a></td>
		<td data-title="Name">@{{ item.name }}</td>
    <td data-title="Type">@{{ item.type }}</td>
    <td data-title="Sender"><a href="@{{ item.sender.link }}">@{{ item.sender.name }}</a></td>
    <td data-title="Receiver"><a href="@{{ item.receiver.link }}">@{{ item.receiver.name }}</a></td>
		<td data-title="Price">@{{ item.price | number: 2 }}</td>
		<td data-title="Quantity">@{{ item.quantity | number: 2 }}</td>
    <td data-title="Discount">@{{ item.discount | number: 2 }}</td>
    <td data-title="Total">@{{ item.total | number: 2 }}</td>
	</tr>
	</tbody>
</table>
</div>

<div ng-show="cs.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row ph">
<div class="center-align">
<a ng-disabled="!cs.showPrevious" ng-click="cs.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!cs.showNext" ng-click="cs.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>

</div>

@section('script')
<script type="text/javascript">
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
  types: {{ $typesJSON }},
	submitURL: "{{ URL::current() }}",
  itemsUrl: "{{ $defaults['item_url'] }}",
  page: "customer-sales",
@if(isset($items))
	paginator: {{ json_encode($items) }}
@endif
}))
</script>
<script src="{{ asset('js/aria/services/itemService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/customer/customerSalesController.js') }}"></script>
@stop