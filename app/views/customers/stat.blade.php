<?
use App\Models\Customer;
?>
<ul class="tabs ph">
<li class="tab"><a target="_self" href="{{ $customer->getDetailLink() }}">Detail</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getTransactionsLink() }}">Transactions</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getItemsLink() }}">Items</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getSalesLink() }}">Sales</a></li>
<li class="tab"><a target="_self" class="active">Stats</a></li>
</ul>

<div ng-controller="customerStatController as cs">
<div ng-show="cs.showProgress" class="progress"><div class="indeterminate"></div></div>

<form class="ph" id="customerListForm" name="customerListForm" autocomplete="off">
<div class="row">
<div class="col s2">
  <label>Month</label>
  <select ng-model="cs.dates.fromMonth" class="browser-default" ng-options="month as month for month in cs.months"></select>
</div>
<div class="col s2">
  <label>Year</label>
  <select ng-model="cs.dates.fromYear" class="browser-default" ng-options="year as year for year in cs.years"></select>
</div>
<div class="col s2">
  <label>&nbsp;</label>
  <h5 class="center-align">To</h5>
</div>
<div class="col s2">
  <label>Month</label>
  <select ng-model="cs.dates.toMonth" class="browser-default" ng-options="month as month for month in cs.months"></select>
</div>
<div class="col s2">
  <label>Year</label>
  <select ng-model="cs.dates.toYear" class="browser-default" ng-options="year as year for year in cs.years"></select>
</div>
<div class="input-field col s2">
  <a class="waves-effect waves-light btn" ng-click="cs.submit()">Search</a>
</div>
</div>

</form>

<div class="table-responsive-vertical">
<table class="table table-striped table-hover">
@if($type == Customer::TYPE_CUSTOMER || $type == Customer::TYPE_RESELLER || $type == Customer::TYPE_WAREHOUSE)
	<thead><tr><th>Month</th><th>Cash In</th><th>Sell</th><th>Return</th><th>Cash Out</th></tr></thead>
	<tbody>
	<tr ng-repeat="stat in cs.stats">
		<td data-title="Date">@{{ stat.date }}</td>
		<td data-title="Cash In">@{{ stat.cash_in | number: 2 }}</td>
		<td data-title="Sell">@{{ stat.sell | number: 2 }}</td>
		<td data-title="Return">@{{ stat.return |number: 2 }}</td>
		<td data-title="Cash Out">@{{ stat.cash_out | number: 2 }}</td>
	</tr>
	<tr>
		<td data-title="Total"><b>Total</b></td>
		<td data-title="Cash In">@{{ cs.getTotalCashIn() | number: 2 }}</td>
		<td data-title="Sell">@{{ cs.getTotalSell() | number: 2 }}</td>
		<td data-title="Return">@{{ cs.getTotalReturn() |number: 2 }}</td>
		<td data-title="Cash Out">@{{ cs.getTotalCashOut() | number: 2 }}</td>
	</tr>
	<tr>
		<td data-title="Netto"><b>Netto</b></td>
		<td colspan="2" data-title="Nett Sell"><span>Sell - Return = </span>@{{ cs.getNettoSell() | number: 2 }}</td>
		<td colspan="2" data-title="Nett Cash"><span>Cash In - Out = </span>@{{ cs.getNettoCash() | number: 2 }}</td>
	</tr>
	</tbody>
@elseif($type == Customer::TYPE_VACCOUNT || $type == Customer::TYPE_ACCOUNT || $type == Customer::TYPE_BANK)
	<thead><tr><th>Month</th><th>Cash In</th><th>Cash Out</th><th>Transfer Out</th><th>Transfer In</th></tr></thead>
	<tr ng-repeat="stat in cs.stats">
		<td data-title="Date">@{{ stat.date }}</td>
		<td data-title="Cash In">@{{ stat.cash_in | number: 2 }}</td>
		<td data-title="Cash Out">@{{ stat.cash_out | number: 2 }}</td>
		<td data-title="Transfer Out">@{{ stat.transfer_out | number: 2 }}</td>
		<td data-title="Transfer In">@{{ stat.transfer - stat.transfer_out | number: 2 }}</td>
	</tr>
@elseif($type == Customer::TYPE_SUPPLIER)
	<thead><tr><th>Bulan</th><th>Buy</th><th>cash_out</th><th>Return Supllier</th></tr></thead>
	<tr ng-repeat="stat in cs.stats">
		<td data-title="Date">@{{ stat.date }}</td>
		<td data-title="Buy">@{{ stat.buy | number: 2 }}</td>
		<td data-title="Cash Out">@{{ stat.cash_out | number: 2 }}</td>
		<td data-title="Return">@{{ stat.return_supplier | number: 2 }}</td>
	</tr>
@elseif($type == Customer::TYPE_VWAREHOUSE || $type == Customer::TYPE_WAREHOUSE)
	<thead><tr><th>Bulan</th><th>Use</th><th>Move</th><th>Buy</th><th>Sell</th></tr></thead>
	<tr ng-repeat="stat in cs.stats">
		<td data-title="Date">@{{ stat.date }}</td>
		<td data-title="Use">@{{ stat.use | number: 2 }}</td>
		<td data-title="Move">@{{ stat.move | number: 2 }}</td>
		<td data-title="Buy">@{{ stat.buy | number: 2 }}</td>
		<td data-title="Sell">@{{ stat.sell | number: 2 }}</td>
	</tr>
@endif
</table>
</div>
</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	dates: {{ json_encode($dates) }},
	stats: {{ json_encode($stats) }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/customer/customerStatController.js') }}"></script>
@stop