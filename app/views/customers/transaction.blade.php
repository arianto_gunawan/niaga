<ul class="tabs ph">
<li class="tab"><a target="_self" href="{{ $customer->getDetailLink() }}">Detail</a></li>
<li class="tab"><a target="_self" class="active">Transactions</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getItemsLink() }}">Items</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getSalesLink() }}">Sales</a></li>
<li class="tab"><a target="_self" href="{{ $customer->getStatLink() }}">Stats</a></li>
</ul>
<div ng-controller="customerTransactionController as ct">

<form id="transactionListForm" name="transactionListForm" autocomplete="off" ng-submit="ct.submit()">
<div class="row">
<div class="col s6 m2">
  <label>From</label>
  <input type="text" ng-model="ct.from" class="dateform" ng-change="ct.resetPage()"/>
</div>
<div class="col s6 m2">
  <label>To</label>
  <input type="text" ng-model="ct.to" class="dateform" ng-change="ct.resetPage()"/>
</div>
<div class="col s6 m2">
	<label>Type</label>
  <select class="browser-default" ng-model="ct.type" ng-change="ct.resetPage()" ng-options="type.name for type in ct.types"></select>
</div>
<div class="input-field col s6 m2">
	<input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>
</form>
<div ng-show="ct.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="col s6">
<div class="ph switch">
  <label>
    Normal
    <input type="checkbox" ng-model="ct.showSummary" ng-change="ct.toggleSummary()">
    <span class="lever"></span>
    Summary
  </label>
</div>
</div>
<div class="col s6">
<a ng-show="!ct.showSummary" class="waves-effect waves-light btn" ng-json-export-excel data="ct.transactions" report-fields="{date: 'Date', type: 'Type', description: 'Description', invoice: 'Invoice', total: 'Total', total_items: 'Items', sender_name: 'Sender', sender_balance: 'Balance' ,receiver_name: 'Receiver', receiver_balance: 'Balance'}" >Export Full</a>
<a ng-show="ct.showSummary" class="waves-effect waves-light btn" ng-json-export-excel data="ct.transactions" report-fields="{date: 'Date', type: 'Type', description: 'Description', invoice: 'Invoice', total: 'Total', total_items: 'Items', summary_balance: 'Balance'}" >Export Summary</a>
</div>
</div>

<div class="table-responsive-vertical" id="invoice">
<table class="table table-hover">
	<thead>
		<tr><th>Date</th><th>Type</th><th>Invoice</th><th ng-show="!ct.showSummary">Description</th><th>Total</th><th>Items</th>
		<th ng-show="!ct.showSummary">Sender</th><th ng-show="!ct.showSummary" ng-hide="ct.hide">Balance</th>
		<th ng-show="!ct.showSummary">Receiver</th><th ng-show="!ct.showSummary" ng-hide="ct.hide">Balance</th>
		<th ng-show="ct.showSummary">Balance</th></tr>
	</thead>
	<tbody>
	<tr ng-class="transaction.class" ng-repeat="transaction in ct.transactions">
		<td data-title="Date"><a href="@{{ transaction.detail_link }}">@{{ transaction.date }}</a></td>
		<td data-title="Type">
			@{{ transaction.type }}
			<h6 class="cyan-text text-darken-4 small" ng-show="transaction.partial">Paid: @{{ transaction.partial | number:2 }}<br/>Due: @{{ ct.due($index) | number: 2 }}</h6>
		</td>
		<td data-title="Invoice">@{{ transaction.invoice }}</td>
		<td data-title="Description" ng-show="!ct.showSummary" ng-bind-html="transaction.description | html"></td>
		<td data-title="Total">@{{ transaction.total | number:2 }}</td>
		<td data-title="Items">@{{ transaction.total_items | number:2 }}</td>
		<td ng-show="!ct.showSummary" data-title="Sender"><a href="@{{ transaction.sender_url }}">@{{ transaction.sender_name }}</a></td>
		<td ng-show="!ct.showSummary" ng-hide="ct.hide" data-title="Sender Balance">@{{ transaction.sender_balance | number:2 }}</td>
		<td ng-show="!ct.showSummary" data-title="Receiver"><a href="@{{ transaction.receiver_url }}">@{{ transaction.receiver_name }}</a></td>
		<td ng-show="!ct.showSummary" ng-hide="ct.hide" data-title="Receiver Balance">@{{ transaction.receiver_balance | number:2 }}</td>
		<td ng-show="ct.showSummary" data-title="Balance">@{{ transaction.summary_balance | number:2 }}</td>
	</tr>
	</tbody>
</table>
</div>

<div ng-show="ct.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row ph">
<div class="center-align">
<a ng-disabled="!ct.showPrevious" ng-click="ct.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!ct.showNext" ng-click="ct.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>

</div>

@section('script')
<script type="text/javascript">
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
submitURL: "{{ URL::current() }}",
  page: "customer-transactions",
 	hide: {{ json_encode($hide) }},
 	tracker: {{ json_encode($balance_tracker) }},
	types: {{ $typesJSON }},
@if(isset($transactions))
		paginator: {{ json_encode($transactions) }}
@endif
}))
</script>
<script src="{{ asset('js/aria/controllers/customer/customerTransactionController.js') }}"></script>
@stop