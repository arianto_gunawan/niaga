{{ Form::open(array('url' => URL::current(), 'method' => 'POST')) }}
<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Nama') }}
		{{ Form::text('nama',InputForm::old('nama'),array('class' => 'form-control')) }}
		{{ Form::hidden('cuti[personnel_id]',InputForm::old('cuti[personnel_id]')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('type') }}
		{{ Form::select('cuti[type]',$types,InputForm::old('cuti[type]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('From') }}
		{{ Form::text('cuti[date_from]',InputForm::old('cuti[date_from]'),array('class' => 'datepick form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('To') }}
		{{ Form::text('cuti[date_to]',InputForm::old('cuti[date_to]'),array('class' => 'datepick form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Description') }}
		{{ Form::textarea('cuti[description]',InputForm::old('cuti[description]'),array('class' => 'form-control')) }}
	</div>
</div>

{{ Form::submit('Save', array('class' => 'btn')) }}
{{ Form::close() }}

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$(".datepick").datepicker();

	$('input[name="nama"]').typeahead([{
		name: "nama",
		remote: "{{ $defaults['personnel_url'] }}?q=%QUERY"
	}]).on('typeahead:selected', function($e, obj) {
		$('input[name="cuti[personnel_id]"]').attr("value",obj.id);
		return false;
	});
});
/*]]>*/
</script>
@stop