<?
use App\Models\Cuti;
?>
@if($cuti)
<table class="table table-striped table-hover">
	<thead><tr><th>Personnel</th><th>From</th><th>To</th><th>Type</th><th>Tahunan</th><th>Sakit</th><th>Description</th><th>Action</th></tr></thead>
	<tbody>
	@foreach($cuti as $p)
	<tr>
		<td><a href="{{ URL::action('PersonnelsController@getDetail',array($p->personnel_id)) }}">{{ $p->personnel->name }}</a></td>
		<td>{{ Dater::display($p->date_from) }}</td>
		<td>{{ Dater::display($p->date_to) }}</td>
		<td>{{ Cuti::$types[$p->type] }}</td>
		<td>{{ $p->sisa_tahunan }}</td>
		<td>{{ $p->sisa_sakit }}</td>
		<td>{{ $p->description }}</td>
		<td><a class="btn red modal-trigger" href="{{ URL::action('CutiController@postDelete',array($p->id)) }}">Delete</a></td>
	</tr>
	@endforeach
	</tbody>
</table>
{{ $cuti->appends(array('name' => Input::get('name')))->links() }}
<div class="modal fade" id="delete-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Are you sure sure?</h4>
			</div>
			<div class="modal-body">
				<p>for every deleted transaction, a kitten is eaten by orcs</p>
			</div>
			<div class="modal-footer">
				{{ Form::open(array('id' => 'modalxform','url' => URL::action('CutiController@postDelete'), 'method' => 'POST')) }}
				<button type="button" class="btn btn-default" data-dismiss="modal">No!!!</button>
				{{ Form::submit('Damn straight!!',array('id' => 'deletebutton', 'class' => 'btn red')) }}
				{{ Form::close() }}
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
@endif

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$(".modal-trigger").click(function(e) {
		var href = this.getAttribute('href');

		$("form#modalxform").attr('action',href);
		$("#delete-modal").modal('show');

		e.preventDefault();
		return false;
	});
});
/*]]>*/
</script>
@stop