{{ Form::open(array('url' => URL::current(), 'method' => 'POST')) }}

<div class="form-group row">
	<div class="col-md-6">
		{{ Form::label('GPU') }}
		{{ Form::text('gaji[gpu]',InputForm::old('gaji[gpu]'),array('disabled' => 'disabled', 'class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-md-6">
		{{ Form::label('Bulanan') }}
		{{ Form::text('gaji[gaji_bulanan]',InputForm::old('gaji[gaji_bulanan]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-md-6">
		{{ Form::label('Gaji Harian') }}
		{{ Form::text('gaji[gaji_harian]',InputForm::old('gaji[gaji_harian]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-md-6">
		{{ Form::label('Premi') }}
		{{ Form::text('gaji[premi]',InputForm::old('gaji[premi]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-md-6">
		{{ Form::label('Sanksi') }}
		{{ Form::text('gaji[sanksi]',InputForm::old('gaji[sanksi]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-md-6">
		{{ Form::label('Cuti') }}
		{{ Form::text('gaji[cuti_mendadak]',InputForm::old('gaji[cuti_mendadak]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-md-6">
		{{ Form::label('Bonus') }}
		{{ Form::text('gaji[bonus]',InputForm::old('gaji[bonus]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-md-3">
		{{ Form::label('Terlambat') }}
		<div class="input-group">
			{{ Form::text('gaji[terlambat]',InputForm::old('gaji[terlambat]'),array('class' => 'form-control')) }}
			<span class="input-group-addon">&#37;</span>
		</div>
	</div>
</div>

{{ Form::submit('Save', array('class' => 'btn')) }}
{{ Form::close() }}

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('input.datepick').datepicker();
});
/*]]>*/
</script>
@stop