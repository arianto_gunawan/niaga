<?
use App\Libraries\GajiManager;
?>
<div class="row ph">
{{ Form::open(array('url' => URL::action('GajiController@'.$formAction), 'method' => 'GET')) }}
<div class="col-xs-6 col-md-3">
	<input type="text" name="date" value="{{ Input::get('date',null) }}" placeholder="Month" class="datepick form-control" data-date-format="mm/yyyy" data-date-viewmode="months" data-date-minviewmode="months"/>
</div>
<div class="col-xs-6 col-md-3">
	{{ Form::submit('Search',array('class' => 'btn')) }}
</div>
</div>
@if($personnels)
<br/>
<table class="table table-bordered table-striped table-hover">
	<thead><tr><th>Date</th>
		<th><input type="text" name="name" placeholder="Name" value="{{ Input::get('name') }}" class="form-control" /></th>
		<th>Bulanan</th><th>Harian</th><th>Premi/Hangus</th><th>Sanksi</th><th>Cuti</th><th>Bonus</th><th>Terlambat</th><th>Total</th><th>Account</th><th>Action</th></tr>
	</thead>
	<tbody>
	@foreach($gaji as $g)
	<tr>
		<td><a href="{{ URL::action('GajiController@getEdit',array($g->id)) }}">{{ $g->month }}/{{ $g->year }}</a></td>
		<td><a href="{{ URL::action('PersonnelsController@getDetail',array($g->personnel_id)) }}">{{ $g->personnel->name }}</a></td>
		<td>{{ nf($g->gaji_bulanan) }}</td>
		<td>{{ nf($g->gaji_harian) }}</td>
		<td>{{ nf($g->premi) }} / {{ $g->premi_hangus == 1?'Y':'N' }}</td>
		<td>{{ nf($g->sanksi) }}</td>
		<td>{{ nf($g->cuti_mendadak) }}</td>
		<td>{{ nf($g->bonus) }}</td>
		<td>{{ nf($g->terlambat) }}% ({{ nf(bcdiv(bcmul($g->gaji_harian,$g->terlambat),100)) }})</td>
		<td>{{ nf(GajiManager::getTotal($g)) }}</td>
		<td>{{ $accounts[$g->personnel->account_id]['name'] }}</td>
		<td><a class="btn red" href="{{ URL::action('GajiController@postReload', array($g->id, $private)) }}" data-toggle="modal" data-target="#myModal">Reload</a></td>
	</tr>
	@endforeach
	</tbody>
</table>
{{ $personnels->appends(array('sort' => Input::get('sort'), 'dir' => Input::get('dir'), 'date' => Input::get('date')))->links() }}
{{ Form::close() }}
<table class="table table-striped table-hover">
	<thead><tr><th>Account</th><th>Total</th></tr></thead>
	<tbody>
@foreach($accounts as $a)
	<tr>
		<td>{{ $a['name'] }}</td>
		<td>{{ nf($a['total']) }}</td>
	</tr>
@endforeach
	</tbody>
</table>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
      	Are you sure you want to delete this file ?
      </div>
{{ Form::open(array('method' => 'POST', 'id' => 'formsembarang')) }}
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::submit('Delete', array('class' => 'btn')) }}
      </div>
{{ Form::close() }}
    </div>
  </div>
</div>

@endif

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('.datepick').datepicker();

	$('#myModal').on('show.bs.modal', function (event) {
		$('#formsembarang').attr('action', event.relatedTarget.href)
	})
});
/*]]>*/
</script>
@stop