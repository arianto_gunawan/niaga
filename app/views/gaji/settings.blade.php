{{ Form::open(array('url' => URL::current(), 'method' => 'POST')) }}
<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Account') }}
		{{ Form::text('account_name',InputForm::old('account_name'), array('class' => 'form-control')) }}
		{{ Form::hidden('settings[account_id]',InputForm::old('settings[account_id]')) }}
	</div>
</div>

{{ Form::submit('Save', array('class' => 'btn')) }}

{{ Form::close() }}

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('input[name="account_name"]').typeahead([{
		name: "account",
		remote: "{{$defaults['account_url']}}" + "?q=%QUERY" + "&t=1"
	}]).on('typeahead:selected', function($e, obj) {
		$('input[name="settings[account_id]"]').attr("value",obj.id);
		return false;
	});
});
/*]]>*/
</script>
@stop