<div class="row">
<div class="col-md-6">
	<dl class="dl-horizontal">
		<dt>Operation Name</dt><dd>{{ $operation->name }}</dd>
		<dt>Description</dt><dd>{{ $operation->description }}</dd>
	</dl>
</div>
<div class="col-md-6">
<ul class="nav nav-pills nav-stacked">
	<li class="active"><a>Detail</a></li>
	<li><a href="#">Stats</a></li>
	<li><a href="{{URL::action('OperationsController@getEdit',array($operation->id))}}">Edit</a></li>
</ul>
</div>
</div>
<hr/>

<div class="row">
{{ Form::open(array('url' => URL::current(), 'method' => 'GET')) }}
	<div class="col-xs-6 col-md-2">{{ Form::text('name',Input::get('name',null),array('placeholder' => 'Name', 'class' => 'form-control')) }}</div>
	<div class="col-xs-6 col-md-2">{{ Form::submit('Seach',array('class' => 'btn')) }}</div>
{{ Form::close() }}
</div>

@if(isset($details) && $details)
<table class="table table-striped table-hover">
	<thead><tr><th>{{ HTML::sort('name') }}</th></tr></thead>
	<tbody>
	@foreach($details as $t)
	<tr>
		<td>{{ HTML::link(URL::action('OperationsController@getAccountDetail',array($t->id)),$t->name) }}</td>
	</tr>
	@endforeach
	</tbody>
</table>
{{ $details->links() }}

@endif