{{ Form::open(array('url' => URL::current(), 'method' => 'POST')) }}
<div class="form-group row">
	<div class="col-lg-6">
		{{ InputForm::text('operation[name]',$errors->first('name'),'name') }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
	{{ Form::label('description') }}
	{{ Form::text('operation[description]',InputForm::old('operation[description]'),array('class' => 'form-control')) }}
	</div>
</div>

{{ Form::submit('Save', array('class' => 'btn')) }}
{{ Form::close() }}