<div ng-controller="hashListController as tlc">
<div layout="column" ng-cloak class="md-inline-form">
  <form id="accountListForm" name="accountListForm" autocomplete="off" method="GET" action="{{ URL::current() }}">
    <div layout-gt-sm="row">
      <md-input-container class="md-block" flex-gt-sm>
        <label>Name</label>
        <input ng-model="tlc.name" name="name" ng-change="tlc.resetPage()">
      </md-input-container>
      <md-input-container class="md-block" flex-gt-sm>
      <md-button class="md-raised md-primary" ng-click="tlc.submit()">Search</md-button>
      </md-input-container>
    </div>
  </form>
</div>

<div flex>
<md-chips ng-model="tlc.hash" readonly="tlc.readonly">
<md-chip-template>
<span><a href="@{{ $chip.detail_link }}">@{{ $chip.name }}</a></span>
</md-chip-template>
</md-chips>
</div>

<div layout="row" layout-align="center center">
<md-button ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="md-fab" aria-label="previous">
  <md-icon aria-label="previous">fast_rewind</md-icon>
</md-button>
<md-button ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="md-fab" aria-label="next">
  <md-icon aria-label="next">fast_forward</md-icon>
</md-button>
</div>
<md-progress-linear ng-show="tlc.showProgress" md-mode="indeterminate"></md-progress-linear>
</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
  submitURL: "{{ URL::current() }}",
  paginator: {{ json_encode($hash) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/journal/hashListController.js') }}"></script>
@stop