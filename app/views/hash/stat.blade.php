<?
use App\Models\Customer;
?>
<div class="row ph">
{{ Form::open(array('url' => URL::current(), 'method' => 'GET')) }}
	<div class="col-xs-2">
		<input type="text" name="month" value="{{ Input::get('month',null) }}" placeholder="Month" class="datepick form-control" data-date-format="mm/yyyy" data-date-viewmode="months" data-date-minviewmode="months"/>
	</div>
	<div class="col-xs-3">
		<input type="text" name="sender" value="{{ Input::get('sender',null) }}" placeholder="Sender" class="form-control" id="sender_search"/>
		{{ Form::hidden('sender_id',Input::get('sender_id',null),array('id'=>'sender_id')) }}
	</div>
	<div class="col-xs-3">
		<input type="text" name="receiver" value="{{ Input::get('receiver',null) }}" placeholder="Receiver" class="form-control" id="receiver_search"/>
		{{ Form::hidden('receiver_id',Input::get('receiver_id',null),array('id'=>'receiver_id')) }}
	</div>
	<div class="col-xs-3">
		{{ Form::select('type',$types,Input::get('type'),array('class' => 'form-control')) }}
	</div>
	<div class="col-xs-1">{{ Form::submit('Search',array('class' => 'btn')) }}</div>
{{ Form::close() }}
</div>

@if(isset($transactions))
<table class="table table-hover table-striped">
	<thead><tr><th>Type</th><th>Sender</th><th>Receiver</th><th>Total</th></tr></thead>
	<tbody>
	@foreach($transactions as $t)
	<tr>
		<td>{{ $types[$t->transaction_type] }}</td>
		<td>{{ Customer::detailLink($t->sender_id,$t->sender_type,$t->sender_name) }}</td>
		<td>{{ Customer::detailLink($t->receiver_id,$t->receiver_type,$t->receiver_name) }}</td>
		<td>{{ number_format($t->sum_total) }}</td>
	</tr>
	@endforeach
	</tbody>
</table>
@endif

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('.datepick').datepicker();

	$('#sender_search').typeahead([{
		name: "senders",
		remote: "{{$defaults['customer_url']}}" + "?q=%QUERY"
	}]).on('typeahead:selected', function($e, obj) {
		$('#sender_id').attr("value",obj.id);
		return false;
	});

	$('#receiver_search').typeahead([{
		name: "receivers",
		remote: "{{$defaults['customer_url']}}" + "?q=%QUERY"
	}]).on('typeahead:selected', function($e, obj) {
		$('#receiver_id').attr("value",obj.id);
		return false;
	});
});
/*]]>*/
</script>
@stop