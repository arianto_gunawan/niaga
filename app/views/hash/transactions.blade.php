<div ng-controller="hashTransactionController as tlc">
<div layout="column" ng-cloak class="md-inline-form">
  <md-content layout-padding>
    <div>
      <form id="transactionListForm" name="transactionListForm" autocomplete="off" method="GET" action="{{ URL::current() }}">
        <div layout-gt-sm="row">
          <md-input-container class="md-block" flex-gt-sm>
            <label>Type</label>
            <md-select ng-model="tlc.type" ng-change="tlc.resetPage()">
              <md-option ng-repeat="type in tlc.types" value="@{{type.id}}">
                @{{type.name}}
              </md-option>
            </md-select>
          </md-input-container>
          <md-input-container class="md-block" flex-gt-sm>
	        <md-button class="md-raised md-primary" ng-click="tlc.submit()">Search</md-button>
          </md-input-container>
        </div>
      </form>
    </div>
  </md-content>
</div>
<md-progress-linear ng-show="tlc.showProgress" md-mode="indeterminate"></md-progress-linear>
<div class="table-responsive-vertical shadow-z-1">
<table st-table="tlc.transactions" class="table table-striped table-bordered table-hover">
	<thead><tr><th>Date</th><th>Type</th><th>Invoice</th><th>Total</th><th>Items</th><th>Sender</th><th>Balance</th><th>Receiver</th><th>Balance</th></tr></thead>
	<tbody>
	<tr ng-repeat="transaction in tlc.transactions">
		<td data-title="Date"><a href="@{{ transaction.detail_link }}">@{{ transaction.date }}</a></td>
		<td data-title="Type">@{{ transaction.type }}</td>
		<td data-title="Invoice">@{{ transaction.invoice }}</td>
		<td data-title="Total">@{{ transaction.total | number:2 }}</td>
		<td data-title="Items">@{{ transaction.total_items | number:2 }}</td>
		<td data-title="Sender"><a href="@{{ transaction.sender_url }}">@{{ transaction.sender_name }}</a></td>
		<td data-title="Sender Balance">@{{ transaction.sender_balance | number:2 }}</td>
		<td data-title="Receiver"><a href="@{{ transaction.receiver_url }}">@{{ transaction.receiver_name }}</a></td>
		<td data-title="Receiver Balance">@{{ transaction.receiver_balance | number:2 }}</td>
	</tr>
	</tbody>
</table>
</div>

<div layout="row" layout-align="center center">
<md-button ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="md-fab" aria-label="previous">
  <md-icon aria-label="previous">fast_rewind</md-icon>
</md-button>
<md-button ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="md-fab" aria-label="next">
  <md-icon aria-label="next">fast_forward</md-icon>
</md-button>
</div>
<md-progress-linear ng-show="tlc.showProgress" md-mode="indeterminate"></md-progress-linear>
</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	types: {{ $typesJSON }},
	submitURL: "{{ URL::current() }}",
	paginator: {{ json_encode($transactions) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/hash/hashTransactionController.js') }}"></script>
@stop