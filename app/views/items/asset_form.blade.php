<?
use App\Models\Item;
?>

<form autocomplete="off" ng-controller="assetFormController as ifc" name="aForm" ng-submit="submit(aForm.$valid)" novalidate enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<label>Code</label>
<input class="required" ng-model="ifc.item.code" required />

<label>Name</label>
<input class="required" ng-model="ifc.item.name" />

<label>Price</label>
<input class="required" ng-model="ifc.item.price" required />

<label>Cost</label>
<input class="required" ng-model="ifc.item.cost" required />

@if($type == Item::TYPE_ASSET_TETAP)
<label>Depreciation</label>
<input class="required" ng-model="ifc.item.depreciation" required />
@endif

<div class="input-field">
<label>Description</label>
<textarea ng-model="ifc.item.description" class="materialize-textarea"></textarea>
</div>

<div class="input-field">
<input type="file" file-model="ifc.image">
</div>

<div class="input-field">
<a ng-click="ifc.submit(aForm.$valid)" ng-disabled="ifc.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</div>
<div ng-show="ifc.disableSubmit" class="progress"><div class="indeterminate"></div></div>

</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	item: {{ json_encode($item) }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/item.js') }}"></script>
<script src="{{ asset('js/aria/controllers/item/assetFormController.js') }}"></script>
@stop
