<div ng-controller="itemListController as ilc">

<form id="transactionListForm" name="transactionListForm" autocomplete="off" ng-submit="ilc.submit()">
<div class="row">
<div class="col s4 m3">
  <label>Name</label>
  <input ng-model="ilc.code" name="code" ng-change="ilc.resetPage()">
</div>
<div class="col s4 m3">
  <label>Code</label>
  <input ng-model="ilc.name" name="name" ng-change="ilc.resetPage()">
</div>
<div class="col s4 m3">
  <input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
<div class="col s4 m3">
<div class="switch">
  <label>
    <input type="checkbox" ng-model="ilc.showImages" ng-change="ilc.toggleImages()">
    <span class="lever"></span>
    <i class="material-icons">image</i>
  </label>
</div>
</div>
</div>
</form>

<div ng-show="ilc.showProgress" class="progress"><div class="indeterminate"></div></div>

<div class="table-responsive-vertical">
<table class="table table-striped table-bordered table-hover">
	<thead><tr><th ng-show="ilc.showImages">Image</th><th>Code</th><th>Name</th><th>Depreciation</th><th>Monthly</th><th>Buy Date</th><th>Price</th><th>Actions</th></tr></thead>
	<tbody>
	<tr ng-repeat="item in ilc.items">
		<td data-title="Image" ng-show="ilc.showImages"><img src="{{ asset('img/noimg.jpg') }}" data-blazy="@{{ item.image }}" width="200"/></td>
		<td data-title="Code"><a href="@{{ item.link }} ">@{{ item.code }}</a></td>
		<td data-title="Name">@{{ item.name }}</td>
		<td data-title="Value">@{{ item.d_value }}</td>
		<td data-title="Monthly">@{{ item.d_monthly }}</td>
		<td data-title="Buy Date">@{{ item.d_date }}</td>
		<td data-title="Price">@{{ item.price | number: 2 }}</td>
		<td data-title="Action"><a class="btn" href="@{{ item.edit }}">edit</a></td>
	</tr>
	</tbody>
</table>
</div>

<div class="row">
<div class="center-align">
<a ng-disabled="!ilc.showPrevious" ng-click="ilc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!ilc.showNext" ng-click="ilc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>
<div ng-show="ilc.showProgress" class="progress"><div class="indeterminate"></div></div>
</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	paginator: {{ json_encode($items) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/item/itemListController.js') }}"></script>
@stop