<?
use App\Models\Item;
?>
<ul class="tabs ph">
<li class="tab"><a target="_self" class="active">Detail</a></li>
<li class="tab"><a target="_self" href="{{ $item->getTransactionsLink() }}">Transactions</a></li>
<li class="tab"><a target="_self" href="{{ $item->getStatsLink() }}">Stats</a></li>
</ul>

<div ng-controller="itemDetailController as idc">
<div class="row">
<div class="col s12 m6">
<img src="{{ asset('img/noimg.jpg') }}" class="b-lazy responsive-img" data-blazy="{{ $item->getImageUrl() }}"/>
</div>

<div class="col s12 m6">
<div id="scrollTo" class="table-responsive-vertical">
<table class="table invoice">
	<tr><th>Name</th><td>{{ $item->name }}</td></tr>
	<tr><th>Type</th><td>{{ $item->printType() }}</td></tr>
@if($item->type == Item::TYPE_ITEM)
	<tr><th>Alias</th><td>{{ $item->group->alias }}</td></tr>
	<tr><th>Description</th><td>{{ $item->group->description }}&nbsp;</td></tr>
@else
	<tr><th>Alias</th><td>{{ $item->alias }}</td></tr>
	<tr><th>Description</th><td>{{ $item->description }}&nbsp;</td></tr>
@endif
	<tr><th>Price</th><td>{{ nf($item->price) }}</td></tr>
	<tr><th>Cost</th><td>{{ nf($item->cost) }}</td></tr>
@if($item->group)
	<tr><th>Group</th><td><a href="{{ $item->group->getDetailLink() }}">{{ $item->group->name }}</a></td></tr>
@endif
	<tr><th>Edit</th><td><a class="btn" href="{{ $item->getEditLink() }}">Edit</a></td></tr>
	<tr><th>Tags</th>
	<td>
	@if(isset($tags))
		@foreach($tags as $tag)
			{{ HTML::link(URL::action('TagsController@getDetail',array($tag->id)),$tag->name) }}
		@endforeach
	@endif
	</td></tr>

	<tr><th>Show 0</th><td>
		<div class="switch">
		  <label>
		    <input type="checkbox" ng-model="idc.show0">
		    <span class="lever"></span>
		  </label>
		</div>
	</td></tr>
</table>
</div>
</div>

</div>

@if(isset($wi))
<table class="invoice">
<tbody>
<th>Warehouse</th><th>Quantity</th>
	@foreach($wi as $w)
	<tr ng-show="idc.checkQuantity( {{ $w->quantity }})">
		<td><a href="{{ $w->warehouse->getDetailLink() }}">{{ $w->warehouse->name }}</a></td>
		<td>{{ $w->quantity }}</td>
	</tr>
	@endforeach
</tbody>
</table>
@endif
@if(isset($b_warehouses))
<table class="invoice">
<tbody>
<th>Warehouse</th><th>Quantity</th>
	@foreach($b_warehouses as $w)
	<tr>
		<td>{{ $w->getDetailLink() }}</td>
		<td>{{ nf($b_quantities[$w->id]) }}</td>
	</tr>
	@endforeach
</tbody>
</table>
@endif

</div>

@section('script')
<script src="{{ asset('js/aria/controllers/item/itemDetailController.js') }}"></script>
@stop
