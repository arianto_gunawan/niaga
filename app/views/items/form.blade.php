<form autocomplete="off" ng-controller="itemFormController as ifc" name="aForm" ng-submit="submit(aForm.$valid)" novalidate enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<label>Code</label>
@if($update)
<input ng-model="ifc.item.pcode" disabled />
@else
<input ng-model="ifc.item.pcode" required />
@endif
<label>Price</label>
<input ng-model="ifc.item.price" required />

<label>Alias</label>
<input ng-model="ifc.item.alias" />

<div class="input-field">
<label>Description</label>
<textarea ng-model="ifc.item.description" class="materialize-textarea"></textarea>
</div>

<input type="file" file-model="ifc.image">

<div ng-repeat="type in ifc.tags" class="row">
<h5>@{{ type.name }}</h5>
<div ng-repeat="data in type.data" class="col s3 m2">
<span>
	<input type="@{{ type.type_id == 2 ? 'radio' : 'checkbox' }}" name="@{{ type.name }}" ng-checked="ifc.checkSelected($parent.$index, $index)" ng-click="ifc.toggleTag($parent.$index, $index)" id="id@{{ data.id }}">
	<label for="id@{{ data.id }}">@{{ data.name }}</label>
</span>
</div>
</div>

<a ng-click="ifc.submit(aForm.$valid)" ng-disabled="ifc.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>
<div ng-show="ifc.disableSubmit" class="progress"><div class="indeterminate"></div></div>

</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	tags: {{ json_encode($tags) }},
	selected: {{ json_encode($selected) }},
	item: {{ json_encode($item) }},
	update: {{ $update?1:0 }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/item.js') }}"></script>
<script src="{{ asset('js/aria/controllers/item/itemFormController.js') }}"></script>
@stop