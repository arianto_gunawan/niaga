<div ng-controller="itemListController as ilc">

<form id="customerListForm" name="customerListForm" autocomplete="off" ng-submit="ilc.submit()">
<div class="row">
<div class="col m2">
  <label>Kode</label>
  <input type="text" ng-model="ilc.name" name="name" ng-change="ilc.resetPage()">
</div>
<div class="col m2">
  <label>Alias</label>
  <input type="text" ng-model="ilc.alias" name="alias" ng-change="ilc.resetPage()">
</div>
<div class="col m2">
  <label>Desc</label>
  <input type="text" ng-model="ilc.desc" name="desc" ng-change="ilc.resetPage()">
</div>
<div class="col m2">
	<input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
<div class="col m2">
<div class="switch">
  <label>
    <input type="checkbox" ng-model="ilc.showImages" ng-change="ilc.toggleImages()">
    <span class="lever"></span>
    <i class="material-icons">image</i>
  </label>
</div>
</div>
</div>
</form>

<div ng-show="ilc.showProgress" class="progress"><div class="indeterminate"></div></div>

<div class="table-responsive-vertical shadow-z-1">
<table class="table table-striped table-bordered table-hover">
	<thead><tr><th ng-show="ilc.showImages">Image</th><th>Kode</th><th>Master</th><th>Alias</th><th>Variant</th><th>Description</th><th>In Warehouse</th></tr></thead>
	<tbody>
	<tr ng-repeat="item in ilc.items">
		<td data-title="Image" ng-show="ilc.showImages"><img src="{{ asset('img/noimg.jpg') }}" class="b-lazy" data-blazy="@{{ item.image }}" width="200"/></td>
		<td data-title="Kode"><a href="@{{ item.link }} ">@{{ item.name }}</a></td>
		<td data-title="Master">@{{ item.master }}</td>
		<td data-title="Alias">@{{ item.alias }}</td>
		<td data-title="Variant">@{{ item.variant }}</td>
		<td data-title="Description">@{{ item.description }}</td>
		<td data-title="Total in Warehouse">@{{ item.total_quantity }}</td>
	</tr>
	</tbody>
</table>
</div>
<div class="row">
<div class="center-align">
<a ng-disabled="!ilc.showPrevious" ng-click="ilc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!ilc.showNext" ng-click="ilc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>
<div ng-show="ilc.showProgress" class="progress"><div class="indeterminate"></div></div>

</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	paginator: {{ json_encode($items) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/item/itemListController.js') }}"></script>
@stop