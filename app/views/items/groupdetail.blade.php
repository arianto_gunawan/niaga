<?
use App\Models\Item;
?>
<div ng-controller="itemDetailController as idc">
<div class="row">
<div class="col s12 m6">
<img src="{{ asset('img/noimg.jpg') }}" class="b-lazy responsive-img" data-blazy="{{ $group->getImageUrl() }}"/>
</div>

<div class="col s12 m6">
<div id="scrollTo" class="table-responsive-vertical">
<table class="table invoice">
  <tr><th>Name</th><td>{{ $group->name }}</td></tr>
  <tr><th>Master</th><td>{{ $group->master }}</td></tr>
  <tr><th>Variant</th><td>{{ $group->variant }}&nbsp;</td></tr>
  <tr><th>Stats</th><td><a href="{{ $group->getStatLink() }}">Stats</a></td></tr>
  <tr><th>Show 0</th><td>
    <div class="switch">
      <label>
        <input type="checkbox" ng-model="idc.show0">
        <span class="lever"></span>
      </label>
    </div>
  </td></tr>
</table>
</div>
</div>
</div>
<hr/>
@foreach($items as $item)
<nav class="blue-grey darken-1">
<div class="nav-wrapper">
  <a href="{{ $item->getDetailLink() }}" class="flow-text">{{ $item->name }}</a>
  <ul class="right hide-on-med-and-down">
    <li><a href="{{ $item->getTransactionsLink() }}">Transactions</a></li>
    <li><a href="{{ $item->getStatsLink() }}">Stats</a></li>
    <li><a href="{{ $item->getEditLink() }}">Edit</a></li>
  </ul>
  <ul class="right hide-on-med-and-up">
    <li><a href="{{ $item->getTransactionsLink() }}"><i class="material-icons">local_atm</i></a></li>
    <li><a href="{{ $item->getStatsLink() }}"><i class="material-icons">trending_up</i></a></li>
  </ul>
</div>
</nav>
<table class="bordered striped">
<tbody>
<? $quantity = 0; ?>
  @foreach($warehouse[$item->id] as $w)
  @if($w->quantity > 0)
  <? $quantity += $w->quantity; ?>
  <tr ng-show="idc.checkQuantity( {{ $w->quantity }})">
    <td><a href="{{ $w->warehouse->getDetailLink() }}">{{ $w->warehouse->name }}</a></td>
    <td>{{ $w->quantity }}</td>
  </tr>
  @endif
  @endforeach
  <tr><th>Total</th><td>{{ $quantity }}</td></tr>
</tbody>
</table>
<hr/>
@endforeach

</div>
@section('script')
<script src="{{ asset('js/aria/controllers/item/itemDetailController.js') }}"></script>
@stop