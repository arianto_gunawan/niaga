<? use App\Models\Item;?>
<div ng-controller="itemListController as ilc">

<form id="customerListForm" name="customerListForm" autocomplete="off" ng-submit="ilc.submit()">
<div class="row">
<div class="col s4 m2">
  <label>ID</label>
  <input type="text" ng-model="ilc.code" name="code" ng-change="ilc.resetPage()">
</div>
<div class="col s4 m2">
  <label>Kode</label>
  <input type="text" ng-model="ilc.name" name="name" ng-change="ilc.resetPage()">
</div>
@if($type == Item::TYPE_ITEM)
<div class="col s4 m2">
  <label>Alias</label>
  <input type="text" ng-model="ilc.alias" name="alias" ng-change="ilc.resetPage()">
</div>
@endif
<div class="col s4 m2">
  <label>Desc</label>
  <input type="text" ng-model="ilc.desc" name="desc" ng-change="ilc.resetPage()">
</div>
<div class="col s4 m2">
	<input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
<div class="col s4 m2">
<div class="switch">
  <label>
    <input type="checkbox" ng-model="ilc.showImages" ng-change="ilc.toggleImages()">
    <span class="lever"></span>
    <i class="material-icons">image</i>
  </label>
</div>
</div>
</div>
</form>

<div ng-show="ilc.showProgress" class="progress"><div class="indeterminate"></div></div>

<div class="table-responsive-vertical shadow-z-1">
<table class="table table-striped table-bordered table-hover">
	<thead><tr><th ng-show="ilc.showImages">Image</th><th>ID</th><th>Kode</th>
@if($type == Item::TYPE_ITEM)
<th>Alias</th>
@endif
<th>Description</th><th>Price</th>
@if($type == Item::TYPE_ASSET_LANCAR)
	<th>Cost</th>
	<th>Code</th>
@endif
	<th>Quantity</th><th>Actions</th></tr></thead>
	<tbody>
	<tr ng-repeat="item in ilc.items">
		<td data-title="Image" ng-show="ilc.showImages"><img src="{{ asset('img/noimg.jpg') }}" class="b-lazy" data-blazy="@{{ item.image }}" width="200"/></td>
		<td data-title="ID"><a href="@{{ item.link }}">@{{ item.id }}</a></td>
		<td data-title="Kode"><a href="@{{ item.link }}">@{{ item.name }}</a></td>
@if($type == Item::TYPE_ITEM)
		<td data-title="Alias">@{{ item.alias }}</td>
@endif
		<td data-title="Description">@{{ item.description }}</td>
		<td data-title="Price">@{{ item.price | number: 2 }}</td>
@if($type == Item::TYPE_ASSET_LANCAR)
		<td data-title="Cost">@{{ item.cost | number: 2 }}</td>
		<td data-title="Code"><a href="@{{ item.link }}">@{{ item.code }}</a></td>
@endif
		<td data-title="Quantity">@{{ item.total_quantity | number: 2 }}</td>
		<td data-title="Action"><a class="btn" href="@{{ item.edit }}">edit</a></td>
	</tr>
	</tbody>
</table>
</div>
<div class="row">
<div class="center-align">
<a ng-disabled="!ilc.showPrevious" ng-click="ilc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!ilc.showNext" ng-click="ilc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>
<div ng-show="ilc.showProgress" class="progress"><div class="indeterminate"></div></div>

</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	paginator: {{ json_encode($items) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/item/itemListController.js') }}"></script>
@stop