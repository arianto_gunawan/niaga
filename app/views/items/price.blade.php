<?
use App\Models\Tag;
$order = array(Tag::TYPE_TYPE,Tag::TYPE_SIZE,Tag::TYPE_COMPONENT);
?>
{{ $errors->first('error','<div data-alert class="alert-box alert">:message<a href="#" class="close">&times;</a></div>') }}
{{ Form::open(array('url' => URL::current(), 'method' => 'POST')) }}
<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Add or Deduct') }}
		<div class="input-group">
		<span class="input-group-addon">Rp.</span>
		{{ Form::text('price',InputForm::old('price'),array('class' => 'form-control')) }}
		</div>
	</div>
</div>

<hr/>
@foreach($order as $i)
<fieldset>
	<legend>{{ Tag::$types[$i] }}</legend>
	<div class="form-group row">
	@foreach($tags[$i] as $tag)
	<div class="col-4 col-lg-2">
		<div class="checkbox">
			<label>
				<?=	Form::checkbox('tag['.$tag->id.']',1,InputForm::has("tag[$tag->id]"));?>
				<span data-toggle="tooltip" title="{{ $tag->name }}">{{ $tag->code }}</a>
			</label>
		</div>
	</div>
	@endforeach
	</div>
</fieldset>
@endforeach

<?
$old = InputForm::old('tag[jahit]');
?>
<fieldset>
	<legend>{{ Tag::$types[Tag::TYPE_JAHIT] }}</legend>
	<div class="form-group row">
	@foreach($tags[Tag::TYPE_JAHIT] as $tag)
	<div class="col-4 col-lg-2">
		<div class="radio">
		<label>
			<?=	Form::radio('tag[jahit]',$tag->id,$tag->id==$old?true:false);?>{{ $tag->code }}
		</label>
		</div>
	</div>
	@endforeach
	</div>
</fieldset>

{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}