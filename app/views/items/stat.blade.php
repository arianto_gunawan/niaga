<div ng-controller="ItemStatController as tlc">
<form id="itemListForm" name="itemListForm" autocomplete="off" ng-submit="tlc.submit()">
<div class="row">
<div class="col s3 m2">
  <label>From</label>
  <input type="text" ng-model="tlc.from" class="dateform"/>
</div>
<div class="col s3 m2">
  <label>To</label>
  <input type="text" ng-model="tlc.to" class="dateform"/>
</div>
<div class="col s3 m2">
  <label>Customer</label>
  <input ng-model="tlc.name" name="name">
</div>
<div class="col s3 m2 input-field">
	<input ng-click="tlc.submit()" type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>
<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>

<div id="scrollTo">
<table id="setoran" class="bordered striped highlight small-table centered">
	<thead>
		<tr><th>Date</th><th>Sell</th><th>Retur</th><th>Move</th><th>Produksi</th></tr>
	</thead>
	<tbody>
	<tr ng-repeat="i in tlc.items track by $index" ng-class="tlc.getClass(p.status)">
		<td>@{{ i.date }}</td>
		<td>@{{ tlc.displaySell(i.date) | number: 0 }}</td>
		<td>@{{ tlc.displayReturn(i.date) | number: 0 }}</td>
		<td>@{{ tlc.displayMove(i.date) | number: 0 }}</td>
		<td>@{{ tlc.displayProduksi(i.date) | number: 0 }}</td>
	</tr>
	</tbody>
	<tr>
		<th>Total</th>
		<td>@{{ tlc.getTotalSell() | number: 0 }}</td>
		<td>@{{ tlc.getTotalReturn() | number: 0  }}</td>
		<td>@{{ tlc.getTotalMove() | number: 0  }}</td>
		<td>@{{ tlc.getTotalProduksi() | number: 0  }}</td>
	</tr>
</table>
</div>


</form>
</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	items: {{ json_encode($items) }},
	sellConstant: {{ App\Models\Transaction::TYPE_SELL }},
	moveConstant: {{ App\Models\Transaction::TYPE_MOVE }},
	returnConstant: {{ App\Models\Transaction::TYPE_RETURN }},
	produksiConstant: {{ App\Models\Transaction::TYPE_PRODUCTION }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/item/ItemStatController.js') }}"></script>
@stop