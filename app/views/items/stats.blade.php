<?
$total = count($chart);
?>
<div class="form-group row">
{{ Form::open(array('url' => URL::current(), 'method' => 'GET')) }}
	<div class="col-xs-6 col-md-3">{{ Form::text('from',Input::get('from',null),array('class' => 'datepick form-control', 'placeholder' => 'From')) }}</div>
	<div class="col-xs-6 col-md-3">{{ Form::text('to',Input::get('to',null),array('class' => 'datepick form-control', 'placeholder' => 'To')) }}</div>
	<div class="col-xs-6 col-md-3">{{ Form::submit('Search',array('class' => 'btn')) }}</div>
{{ Form::close() }}
</div>
<div class="row">
	<div class="col-xs-8">
		<canvas id="pie" data-type="Pie" height="400">&nbsp;</canvas>
	</div>
	<div class="col-xs-4">
		<table class="table">
			<thead><tr><td>Color</td><td>Item</td></tr></thead>
		@for($i = 0; $i < $total; $i++)
			<tr>
				<td style="background:{{ $chart[$stat[$i]->item_id]['color'] }}">&nbsp;</td>
				<td>{{ $stat[$i]->item->code }}</td>
			</tr>
		@endfor
		</table>
	</div>
</div>
<h3>Details:</h3>
<table class="table table-striped table-bordered">
<thead><tr><th>Item</th><th>Tags</th><th>Price</th><th>Q</th><th>Total</th></tr></thead>
@foreach($stat as $s)
<tr>
	<td>{{ HTML::link(URL::action('ItemsController@getDetail', array($s->item_id)), $s->item->code) }}</td>
	<td>
@foreach($s->item->tags as $t)
	{{ $t->name }}&nbsp;
@endforeach
	</td>
	<td>{{ number_format($s->item->price) }}</td>
	<td>{{ number_format($s->total) }}</td>
	<td>{{ number_format($s->omset) }}</td>
</tr>
@endforeach
</table>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('.datepick').datepicker();
	$("#pie").attr('width',$('#pie').parent().width());

var data = [
@foreach($chart as $c)
<? $total--; ?>
	{
		value : {{ $c['value'] }},
		color : "{{ $c['color'] }}"
@if($total > 0)
	},
@else
	}
@endif
@endforeach
]

//Get context with jQuery - using jQuery's .get() method.
var ctx = $("#pie").get(0).getContext("2d");
//This will get the first returned node in the jQuery collection.
var myNewChart = new Chart(ctx).Pie(data);

});
/*]]>*/
</script>
@stop