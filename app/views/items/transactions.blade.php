<div ng-controller="itemTransactionController as it">

<ul class="tabs ph">
<li class="tab"><a target="_self" href="{{ $item->getDetailLink() }}">Detail</a></li>
<li class="tab"><a target="_self" class="active">Transactions</a></li>
<li class="tab"><a target="_self" href="{{ $item->getStatsLink() }}">Stats</a></li>
</ul>

<form class="ph" id="transactionListForm" autocomplete="off" ng-submit="it.submit()">
<div class="row">
<div class="input-field col s4 m3">
  <select class="browser-default" ng-model="it.type" ng-change="it.resetPage()" ng-options="type.name for type in it.types"></select>
</div>

<div class="col s4 m3">
	<label>Addr Book</label>
  <input type="text" typeahead-min-length="3" ng-change="it.resetPage()" ng-model="it.customer" uib-typeahead="item as item.name for item in it.getCustomer($viewValue)" >
</div>

<div class="col s4 m3">
	<input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>
</form>

<div ng-show="it.showProgress" class="progress"><div class="indeterminate"></div></div>

<div class="table-responsive-vertical shadow-z-1">
<table class="table table-hover table-striped">
	<thead><tr><th>Date</th><th>Type</th><th>Invoice</th><th>Description</th><th>Quantity</th><th>Sender</th><th>Receiver</th><th>Price</th></tr></thead>
	<tbody>
	<tr ng-repeat="transaction in it.transactions">
		<td data-title="Date"><a href="@{{ transaction.detail_link }}">@{{ transaction.date }}</a></td>
		<td data-title="Type">@{{ transaction.type }}</td>
		<td data-title="Invoice">@{{ transaction.invoice }}</td>
		<td data-title="Description">@{{ transaction.description }}</td>
		<td data-title="Quantity">@{{ transaction.quantity }}</td>
		<td data-title="Sender"><a href="@{{ transaction.sender_url }}">@{{ transaction.sender_name }}</a></td>
		<td data-title="Receiver"><a href="@{{ transaction.receiver_url }}">@{{ transaction.receiver_name }}</a></td>
		<td data-title="Price">@{{ transaction.price | number: 2 }}</td>
	</tr>
	</tbody>
</table>
</div>

<div ng-show="it.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row ph">
<div class="center-align">
<a ng-disabled="!it.showPrevious" ng-click="it.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!it.showNext" ng-click="it.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>
</div>

@section('script')
<script type="text/javascript">
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	types: {{ $typesJSON }},
	customersUrl: "{{ $defaults['customer_url'] }}",
	paginator: {{ json_encode($transactions) }}
}))
</script>
<script src="{{ asset('js/aria/services/customerService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/item/itemTransactionController.js') }}"></script>
@stop