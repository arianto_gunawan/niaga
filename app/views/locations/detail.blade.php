<div ng-controller="locationDetailController as ldc">

<table class="invoice">
	<tbody>
		<tr>
			<th>Can be seen by:</th>
			<td>
				@if(!empty($parents))
					@foreach($parents as $id => $name)
						{{ HTML::link(URL::action('LocationsController@getDetail',array($id)),$name) }}
					@endforeach
				@else
					No One
				@endif
			</td>
		</tr>
		<tr>
			<th>Can see:</th>
			<td>
				@if(!empty($children))
					@foreach($children as $id => $name)
						{{ HTML::link(URL::action('LocationsController@getDetail',array($id)),$name) }}
					@endforeach
				@else
					No One
				@endif
			</td>
		</tr>
	</tbody>
</table>

<form id="customerListForm" name="customerListForm" autocomplete="off" method="GET">
@if($assign)
<div class="row">
<div class="input-field col s6">
	<label>Name</label>
	<input type="text" typeahead-min-length="3" ng-model="ldc.customer" uib-typeahead="customer as customer.name for customer in ldc.getCustomer($viewValue)">
</div>
<div class="input-field col s2">
	<a class="btn" ng-click="ldc.assign()">Assign</a>
</div>
</div>
@endif

<div class="row">
<div class="input-field col s6">
  <label>Name</label>
  <input ng-model="ldc.name" type="text" ng-change="ldc.resetPage()">
</div>

<div class="input-field col s2">
	<a class="btn" ng-click="ldc.submit()">Search</a>
</div>
</form>

<div ng-show="ldc.showProgress" class="progress"><div class="indeterminate"></div></div>

<div class="table-responsive-vertical">
<table class="table table-striped table-bordered table-hover">
	<tbody>
	<tr ng-repeat="detail in ldc.details">
		<td data-title="Name"><a href="@{{ detail.link }} ">@{{ detail.name }}</a></td>
		<td data-title="Type">@{{ detail.type }}</td>
		<td data-title="Owner">
			<span ng-repeat="owner in detail.owners">
				<a href="@{{ owner.link }}">@{{ owner.name }}</a>
			</span>
		</td>
		<td data-title="Actions">
@if($dismiss)
	<form autocomplete="off" method="POST" action="{{ URL::current() }}">
	<input type="hidden" name="customer_ids" value="@{{ detail.id }}">
	<input type="submit" class="btn red" value="Dismiss">
	</form>
@endif
		</td>
	</tr>
	</tbody>
</table>

<div ng-show="ldc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="center-align">
<a ng-disabled="!ldc.showPrevious" ng-click="ldc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!ldc.showNext" ng-click="ldc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>

</div>



@section('script')
<script type="text/javascript">
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
@if(isset($details))
	details: {{ json_encode($details) }},
@endif
	currentUrl: "{{ URL::current() }}",
	customersUrl: "{{ $defaults['customer_url'] }}",
	assignUrl: "{{ URL::action('LocationsController@postAssign',array($location->id)) }}",
}))
</script>
<script src="{{ asset('js/aria/services/customerService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/user/locationDetailController.js') }}"></script>
@stop
