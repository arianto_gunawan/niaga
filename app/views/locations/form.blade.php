<form autocomplete="off" ng-controller="locationFormController as ifc" name="aForm" ng-submit="submit(aForm.$valid)" novalidate enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<label>Name</label>
<input required ng-model="ifc.location.name" />

<h3>Available Child Locations</h3>

<div class="input-field" ng-repeat="data in ifc.children">
<input type="checkbox" ng-checked="ifc.checkSelected($index)" ng-click="ifc.toggleTag($index)" id="id@{{ data.id }}">
<label for="id@{{ data.id }}">@{{ data.name }}</label>
</div>

<div class="input-field">
<div ng-show="ifc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="ifc.submit(aForm.$valid)" ng-disabled="ifc.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</div>
</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	location: {{ json_encode($location) }},
	children: {{ json_encode($children) }},
	selected: {{ json_encode($selected) }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/user/locationFormController.js') }}"></script>
@stop
