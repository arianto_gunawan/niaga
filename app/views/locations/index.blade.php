<div class="table-responsive-vertical shadow-z-1">
<table class="table table-striped table-hover">
<thead><tr><th>Name</th><th>Actions</th></tr></thead>
<tbody>
@foreach($locations as $l)
<tr>
	<td data-title="Name"><a href="{{ URL::action('LocationsController@getDetail',array($l->id)) }}">{{ $l->name }}</a></td>
	<td data-title="Actions">
@if($edit)
		<a href="{{ URL::action('LocationsController@getEdit',array($l->id)) }}" class="btn">Edit</a>
@endif
@if($settings)
		<a href="{{ URL::action('LocationsController@getSettings',array($l->id)) }}" title="Settings" class="btn red">Settings</a>
@endif
	</td>
</tr>
@endforeach
</tbody>
</table>
</div>