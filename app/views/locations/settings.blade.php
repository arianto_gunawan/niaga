<?
use App\Models\Settings;
$hour = array();
for($i = 1; $i <= 24; $i++)
	$hour[$i] = $i;
$minutes = array('00' => '00','15' => '15','30' => '30','45' => '45');
?>
<form autocomplete="off" name="aForm" method="POST" action="{{ URL::current() }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="row">
<div class="col s2">
<label>Start Hour</label>
<select name="start_hour">
@foreach($hour as $val)
	<option value="{{ $val }}" {{ $val == $start_hour ? 'selected':'' }}>{{ $val }}</option>
@endforeach
</select>
</div>
<div class="col s2">
<label>Start Minutes</label>
<select name="start_minutes">
@foreach($minutes as $val)
	<option value="{{ $val }}" {{ $val == $start_minutes ? 'selected':'' }}>{{ $val }}</option>
@endforeach
</select>
</div>
</div>

<div class="row">
<div class="col s2">
<label>Stop Hour</label>
<select name="stop_hour">
@foreach($hour as $val)
	<option value="{{ $val }}" {{ $val == $stop_hour ? 'selected':'' }}>{{ $val }}</option>
@endforeach
</select>
</div>
<div class="col s2">
<label>Stop Minutes</label>
<select name="stop_minutes">
@foreach($minutes as $val)
	<option value="{{ $val }}" {{ $val == $stop_minutes ? 'selected':'' }}>{{ $val }}</option>
@endforeach
</select>
</div>
</div>

<div class="input-field">
	<input type="checkbox" id="sunday_off" name="settings[sunday_off]" {{ $settings['sunday_off'] ? 'checked' : '' }} />
  <label for="sunday_off">Sunday Off?</label>
</div>

<div class="input-field">
	<input type="checkbox" id="turn_off" name="settings[turn_off]" {{ $settings['turn_off'] ? 'checked' : '' }} />
  <label for="turn_off">Turn Off?</label>
</div>

<div class="input-field">
<input type="submit" name="submit" value="Save" class="btn">
</div>
</form>

@yield('global')