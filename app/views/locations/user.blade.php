<div class="row">
	<div class="col-md-6">
		<h5>{{ $location->name }}</h5>
	</div>
@if($edit)
	<div class="col-md-6">
		<a href="{{URL::action('LocationsController@getEdit',array($location->id))}}" title="Edit" class="btn">Edit</a>
		<a href="{{URL::action('LocationsController@getSettings',array($location->id))}}" title="Settings" class="btn red">Settings</a>
	</div>
@endif
</div>
<ul class="nav nav-pills">
	<li><a href="{{ URL::action('LocationsController@getDetail',array($location->id)) }}">Addr Book</a></li>
	<li class="active"><a>User</a></li>
</ul>
<hr/>
@if($assign)
<div class="row">
{{ Form::open(array('url' => URL::action('LocationsController@postAssignUser',array($location->id)), 'method' => 'POST')) }}
	<div class="col-xs-6 col-md-3">{{ Form::text('user','',array('id'=>'user_search','class' => 'form-control')) }}{{ Form::hidden('user_id','',array('id'=>'user_id')) }}</div>
	<div class="col-xs-6 col-md-3">{{ Form::submit('Assign',array('class' => 'btn')) }}</div>
{{ Form::close() }}
</div>
@endif

@if($data)
<table class="table table-striped table-hover">
	<thead><tr><th>Name</th><th>Role</th>
@if($dismiss)
	<th>Actions</th>
@endif
	</tr></thead>
	<tbody>
	@foreach($data as $l)
	<tr>
		<td>{{ $l->user->username }}</td>
		<td>{{ $l->user->role->name }}</td>
		<td>
@if($dismiss)
	{{ Form::open(array('url' => URL::action('LocationsController@postDismissUser',array($location->id)), 'method' => 'POST')) }}
	{{ Form::hidden('user_id',$l->user_id) }}
	{{ Form::submit('Dismiss',array('class' => 'btn red')) }}
	{{ Form::close() }}
@endif
		</td>
	</tr>
	@endforeach
	</tbody>
</table>
{{ $data->links() }}
</div>
</div>
@endif

@section('script')
@if($assign)
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	//need to capture this keypress, cos the .live somehow doesnt work well with typeahead
	$('#user_search').typeahead([{
		name: "user",
		remote: "{{$defaults['user_url']}}" + "?q=%QUERY"
	}]).on('typeahead:selected', function($e, obj) {
		$('#user_id').attr("value",obj.id);
		return false;
	});
});
/*]]>*/
</script>
@endif
@stop