@if($result)
<table class="table table-striped table-hover">
<thead><th>Name</th><th>Date</th><th>Invoice</th><th>Total</th></thead>
<tbody>
@foreach($data as $d)
@if(!isset($result[$d->customer_id]))
<? continue; ?>
@endif
	<tr>
		<td><strong>{{ $result[$d->customer_id]['customer']['url'] }}</strong></td>
		<td colspan="3">
<? $total = 0; ?>
		<table class="table"><tbody>
	@foreach($result[$d->customer_id]['transactions'] as $tid => $t)
			<tr><td>{{ $t['date'] }}</td><td>{{ $t['url'] }}</td><td>{{ nf($t['total']) }}</td></tr>
			<? $total += $t['total']; ?>
	@endforeach
		<tr><td colspan="3"><strong>Total: {{ nf($total) }}</strong></td></tr>
		</tbody>
		</table>
		</td>
	</tr>
@endforeach
</tbody>
</table>
{{ $data->links() }}
@endif