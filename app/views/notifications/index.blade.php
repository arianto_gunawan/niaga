<?
$now = Dater::now();
?>
@if($notifications)
<table class="table table-striped table-hover">
<tbody>
@foreach($notifications as $app_id => $ns)
	<tr class="success"><td><strong>App: {{ Apps::$list[$app_id] }}</strong></td></tr>
		@foreach($ns as $n)
		<tr><td><p>{{ $n->comment }} <small>{{ $n->created_at->diffForHumans($now) }}</small></p></td></tr>
		@endforeach
@endforeach
</tbody>
</table>
@endif