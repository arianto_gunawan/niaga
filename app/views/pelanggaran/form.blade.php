{{ Form::open(array('url' => URL::current(), 'method' => 'POST')) }}
<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Nama') }}
		{{ Form::text('nama',InputForm::old('nama'),array('class' => 'form-control')) }}
		{{ Form::hidden('pelanggaran[personnel_id]',InputForm::old('pelanggaran[personnel_id]')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ InputForm::date('pelanggaran[date]',$errors->first('date'),'date',Dater::display(InputForm::old('pelanggaran[date]'))) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('jenis') }}
		{{ Form::text('pelanggaran[jenis]',InputForm::old('pelanggaran[jenis]'),array('class' => 'form-control')) }}
	</div>
</div>


<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('disipliner') }}
		{{ Form::select('pelanggaran[disipliner]',$disipliner,InputForm::old('pelanggaran[disipliner]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('administrative') }}
		{{ Form::text('pelanggaran[administrative]',InputForm::old('pelanggaran[administrative]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ InputForm::date('pelanggaran[disipliner_date]',$errors->first('disipliner_date'),'Tgl Disipliner',Dater::display(InputForm::old('pelanggaran[disipliner_date]'))) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Description') }}
		{{ Form::textarea('pelanggaran[description]',InputForm::old('pelanggaran[description]'),array('class' => 'form-control')) }}
	</div>
</div>

{{ Form::submit('Save', array('class' => 'btn')) }}
{{ Form::close() }}

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$(".datepick").datepicker();

	$('input[name="nama"]').typeahead([{
		name: "nama",
		remote: "{{ $defaults['personnel_url'] }}?q=%QUERY"
	}]).on('typeahead:selected', function($e, obj) {
		$('input[name="pelanggaran[personnel_id]"]').attr("value",obj.id);
		return false;
	});
});
/*]]>*/
</script>
@stop