<?
use App\Models\Pelanggaran;
?>
@if($pelanggaran)
<table class="table table-striped table-hover">
	<thead><tr><th>Date</th><th>Personnel</th><th>Jenis</th><th>Disipliner</th><th>Administrative</th><th>Tgl Disipliner</th><th>Description</th><th>Action</th></tr></thead>
	<tbody>
	@foreach($pelanggaran as $p)
	<tr>
		<td><a href="{{ URL::action('PelanggaranController@getEdit',array($p->id)) }}">{{ Dater::display($p->date) }}</a></td>
		<td>{{ $p->personnel->name }}</td>
		<td>{{ $p->jenis }}</td>
		<td>{{ Pelanggaran::$disipliner[$p->disipliner] }}</td>
		<td>{{ $p->administrative }}</td>
		<td>{{ Dater::display($p->disipliner_date) }}</td>
		<td>{{ $p->description }}</td>
		<td><a class="btn red modal-trigger" href="{{ URL::action('PelanggaranController@postDelete',array($p->id)) }}">Delete</a></td>
	</tr>
	@endforeach
	</tbody>
</table>
{{ $pelanggaran->appends(array('name' => Input::get('name')))->links() }}
<div class="modal fade" id="delete-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Are you sure sure?</h4>
			</div>
			<div class="modal-body">
				<p>for every deleted transaction, a kitten is eaten by orcs</p>
			</div>
			<div class="modal-footer">
				{{ Form::open(array('id' => 'modalxform','url' => URL::action('PelanggaranController@postDelete'), 'method' => 'POST')) }}
				<button type="button" class="btn btn-default" data-dismiss="modal">No!!!</button>
				{{ Form::submit('Damn straight!!',array('id' => 'deletebutton', 'class' => 'btn red')) }}
				{{ Form::close() }}
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
@endif

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$(".modal-trigger").click(function(e) {
		var href = this.getAttribute('href');

		$("form#modalxform").attr('action',href);
		$("#delete-modal").modal('show');

		e.preventDefault();
		return false;
	});
});
/*]]>*/
</script>
@stop