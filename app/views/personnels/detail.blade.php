<ul class="tabs ph">
<li class="tab"><a target="_self" class="active">Detail</a></li>
<li class="tab"><a target="_self" href="{{ $personnel->getCutiLink() }}">Transactions</a></li>
<li class="tab"><a target="_self" href="{{ $personnel->getPelanggaranLink() }}">Items</a></li>
<li class="tab"><a target="_self" href="{{ $personnel->getGpuLink() }}">GPU</a></li>
</ul>

<div ng-controller="personnelDetailController as cd" class="detail_info">
<p><span>Address</span>{{ nl2br($personnel->address) }}</p>
<p><span>Phone</span>{{ $personnel->phone }}</p>

@if(!empty($personnel->department))
	<p><span>Dept</span>{{ $personnel->printDepartment() }}</p>
@endif
@if(!empty($personnel->status))
	<p><span>Status</span>{{ $personnel->printStatus() }}</p>
@endif

@if($personnel->birthplace && $personnel->birthdate)
	<p><span>Place, DOB</span>{{ $personnel->birthplace }}, {{ Dater::display($personnel->birthdate) }}</p>
@endif
@if(!empty($personnel->birthdate))
	<p><span>Age</span>{{ Dater::fromSQL($personnel->birthdate)->age }}</p>
@endif
@if($personnel->group)
	<p><span>Group</span>{{ $personnel->group }}</p>
@endif
@if($personnel->date_in)
	<p><span>Masuk, Masa Kerja</span>{{ Dater::display($personnel->date_in) }}, {{ Dater::fromSQL($personnel->date_in)->age }} th</p>
@endif
@if($edit_cuti)
	<p><span>Sisa Tahunan</span>{{ $sisa_cuti->sisa_tahunan }}</p>
	<p><span>Sisa Sakit</span>{{ $sisa_cuti->sisa_sakit }}</p>
@endif
@if($view_gpu)
	<p><span>Bulanan</span>{{ nf($personnel->bulanan) }}</p>
	<p><span>Harian</span>{{ nf($personnel->harian) }}</p>
	<p><span>Premi</span>{{ nf($personnel->premi) }}</p>
	@if($personnel->account_id > 0)
	<p><span>Account</span>{{ $personnel->account->name}}</p>
	@endif
@endif
@if($delete || $restore)
<p>
	<a href="#" class="waves-effect red btn" ng-click="cd.showModal()">@{{ cd.action.button }}</a>
</p>
@endif
<p>
	<a href="{{ $personnel->getEditLink() }}" class="btn">Edit</a>
</p>

<div id="personnelModal" class="modal">
  <div class="modal-content">
    <h4>Are you sure??</h4>
    <p>@{{ cd.msg }}</p>
  </div>
  <div class="modal-footer">
    <a href="#!" ng-click="cd.closeModal()" class="waves-effect red btn">Cancel!</a>
    <a href="#!" ng-click="cd.submit()" class="waves-effect green btn">Sure</a>
  </div>
</div>
</div>

@section('script')
<script type="text/javascript">
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	action: {{ json_encode(array('deleted_at' => $personnel->deleted_at, 'delete' => $delete, 'restore' => $restore)) }},
@if($delete || $restore)
	deleteUrl: '{{ URL::action('PersonnelsController@postDelete',array($personnel->id)) }}',
	restoreUrl: '{{ URL::action('PersonnelsController@postRestore',array($personnel->id)) }}'
@endif
}))
</script>

<script src="{{ asset('js/aria/controllers/personnel/personnelDetailController.js') }}"></script>
@stop