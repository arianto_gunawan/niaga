{{ Form::open(array('url' => URL::current(), 'method' => 'POST')) }}
<div class="form-group row">
	<div class="col-lg-6">
		{{ InputForm::text('personnel[name]',$errors->first('name'),'name') }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ InputForm::text('personnel[address]',$errors->first('address'),'address') }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ InputForm::text('personnel[phone]',$errors->first('phone'),'phone') }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-3">
		{{ Form::label('Tempat') }}
		{{ Form::text('personnel[birthplace]',InputForm::old('personnel[birthplace]'),array('class' => 'form-control')) }}
	</div>
	<div class="col-lg-3">
		{{ Form::label('Tgl Lahir') }}
		{{ Form::text('personnel[birthdate]',Dater::display(InputForm::old('personnel[birthdate]')),array('class' => 'datepick form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Department') }}
		{{ Form::select('personnel[department]',$departments,InputForm::old('personnel[department]'),array('class' => 'form-control')) }}
	</div>
</div>


<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('jabatan') }}
		{{ Form::text('personnel[jabatan]',InputForm::old('personnel[jabatan]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('group') }}
		{{ Form::text('personnel[group]',InputForm::old('personnel[group]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('In') }}
		{{ Form::text('personnel[date_in]',Dater::display(InputForm::old('personnel[date_in]')),array('class' => 'datepick form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Out') }}
		{{ Form::text('personnel[date_out]',InputForm::has('personnel[date_out]')?Dater::display(InputForm::old('personnel[date_out]')):0,array('class' => 'datepick form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Status') }}
		{{ Form::select('personnel[status]',$status,InputForm::old('personnel.status'),array('class' => 'form-control')) }}
	</div>
</div>

@if($edit_cuti)
<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Sisa Cuti Tahunan') }}
		{{ Form::text('sisa_tahunan',InputForm::old('sisa_tahunan'),array('class' => 'form-control')) }}
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Sisa Cuti Sakit') }}
		{{ Form::text('sisa_sakit',InputForm::old('sisa_sakit'),array('class' => 'form-control')) }}
	</div>
</div>
@endif

{{ Form::submit('Save', array('class' => 'btn')) }}
{{ Form::close() }}

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('input.datepick').datepicker();
});
/*]]>*/
</script>
@stop