@if($personnels)
<table class="table table-striped table-hover">
	<thead><tr><th>{{ HTML::sort('name') }}</th><th>{{ HTML::sort('department') }}</th><th>Bulanan</th><th>Harian</th><th>Premi</th><th>Monthly (est.)</th><th>Account</th><th>Actions</th></tr></thead>
	<tbody>
	<tr>
	{{ Form::open(array('url' => URL::current(), 'method' => 'GET', 'autocomplete' => 'off')) }}
		<td>{{ Form::text('name',Input::get('name'),array('class' => 'form-control')) }}</td>
		<td>{{ Form::select('department',$departments,Input::get('department'),array('class' => 'form-control')) }}</td>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		<td>&nbsp;</td><td>&nbsp;</td>
		<td>{{ Form::submit('Seach',array('class' => 'btn')) }}</td>
	{{ Form::close() }}
	</tr>
	@foreach($personnels as $p)
	<tr>
		<td><a href="{{ URL::action('PersonnelsController@getDetail',array($p->id)) }}">{{ $p->name }}</a></td>
		<td>{{ $departments[$p->department] }}</td>
		<td>{{ nf($p->bulanan) }}</td>
		<td>{{ nf($p->harian) }}</td>
		<td>{{ nf($p->premi) }}</td>
		<td>{{ nf($p->getMonthly()) }}</td>
		<td>{{ $p->account_id?$p->account->name:''}}</td>
		<td>
			<a class="btn" href="{{ URL::action('PersonnelsController@getEditGpu', array($p->id) )}}">Edit</a>
			<a class="btn btn-info" href="{{ URL::action('PersonnelsController@getGpuHistory', array($p->id) )}}">History</a>
		</td>
	</tr>
	@endforeach
	</tbody>
</table>
{{ $personnels->appends(array('name' => Input::get('name'), 'sort' => Input::get('sort'),'department' => Input::get('department'),'dir' => Input::get('dir')))->links() }}
@endif