{{ Form::open(array('url' => URL::current(), 'method' => 'POST')) }}

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Bulanan') }}
		{{ Form::text('gpu[bulanan]',InputForm::old('personnel[bulanan]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Harian') }}
		{{ Form::text('gpu[harian]',InputForm::old('personnel[harian]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Premi') }}
		{{ Form::text('gpu[premi]',InputForm::old('personnel[premi]'),array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Account') }}
		{{ Form::select('account_id',$accounts,InputForm::old('personnel[account_id]'),array('class' => 'form-control')) }}
	</div>
</div>

@if($private)
<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Private') }}
		<input type="checkbox" name="gpu[private]" value="1" <?= InputForm::old('personnel[private]')==1?'checked="checked"':''?> />
	</div>
</div>
@endif


{{ Form::submit('Save', array('class' => 'btn')) }}
{{ Form::close() }}
