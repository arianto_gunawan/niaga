<div ng-controller="personnelListController as tlc">
<form id="customerListForm" name="customerListForm" autocomplete="off" ng-submit="tlc.submit()">
<div class="row">
<div class="col s4 m2">
	<label>Name</label>
	<input type="text" ng-model="tlc.name" ng-change="tlc.resetPage()"/>
</div>
<div class="col s4 m2 input-field">
<div class="switch">
  <label>
    <input type="checkbox" ng-model="tlc.deleted">
    <span class="lever"></span>
    Show Deleted
  </label>
</div>
</div>
<div class="col s4 m2">
	<label>Department</label>
  <select class="browser-default" ng-model="tlc.dept" ng-change="tlc.resetPage()" ng-options="d.name for d in tlc.depts"></select>
</div>
<div class="col s4 m2">
	<label>Status</label>
  <select class="browser-default" ng-model="tlc.status" ng-change="tlc.resetPage()" ng-options="d.name for d in tlc.status"></select>
</div>
<div class="col s4 m2 input-field">
	<input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>
</form>

<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="table-responsive-vertical">
<table class="table table-striped table-bordered table-hover">
<thead><tr><th>Name</th><th>Department</th><th>Position</th><th>Age</th><th>Masa</th><th>Status</th></tr>
<tbody>
	<tr ng-repeat="personnel in tlc.personnels">
	<td data-title="Name"><a href="@{{ personnel.detail_link }}">@{{ personnel.name }}</a></td>
	<td data-title="Department">@{{ personnel.department }}</td>
	<td data-title="Position">@{{ personnel.position }}</td>
	<td data-title="Age">@{{ personnel.birthdate | age | number: 1 }}</td>
	<td data-title="Masa">@{{ personnel.date_in | age | number: 1 }}</td>
	<td data-title="Status">@{{ personnel.status }}</td>
	</tr>
</tbody>
</thead>
</table>
</div>

<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="center-align">
<a ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>

</div>


@section('script')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	paginator: {{ json_encode($personnels) }},
	depts: {{ $deptsJSON }},
	status: {{ $statusJSON }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/personnel/personnelListController.js') }}"></script>
@stop