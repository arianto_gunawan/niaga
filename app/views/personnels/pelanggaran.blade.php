@extends('personnels.detail')
@section('personnel')
@if($pelanggaran)
<table class="table table-striped table-hover">
	<thead><tr><th>Date</th><th>Personnel</th><th>Jenis</th><th>Disipliner</th><th>Administrative</th><th>Tgl Disipliner</th><th>Description</th></tr></thead>
	<tbody>
	@foreach($pelanggaran as $p)
	<tr>
		<td><a href="{{ URL::action('PelanggaranController@getEdit',array($p->id)) }}">{{ Dater::display($p->date) }}</a></td>
		<td>{{ $p->personnel->name }}</td>
		<td>{{ $p->jenis }}</td>
		<td>{{ App\Models\Pelanggaran::$disipliner[$p->disipliner] }}</td>
		<td>{{ $p->administrative }}</td>
		<td>{{ Dater::display($p->disipliner_date) }}</td>
		<td>{{ $p->description }}</td>
	</tr>
	@endforeach
	</tbody>
</table>
{{ $pelanggaran->appends(array('name' => Input::get('name')))->links() }}
@endif
@stop