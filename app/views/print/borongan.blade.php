<table width="100%">
	<tbody>
		<tr><td>Date</td><td>{{ Dater::display($borongan->date) }}</td></tr>
		<tr><td>Group</td><td>{{ $borongan->group }}</td></tr>
		<tr><td>Tres</td><td>{{ number_format($borongan->tres) }}</td></tr>
		<tr><td>Permak</td><td>{{ number_format($borongan->permak) }}</td></tr>
		<tr><td>Lain2</td><td>{{ number_format($borongan->lain2) }}</td></tr>
		<tr><td>Total</td><td>{{ number_format($borongan->total) }}</td></tr>
		<tr><td>Items</td><td>{{ number_format($borongan->total_items) }}</td></tr>
	</tbody>
</table>

@if(isset($details))
<hr/>
<table width="100%">
	<thead><tr><th>Item</th><th>Price</th><th>Quantity</th><th>Total</th></tr></thead>
	<tbody>
@foreach($details as $detail)
	<tr>
		<td>{{ HTML::link(URL::action('ItemsController@getDetail',array($detail->item_id)),$detail->item->code) }}</td>
		<td>{{ number_format($detail->ongkos) }}</td>
		<td>{{ number_format($detail->quantity) }}</td>
		<td>{{ number_format($detail->total) }}</td>
	</tr>
@endforeach
	</tbody>
</table>
@endif