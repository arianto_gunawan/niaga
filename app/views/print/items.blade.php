@if(isset($items))
<table class="table table-striped table-hover">
	<thead><tr><th>{{ HTML::sort('code') }}</th><th>{{ HTML::sort('name') }}</th><th>Price</th><th>{{ HTML::sort('quantity') }}</th></tr></thead>
	<tbody>
	@foreach($items as $t)
	<tr>
		<td>{{ App\Models\Item::getDetailUrl($t->item_id,$t->code,$t->type) }}</td>
		<td>{{ $t->name }}</td>
		<td>{{ number_format($t->price) }}</td>
		<td>{{ number_format($t->quantity) }}</td>
	</tr>
	@endforeach
	</tbody>
</table>
{{ $items->appends(array('sort' => Input::get('sort'), 'dir' => Input::get('dir')))->links() }}
@endif