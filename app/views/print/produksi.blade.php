@if(isset($produksi))
<table class="border">
	<thead><tr>
		<th>{{ HTML::sort('potong_in','Tgl Potong') }}</th>
		<th>Potong</th>
		<th>J Potong</th>
		<th>Barang</th>
		<th>Size</th>
		<th>Warna</th>
		<th>Cust</th>
		<th>Group</th>
		<th>Keluar</th>
		<th>{{ HTML::sort('jahit_out','Ke Jahit') }}</th>
		<th>{{ HTML::sort('jahit_in','Dari Jahit') }}</th>
		<th>Keterangan</th>
		<th>{{ HTML::sort('permak_out','Ke Permak') }}</th>
		<th>{{ HTML::sort('permak_in','Dari Permak') }}</th>
	</tr></thead>
	<tbody>
	@foreach($produksi as $t)
	<tr{{ $t->urgent?' class="success"':'' }}>
		<td>{{ HTML::link(URL::action('ProductionsController@getEdit',array($t->id)),Dater::display($t->potong_in)) }}</td>
		<td>{{ $t->potong->name }}</td>
		<td>{{ $t->quantity }}</td>
		<td>
@if($t->item_id)
			{{ $t->item->code}}
@else
			{{ $t->temp_name }}
@endif
		</td>
		<td>{{ $t->size?$t->size->code:'' }}</td>
		<td>{{ $t->warna }}</td>
		<td>{{ $t->customer }}</td>
		<td>
			{{ $t->jahit }}
		</td>
		<td>
			{{ number_format($t->q_out) }}
		</td>
		<td>
			{{ Dater::display($t->jahit_out) }}
		</td>
		<td>
			{{ Dater::display($t->jahit_in) }}
		</td>
		<td>{{ $t->description }}</td>
		<td>
@if(!$t->permak_in)
			&nbsp;
@else
			{{ Dater::display($t->permak_in) }}
@endif
		</td>
		<td>
@if(!$t->permak_out)
			&nbsp;
@else
			{{ Dater::display($t->permak_out) }}
@endif
		</td>
	</tr>
	@endforeach
	</tbody>
</table>
{{ $produksi->appends(array('warna' => Input::get('warna'), 'customer' => Input::get('customer'), 'jahit' => Input::get('jahit'), 'potong' => Input::get('potong'), 'sort' => Input::get('sort'), 'dir' => Input::get('dir'), 'from' => Input::get('from'), 'to' => Input::get('to')))->links() }}
@endif