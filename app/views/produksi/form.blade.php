<form autocomplete="off" ng-controller="produksiFormController as cc" name="tForm" ng-submit="submit(tForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="row">
<div class="input-field">
<label>Date</label>
<input type="text" ng-model="cc.date" class="dateform" />
</div>
</div>

<div class="row">
<label>Potong</label>
<select class="browser-default" ng-model="cc.potong" ng-options="a.name for a in cc.potongs"></select>
</div>

<div class="row">
<h5>Items</h5>
<table class="table">
	<tbody>
	<tr ng-repeat="detail in cc.details track by $index">
		<td>
			<label>Name</label>
			<input focus ng-model="detail.temp_name" required />
		</td>
		<td width="120">
			<label>Size</label>
			<select class="browser-default" ng-model="detail.size" ng-options="a.name for a in cc.sizes"></select>
		</td>
		<td>
			<label>Quantity</label>
			<input focus ng-model="detail.quantity" required />
		</td>
		<td>
			<label>Customer</label>
			<input focus required ng-model="detail.customer" />
		</td>
		<td>
			<label>Warna</label>
			<input focus ng-model="detail.warna" add-row="@{{ $index }}" />
		</td>
		<td>
			<a href="#" ng-click="cc.removeRow($index)"><i class="material-icons">clear</i></a>
		</td>
	</tr>
	</tbody>
</table>
</div>
<div ng-show="cc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="cc.submit(tForm.$valid)" ng-disabled="cc.disableSubmit || tForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	jahits: {{ json_encode($jahit) }},
	potongs: {{ json_encode($potong) }},
	sizes: {{ json_encode($size) }},
	submitURL: "{{ URL::current() }}"
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/transaction.js') }}"></script>
<script src="{{ asset('js/aria/controllers/produksi/produksiFormController.js') }}"></script>
@stop