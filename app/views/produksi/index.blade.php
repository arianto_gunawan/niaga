<div ng-controller="produksiListController as tlc">
<form id="produksiListForm" name="produksiListForm" autocomplete="off" ng-submit="tlc.submit()">
<div class="row">
<div class="col s4 m2">
  <label>From</label>
  <input type="text" ng-model="tlc.from" class="dateform" ng-change="tlc.resetPage()"/>
</div>
<div class="col s4 m2">
  <label>To</label>
  <input type="text" ng-model="tlc.to" class="dateform" ng-change="tlc.resetPage()"/>
</div>
<div class="col s4 m2 input-field">
	<input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>

<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>

<div id="scrollTo">
<table class="bordered striped highlight small-table centered" id="produksi">
	<thead><tr>
		<th>Kitir</th><th>Kode</th><th>Jumlah</th><th>Potong</th><th>Size</th><th>Warna</th><th>Customer</th>
		<th>Jahit</th><th>Action</th>
	</tr></thead>
	<tbody>
	<tr>
		<td><input type="text" ng-model="tlc.serial"></td>
		<td><input type="text" ng-model="tlc.kode"></td>
		<td>&nbsp;</td>
		<td>
		  <select class="browser-default" ng-model="tlc.potong" ng-change="tlc.resetPage()" ng-options="potong.name for potong in tlc.potongs"></select>
		</td>
		<td>&nbsp;</td>
		<td><input type="text" ng-model="tlc.warna"></td>
		<td><input type="text" ng-model="tlc.customer"></td>
		<td><select class="browser-default" ng-model="tlc.jahit" ng-options="j.name for j in tlc.jahits" required></select></td>
		<td><a ng-click="tlc.submit()" class="btn-floating btn-large waves-effect waves-light blue"><i class="material-icons">search</i></a></td>
	</tr>
	<tr ng-repeat="p in tlc.produksi track by p.id">
		<td><a href="@{{ p.edit_link }}">@{{ tlc.printSerial($index) }}</a></td>
		<td>@{{ tlc.displayItemName($index) }}</td>
		<td>@{{ p.quantity }}</td>
		<td>@{{ p.potong_date }}<hr/>@{{ p.potong.name }}</td>
		<td>@{{ p.size.name }}</td>
		<td>@{{ p.warna }}</td>
		<td>@{{ p.customer }}</td>
		<td ng-show="tlc.jahitInput($index)">
			@{{ p.jahit_date }}<hr/>@{{ p.jahit.name }}
		</td>
		<td ng-show="!tlc.jahitInput($index)">
		  <select class="browser-default" ng-model="p.jahit_form" ng-options="j.name for j in tlc.jahits" required></select>
		</td>
		<td>
			<a type="submit" value="Save" ng-show="tlc.isValid($index)" class="waves-effect waves-light btn" ng-click="tlc.saveRow($index)" ng-disabled="tlc.rowDisabled($index)">Save</a>
			<a type="submit" value="Setor" ng-show="tlc.isValidSetor($index)" class="waves-effect waves-light btn red" ng-click="tlc.setorRow($index)" ng-disabled="tlc.rowSetorDisabled($index)">Setor</a>
		</td>
	</tr>
	</tbody>
</table>
</div>
<br/>
<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="center-align">
<a ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>
</form>
</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	saveURL: "{{ URL::action('ProduksiController@postSaveRow') }}",
	setorURL: "{{ URL::action('ProduksiController@postSetor') }}",
	potongs: {{ json_encode($potong) }},
	jahits: {{ json_encode($jahit) }},
	paginator: {{ json_encode($produksi) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/produksi/produksiListController.js') }}"></script>
@stop