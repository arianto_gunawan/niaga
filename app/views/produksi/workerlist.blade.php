<?
use App\Models\Worker;
?>
<div class="table-responsive-vertical">
<table class="table table-striped table-bordered table-hover">
<thead><tr><th>Name</th><th>Edit</th><th>Action</th></tr>
<tbody>
@foreach($workers as $w)
	<tr>
	<td data-title="Name">{{ $w->name }}</td>
	<td data-title="Edit"><a href="{{ URL::action('ProduksiController@'.$edit_action, array($w->id)) }}">Edit</a></td>
	<td data-title="Action">
@if($w->deleted_at)
<form action="{{ URL::action('ProduksiController@'.$restore_action, array($w->id)) }}" method="POST">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="submit" value="Restore" class="btn red">
</form>
@else
<form action="{{ URL::action('ProduksiController@'.$delete_action, array($w->id)) }}" method="POST">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="submit" value="Delete" class="btn">
</form>
@endif
	</td>
	</tr>
@endforeach
</tbody>
</thead>
</table>
</div>