<?
use App\Models\UserSetting;
?>
<div ng-controller="profileFormController as pfc">
<form autocomplete="off" name="aForm" ng-submit="pfc.submit(aForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

@foreach(UserSetting::$list as $name => $val)
<div class="row">
<div class="col s12 m6">
<label>{{ $val['desc'] }}</label>
@if($val['type'] == UserSetting::TYPE_WAREHOUSE)
<select class="browser-default" ng-model="pfc.settings.{{ $name }}.value" ng-options="a.name for a in pfc.warehouse"></select>
@else
<select class="browser-default" ng-model="pfc.settings.{{ $name }}.value" ng-options="a.name for a in pfc.account"></select>
@endif
</div>
</div>
@endforeach

<div class="input-field">
<a ng-click="pfc.submit(aForm.$valid)" ng-disabled="pfc.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</div>
<div ng-show="pfc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	list: {{ json_encode(UserSetting::$list) }},
	account: {{ json_encode($account) }},
	warehouse: {{ json_encode($warehouse) }},
	settings: {{ json_encode($settings) }},
	warehouseType: {{ UserSetting::TYPE_WAREHOUSE }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/profile/profileFormController.js') }}"></script>
@stop
