<table class="table table-striped table-hover table-bordered">
<tr>
	<th colspan="2">Asset</th>
</tr>
@foreach($asset as $a)
<tr>
	<td><a href="{{ URL::action('WarehousesController@getDetail', array($a->warehouse_id)) }}">{{ $a->name }}</a></td>
	<td>{{ number_format($a->total_asset) }}</td>
</tr>
@endforeach
<tr>
	<th>Sales</th>
	<td>{{ number_format($this_month['report']['revenue']) }}</td>
</tr>
<tr>
	<th>Profit</th>
	<td>{{ number_format($this_month['report']['nett']) }}</td>
</tr>
<tr>
	<th>Cash</th>
	<td>{{ number_format($cash) }}</td>
</tr>
</table>