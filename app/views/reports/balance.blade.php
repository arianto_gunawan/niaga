<div ng-controller="balanceController as plc" id="balance">

<ul class="collapsible" data-collapsible="expandable">
<li>
  <div class="collapsible-header"><i class="material-icons">attach_money</i>ASSET LANCAR</div>
</li>
<li>
  <div class="collapsible-header">Kas dan setara kas<i class="material-icons">expand_more</i> <span class="right">@{{ plc.total.kas | number: 2 }}</span></div>
  <div class="collapsible-body">
  <table class="table">
  <tbody>
  	<tr ng-repeat="kas in plc.data.kas">
  		<td><a href="@{{ plc.getLink(kas.id, 'kas') }}" target="_blank">@{{ kas.name }}</a></td>
  		<td>@{{ kas.stat.balance | number: 2 }}</td>
  	</tr>
  </tbody>
  </table>
  </div>
</li>
<li>
  <div class="collapsible-header">Piutang Reseller<i class="material-icons">expand_more</i> <span class="right">@{{ plc.total.piutang_reseller | number: 2 }}</span></div>
  <div class="collapsible-body">
  <table class="table">
  <tbody>
  	<tr ng-repeat="data in plc.data.piutang_reseller" ng-show="data.stat.balance > 0">
  		<td><a href="@{{ plc.getLink(data.id, 'reseller') }}" target="_blank">@{{ data.name }}</a></td>
  		<td>@{{ 0 - data.stat.balance | number: 2 }}</td>
  	</tr>
  </tbody>
  </table>
  </div>
</li>
<li>
  <div class="collapsible-header">Piutang Customer<i class="material-icons">expand_more</i> <span class="right">@{{ plc.total.piutang_customer | number: 2 }}</span></div>
  <div class="collapsible-body">
  <table class="table">
  <tbody>
  	<tr ng-repeat="data in plc.data.piutang_customer" ng-show="data.stat.balance > 0">
  		<td><a href="@{{ plc.getLink(data.id, 'customer') }}" target="_blank">@{{ data.name }}</a></td>
  		<td>@{{ 0 - data.stat.balance | number: 2 }}</td>
  	</tr>
  </tbody>
  </table>
  </div>
</li>
<li>
  <div class="collapsible-header"><strong>GRAND TOTAL <span class="right">@{{ plc.total.asset_lancar | number: 2 }}</span></strong></div>
</li>
<li>
  <div class="collapsible-header"><i class="material-icons">expand_more</i>ASSET TETAP</div>
  <div class="collapsible-body">
  <table class="table">
  <tbody>
  	<tr ng-repeat="kas in plc.data.asset_tetap">
  		<td>@{{ kas.name }}</td>
  		<td>@{{ kas.d_value | number: 2 }}</td>
  	</tr>
  </tbody>
  </table>
  </div>
</li>
<li>
	<div class="collapsible-header"><strong>GRAND TOTAL <span class="right">@{{ plc.total.asset_tetap | number: 2 }}</span></strong></div>
</li>
<li>
  <div class="collapsible-header"><i class="material-icons">expand_more</i>HUTANG USAHA</div>
  <div class="collapsible-body">
  <table class="table">
  <tbody>
  	<tr ng-repeat="kas in plc.data.hutang_usaha">
  		<td><a href="@{{ plc.getLink(kas.id, 'supplier') }}" target="_blank">@{{ kas.name }}</a></td>
  		<td>@{{ kas.stat.balance | number: 2 }}</td>
  	</tr>
  </tbody>
  </table>
  </div>
</li>
<li>
	<div class="collapsible-header"><strong>GRAND TOTAL <span class="right">@{{ plc.total.hutang_usaha | number: 2 }}</span></strong></div>
</li>
</ul>


</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	data: {{ json_encode($data) }},
	customerDetailLink: "{{ URL::action('CustomersController@getDetail') }}",
	resellerDetailLink: "{{ URL::action('ResellersController@getDetail') }}",
	kasDetailLink: "{{ URL::action('BankAccountsController@getDetail') }}",
	supplierDetailLink: "{{ URL::action('SuppliersController@getDetail') }}"
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/reports/balanceController.js') }}"></script>
@stop