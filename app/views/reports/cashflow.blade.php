<?
use App\Models\Customer;
$oCashIn = $oCashOut = $cashIn = $cashOut = 0;
?>
<div class="row">
{{ Form::open(array('url' => URL::current(), 'method' => 'GET')) }}
	<div class="col-xs-6 col-md-4">
		<input type="text" name="month" value="{{ Input::get('month',null) }}" placeholder="Month" class="datepick form-control" data-date-format="mm/yyyy" data-date-viewmode="months" data-date-minviewmode="months"/>
	</div>
	<div class="col-xs-6 col-md-2">{{ Form::submit('Search',array('class' => 'btn')) }}</div>
{{ Form::close() }}
</div>
<hr/>

<table class="table table-bordered table-striped">
<thead><tr><th>From</th><th>Cash In</th><th>Cash Out</th></tr></thead>
<tbody>
@foreach(array(Customer::TYPE_SUPPLIER, Customer::TYPE_CUSTOMER, Customer::TYPE_RESELLER) as $type)
@if(is_array($cf[$type]))
<tr><td>{{ Customer::$types[$type] }}</td><td>0</td><td>0</td></tr>
@else
<tr><td>{{ Customer::$types[$type] }}</td><td>{{ nf($cf[$type]->total_income) }}</td><td>{{ nf($cf[$type]->total_expense) }}</td></tr>
<? $cashIn += $cf[$type]->total_income; $cashOut += $cf[$type]->total_expense; ?>
@endif
@endforeach
<tr class="success"><th class="text-center">Total</th><th>{{ nf($cashIn) }}</th><th>{{ nf($cashOut) }}</th></tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr><th>Operational</th><th>Cash In</th><th>Cash Out</th></tr>
@foreach($operations as $key => $type)
	@if(isset($pl['operational'][$key]))
	<tr>
		<td><a href="{{ URL::action('OperationsController@getIndex',array('from' => $links['from'], 'to' => $links['to'], 'operation' => $key)) }}"><strong>{{ $type }}</strong></a></td>
		<td>{{ nf($pl['operational'][$key]['cash_in']) }}</td>
		<td>{{ nf($pl['operational'][$key]['cash_out']) }}</td>
	</tr>
	<? $oCashIn += $pl['operational'][$key]['cash_in']; $oCashOut += $pl['operational'][$key]['cash_out']; ?>
	@endif
@endforeach
<tr class="success"><th class="text-center">Total</th><th>{{ nf($oCashIn) }}</th><th>{{ nf($oCashOut) }}</th></tr>
<tr class="warning"><th class="text-center">Grand Total</th><th>{{ nf($cashIn + $oCashIn) }}</th><th>{{ nf($cashOut + $oCashOut) }}</th></tr>
<tr class="info"><th colspan="3" class="text-center">Nett: {{ nf($cashIn + $oCashIn - $cashOut - $oCashOut) }}</th></tr>
</tbody>
</table>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('form .datepick').datepicker();
});
/*]]>*/
</script>
@stop