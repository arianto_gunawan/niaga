<?
use App\Models\Customer;
$i = 0;
$customer_type = Customer::TYPE_CUSTOMER;
?>
<div class="form-group row">
{{ Form::open(array('url' => URL::current(), 'method' => 'GET')) }}
	<div class="col-xs-6 col-md-3">
		<input type="text" name="from" value="{{ Input::get('from',null) }}" placeholder="From" class="datepick form-control" data-date-format="mm/yyyy" data-date-viewmode="months" data-date-minviewmode="months"/>
	</div>
	<div class="col-xs-6 col-md-3">
		<input type="text" name="to" value="{{ Input::get('to',null) }}" placeholder="To" class="datepick form-control" data-date-format="mm/yyyy" data-date-viewmode="months" data-date-minviewmode="months"/>
	</div>
	<div class="col-xs-6 col-md-3">{{ Form::submit('Go!',array('class' => 'btn')) }}</div>
{{ Form::close() }}
</div>

@foreach($chart as $date => $val)
<? $my = Dater::displayMonthYear($val['date']); ?>
<p class="lead">{{ $my }}</p>
<div class="row">
	<div class="col-xs-5">
		<canvas id="pie{{ $i }}" data-type="Pie" height="500">&nbsp;</canvas>
	</div>
	<div class="col-xs-7">
		<table class="table">
			<thead><tr><th>Class</th><th>Total</th><th>Sell</th><th>Cash In</th><th>Return</th><th>Cash Out</th></tr></thead>
		@foreach($val['data'] as $class => $v)
			<tr>
				<td style="background:{{ $v['color'] }}"><a href="{{ URL::action('CustomersController@getIndex',array('type'=>$customer_type,'class'=>$class)) }}">{{ $class }}</a></td>
				<td>{{ $v['value'] }}</td>
				<td>{{ number_format($v['sell']) }}</td>
				<td>{{ number_format($v['income']) }}</td>
				<td>{{ number_format($v['return']) }}</td>
				<td>{{ number_format($v['expense']) }}</td>
			</tr>
		@endforeach
		</table>
	</div>
</div>
<? $i++; ?>
@endforeach

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
$('.datepick').datepicker();
<? $i = 0; ?>
@foreach($chart as $date => $val)

$("#pie{{ $i }}").attr('width',$('#pie').parent().width());
var data = [
			{
				value : {{ $val['data']['A']['value'] }},
				color : "{{ $val['data']['A']['color'] }}"
			},
			{
				value : {{ $val['data']['B']['value'] }},
				color : "{{ $val['data']['B']['color'] }}"
			},
			{
				value : {{ $val['data']['C']['value'] }},
				color : "{{ $val['data']['C']['color'] }}"
			},
			{
				value : {{ $val['data']['D']['value'] }},
				color : "{{ $val['data']['D']['color'] }}"
			},
		];

var ctx = $("#pie{{ $i }}").get(0).getContext("2d");
var myNewChart = new Chart(ctx).Pie(data);
<? $i++; ?>
@endforeach
});
/*]]>*/
</script>
@stop