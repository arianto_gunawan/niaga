<div class="row">
{{ Form::open(array('url' => URL::current(), 'method' => 'GET')) }}
	<div class="col-xs-6 col-md-2">
		<input type="text" name="from" value="{{ Input::get('from',null) }}" placeholder="From" class="datepick form-control" data-date-format="mm/yyyy" data-date-viewmode="months" data-date-minviewmode="months"/>
	</div>
	<div class="col-xs-6 col-md-2">
		<input type="text" name="to" value="{{ Input::get('to',null) }}" placeholder="To" class="datepick form-control" data-date-format="mm/yyyy" data-date-viewmode="months" data-date-minviewmode="months"/>
	</div>
	<div class="col-xs-6 col-md-2">{{ Form::select('class',array('0'=>'-','A'=>'A','B'=>'B','C'=>'C','D'=>'D','E'=>'E'),Input::get('class'),array('class' => 'form-control')) }}</div>
	<div class="col-xs-6 col-md-2">{{ Form::select('type',$types,Input::get('type'),array('class' => 'form-control')) }}</div>
	<div class="col-xs-6 col-md-2">{{ Form::submit('Search',array('class' => 'btn')) }}</div>
{{ Form::close() }}
</div>

@if(isset($list))
<table class="table table-hover table-striped">
	<thead><tr><th>{{ HTML::sort('date','MM/YYYY') }}</th><th>Name</th><th>{{ HTML::sort('class') }}</th><th>{{ HTML::sort('rating') }}</th><th>{{ HTML::sort('sell') }}</th><th>{{ HTML::sort('income') }}</th><th>{{ HTML::sort('return') }}</th><th>Expense</th><th>Buy</th><th>Move</th><th>Use</th></tr></thead>
	<tbody>
	@foreach($list as $t)
	<tr>
		<td>{{ Dater::displayMonthYear($t->date) }}</td>
		<td>{{ $t->customer->getDetailLink() }}</td>
		<td>{{ $t->class }}</td>
		<td>{{ $t->rating }}</td>
		<td>{{ number_format($t->sell) }}</td>
		<td>{{ number_format($t->income) }}</td>
		<td>{{ number_format($t->return) }}</td>
		<td>{{ number_format($t->expense) }}</td>
		<td>{{ number_format($t->buy) }}</td>
		<td>{{ number_format($t->move) }}</td>
		<td>{{ number_format($t->use) }}</td>
	</tr>
	@endforeach
	</tbody>
</table>
{{ $list->appends(array('from'=> Input::get('from'), 'to'=> Input::get('to'), 'type'=> Input::get('type'), 'class'=> Input::get('class'),'sort' => Input::get('sort')))->links() }}
@endif

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('.datepick').datepicker();
});
/*]]>*/
</script>
@stop