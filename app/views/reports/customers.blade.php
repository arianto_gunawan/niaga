<?
use App\Models\Customer;
$total = count($this_month[Customer::TYPE_CUSTOMER]) + count($this_month[Customer::TYPE_RESELLER]);
$i = 1;
$cust = array('sell' => 0, 'income' => 0, 'return' => 0);
$reseller = array('sell' => 0, 'income' => 0, 'return' => 0);
?>

<div class="row">
{{ Form::open(array('url' => URL::current(), 'method' => 'GET')) }}
	<div class="col-xs-6 col-md-4">
		<input type="text" name="month" value="{{ Input::get('month',null) }}" placeholder="Month" class="datepick form-control" data-date-format="mm/yyyy" data-date-viewmode="months" data-date-minviewmode="months"/>
	</div>
	<div class="col-xs-6 col-md-2">{{ Form::submit('Search',array('class' => 'btn')) }}</div>
{{ Form::close() }}
</div>

@if($total > 0)
<div class="row">
	<div class="col-xs-12" id="pie1" style="height: 500px;">&nbsp;</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<table class="table table-striped table-bordered table-hover">
		<thead><tr><th>Type</th><th>Sales</th><th>Return</th><th>Income</th></tr></thead>
		<tbody>
		<tr><td colspan="4"><strong>Customer</strong></td></tr>
@foreach($this_month[Customer::TYPE_CUSTOMER] as $cat => $m)
<?
$cust['sell'] = $cust['sell'] + $m->total_sell;
$cust['return'] = $cust['return'] + $m->total_return;
$cust['income'] = $cust['income'] + $m->total_income;
?>
		<tr>
		<td>{{ Customer::cats($m->category) }}</td>
		<td>{{ number_format($m->total_sell) }}</td>
		<td>{{ number_format($m->total_return) }}</td>
		<td>{{ number_format($m->total_income) }}</td>
		</tr>
@endforeach
		<tr><th>Total</th><th>{{ number_format($cust['sell']) }}</th><th>{{ number_format($cust['return']) }}</th><th>{{ number_format($cust['income']) }}</th></tr>
		<tr><td colspan="4"><strong>Reseller</strong></td></tr>
@foreach($this_month[Customer::TYPE_RESELLER] as $cat => $m)
<?
$reseller['sell'] = $reseller['sell'] + $m->total_sell;
$reseller['return'] = $reseller['return'] + $m->total_return;
$reseller['income'] = $reseller['income'] + $m->total_income;
?>
		<tr>
		<td>{{ Customer::rcats($m->category) }}</td>
		<td>{{ number_format($m->total_sell) }}</td>
		<td>{{ number_format($m->total_return) }}</td>
		<td>{{ number_format($m->total_income) }}</td>
		</tr>
@endforeach
		<tr><th>Total</th><th>{{ number_format($reseller['sell']) }}</th><th>{{ number_format($reseller['return']) }}</th><th>{{ number_format($reseller['income']) }}</th></tr>
		<tr><th>Grand Total</th><th>{{ number_format($reseller['sell'] + $cust['sell']) }}</th><th>{{ number_format($reseller['return'] + $cust['return']) }}</th><th>{{ number_format($reseller['income'] + $cust['return']) }}</th></tr>
		</tbody>
		</table>
	</div><!-- col -->
</div>
@else
<div class="row">
	<div class="col-xs-12"><p>no stats</p></div>
</div>
@endif

@section('script')
@if($total > 0)
<script type="text/javascript">

$(document).ready(function(){
$('.datepick').datepicker();

google.setOnLoadCallback(drawChart);
function drawChart() {
  var data = google.visualization.arrayToDataTable([
	['Type', 'Total'],
@foreach($this_month[Customer::TYPE_CUSTOMER] as $m)
    [ '{{ Customer::cats($m->category) }}', {{ $m->total_sell - $m->total_return }} ],
	<? $i++; ?>
@endforeach
@foreach($this_month[Customer::TYPE_RESELLER] as $m)
    [ '{{ Customer::rcats($m->category) }}', {{ $m->total_sell - $m->total_return }} ] <? if($i+1 <= $total) echo ','; ?>
	<? $i++; ?>
@endforeach
]);

var options = {
	title: 'Customer Revenue'
};

var chart = new google.visualization.PieChart(document.getElementById('pie1'));
chart.draw(data, options);
}

});
</script>
@endif
@stop