<?
use App\Models\Customer;
?>
{{ $errors->first('error','<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>:message</div>') }}

<form autocomplete="off" ng-controller="geoController" name="aForm" ng-submit="submit(aForm.$valid)" novalidate>
<alert type="danger" ng-repeat="error in errors">@{{ error.msg }}</alert>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="row">
<div class="col-xs-12">
<div map-lazy-load="https://maps.google.com/maps/api/js">
  <map center="41,-87" zoom="6"></map>
</div>
</div>
</div>
<hr/>
<div>
  <select class="form-control" ng-model="customerType" ng-options="customerType.name for customerType in customerTypes" ng-change="changeCustomer()"></select>
</div>
<hr/>
<table st-table="tableData" st-safe-src="provinceData" class="table table-striped">
	<thead>
	<tr>
		<th st-sort="name">Province</th>
		<th st-sort="total_customers">Total</th>
		<th st-sort="total_sell">Sell</th>
		<th st-sort="total_return">Return</th>
		<th st-sort="revenue">Revenue</th>
		<th st-sort="total_cash_in">Cash In</th>
	</tr>
	</thead>
	<tbody>
	<tr ng-repeat="row in tableData">
		<td><a href="#" ng-click="loadCities(row.id)">@{{ row.name }}</a></td>
		<td>@{{ row.total_customers }}</td>
		<td>@{{ row.total_sell | number: 2 }}</td>
		<td>@{{ row.total_return | number: 2 }}</td>
		<td>@{{ row.revenue | number: 2 }}
		<td>@{{ row.total_cash_in | number: 2 }}</td>
	</tr>
	</tbody>
	<tfoot>
	<tr>
		<th>Total</th>
		<th>@{{ grandTotal.customers | number: 2 }}</th>
		<th>@{{ grandTotal.sell | number: 2 }}</th>
		<th>@{{ grandTotal.return | number: 2 }}</th>
		<th>@{{ grandTotal.revenue | number: 2 }}</th>
		<th>@{{ grandTotal.cashIn | number: 2 }}</th>
	</tr>
	</tfoot>
</table>

</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	provinceData: {{ json_encode($provinceData) }},
	provinces: {{ json_encode($provinces) }},
	customerTypes: {{ json_encode($customerTypes) }},
	defaults: {{ json_encode($defaultData) }},
	geoDataUrl: "{{ $defaults['geoData_url'] }}"
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/inits/transaction.js') }}"></script>
<script src="{{ asset('js/aria/services/geoDataService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/geoController.js') }}"></script>
<script src="{{ asset('js/aria/modules.js') }}"></script>
@stop