<div ng-controller="profitLossController as plc">

<form class="ph" id="customerListForm" name="customerListForm" autocomplete="off">
<div class="row">
<div class="col s2">
  <label>Month</label>
  <select ng-model="plc.dates.month" class="browser-default" ng-options="month as month for month in plc.months"></select>
</div>
<div class="col s2">
  <label>Year</label>
  <select ng-model="plc.dates.year" class="browser-default" ng-options="year as year for year in plc.years"></select>
</div>
<div class="input-field col s2">
  <a class="waves-effect waves-light btn" ng-click="plc.submit()">Load</a>
</div>
</div>
</form>
<div ng-show="plc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="table-responsive-vertical">
<table class="table table-striped table-hover">
<thead><tr><th>&nbsp;</th><th>This Month</th><th>vs @{{ plc.getMY() }}</th></tr></thead>
<tbody>
	<tr>
		<td>Sales</td>
		<td data-title="This Month">@{{ plc.pl.report.revenue | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.revenue | number: 2 }}</td>
	</tr>
	<tr>
		<td>Return</td>
		<td data-title="This Month">@{{ plc.pl.report.return | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.return | number: 2 }}</td>
	</tr>
	<tr>
		<td class="text-right">Total</td>
		<td data-title="This Month">@{{ plc.pl.report.total_revenue | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.total_revenue | number: 2 }}</td>
	</tr>

	<tr>
		<td>Bahan Baku</td>
		<td data-title="This Month">@{{ plc.pl.report.use | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.use | number: 2 }}</td>
	</tr>
	<tr>
		<td>COGS</td>
		<td data-title="This Month">@{{ plc.pl.report.cogs | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.cogs | number: 2 }}</td>
	</tr>
	<tr>
		<td class="text-right">Gross Profit</td>
		<td data-title="This Month">@{{ plc.pl.report.gross_profit | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.gross_profit | number: 2 }}</td>
	</tr>

	<tr ng-repeat="ops in plc.ops">
		<td>@{{ ops.name }}</td>
		<td data-title="This Month">@{{ plc.getOps(ops.id) | number : 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.getCompareOps(ops.id) | number : 2 }}</td>
	</tr>

	<tr>
		<td>Total Operational</td>
		<td data-title="This Month">@{{ plc.pl.report.total_operational | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.total_operational | number: 2 }}</td>
	</tr>
	<tr>
		<td class="text-right">EBITDA</td>
		<td data-title="This Month">@{{ plc.pl.report.ebitda | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.ebitda | number: 2 }}</td>
	</tr>
	<tr>
		<td>Penyusutan</td>
		<td data-title="This Month">@{{ plc.pl.report.depreciation | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.depreciation | number: 2 }}</td>
	</tr>
	<tr>
		<td>Adjustment</td>
		<td data-title="This Month">@{{ plc.pl.report.adjustment | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.adjustment | number: 2 }}</td>
	</tr>
	<tr>
		<td class="text-right">Nett</td>
		<td data-title="This Month">@{{ plc.pl.report.nett | number: 2 }}</td>
		<td data-title="@{{ plc.getMY() }}">@{{ plc.compare.report.nett | number: 2 }}</td>
	</tr>

</tbody>
</table>
</div>
</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	pl: {{ json_encode($pl) }},
	compare: {{ json_encode($compare) }},
	operations: {{ json_encode($operations) }},
	compareDate: {{ json_encode($compareDate) }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/reports/profitLossController.js') }}"></script>
@stop