<table class="table table-striped table-hover table-bordered">
<thead>
<tr>
	<th>Code</th><th>Price</th><th>Quantity</th><th>Total Amount</th><th>Average Price</th>
</tr>
</thead>
<tbody>
@foreach($data as $d)
	<tr>
		<td>{{ $d->itemName }}</td>
		<td>{{ $d->detailPrice }}</td>
		<td>{{ $d->total_items }}</td>
		<td>{{ $d->total_amount }}</td>
		<td>{{ bcdiv($d->total_amount, $d->total_items,2) }}</td>
	</tr>
@endforeach
</tbody>
</table>