<?
use App\Models\Customer;
?>
<div ng-controller="customerListController as tlc">
<form id="customerListForm" name="customerListForm" autocomplete="off" ng-submit="tlc.submit()">
<div class="row">
<div class="col s4">
	<label>Name</label>
	<input type="text" ng-model="tlc.name" ng-change="tlc.resetPage()"/>
</div>
<div class="col s4">
<div class="switch">
  <label>
    <input type="checkbox" ng-model="tlc.deleted">
    <span class="lever"></span>
    Show Deleted
  </label>
</div>
</div>
<div class="col s4">
	<input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>
</form>

<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>

<div class="table-responsive-vertical shadow-z-1">
<table st-table="tlc.customers" class="table table-striped table-bordered table-hover">
	<thead><tr><th>Name</th>
	<th>Sell</th>
	<th>Return</th>
	<th>Discount</th>
	<th>Balance</th>
	<th>Location</th>
	<th>Actions</th>
	</tr>
	</thead>
	<tbody>
	<tr ng-repeat="customer in tlc.customers">
		<td data-title="Name"><a href="@{{ customer.detail_link }} ">@{{ customer.name }}</a></td>
		<td data-title="Sell">@{{ customer.sell | number: 2 }}</td>
		<td data-title="Return">@{{ customer.return | number: 2 }}</td>
		<td data-title="Discount">@{{ customer.discount }}&#37;</td>
		<td data-title="Balance">@{{ customer.balance | number: 2 }}</td>
		<td data-title="Location">
		<span ng-repeat="location in customer.locations">
			<a href="@{{ location.link }}">@{{ location.name }}</a>&nbsp;
		</span>
		</td>
		<td data-title="Action">
			<a class="btn" href="@{{ customer.edit_link }}">edit</a>
		</td>
	</tr>
	</tbody>
</table>
</div>
<div class="row">
<div class="center-align">
<a ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>

</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	paginator: {{ json_encode($customers) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/transaction.js') }}"></script>
<script src="{{ asset('js/aria/services/customerService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/customer/customerListController.js') }}"></script>
@stop