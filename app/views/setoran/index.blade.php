<div ng-controller="setoranListController as tlc">
<form id="produksiListForm" name="produksiListForm" autocomplete="off" ng-submit="tlc.submit()">
<div class="row">
<div class="col s3 m2">
  <label>From</label>
  <input type="text" ng-model="tlc.from" class="dateform" ng-change="tlc.resetPage()"/>
</div>
<div class="col s3 m2">
  <label>To</label>
  <input type="text" ng-model="tlc.to" class="dateform" ng-change="tlc.resetPage()"/>
</div>
<div class="col s3 m2">
  <label>Status</label>
  <select class="browser-default" ng-model="tlc.status" ng-change="tlc.resetPage()" ng-options="type.name for type in tlc.statuses"></select>
</div>
<div class="col s3 m2 input-field">
	<input ng-click="tlc.submit()" type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>
<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>

<div id="scrollTo">
<table id="setoran" class="bordered striped highlight small-table centered">
	<thead><tr>
		<th>Serial</th><th>Kode</th><th>Potong</th><th>Jumlah</th><th>Size</th><th>Warna</th><th>Customer</th>
		<th>Jahit</th><th>Invoice</th><th>Action</th>
	</tr></thead>
	<tbody>
	<tr>
		<td><input type="text" ng-model="tlc.serial"></td>
		<td><input type="text" ng-model="tlc.kode"></td>
		<td>
		  <select class="browser-default" ng-model="tlc.potong" ng-change="tlc.resetPage()" ng-options="potong.name for potong in tlc.potongs"></select>
		</td>
		<td>&nbsp;</td><td>&nbsp;</td>
		<td><input type="text" ng-model="tlc.warna"></td>
		<td><input type="text" ng-model="tlc.customer"></td>
		<td><select class="browser-default" ng-model="tlc.jahit" ng-options="j.name for j in tlc.jahits" required></select></td>
		<td><input type="text" ng-model="tlc.invoice"></td>
		<td><a ng-click="tlc.submit()" class="btn-floating btn-large waves-effect waves-light blue"><i class="material-icons">search</i></a></td>
	</tr>
	<tr ng-repeat="p in tlc.setoran track by p.id" ng-class="tlc.getClass(p.status)">
		<td><a href="@{{ p.edit_link }}">@{{ tlc.printSerial($index) }}</a></td>
		<td>
@if($can_edit_item)
			<input ng-show="tlc.itemInput($index)" type="text" style="width:135px;font-size: 14px;" typeahead-min-length="3" typeahead-on-select="tlc.onSelectItem($model, $index)" ng-model="p.item_form" uib-typeahead="item as item.name for item in tlc.getItem($viewValue)">
			<a type="submit" ng-show="tlc.itemInput($index)" value="Save" ng-disabled="!tlc.isValid($index)" class="waves-effect waves-light btn" ng-click="tlc.saveItem($index)" ng-disabled="tlc.rowDisabled($index)">Save</a>
			<span ng-show="!tlc.itemInput($index)">@{{ tlc.displayItemName($index) }}</span><a ng-show="tlc.canEditItem($index)" ng-click="tlc.toggleItemInput($index)" href="#"><i class="material-icons">create</i></a>
@else
			<span>@{{ tlc.displayItemName($index) }}</span>
@endif
		</td>
		<td>@{{ p.potong_date }}<hr/><b>@{{ p.potong.name }}</b></td>
		<td>@{{ p.quantity }}</td>
		<td>@{{ p.size.name }}</td>
		<td>@{{ p.warna }}</td>
		<td>@{{ p.customer }}</td>
		<td>@{{ p.jahit_date }}<hr/><b>@{{ p.jahit.name }}</b></td>
		<td ng-show="!tlc.gudangInput($index)"><input type="text" ng-model="p.invoice_form"></td>
		<td ng-show="tlc.gudangInput($index)"><a href="@{{ p.transaction_link }}" target="_blank">@{{ p.invoice }}</a></td>
		<td>
@if($can_edit_item)
			<a type="submit" value="Gudang" ng-disabled="!tlc.isValidGudang($index)" class="waves-effect waves-light blue btn" ng-click="tlc.gudangRow($index)" ng-disabled="tlc.rowDisabled($index)">Gudang</a>
@endif
@if($can_delete)
			<a type="submit" value="Delete" class="waves-effect waves-light red btn" ng-click="tlc.openDeleteModal($index)" ng-disabled="tlc.showProgress">Delete</a>
@endif
		</td>
	</tr>
	<tr>
		<td colspan="3"><h5>Total</h5></td>
		<td><h5>@{{ tlc.getTotal() }}</h5></td>
		<td colspan="6">&nbsp;</td>
	</tr>
	</tbody>
</table>
</div>
<br/>
<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="center-align">
<a ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>

<div id="deleteModal" class="modal">
  <div class="modal-content">
    <h4>Are you sure??</h4>
    <p>Do you want to delete @{{ tlc.delete_serial }}?</p>
    <p>Delete Checklist</p>
    <ul>
    	<li>- Hapus quantity dari warehouse</li>
    	<li>- TTD kitir produksi</li>
    </ul>
  </div>
  <div class="modal-footer">
    <a href="#!" ng-click="tlc.closeModal()" class="waves-effect red btn">Cancel!</a>
    <a href="#!" ng-click="tlc.delete()" class="waves-effect green btn">Sure</a>
  </div>
</div>
</div>
</form>
</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	gudangURL: "{{ URL::action('SetoranController@postGudang') }}",
	saveURL: "{{ URL::action('SetoranController@postEditItem') }}",
	deleteURL: "{{ URL::action('SetoranController@postDelete') }}",
	itemsUrl: "{{ $defaults['item_url'] }}",
	potongs: {{ json_encode($potong) }},
	jahits: {{ json_encode($jahit) }},
	paginator: {{ json_encode($produksi) }},
	statuses: {{ $statusJSON }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/services/itemService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/setoran/setoranListController.js') }}"></script>
@stop