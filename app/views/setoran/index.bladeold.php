<? $total = 0; ?>
@if(isset($produksi))
{{ Form::open(array('url' => URL::action('PrintController@getProduksi'), 'method' => 'GET')) }}
{{ Form::hidden('type','arsip') }}
{{ Form::hidden('warna',Input::get('warna')) }}
{{ Form::hidden('customer',Input::get('customer')) }}
{{ Form::hidden('jahit',Input::get('jahit')) }}
{{ Form::hidden('size',Input::get('size')) }}
{{ Form::hidden('status',Input::get('status')) }}
{{ Form::hidden('potong',Input::get('potong')) }}
{{ Form::hidden('sort',Input::get('sort')) }}
{{ Form::hidden('dir',Input::get('dir')) }}
{{ Form::hidden('from',Input::get('from')) }}
{{ Form::hidden('to',Input::get('to')) }}
{{ Form::hidden('page',Input::get('page')) }}
{{ Form::hidden('invoice',Input::get('invoice')) }}
{{ Form::hidden('id',Input::get('id')) }}
<button type="submit" class="btn btn-primary">Print</button>
{{ Form::close() }}
<hr/>
<div class="row">
{{ Form::open(array('url' => URL::current(), 'method' => 'GET', 'autocomplete' => 'off')) }}
<div class="col-xs-6 col-md-3"><input type="text" name="from" class="datepick form-control" value="{{ Input::get('from',null) }}" placeholder="From"></div>
<div class="col-xs-6 col-md-3"><input type="text" name="to" class="datepick form-control" value="{{ Input::get('to',null) }}" placeholder="To"></div>
<div class="col-xs-6 col-md-3">{{ Form::select('status',$status,Input::get('status'),array('class' => 'form-control')) }}</div>
<div class="col-xs-6 col-md-3"><button type="submit" class="btn btn-primary">Search</button></div>
</div>
<hr/>
<table class="table table-striped table-bordered">
	<thead><tr>
		<th>Kitir</th>
		<th>{{ HTML::sort('potong_in','Tgl Potong') }}</th>
		<th>Potong</th>
		<th>J Potong</th>
		<th>Barang</th>
		<th>Size</th>
		<th>Warna</th>
		<th>Cust</th>
		<th>Group</th>
		<th>Keluar</th>
		<th>Invoice</th>
		<th>{{ HTML::sort('jahit_out','Ke Jahit') }}</th>
		<th>{{ HTML::sort('jahit_in','Dari Jahit') }}</th>
		<th>Keterangan</th>
		<th>{{ HTML::sort('permak_out','Ke Permak') }}</th>
		<th>{{ HTML::sort('permak_in','Dari Permak') }}</th>
	</tr></thead>
	<tbody>
	<tr>
		<td>{{ Form::text('id',Input::get('id')) }}</td>
		<td>&nbsp;</td>
		<td>{{ Form::select('potong',$potong,Input::get('potong')) }}</td>
		<td>&nbsp;</td>
		<td>{{ Form::text('kode',Input::get('kode')) }}</td>
		<td>{{ Form::select('size',$size,Input::get('size')) }}</td>
		<td>{{ Form::text('warna',Input::get('warna')) }}</td>
		<td>{{ Form::text('customer',Input::get('customer')) }}</td>
		<td>{{ Form::select('jahit',$jahit,Input::get('jahit')) }}</td>
		<td>&nbsp;</td>
		<td>{{ Form::text('invoice',Input::get('invoice')) }}</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
{{ Form::close() }}
{{ Form::open(array('url' => URL::action('SetoranController@postUpdate'), 'method' => 'POST', 'autocomplete' => 'off')) }}
	@foreach($produksi as $t)
<? $total += $t->quantity; ?>
	<tr class="{{ $t->printClass() }}">
		<td>{{ $t->serial() }} / {{ $t->id }}</td>
		<td>{{ HTML::link(URL::action('SetoranController@getEdit',array($t->id)),Dater::display($t->potong_in)) }}</td>
		<td>{{ $t->potong? $t->potong->name : '' }}</td>
		<td>{{ $t->quantity }}</td>
		<td>
@if($t->item_id)
			{{ $t->item? $t->item->code : ''}}
@else
			{{ $t->temp_name }}
@endif
		</td>
		<td>{{ $t->size?$t->size->code:'' }}</td>
		<td>{{ $t->warna }}</td>
		<td>{{ $t->customer }}</td>
		<td>{{ $t->jahit }}</td>
		<td>{{ number_format($t->q_out) }}</td>
@if($t->invoice)
		<td>{{ $t->invoice }}</td>
@else
		<td>{{ Form::text("produksi[$t->id][invoice]","") }}</td>
@endif
		<td>{{ Dater::display($t->jahit_out) }}</td>
		<td>{{ Dater::display($t->jahit_in) }}</td>
		<td>{{ $t->description }}</td>
		<td>
@if(!$t->permak_out)
			{{ Form::text("produksi[$t->id][permak_out]","",array('class'=> 'datepick')) }}
@else
			{{ Dater::display($t->permak_out) }}
@endif
		</td>
		<td>
@if(!$t->permak_in)
			{{ Form::text("produksi[$t->id][permak_in]","",array('class'=> 'datepick')) }}
@else
			{{ Dater::display($t->permak_in) }}
@endif
		</td>
	</tr>
	@endforeach
	<tr><td colspan="10">Total {{ number_format($total) }}</td></tr>
	</tbody>
</table>

<button type="submit" class="btn btn-primary">Save</button>
{{ $produksi->appends(array('id'=>Input::get('id'), 'invoice'=>Input::get('invoice'), 'size'=>Input::get('size'), 'status' => Input::get('status'), 'kode' => Input::get('kode'), 'warna' => Input::get('warna'), 'customer' => Input::get('customer'), 'jahit' => Input::get('jahit'), 'potong' => Input::get('potong'), 'sort' => Input::get('sort'), 'dir' => Input::get('dir'), 'from' => Input::get('from'), 'to' => Input::get('to')))->links() }}
{{ Form::close() }}
@endif

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$(".datepick").datepicker();
});
/*]]>*/
</script>
@stop