<form autocomplete="off" ng-controller="settingsController as cc" name="aForm" ng-submit="cc.submit(aForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<label>Tutup Buku</label>
<select required ng-model="cc.settings.tutup_buku" ng-options="date as date for date in cc.tutupBuku"></select>

<div class="input-field">
<label>Account for 100% Discount</label>
<input required type="text" typeahead-min-length="3" ng-model="cc.sell" uib-typeahead="a as a.name for a in cc.getAccount($viewValue)">
</div>

<div class="input-field">
<label>Account for Ongkir</label>
<input required type="text" typeahead-min-length="3" ng-model="cc.ongkir" uib-typeahead="a as a.name for a in cc.getAccount($viewValue)">
</div>

<div ng-show="cc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="cc.submit(aForm.$valid)" ng-disabled="cc.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>

</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	customersUrl: "{{ $defaults['customer_url'] }}",
	settings: {{ json_encode($settings) }},
	sell: {{ json_encode($sell) }},
	ongkir: {{ json_encode($ongkir) }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/services/customerService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/settings/settingsController.js') }}"></script>
@stop