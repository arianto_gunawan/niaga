{{ Form::open(array('url' => URL::current(), 'method' => 'POST')) }}
<h5>Edit Warna dan Customer</h5>
<table class="bordered striped highlight">
<tbody>
<tr>
	<th>Kitir</th><th>{{ $produksi['serial'] }}</th>
</tr>
@if(!empty($produksi['original_id']))
<tr>
	<th>Kitir Asli</th><th>{{ $produksi['original_serial'] }}</th>
</tr>
@endif
<tr>
	<th>Kode</th><th>{{ $produksi['temp_name'] }}</th>
</tr>
<tr>
	<th>Jumlah Potong</th><th>{{ $produksi['quantity'] }}</th>
</tr>
<tr>
	<th>Potong</th><th>{{ $produksi['potong']['name'] }}</th>
</tr>
<tr>
	<th>Tanggal Potong</th><th>{{ $produksi['potong_date'] }}</th>
</tr>
<tr>
	<th>Size</th><th>{{ $produksi['size']['name'] }}</th>
</tr>
<tr>
	<th>Warna</th>
	<th><div class="input-field"><input type="text" name="warna" value="{{ $produksi['warna'] }}"></div></th>
</tr>
<tr>
	<th>Customer</th>
	<th><div class="input-field"><input type="text" name="customer" value="{{ $produksi['customer'] }}"></div></th>
</tr>
<tr>
	<th>Tanggal Jahit</th><th>{{ $produksi['jahit_date'] }}</th>
</tr>
<tr>
	<th>Penjahit</th><th>{{ $produksi['jahit']['name'] }}</th>
</tr>
<tr>
	<th>&nbsp;</th>
	<th><input class="btn" type="submit" value="Save"></th>
</tr>
</tbody>
</table>
{{ Form::close() }}

@if($pisah_jahit && $can_pisah_jahit)
{{ Form::open(array('url' => URL::action('ProduksiController@postPisahJahit',array($produksi['id'])), 'method' => 'POST')) }}
<hr/>
<h5>Pisah Jahit</h5>
<table class="bordered striped highlight">
<tbody>
<tr>
	<th>Pisah Jahit</th>
	<th><div class="input-field"><input type="text" name="split_q"></div></th>
</tr>
<tr>
	<th>&nbsp;</th>
	<th><input class="btn" type="submit" value="Save"></th>
</tr>
</tbody>
</table>
{{ Form::close() }}
@endif

@if($ganti_jahit && $can_ganti_jahit)
<hr/>
<h5>Edit Jahit</h5>
{{ Form::open(array('url' => URL::action('ProduksiController@postGantiJahit',array($produksi['id'])), 'method' => 'POST')) }}
<table class="bordered striped highlight">
<tbody>
<tr>
	<th>Edit Jahit</th>
	<th>
		<select name="jahit_id" class="browser-default">
			<option value="" selected disabled hidden>Choose here</option>
			@foreach($jahit as $j)
				<option value="{{ $j->id }}">{{ $j->name }}</option>
			@endforeach
		</select>
	</th>
</tr>
<tr>
	<th>&nbsp;</th>
	<th><input class="btn" type="submit" value="Save"></th>
</tr>
</tbody>
</table>
{{ Form::close() }}
@endif