<form autocomplete="off" ng-controller="spgFormController as cc" name="tForm" ng-submit="submit(tForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="row">
<div class="input-field">
<label>Nama SPG</label>
<input type="text" ng-model="cc.name" />
</div>
</div>

<div class="row">
<div class="input-field">
<label>UserName</label>
<input type="text" ng-model="cc.username" />
</div>
</div>

<div class="row">
<label>Store</label>
<select class="browser-default" ng-model="cc.store" ng-options="a.name for a in cc.stores"></select>
</div>

@if($update)
<p>
<input id="updatepass" type="checkbox" ng-model="cc.update_password" >
<label for="updatepass">Update Password? @{{ cc.update_password ? 'Yes' : 'No' }}</label>
</p>
@endif

<div ng-show="cc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="cc.submit(tForm.$valid)" ng-disabled="cc.disableSubmit || tForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	stores: {{ json_encode($stores) }},
	submitURL: "{{ URL::current() }}",
	spg: {{ json_encode($spg) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/spg/spgFormController.js') }}"></script>
@stop