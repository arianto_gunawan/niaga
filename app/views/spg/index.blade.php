<div ng-controller="spgListController as tlc">
<form id="spgListForm" name="spgListForm" autocomplete="off" ng-submit="tlc.submit()">
<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div id="scrollTo">
<table class="bordered striped highlight small-table centered" id="spg">
	<thead><tr>
		<th>Nama</th><th>Username</th><th>Store</th><th>Action</th>
	</tr></thead>
	<tbody>
	<tr ng-repeat="p in tlc.spg track by p.id">
		<td>@{{ p.name }}</td>
		<td>@{{ p.username }}</td>
		<td><a href="@{{ p.warehouse_link }}">@{{ p.warehouse_name }}</a></td>
		<td>
			<a type="submit" value="Edit" class="waves-effect waves-light btn" a href="@{{ p.edit_link }}">Edit</a>
			<a type="submit" value="Setor" ng-show="tlc.isValidSetor($index)" class="waves-effect waves-light btn red" ng-click="tlc.setorRow($index)" ng-disabled="tlc.rowSetorDisabled($index)">Setor</a>
		</td>
	</tr>
	</tbody>
</table>
</div>
<br/>
<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="center-align">
<a ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>
</form>
</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	assignURL: "{{ URL::action('SpgController@postAssign') }}",
	paginator: {{ json_encode($spg) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/spg/spgListController.js') }}"></script>
@stop