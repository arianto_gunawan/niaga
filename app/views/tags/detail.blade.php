<div ng-controller="tagDetailController as tdc">

<table class="invoice ph">
	<tr><th>Type</th><td>{{ $tag->printType() }}</td></tr>
	<tr><th>Code</th><td>{{ $tag->code }}</td></tr>
	<tr><th colspan="2">
	<div class="switch">
	  <label>
	    <input type="checkbox" ng-model="tdc.showImages" ng-change="tdc.toggleImages()">
	    <span class="lever"></span>
	    <i class="material-icons">image</i>
	  </label>
	</div>
	</th></tr>
</table>

<div class="table-responsive-vertical shadow-z-1">
<table class="table table-striped table-bordered table-hover">
	<thead><tr><th ng-show="tdc.showImages">Image</th><th>Code</th><th>Name</th><th>Price</th><th>Actions</th></tr></thead>
	<tbody>
	<tr ng-repeat="item in tdc.items">
		<td data-title="Image" ng-show="tdc.showImages"><img src="{{ asset('img/noimg.jpg') }}" class="b-lazy" data-blazy="@{{ item.image }}" width="200"/></td>
		<td data-title="Code"><a href="@{{ item.detail_link }}">@{{ item.code }}</td>
		<td data-title="Name">@{{ item.name }}</td>
		<td data-title="Price">@{{ item.price }}</td>
		<td data-title="Action">
			<a class="btn" href="@{{ item.edit_link }}">Edit</a>
		</td>
	</tr>
	</tbody>
</table>
</div>

<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="center-align">
<a ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>

</div>


@section('script')
<script type="text/javascript">
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	paginator: {{ json_encode($items) }}
}))
</script>
<script src="{{ asset('js/aria/controllers/tag/tagDetailController.js') }}"></script>
@stop