<form autocomplete="off" ng-controller="tagFormController as ifc" name="aForm" ng-submit="submit(aForm.$valid)" novalidate>

<label>Name</label>
<input class="required" ng-model="ifc.tag.name" required />

<label>Type</label>
<select class="browser-default" ng-model="ifc.role" ng-options="a.name for a in ifc.types"></select>

<label>Code</label>
<input class="required" ng-model="ifc.tag.code" required />

<div class="input-field">
<div ng-show="ifc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="ifc.submit(aForm.$valid)" ng-disabled="ifc.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</div>

</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	types: {{ $typesJSON }},
	tag: {{ json_encode($tag) }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/tag/tagFormController.js') }}"></script>
@stop
