<div ng-controller="tagsListController as tlc">

<form class="ph" id="tagsListForm" name="tagsListForm" autocomplete="off" method="GET" action="{{ URL::current() }}">
<div class="row">
  <div class="input-field inline">
    <label>Name</label>
    <input ng-model="tlc.name" type="text" ng-change="tlc.resetPage()">
  </div>
  <div class="input-field inline">
    <a class="btn" ng-click="tlc.submit()">Search</a>
  </div>
</div>
</form>

<hr/>

<div class="chip" ng-repeat="tag in tlc.tags">
<a href="@{{ tag.detail_link }}">@{{ tag.name }}</a>
</div>

<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="center-align">
<a ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>
</div>
@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	paginator: {{ json_encode($tags) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/tag/tagsListController.js') }}"></script>
@stop