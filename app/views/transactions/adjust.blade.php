<form autocomplete="off" name="aForm" ng-controller="adjustController as ac" ng-submit="submit(aForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="col s6">
  <label>Date</label>
  <input type="text" ng-model="ac.transaction.date" class="dateform" />
</div>


<label>Invoice</label>
<input ng-model="ac.transaction.invoice" type="text">

<div class="row">
<div class="col s6">
  <label>Credit (+)</label>
  <input focus required type="text" typeahead-min-length="3" ng-model="ac.receiver" uib-typeahead="customer as customer.name for customer in ac.getCustomer($viewValue)">
</div>

<div class="col s6">
<label>Value</label>
<input disabled value="@{{ ac.helper.parseFloat(ac.receiver.balance) + ac.helper.parseFloat(ac.transaction.total) | number: 2 }}">
</div>
</div>

<div class="row">
<div class="col s6">
  <label>Debit (-)</label>
  <input focus required type="text" typeahead-min-length="3" ng-model="ac.sender" uib-typeahead="customer as customer.name for customer in ac.getCustomer($viewValue)">
</div>

<div class="col s6">
<label>Value</label>
<input disabled value="@{{ ac.helper.parseFloat(ac.sender.balance) - ac.helper.parseFloat(ac.transaction.total) | number: 2 }}">
</div>
</div>

<label>Note</label>
<input focus ng-model="ac.transaction.description" type="text">

<label>Total</label>
<input focus ng-model="ac.transaction.total" required type="text">

<div ng-show="ac.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="ac.submit(aForm.$valid)" ng-disabled="ac.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>

</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	customersUrl: "{{ $defaults['customer_url'] }}"
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/transaction.js') }}"></script>
<script src="{{ asset('js/aria/services/customerService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/adjustController.js') }}"></script>
@stop