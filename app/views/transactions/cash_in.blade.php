<form autocomplete="off" ng-controller="cashController as cc" name="tForm" ng-submit="submit(tForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<label>Date</label>
<input type="text" ng-model="cc.date" class="dateform" />

<label>Account</label>
<select ng-model="cc.account" ng-options="a.name for a in cc.accounts"></select>

<div class="table-responsive-vertical shadow-z-1">
<table class="table">
	<tbody>
	<tr ng-repeat="detail in cc.details track by $index">
		<td data-title="Name">
			<label>Name</label>
			<input type="text" focus required typeahead-on-select="cc.onSelect($model, $index)" ng-model="detail.customer" uib-typeahead="customer as customer.name for customer in cc.getCustomer($viewValue)" >
		</td>
		<td data-title="Invoice">
			<label>Invoice</label>
			<input focus ng-model="detail.invoice">
		</td>
		<td data-title="Note">
			<label>Note</label>
			<input focus ng-model="detail.description">
		</td>
		<td data-title="Total">
			<label>Total</label>
			<input focus ng-model="detail.total" add-row="@{{ $index }}" required />
		</td>
		<td data-title="Delete">
			<a href="#" ng-click="cc.removeRow($index)"><i class="material-icons">clear</i></a>
		</td>
	</tr>
	</tbody>
</table>
</div>
<div ng-show="cc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="cc.submit(tForm.$valid)" ng-disabled="cc.disableSubmit || tForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</form>


@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	accounts: {{ $accounts }},
	accountId: {{ $defaultAccount }},
	customersUrl: "{{ $defaults['customer_url'] }}"
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/transaction.js') }}"></script>
<script src="{{ asset('js/aria/services/customerService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/cashController.js') }}"></script>
@stop