<div layout="column" layout-fill ng-controller="cashController as cc" id="appForm">
<form autocomplete="off" name="cForm" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div layout="column" flex="90">
<md-datepicker ng-model="cc.date" md-placeholder="Enter date"></md-datepicker>

<md-input-container class="md-icon-float md-block">
<md-icon aria-label="account icon">attach_money</md-icon>
<label>Account</label>
<md-select ng-model="cc.account" ng-model-options="{trackBy: '$value.id'}">
	<md-option ng-repeat="a in cc.accounts" ng-value="a">
		@{{a.name}}
	</md-option>
</md-select>
</md-input-container>
</div>
<div class="table-responsive-vertical shadow-z-1">
<table class="table">
	<tbody>
	<tr ng-repeat="detail in cc.details track by $index">
		<td data-title="Name">
		<md-autocomplete required focus
		        md-selected-item="cc.details[$index].customer"
		        md-search-text="cc.details[$index].searchText"
		        md-items="item in cc.getCustomer(cc.details[$index].searchText)"
		        md-selected-item-change="cc.selectedItemChange(item)"
		        md-item-text="item.name"
		        md-min-length="2"
		        md-floating-label="Name"
		        md-menu-class="autocomplete-custom-template">
		    <md-item-template>
					<span md-highlight-text="cc.details[$index].searchText" md-highlight-flags="^i" class="item-title">@{{item.name}}</span>
					<span class="item-metadata">
					  <span><strong>@{{item.type}}</strong></span>
					</span>
		    </md-item-template>
				<md-not-found>
				  No matches found.
				</md-not-found>
		</md-autocomplete>

		</td>
		<td data-title="Invoice">
			<md-input-container>
			<label>Invoice</label>
			<input focus ng-model="detail.invoice">
			</md-input-container>
		</td>
		<td data-title="Note">
			<md-input-container>
			<label>Note</label>
			<input focus ng-model="detail.description">
			</md-input-container>
		</td>
		<td data-title="Total">
			<md-input-container>
			<label>Total</label>
			<input focus ng-model="detail.total" add-row="@{{ $index }}" required />
			</md-input-container>
		</td>
		<td data-title="">
			<md-button class="md-icon-button" ng-click="cc.removeRow($index)" aria-label="More" required>
				<md-icon aria-label="clear">clear</md-icon>
			</md-button>
		</td>
	</tr>
	</tbody>
</table>
</div>
<md-button ng-disabled="cc.disableSubmit || cForm.$invalid" class="md-raised md-primary" ng-click="cc.submit(cForm.$valid)">Submit</md-button>
<md-progress-linear ng-show="cc.disableSubmit" md-mode="indeterminate"></md-progress-linear>

</form>
</div>


@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	accounts: {{ $accounts }},
	accountId: {{ $app['default_income_account'] }},
	customersUrl: "{{ $defaults['customer_url'] }}"
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/transaction.js') }}"></script>
<script src="{{ asset('js/aria/services/customerService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/cashController.js') }}"></script>
@stop