<div ng-controller="invoiceController as ic" id="invoice">
<div class="table-responsive-vertical">
<table class="table">
<tr>

<td>
<div class="valign-wrapper">
	<img src="{{ asset('img/logo.png') }}" /><h5 class="valign">INVOICE #{{ $transaction->invoice }}</h5>
</div>
<table class="invoice">
@if($transaction->sender)
  <tr><th>From:</th><td><a href="{{$transaction->sender->getDetailLink()}}" target="_blank">{{ $transaction->sender->name }}</a></td></tr>
@endif
@if($transaction->receiver)
  <tr><th>To:</th><td><a href="{{ $transaction->receiver->getDetailLink() }}" target="_blank">{{ $transaction->receiver->name }}</a></td></tr>
@endif
@if($transaction->description)
  <tr><th>Note:</th><td>{{ nl2br($transaction->description) }}</td></tr>
@endif
</table>
<hr class="sh" />
<div><h5 class="center-align">Total: {{ nf($transaction->total) }}</h5></div>
</td>

<td>
<table class="invoice">
<tbody>
	<tr><th>Date</th><td>{{ $transaction->printDate() }}</td></tr>
	<tr class="ph"><th>Due</th><td>{{ $transaction->printDue() }}</td></tr>
	<tr><th>Invoice Discount</th><td>{{$transaction->discount}}%</td></tr>
	<tr><th>Adjustment</th><td>{{ nf($transaction->adjustment) }}</td></tr>
	<tr><th>Items</th><td>{{ $transaction->total_items }}</td></tr>
@if($transaction->user)
	<tr class="ph"><th>User</th><td>{{ $transaction->user->username }}</td></tr>
@endif
	<tr class="ph"><th>Total Before Disc</th><td>@{{ ic.getTotal() | number: 2 }}</td></tr>
</tbody>
</table>
</td>
</tr></table></div>
<div class="row ph">
<div class="col s6 m2">
<a ng-click="ic.print()" class="btn-floating waves-effect waves-light red"><i class="material-icons">print</i></a>
<a href="{{ URL::current() }}?receipt=1" class="btn-floating waves-effect waves-light red"><i class="material-icons">receipt</i></a>
</div>
@if(!isset($deleted))
@if($delete)
<div class="col s6 m2">
<a class="btn-floating waves-effect waves-light red" ng-click="ic.showModal()">
  <i class="material-icons">not_interested</i>
</a>
</div>
@endif
@endif
<div class="col s4 m2 switch">
  <label>
    <input type="checkbox" ng-model="ic.showImages" ng-change="ic.toggleImages()">
    <span class="lever"></span>
    <i class="material-icons">image</i>
  </label>
</div>
<div class="col s4 m2 switch">
  <label>
    <input type="checkbox" ng-model="ic.showDesc" ng-change="ic.toggleDesc()">
    <span class="lever"></span>
    <i class="material-icons">description</i>
  </label>
</div>
<div class="col s4 m2 switch hide-on-med-and-up">
<label>
  <input type="checkbox" ng-model="ic.horizontal">
  <span class="lever"></span>
  <i class="material-icons">swap_vertical_circle</i>
</label>
</div>
</div>

@if(isset($details))
<div ng-class="ic.horizontal ? '' : 'table-responsive-vertical'">
<table st-table="ic.tableData" class="table table-hover table-striped">
	<thead><tr>
		<th ng-show="ic.showImages">Image</th>
		<th ng-show="ic.showDesc">Barcode</th>
		<th class="ph"><a href="#" ng-click="ic.sortType = 'pcode'; ic.sortReverse = !ic.sortReverse">Code</a></th>
		<th><a href="#" ng-click="ic.sortType = 'name'; ic.sortReverse = !ic.sortReverse">Name</a></th>
		<th ng-show="ic.showDesc">Alias</th>
		<th ng-show="ic.showDesc">Desc</th>
		<th><a href="#" ng-click="ic.sortType = 'quantity'; ic.sortReverse = !ic.sortReverse">Quantity</a></th>
		<th><a href="#" ng-click="ic.sortType = 'price'; ic.sortReverse = !ic.sortReverse">Price</a></th>
		<th>Discount(%)</th>
		<th>Sub-Total</th>
	</tr></thead>
	<tbody>
	<tr ng-repeat="row in ic.tableData | orderBy:ic.sortType:ic.sortReverse">
		<td data-title="Image" ng-show="ic.showImages"><img src="{{ asset('img/noimg.jpg') }}" class="b-lazy" data-blazy="@{{ row.image }}" width="200"/></td>
		<td data-title="Barcode" ng-show="ic.showDesc">@{{ row.id }}</td>
		<td class="ph" data-title="Code"><a href="@{{ row.link }}">@{{ row.code }}</a></td>
		<td data-title="Name">@{{ row.name }}</td>
		<td data-title="Alias" ng-show="ic.showDesc">@{{ row.alias }}</td>
		<td data-title="Description" ng-show="ic.showDesc">@{{ row.description }}</td>
		<td data-title="Quantity">@{{ row.quantity }}</td>
		<td data-title="Price">@{{ row.price | number: 2 }}</td>
		<td data-title="Discount">@{{ row.discount }}</td>
		<td data-title="Sub-Total">@{{ row.total | number: 2 }}</td>
	</tr>
	</tbody>
</table>
<div class="sh">
<hr/>
<table width="100%">
<tr><td>Yang Mengetahui,</td><td>Pemberi,</td><td>Penerima,</td></tr>
</table>
</div>
</div>
@endif

<div class="ph">
<hr/>
<img src="{{ asset('img/noimg.jpg') }}" class="b-lazy" data-blazy="{{ $transaction->getImageUrl() }}"/>
@if($image)
<br/>
<form autocomplete="off" name="aForm" novalidate enctype="multipart/form-data">
<input type="file" file-model="ic.image">
<a ng-click="ic.addImage(aForm.$valid)" ng-disabled="ic.checkImage()" class="waves-effect waves-light btn">Add Image</a>
</form>
@endif
</div>

<div id="deleteModal" class="modal">
  <div class="modal-content">
    <h4>Are you sure??</h4>
    <p>For every deleted transaction, your IQ will drop by 1pt</p>
  </div>
  <div class="modal-footer">
    <a href="#!" ng-click="ic.closeModal()" class="waves-effect red btn">Cancel!</a>
    <a href="#!" ng-click="ic.submit()" class="waves-effect green btn">Sure</a>
  </div>
</div>
</div>

@section('script')
<script type="text/javascript">
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	deleteUrl: '{{ URL::action('TransactionsController@postDelete',array($transaction->id,$transaction->date)) }}',
@if($image)
	imageUrl: '{{ URL::action('TransactionsController@postImage',array($transaction->id,$transaction->date)) }}',
@endif
@if(isset($details))
	details: {{ json_encode($details) }}
@endif
}))
</script>
<script src="{{ asset('js/aria/directives/item.js') }}"></script>
<script src="{{ asset('js/aria/controllers/invoiceController.js') }}"></script>
@stop