{{ Form::open(array('url' => URL::current(), 'method' => 'POST', 'autocomplete' => 'off')) }}
<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('date') }}
		{{ Form::text('date',$transaction->printDate(),array('disabled' => 'disabled','class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('Sender') }}
		{{ Form::text('sender',$transaction->sender->name,array('disabled' => 'disabled','class' => 'form-control')) }}
	</div>
</div>

@if($transaction->receiver)
<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('receiver') }}
		{{ Form::text('receiver',$transaction->receiver->name,array('disabled' => 'disabled','class' => 'form-control')) }}
	</div>
</div>
@endif

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('invoice') }}
		{{ Form::text('invoice',$transaction->invoice,array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-6">
		{{ Form::label('description') }}
		{{ Form::textarea('description',$transaction->description,array('class' => 'form-control')) }}
	</div>
</div>

{{ Form::submit('Save', array('class' => 'btn')) }}

{{ Form::close() }}