<form autocomplete="off" ng-controller="transactionController as cc" name="tForm" ng-submit="submit(tForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="row">
<div class="col s6">
<label>Date</label>
<input type="text" ng-model="cc.transaction.date" class="dateform" />
</div>
	@yield('due')
</div>

@yield('sender')

@yield('receiver')

<div class="input-field">
  <input focus type="text" ng-model="cc.transaction.invoice">
  <label>Invoice</label>
</div>

<div class="input-field">
	<textarea ng-model="cc.transaction.description" class="materialize-textarea"></textarea>
	<label>Note</label>
</div>

@yield('type')

<div ng-show="cc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="cc.submit(tForm.$valid)" ng-disabled="cc.disableSubmit || tForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</form>