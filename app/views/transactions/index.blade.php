<div ng-controller="transactionListController as tlc">
<form id="transactionListForm" name="transactionListForm" autocomplete="off" ng-submit="tlc.submit()">
<div class="row">
<div class="col s4 m2">
  <label>From</label>
  <input type="text" ng-model="tlc.from" class="dateform" ng-change="tlc.resetPage()"/>
</div>
<div class="col s4 m2">
  <label>To</label>
  <input type="text" ng-model="tlc.to" class="dateform" ng-change="tlc.resetPage()"/>
</div>
<div class="col s4 m2">
  <label>Invoice</label>
  <input type="text" ng-model="tlc.invoice" ng-change="tlc.resetPage()"/>
</div>
<div class="col s4 m2">
  <label>Total</label>
  <input type="text" ng-model="tlc.total" ng-change="tlc.resetPage()"/>
</div>
<div class="col s4 m2">
	<label>Type</label>
  <select class="browser-default" ng-model="tlc.type" ng-change="tlc.resetPage()" ng-options="type.name for type in tlc.types"></select>
</div>
<div class="col s4 m2 input-field">
	<input type="submit" value="Search" class="waves-effect waves-light btn">
</div>
</div>

</form>

<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>

<div id="scrollTo" class="table-responsive-vertical">
<table class="table table-striped table-bordered table-hover">
	<thead><tr><th>Date</th><th>Type</th><th>Invoice</th><th>Total</th><th>Items</th><th>Sender</th><th ng-hide="tlc.hide">Balance</th><th>Receiver</th><th ng-hide="tlc.hide">Balance</th></tr></thead>
	<tbody>
	<tr ng-repeat="transaction in tlc.transactions">
		<td data-title="Date"><a href="@{{ transaction.detail_link }}">@{{ transaction.date }}</a></td>
		<td data-title="Type">@{{ transaction.type }}</td>
		<td data-title="Invoice">@{{ transaction.invoice }}</td>
		<td data-title="Total">@{{ transaction.total | number:2 }}</td>
		<td data-title="Items">@{{ transaction.total_items | number:2 }}</td>
		<td data-title="Sender"><a href="@{{ transaction.sender_url }}">@{{ transaction.sender_name }}</a></td>
		<td ng-hide="tlc.hide" data-title="Sender Balance">@{{ transaction.sender_balance | number:2 }}</td>
		<td data-title="Receiver"><a href="@{{ transaction.receiver_url }}">@{{ transaction.receiver_name }}</a></td>
		<td ng-hide="tlc.hide" data-title="Receiver Balance">@{{ transaction.receiver_balance | number:2 }}</td>
	</tr>
	</tbody>
</table>
</div>
<div ng-show="tlc.showProgress" class="progress"><div class="indeterminate"></div></div>
<div class="row">
<div class="center-align">
<a ng-disabled="!tlc.showPrevious" ng-click="tlc.prevPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_rewind</i></a>
<a ng-disabled="!tlc.showNext" ng-click="tlc.nextPage()" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">fast_forward</i></a>
</div>
</div>
</div>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	types: {{ $typesJSON }},
	submitURL: "{{ URL::current() }}",
	paginator: {{ json_encode($transactions) }},
	hide: {{ json_encode($hide) }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/transactionListController.js') }}"></script>
@stop