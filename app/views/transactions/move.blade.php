@extends('transactions.form')

@section('sender')
<div class="input-field col s6">
	<label>Sender</label>
	<input focus required type="text" typeahead-min-length="3" ng-model="cc.warehouse" uib-typeahead="customer as customer.name for customer in cc.getWarehouse($viewValue)">
</div>
@stop

@section('receiver')
<div class="input-field col s6">
	<label>Receiver</label>
	<input focus required type="text" typeahead-min-length="3" ng-model="cc.receiver" uib-typeahead="customer as customer.name for customer in cc.getWarehouse($viewValue)">
</div>
@stop

@section('type')
<div class="switch">
  <label>
    <input type="checkbox" ng-model="cc.journal.checked">
    <span class="lever"></span>
    Journal
  </label>
</div>
<div class="row" ng-show="cc.journal.checked">
	<div class="input-field col s6">
		<label>Account</label>
		<input focus typeahead-no-results="cc.noResults" type="text" typeahead-min-length="3" ng-model="cc.journal.account" uib-typeahead="customer as customer.name for customer in cc.getAccount($viewValue)">
		<span ng-show="cc.noResults">No Results Found</span>
	</div>
</div>

<div class="table-responsive-vertical shadow-z-1">
<table class="table">
	<tbody>
	<tr ng-repeat="detail in cc.details track by $index">
		<td data-title="Code">
			<label>Code</label>
			<input focus ng-model="detail.code" code-search="@{{ $index }}" required />
		</td>
		<td data-title="Name">
			<label>Name</label>
			<input type="text" typeahead-min-length="3" focus required typeahead-on-select="cc.onSelect($model, $index)" ng-model="detail.name" uib-typeahead="item as item.name for item in cc.getItem($viewValue)" >
		</td>
		<td data-title="Quantity">
			<label>Quantity</label>
			<input focus class="required" ng-model="detail.quantity" name="detailForm_@{{$index}}" ng-change="cc.checkQuantity($index)" required />
		</td>
		<td data-title="Warehouse">
			<label>Warehouse</label>
			<input value="@{{ detail | warehouseQ : cc.warehouse.id }}" disabled="disabled" />
		</td>
		<td data-title="Price">
			<label>Price</label>
			<input focus ng-model="detail.price" add-row="@{{ $index }}" />
		</td>
		<td data-title="Subtotal">
			<label>Subtotal</label>
			<input class="form-control" value="@{{ detail | subtotal }}" disabled="disabled" />
		</td>
		<td data-title="Action">
			<a href="#" ng-click="cc.removeRow($index)"><i class="material-icons">clear</i></a>
		</td>
	</tr>
	</tbody>
</table>
</div>

<div class="row">
<div class="col s6">
<label>Total</label>
<input disabled="disabled" value="@{{ cc.getTotal() }}">
</div>
<div class="col s6">
<label>Total Quantity</label>
<input disabled="disabled" value="@{{ cc.totalQuantity | number: 2 }}">
</div>
</div>

@stop

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	customersUrl: "{{ $defaults['customer_url'] }}",
	warehouseType: "wv",
	itemsUrl: "{{ $defaults['item_url'] }}",
	price: 'price',
	defaultWarehouse: {{ $defaultWarehouse }}
}))

/*]]>*/
</script>
@include('transactions.scripts')
@stop