@extends('transactions.form')
<?
use App\Models\Transaction;
?>
@section('due')
@if('due')
<div class="col s6">
	<label>Due</label>
	<input type="text" ng-model="cc.transaction.due" class="datedue"/>
</div>
@endif
@stop

@section('sender')
<div class="input-field col s6">
	<label>{{ $txt['sender'] }}</label>
	<input focus required type="text" typeahead-min-length="3" ng-model="cc.warehouse" uib-typeahead="customer as customer.name for customer in cc.getWarehouse($viewValue)">
</div>
@stop

@section('receiver')
<div class="input-field col s6">
	<label>{{ $txt['receiver'] }}</label>
	<input focus required type="text" typeahead-min-length="3" ng-model="cc.customer" uib-typeahead="customer as customer.name for customer in cc.getCustomer($viewValue)">
</div>
@stop

@section('type')
<input type="file" import-sheet-js="" multiple="false"  />
<h5>Items</h5>
<div class="table-responsive-vertical">
<table class="table">
	<tbody>
	<tr ng-repeat="detail in cc.details track by $index">
		<td data-title="Code">
			<label>Code</label>
			<input focus ng-model="detail.code" code-search="@{{ $index }}" required />
		</td>
		<td data-title="Name">
			<label>Name</label>
			 <input type="text" typeahead-min-length="3" focus required typeahead-on-select="cc.onSelect($model, $index)" ng-model="detail.name" uib-typeahead="item as item.name for item in cc.getItem($viewValue)" >
		</td>
		<td data-title="Quantity">
			<label>Quantity</label>
			<input focus class="required" ng-model="detail.quantity" name="detailForm_@{{$index}}" ng-change="cc.checkQuantity($index)" warehouse-id="@{{ cc.warehouse.id }}" item-type="@{{ detail.type }}" required />
		</td>
		<td data-title="Warehouse">
			<label>Warehouse</label>
			<input value="@{{ detail | warehouseQ : cc.warehouse.id }}" disabled="disabled" />
		</td>
		<td data-title="Price">
			<label>Price</label>
			<input focus ng-model="detail.price" />
		</td>
		<td data-title="Discount">
			<label>Discount</label>
			<input focus ng-model="detail.discount" add-row="@{{ $index }}" />
		</td>
		<td data-title="Subtotal">
			<label>Subtotal</label>
			<input value="@{{ detail | subtotal }}" disabled="disabled" />
		</td>
		<td data-title="Action">
			<a href="#" ng-click="cc.removeRow($index)"><i class="material-icons">clear</i></a>
		</td>
	</tr>
	</tbody>
</table>
</div>

<div class="col s6">
<label>Discount &#37;</label>
<input ng-model="cc.transaction.discount">
</div>

<div class="row">
<div class="col s6">
<label>Adjustment</label>
<input ng-model="cc.transaction.adjustment">
</div>

<div class="col s6">
<label>Total Quantity</label>
<input disabled="disabled" value="@{{ cc.totalQuantity | number: 2 }}">
</div>
</div>

<div class="row">
<div class="col s6">
<label>Total</label>
<input disabled="disabled" value="@{{ cc.getTotal() | number: 2 }}">
</div>

<div class="col s6">
<label>Total Before Disc</label>
<input disabled="disabled" value="@{{ cc.beforeDisc | number: 2 }}">
</div>

</div>


@section('script')
<script type="text/javascript" src="{{ asset('js/xlsx.core.min.js') }}"></script>
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	price: 'price',
	customersUrl: "{{ $defaults['customer_url'] }}",
	warehouseType: "w",
	customerType: "{{ $customerType}}",
	itemsUrl: "{{ $defaults['item_url'] }}",
	defaultWarehouse: {{ $defaultWarehouse }},
	checkQuantity: true
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/transaction.js') }}"></script>
<script src="{{ asset('js/aria/services/customerService.js') }}"></script>
<script src="{{ asset('js/aria/services/itemService.js') }}"></script>
<script src="{{ asset('js/aria/controllers/transaction/sellBatchController.js') }}"></script>
@stop