<form autocomplete="off" ng-controller="transferController as tc" name="tForm" ng-submit="submit(tForm.$valid)" novalidate>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<label>Date</label>
<input type="text" ng-model="tc.transaction.date" class="dateform" />

<label>From</label>
<select class="browser-default" required ng-model="tc.sender" ng-options="a.name for a in tc.accounts"></select>

<label>To</label>
<select class="browser-default" required ng-model="tc.receiver" ng-options="a.name for a in tc.accounts"></select>

<label>Invoice</label>
<input ng-model="tc.transaction.invoice" type="text">

<div class="input-field">
<textarea ng-model="tc.transaction.description" class="materialize-textarea"></textarea>
<label>Note</label>
</div>

<label>Total</label>
<input ng-model="tc.transaction.total" required type="text">

<div ng-show="tc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="tc.submit(tForm.$valid)" ng-disabled="tc.disableSubmit || tForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config')
.constant('ariaConstants', Object.freeze({
	accounts: {{ $accounts }},
	accountId: {{ $app['default_income_account'] }}
}))

/*]]>*/
</script>
<script src="{{ asset('js/aria/directives/transaction.js') }}"></script>
<script src="{{ asset('js/aria/controllers/transferController.js') }}"></script>
@stop