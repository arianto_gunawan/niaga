@extends('transactions.form')

@section('sender')
<div class="input-field col s6">
	<label>From Warehouse</label>
	<input focus required type="text" typeahead-min-length="3" ng-model="cc.warehouse" uib-typeahead="customer as customer.name for customer in cc.getWarehouse($viewValue)">
</div>
@stop

@section('type')
<h5>Items</h5>
<div class="table-responsive-vertical">
<table class="table">
	<tbody>
	<tr ng-repeat="detail in cc.details track by $index">
		<td data-title="Code">
			<label>Code</label>
			<input focus ng-model="detail.code" code-search="@{{ $index }}" required />
		</td>
		<td data-title="Name">
			<label>Name</label>
			 <input type="text" typeahead-min-length="3" focus required typeahead-on-select="cc.onSelect($model, $index)" ng-model="detail.name" uib-typeahead="item as item.name for item in cc.getItem($viewValue)" >
		</td>
		<td data-title="Quantity">
			<label>Quantity</label>
			<input focus class="required" ng-model="detail.quantity" name="detailForm_@{{$index}}" ng-change="cc.checkQuantity($index)" warehouse-id="@{{ cc.warehouse.id }}" item-type="@{{ detail.type }}" required add-row="@{{ $index }}" />
		</td>
		<td data-title="Warehouse">
			<label>Warehouse</label>
			<input value="@{{ detail | warehouseQ : cc.warehouse.id }}" disabled="disabled" />
		</td>
		<td data-title="Price">
			<label>Price</label>
			<input disabled="disabled" ng-model="detail.price" />
		</td>
		<td data-title="Subtotal">
			<label>Subtotal</label>
			<input value="@{{ detail | subtotal }}" disabled="disabled" />
		</td>
		<td data-title="Action">
			<a href="#" ng-click="cc.removeRow($index)"><i class="material-icons">clear</i></a>
		</td>
	</tr>
	</tbody>
</table>
</div>
@stop

@section('script')
<script type="text/javascript">
/*<![CDATA[*/

angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	customersUrl: "{{ $defaults['customer_url'] }}",
	warehouseType: "w",
	itemsUrl: "{{ $defaults['item_url'] }}",
	defaultWarehouse: {{ $defaultWarehouse }},
	price: 'cost'
}))

/*]]>*/
</script>
@include('transactions.scripts')
@stop