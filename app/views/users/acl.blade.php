<form autocomplete="off" ng-controller="aclFormController as ifc" name="aForm" ng-submit="submit(aForm.$valid)" novalidate>
<ul class="tabs">
  <li class="tab"><a href="#transactions">Transactions</a></li>
  <li class="tab"><a href="#addrbook">Addr Book</a></li>
  <li class="tab"><a href="#items">Items</a></li>
  <li class="tab"><a href="#operations">Operations</a></li>
  <li class="tab"><a href="#personnels">Personnels</a></li>
  <li class="tab"><a href="#users">Users</a></li>
  <li class="tab"><a href="#system">System</a></li>
	<li class="tab"><a href="#produksi">Produksi</a></li>
	<li class="tab"><a href="#contributors">Contributors</a></li>
</ul>

<div id="transactions" class="col s12">
<fieldset>
	<legend>Transactions</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::TRANSACTIONS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::TRANSACTIONS }}, key)" ng-click="ifc.toggleAction({{ Apps::TRANSACTIONS }}, key)" id="id@{{ key }}{{ Apps::TRANSACTIONS }}">
	<label for="id@{{ key }}{{ Apps::TRANSACTIONS }}">@{{ item }}</label>
	</span>
</fieldset>
</div>

<div id="addrbook" class="col s12">
<fieldset>
	<legend>Customers</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::CUSTOMERS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::CUSTOMERS }}, key)" ng-click="ifc.toggleAction({{ Apps::CUSTOMERS }}, key)" id="id@{{ key }}{{ Apps::CUSTOMERS }}">
	<label for="id@{{ key }}{{ Apps::CUSTOMERS }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Resellers</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::RESELLERS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::RESELLERS }}, key)" ng-click="ifc.toggleAction({{ Apps::RESELLERS }}, key)" id="id@{{ key }}{{ Apps::RESELLERS }}">
	<label for="id@{{ key }}{{ Apps::RESELLERS }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Suppliers</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::SUPPLIERS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::SUPPLIERS }}, key)" ng-click="ifc.toggleAction({{ Apps::SUPPLIERS }}, key)" id="id@{{ key }}{{ Apps::SUPPLIERS }}">
	<label for="id@{{ key }}{{ Apps::SUPPLIERS }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Warehouses</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::WAREHOUSES }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::WAREHOUSES }}, key)" ng-click="ifc.toggleAction({{ Apps::WAREHOUSES }}, key)" id="id@{{ key }}{{ Apps::WAREHOUSES }}">
	<label for="id@{{ key }}{{ Apps::WAREHOUSES }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Accounts</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::BANKACCOUNTS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::BANKACCOUNTS }}, key)" ng-click="ifc.toggleAction({{ Apps::BANKACCOUNTS }}, key)" id="id@{{ key }}{{ Apps::BANKACCOUNTS }}">
	<label for="id@{{ key }}{{ Apps::BANKACCOUNTS }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Virtual Warehouses</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::VWAREHOUSES }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::VWAREHOUSES }}, key)" ng-click="ifc.toggleAction({{ Apps::VWAREHOUSES }}, key)" id="id@{{ key }}{{ Apps::VWAREHOUSES }}">
	<label for="id@{{ key }}{{ Apps::VWAREHOUSES }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Virtual Accounts</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::VACCOUNTS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::VACCOUNTS }}, key)" ng-click="ifc.toggleAction({{ Apps::VACCOUNTS }}, key)" id="id@{{ key }}{{ Apps::VACCOUNTS }}">
	<label for="id@{{ key }}{{ Apps::VACCOUNTS }}">@{{ item }}</label>
	</span>
</fieldset>
</div>

<div id="items" class="col s12">
<fieldset>
	<legend>Items</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::ITEMS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::ITEMS }}, key)" ng-click="ifc.toggleAction({{ Apps::ITEMS }}, key)" id="id@{{ key }}{{ Apps::ITEMS }}">
	<label for="id@{{ key }}{{ Apps::ITEMS }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Asset Lancar</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::ASSET_LANCAR }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::ASSET_LANCAR }}, key)" ng-click="ifc.toggleAction({{ Apps::ASSET_LANCAR }}, key)" id="id@{{ key }}{{ Apps::ASSET_LANCAR }}">
	<label for="id@{{ key }}{{ Apps::ASSET_LANCAR }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Asset Tetap</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::ASSET_TETAP }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::ASSET_TETAP }}, key)" ng-click="ifc.toggleAction({{ Apps::ASSET_TETAP }}, key)" id="id@{{ key }}{{ Apps::ASSET_TETAP }}">
	<label for="id@{{ key }}{{ Apps::ASSET_TETAP }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Virtual Items</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::VITEM }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::VITEM }}, key)" ng-click="ifc.toggleAction({{ Apps::VITEM }}, key)" id="id@{{ key }}{{ Apps::VITEM }}">
	<label for="id@{{ key }}{{ Apps::VITEM }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Tags</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::TAGS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::TAGS }}, key)" ng-click="ifc.toggleAction({{ Apps::TAGS }}, key)" id="id@{{ key }}{{ Apps::TAGS }}">
	<label for="id@{{ key }}{{ Apps::TAGS }}">@{{ item }}</label>
	</span>
</fieldset>
</div>

<div id="operations" class="col s12">
<fieldset>
	<legend>Operations</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::OPERATION }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::OPERATION }}, key)" ng-click="ifc.toggleAction({{ Apps::OPERATION }}, key)" id="id@{{ key }}{{ Apps::OPERATION }}">
	<label for="id@{{ key }}{{ Apps::OPERATION }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Journals</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::ACCOUNTS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::ACCOUNTS }}, key)" ng-click="ifc.toggleAction({{ Apps::ACCOUNTS }}, key)" id="id@{{ key }}{{ Apps::ACCOUNTS }}">
	<label for="id@{{ key }}{{ Apps::ACCOUNTS }}">@{{ item }}</label>
	</span>
</fieldset>
</div>

<div id="personnels" class="col s12">
<fieldset>
	<legend>Personnels</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::PERSONNELS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::PERSONNELS }}, key)" ng-click="ifc.toggleAction({{ Apps::PERSONNELS }}, key)" id="id@{{ key }}{{ Apps::PERSONNELS }}">
	<label for="id@{{ key }}{{ Apps::PERSONNELS }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Pelanggaran</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::PELANGGARAN }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::PELANGGARAN }}, key)" ng-click="ifc.toggleAction({{ Apps::PELANGGARAN }}, key)" id="id@{{ key }}{{ Apps::PELANGGARAN }}">
	<label for="id@{{ key }}{{ Apps::PELANGGARAN }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Cuti</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::CUTI }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::CUTI }}, key)" ng-click="ifc.toggleAction({{ Apps::CUTI }}, key)" id="id@{{ key }}{{ Apps::CUTI }}">
	<label for="id@{{ key }}{{ Apps::CUTI }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Gaji</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::GAJI }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::GAJI }}, key)" ng-click="ifc.toggleAction({{ Apps::GAJI }}, key)" id="id@{{ key }}{{ Apps::GAJI }}">
	<label for="id@{{ key }}{{ Apps::GAJI }}">@{{ item }}</label>
	</span>
</fieldset>
</div>
<div id="users" class="col s12">
<fieldset>
	<legend>Users</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::USERS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::USERS }}, key)" ng-click="ifc.toggleAction({{ Apps::USERS }}, key)" id="id@{{ key }}{{ Apps::USERS }}">
	<label for="id@{{ key }}{{ Apps::USERS }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Locations</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::LOCATIONS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::LOCATIONS }}, key)" ng-click="ifc.toggleAction({{ Apps::LOCATIONS }}, key)" id="id@{{ key }}{{ Apps::LOCATIONS }}">
	<label for="id@{{ key }}{{ Apps::LOCATIONS }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>Users</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::USERS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::USERS }}, key)" ng-click="ifc.toggleAction({{ Apps::USERS }}, key)" id="id@{{ key }}{{ Apps::USERS }}">
	<label for="id@{{ key }}{{ Apps::USERS }}">@{{ item }}</label>
	</span>
</fieldset>
</div>

<div id="system" class="col s12">
<fieldset>
	<legend>Reports</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::REPORTS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::REPORTS }}, key)" ng-click="ifc.toggleAction({{ Apps::REPORTS }}, key)" id="id@{{ key }}{{ Apps::REPORTS }}">
	<label for="id@{{ key }}{{ Apps::REPORTS }}">@{{ item }}</label>
	</span>
</fieldset>
</div>

<div id="produksi" class="col s12">
<fieldset>
	<legend>PRODUKSI</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::PRODUKSI }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::PRODUKSI }}, key)" ng-click="ifc.toggleAction({{ Apps::PRODUKSI }}, key)" id="id@{{ key }}{{ Apps::PRODUKSI }}">
	<label for="id@{{ key }}{{ Apps::PRODUKSI }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>SETORAN</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::SETORAN }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::SETORAN }}, key)" ng-click="ifc.toggleAction({{ Apps::SETORAN }}, key)" id="id@{{ key }}{{ Apps::SETORAN }}">
	<label for="id@{{ key }}{{ Apps::SETORAN }}">@{{ item }}</label>
	</span>
</fieldset>
<fieldset>
	<legend>BORONGAN</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::BORONGAN }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::BORONGAN }}, key)" ng-click="ifc.toggleAction({{ Apps::BORONGAN }}, key)" id="id@{{ key }}{{ Apps::BORONGAN }}">
	<label for="id@{{ key }}{{ Apps::BORONGAN }}">@{{ item }}</label>
	</span>
</fieldset>
</div>

<div id="contributors" class="col s12">
<fieldset>
	<legend>CONTRIBUTORS</legend>
	<span ng-repeat="(key, item) in ifc.getActions({{ Apps::CONTRIBUTORS }})">
	<input type="checkbox" ng-checked="ifc.checkSelected({{ Apps::CONTRIBUTORS }}, key)" ng-click="ifc.toggleAction({{ Apps::CONTRIBUTORS }}, key)" id="id@{{ key }}{{ Apps::CONTRIBUTORS }}">
	<label for="id@{{ key }}{{ Apps::CONTRIBUTORS }}">@{{ item }}</label>
	</span>
</fieldset>
</div>

<hr/>
<div ng-show="ifc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="ifc.submit(aForm.$valid)" ng-disabled="ifc.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>

</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	actions: {{ json_encode($actions) }},
	acl: {{ json_encode($acl) }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/user/aclFormController.js') }}"></script>
@stop
