<form autocomplete="off" ng-controller="userFormController as ifc" name="aForm" ng-submit="submit(aForm.$valid)" novalidate>

<label>Name</label>
<input class="required" ng-model="ifc.user.username" required />

<label>Role</label>
<select class="browser-default" ng-model="ifc.role" ng-options="a.name for a in ifc.roles"></select>

<label>Location</label>
<select class="browser-default" ng-model="ifc.location" ng-options="a.name for a in ifc.locations"></select>

@if($update)
<p>
<input id="updatepass" type="checkbox" ng-model="ifc.update_password" >
<label for="updatepass">Update Password? @{{ ifc.update_password ? 'Yes' : 'No' }}</label>
</p>
@endif

<div class="input-field">
<div ng-show="ifc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="ifc.submit(aForm.$valid)" ng-disabled="ifc.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>
</div>

</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	roles: {{ json_encode($roles) }},
	locations: {{ json_encode($locations) }},
	user: {{ json_encode($user) }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/user/userFormController.js') }}"></script>
@stop
