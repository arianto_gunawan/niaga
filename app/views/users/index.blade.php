<div class="table-responsive-vertical">
<table class="table table-hover table-striped">
<thead><tr><th width="200">Username</th><th width="150">Role</th><th width="150">Location</th><th width="150">Actions</th></tr></thead>
<tbody>
@foreach($users as $u)
<tr>
	<td data-title="Username"><a href="{{ URL::to('users/edit',array($u->id)) }}">{{ $u->username }}</a></td>
	<td data-title="Role"><a href="{{ URL::action('UsersController@getAcl',array($u->role->id)) }}">{{ $u->role->name }}</a></td>
	<td data-title="Location">
	@if(empty($u->location))
		&nbsp;
	@else
		<a href="{{ URL::action('LocationsController@getDetail',array($u->location->id)) }}">{{ $u->location->name }}</a>
	@endif
	</td>
	<td data-title="Actions">
@if($u->active == 1)
		<a href="{{ URL::action('UsersController@getBan',array($u->id)) }}" class="btn red">Ban</a>
@else
		<a href="{{ URL::action('UsersController@getUnban',array($u->id)) }}" class="btn red">UnBan</a>
@endif
		<a href="{{ URL::action('UsersController@getEdit',array($u->id)) }}" class="btn">Edit</a>
	</td>
</tr>
@endforeach
</tbody>
</table>
</div>