<form autocomplete="off" ng-controller="roleFormController as ifc" name="aForm" ng-submit="submit(aForm.$valid)" novalidate>

<label>Name</label>
<input ng-model="ifc.role.name" required />

<div ng-show="ifc.disableSubmit" class="progress"><div class="indeterminate"></div></div>
<a ng-click="ifc.submit(aForm.$valid)" ng-disabled="ifc.disableSubmit || aForm.$invalid" class="waves-effect waves-light btn">Submit</a>

</form>

@section('script')
<script type="text/javascript">
/*<![CDATA[*/
angular.module('ariaApp.config', [])
.constant('ariaConstants', Object.freeze({
	submitURL: "{{ URL::current() }}",
	role: {{ json_encode($role) }}
}))
/*]]>*/
</script>
<script src="{{ asset('js/aria/controllers/user/roleFormController.js') }}"></script>
@stop
