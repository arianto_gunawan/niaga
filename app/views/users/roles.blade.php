<div class="table-responsive-vertical">
<table class="table table-hover table-striped">
<thead><tr><th width="200">Name</th><th width="150">Actions</th></tr></thead>
<tbody>
@foreach($roles as $r)
<tr>
	<td data-title="Name"><a href="{{ URL::action('UsersController@getAcl',array($r->id)) }}">{{ $r->name }}</a></td>
	<td data-title="Actions">
		<a href="{{ URL::action('UsersController@getEditRole',array($r->id)) }}" class="btn">Edit</a>
		<a href="{{ URL::action('UsersController@getAcl',array($r->id)) }}" class="btn btn-warning">Access Control</a>
	</td>
</tr>
@endforeach
</tbody>
</table>
</div>